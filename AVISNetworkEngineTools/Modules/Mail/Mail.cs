﻿using A4Engine;
using AVISNetworkCommon;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AVISNetworkEngineTools.Modules
{
   public class Mail
    {
        Logger Log = new Logger(Logger.LoggerType.WriteConsole);

        string senderEmail = Settings.Server.senderEmail;
        string senderName = Settings.Server.senderName;
        string password = Settings.Server.senderPassword;
        string smtpServer = Settings.Server.SmtpHost; // "smtp.timeweb.ru";
        int smtpPort = Settings.Server.SmtpPort;
        static Mail Instance;
        private Mail()
        {

        }
        public static Mail Get()
        {
            if(Instance == null)
            {
                Instance = new Mail();
            }
            return Instance;
        }

        public void SendEmail(string toEmail, string subject, string body)
        {
            try
            {
                // отправитель - устанавливаем адрес и отображаемое в письме имя
                MailAddress from = new MailAddress(senderEmail, senderName);
                // кому отправляем
                MailAddress to = new MailAddress(toEmail);
                // создаем объект сообщения
                MailMessage m = new MailMessage(from, to);
                // тема письма
                m.Subject = subject;
                // текст письма
                m.Body = body;
                // письмо представляет код html
                m.IsBodyHtml = true;
                // адрес smtp-сервера и порт, с которого будем отправлять письмо
                SmtpClient smtp = new SmtpClient(smtpServer, smtpPort);
                // логин и пароль
                smtp.Credentials = new NetworkCredential(senderEmail, password);
                if (smtpPort == 25)
                {
                    smtp.EnableSsl = false;
                }
                smtp.Send(m);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
        }
    }
}
