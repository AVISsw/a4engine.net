﻿using MessagePack;
using AVISNetworkCommon;
using AVISNetworkEngineORM.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using A4Engine.Modules;
using System.Web;
using A4Engine.Modules.ItemEditor;
using System.Threading.Tasks;

namespace A4Engine.Modules
{
    public class EffectEditor : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context); 
        }

        public async Task<List<ItemEffectData>> GetEffects(int ItemId)
        {
            List<ItemEffectData> result = new List<ItemEffectData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemEffectData>("ItemId={0}", ItemId))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ItemEffectData>());
                }
            }
            return result;
        }
        public async Task<List<DataORM.Models.PropertyData>> GetPropertys()
        {
            List<DataORM.Models.PropertyData> result = new List<DataORM.Models.PropertyData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<DataORM.Models.PropertyData>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<DataORM.Models.PropertyData>());
                }
            }
            return result;
        }

        public async Task<bool> AddEffect(NameValueCollection data)
        {
            List<ItemEffectData> result = new List<ItemEffectData>();
            bool isAdd = false;
            try
            {
                long lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<ItemEffectData>(data));
                if (lastId > 0)
                    isAdd = true;
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<bool> UpdateEffect(int id, NameValueCollection data)
        {
            List<ItemEffectData> result = new List<ItemEffectData>();
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<ItemEffectData>(data), " Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveEffect(int id)
        {
            List<ItemEffectData> result = new List<ItemEffectData>();
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<ItemEffectData>(" Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            int id = 0;
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {
                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(await this.AddEffect(Context.postParams)));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.UpdateEffect(id, Context.postParams)));
                            break;
                        case "get":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.GetEffects(id)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveEffect(id)));
                            break;
                        case "getpropertylist":
                            var result = JsonConvert.SerializeObject(await this.GetPropertys());
                            Draw(result);
                            Console.WriteLine(result);
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }
    }
    [MessagePackObject]
    [DBTable("item_effects_template")]
    public class ItemEffectData
    {
        [Key(0), DBField(FieldAttrkey.AutoIncrement)]
        public long Id;
        [IgnoreMember, DBField]
        public long ItemId;
        [Key(1), DBField]
        public string PropertyName;
        [Key(2), DBField]
        public string ValueName;
        [Key(3), DBField]
        public float MinValue;
        [Key(4), DBField]
        public float MaxValue;
        [Key(5), DBField]
        public string Key;
        [Key(6), DBField]
        public float Time;
        [IgnoreMember, DBField]
        public bool Permanent;
        [IgnoreMember, DBField]
        public bool InstanceEffect = false;
        [IgnoreMember]
        public bool DbExist = false;
    }
}
