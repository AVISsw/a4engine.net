﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AVISNetworkEngineData;
using AVISNetworkEngineData.Models;
using AVISNetworkCommon;
using Newtonsoft.Json;

namespace A4Engine.Modules.Lenta
{
    public class Lenta : Module
    {

        public override Task OnLoad()
        {
            return Task.FromResult(0);
        }

        public override async Task Start(RequestContext Context)
        {
            string index = FileGetContents(DirModule + "/html/index.html");
            string result = JsonConvert.SerializeObject(GetItemTemplate(new ItemData(), 5));
            //  Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
            Draw(result);
 
        }

        public async Task<ItemData> GetItemTemplate(ItemData page, int id)
        {
            return await Controller.Storage.Get.Where<ItemData>(" item_id='{0}' ", id);
        }
        // есть проблема, в том что таблица для работы с бд указывается в модели
        public async Task<bool> UpdateItemTemplate(ItemData page, int id)
        {
            return await Controller.Storage.Update.Where(page, " item_id='{0}' ", id);
        }

    }
}
