﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4Engine.Modules;
using AVISNetworkCommon;
using DataORM.Models;
using Newtonsoft.Json;

namespace AVISNetworkEngineTools.Modules.LogHandler
{
    public class LogHandler : Handler
    {
        int range = 100;
        public static List<LogObject> Logs = new List<LogObject>();
        Logger Log;

        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            OpenLogs(DateTime.Now.AddDays(-1), DateTime.Now);
        }

        void OpenLogs(DateTime start, DateTime end)
        {
            Logs.Clear();
            int days = (end - start).Days;
            Console.WriteLine("Days " + days);
            Console.WriteLine("start " + start.ToString());
            Console.WriteLine("end " + end.ToString());

            for (int i = 0; i < days; i++)
            {
                var currentDate = start.AddDays(i);
                string path = Write.GetPath(currentDate);
                if (File.Exists(path))
                {
                    using (var sr = File.OpenText(path))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Length < 1)
                                continue;
                            Logs.Add(JsonConvert.DeserializeObject<LogObject>(line));
                        }

                    }
                }
            }
        }
        void OpenLogs(GetLog getData)
        {
            OpenLogs(getData.StartDate, getData.EndDate);
            int page = 0;
            int trange = range;

            page = getData.Page;
            trange = getData.Range;
 
            List<LogObject> list = new List<LogObject>();

            foreach (var log in Logs)
            {
                bool condition = true;

                if (getData.ConnectionId != -1)
                {
                    if (getData.ConnectionId != log.ConnectionId)
                    {
                        condition = false;
                    }
                }
                if (getData.UserId != -1)
                {
                    if (getData.UserId != log.UserId)
                    {
                        condition = false;
                    }
                }
                if (getData.MessageKey.Length > 0)
                {
                    if (!getData.MessageKey.Contains(log.MessageKey))
                    {
                        condition = false;
                    }
                }
                if (condition)
                    list.Add(log);
            }
            int offset = page * trange;
            int startRange = list.Count - (trange + offset);
            int tempRange = trange;
            if (startRange < 0)
                startRange = 0;


            if (tempRange > list.Count) // 2 > 1
                tempRange = list.Count; // == 1

            Log.Info("start {0}, range {1}", startRange, tempRange);
            var rangeList = list.GetRange(startRange, tempRange);

            Draw(JsonConvert.SerializeObject(new Response() {RequestType = RequestType.OpenLogs, Result = rangeList }));
        }
        public override async Task Before(RequestContext Context)
        {
            Start(Context);
        }

        [Serializable]
        public class GetLog
        {
            public RequestType RequestType;
            public DateTime StartDate;
            public DateTime EndDate;
            public long UserId;
            public long ConnectionId;
            public string UserName;
            public int Page;
            public int Range;
            public LogType Type;
            public TimeSpan Time;
            public LogMessageKey[] MessageKey;
        }
        [Serializable]
        public class Response
        {
            public RequestType RequestType;
            public object Result;

        }
        public enum RequestType
        {
            Init,
            OpenLogs
        }
        void Init()
        {
            List<FilterObject> Filters = new List<FilterObject>();

            foreach(var key in Enum.GetNames(typeof(LogMessageKey)))
            {
                Filters.Add(new FilterObject() { Name = key });
            }
            Draw(JsonConvert.SerializeObject(new Response() { RequestType = RequestType.Init, Result = Filters }));
        }
        [Serializable]
         public class FilterObject
        {
            public string Name;
            public bool Value;
        }
        public override async Task Start(RequestContext Context)
        {
            try
            {
               
                GetLog getData = JsonConvert.DeserializeObject<GetLog>(Context.rawParams);

                switch (getData.RequestType)
                {
                    case RequestType.Init:
                        Init();
                        break;
                    case RequestType.OpenLogs:
                        OpenLogs(getData);
                        break;
                }

  
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
