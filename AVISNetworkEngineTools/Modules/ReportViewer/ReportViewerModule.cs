﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.ReportViewer
{
   public class ReportViewerModule : Module
    {
        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            if (User != null)
            {
                bool isShow = false;
                isShow |= User.GroupRight.Can("ViewBug");
                isShow |= User.GroupRight.Can("ViewPerformanceIssue");
                isShow |= User.GroupRight.Can("ViewComplaintsToTheAdministration");
                isShow |= User.GroupRight.Can("ViewPlayerComplaint");
                isShow |= User.GroupRight.Can("ViewSuggestions");
                if (isShow)
                {

                    string index = FileGetContents(DirModule + "/form.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
            Context.Redirect("/page/auth");
            Draw("<b>Доступ запрещен!</b>");
        }
    }
}
