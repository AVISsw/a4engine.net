
function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
    }
    return newArray;
  }

$(window).on('load', function () {
    var itemListModule;

    jsonRequestData("/handler/ItemModsHandler/get-item-list", {}, function (requestDataItemList) {

        requestDataItemList = requestDataItemList.reverse();
        console.log(requestDataItemList);
        jsonRequestData("/handler/ItemModsHandler/get-item-mods", {}, function (requestDataReceptList) {
 
            console.log(requestDataReceptList);
            var firstMod = null;
            var firstItem;

            for (var i in requestDataItemList) {
                firstItem = requestDataItemList[i];
                break;
            }        

            for (var i in requestDataReceptList) {
                var tempMod = requestDataReceptList[i];
                if(tempMod.ItemId == firstItem.ItemId){
                    firstMod = tempMod;
                    break;
                }
            }        
 
            itemListModule = new Vue({
                el: '#itemListModule',
                data: {
                    Items: requestDataItemList,
                    ItemMods: requestDataReceptList,
                    ItemMod: firstMod,
                    SelectedItem: firstItem
                },
                methods: {
                    SelectMod(index, item) {
                        this.ItemMod = item;
                        console.log(this.ItemMod);
                    },
                    SelectItem(index, item) {
                        this.SelectedItem = item;
                        console.log(this.SelectedItem);
                    },
                    AddItemMod() {
                        console.log(this.ItemMods);
                        this.ItemMods.ModTemplates.push(
                            {
                                Effects: [],
                                Name: "",
                                Icon:"",
                                Cost: 0,
                                ItemId: this.SelectedItem.ItemId,
                                RequireModId:[],
                                RequireItemId:[],
                                BlockModId:[],
                                NotPublic: false,
                                IsDbExits: false,
                                IsRemove: false
                            });
                    },RmItemMod(index, item) {
                        console.log(item);
                        item.IsRemove = true;
                    },AddEffect() {
 
                        this.ItemMod.Effects.push(
                            {
                                PropertyName: "skin",
                                ModTemplateId: this.ItemMod.Id,
                                MinValue: 0,
                                MaxValue: 0,
                                IsDbExits: false,
                                IsRemove: false
                                });
                    },RmEffect(index, effect) {
                        console.log(effect);
                        effect.IsRemove = true;
                    },AddResult() {
                        if(this.ItemMod.RequireItemId == null || this.ItemMod.RequireItemId == undefined){
                            this.ItemMod.RequireItemId = [];
                        }
                        console.log(this.ItemMod);
                        this.ItemMod.RequireItemId.push({ModId:this.ItemMod.Id, Count: 1, IsRemove: false});
                    },RmResult(index, condition) {
                        console.log(condition);
                        condition.IsRemove = true;
                    },
                    SaveRecept(Recept) {
                        console.log("post request save");
  
                        var itemModRequestData = {
                            ModTemplates:[]
                        };
                        this.ItemMods.ModTemplates.forEach(element => {
                            if(element.ItemId == this.SelectedItem.ItemId){

                                if(element.RequireItemId == null || element.RequireItemId == undefined){
                                    element.RequireItemId = [];
                                }
                                if(element.BlockModId == null || element.BlockModId == undefined){
                                    element.BlockModId = [];
                                }
                                if(element.RequireModId == null || element.RequireModId == undefined){
                                    element.RequireModId = [];
                                }
                                element.RequireItemId.forEach(element_item => {
                                    console.log(element_item);
                                    if (!Array.isArray(element_item.ItemId)) {
                                        element_item.ItemId = element_item.ItemId.split(",");
                                    }
                                    element_item.ItemId = cleanArray(element_item.ItemId);
                                });
                                console.log("element.BlockModId");
                                console.log(element.BlockModId);
                                if (!Array.isArray(element.BlockModId)) {
                                    element.BlockModId = element.BlockModId.split(",");
                                }
                                element.BlockModId = cleanArray(element.BlockModId);


                                if (!Array.isArray(element.RequireModId)) {
                                    element.RequireModId = element.RequireModId.split(",");
                                }
                                element.RequireModId = cleanArray(element.RequireModId);

                                itemModRequestData.ModTemplates.push(element);
                            }
                        });               
                        postRequestData("/handler/ItemModsHandler/update-item-mods", itemModRequestData, function (responseData) {
                            itemListModule.$data.ItemMods = responseData;
                            console.log(responseData);
                        });
                    }
                },
                computed: {
                    filteredList: function () {
                        var name = this.ItemName;
                        let result = Object.values(this.Items).filter(function (elem) {
                            if (name === '') return true;
                            else return elem.Desc.indexOf(name) > -1;
                        });
                        return result;
                    }
                }
            });

        });
    });
});
