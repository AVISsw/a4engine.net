﻿using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using A4Engine.Modules;
using System.Threading.Tasks;

namespace A4Engine.Modules.WeaponEditor
{
    class WeaponEditorHandler : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }


        public async Task<bool> AddWeaponTemplate<T>(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long lastId =await Controller.Storage.Add.In(Controller.GetObjectByParams<WeaponData>(data));
                if (lastId > 0)
                    isAdd = true;
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<List<ItemEditor.ItemData>> GetItemsByWeaponType()
        {
            List<ItemEditor.ItemData> result = new List<ItemEditor.ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemEditor.ItemData>("IntType={0}",2))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ItemEditor.ItemData>());
                }
            }
            return result;
        }

        public async Task<bool> UpdateWeaponTemplate(int id, NameValueCollection data)
        {
            bool isAdd = false;
            try
            {

                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<WeaponData>(data), " Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveWeaponTemplate(int id)
        {
            bool isAdd = false;
            try
            {

                isAdd = await Controller.Storage.Remove.Where<WeaponData>(" Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public override async Task Start(RequestContext Context)
        {

            string key = Context.Path[3].ToString();
            int id = 0;

            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(await this.AddWeaponTemplate<WeaponData>(Context.postParams)));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);

                            Draw(JsonConvert.SerializeObject(await this.UpdateWeaponTemplate(id, Context.postParams)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveWeaponTemplate(id)));
                            break;
                        case "getlist":
                            Draw(JsonConvert.SerializeObject(await this.GetWeaponList()));
                            break;
                        case "getitems":
                            Draw(JsonConvert.SerializeObject(await this.GetItemsByWeaponType()));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }

        private async Task<List<WeaponData>> GetWeaponList()
        {
            List<WeaponData> result = new List<WeaponData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<WeaponData>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<WeaponData>());
                }
            }
            return result;
        }
    }
}