﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using A4Engine.Controllers;
using A4Engine.User;
using AVISNetworkEngineORM.DB;
using A4Engine;
using AVISNetworkCommon;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Hosting;
using uhttpsharp.RequestProviders;
using uhttpsharp.Listeners;
using uhttpsharp;
using System.Net.Sockets;
using uhttpsharp.Handlers;
using A4Engine.Transport;
using IHttpContext = A4Engine.Transport.IHttpContext;
using System.Collections.Specialized;

namespace WebPanel
{
    public class Index : IHostedService, IDisposable
    {
        public Router Router;

        public Storage Storage { get; private set; }
        public SessionManager SessionManager { get; private set; }
        AVISNetworkCommon.Logger Log;

        public Index()
        {
            Logger.Init();
            Log = new Logger(AVISNetworkCommon.Logger.LoggerType.WriteConsole);
        }
        static X509Certificate serverCertificate = null;
        void LoadSsl()
        {
            try
            {

                string path = Settings.Server.SslCertificate;
 
                Log.Info("Пытаемся загрузить SSL сертификат " + path);
                if (File.Exists(path))
                {
                    Log.Info("Сертефикат найден " + path);
                    serverCertificate = new X509Certificate(path, Settings.Server.SslPassword);
                    Log.Done("Успешно загружен SSL " + path);
                    X509Store store = new X509Store();
                    store.Open(OpenFlags.ReadWrite);
                    store.Certificates.Add(serverCertificate);
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
        private async Task ServerInit()
        {
            Settings.Init();
            Log.Done(".: Инициализация веб сервера");
            LoadSsl();
            Router = new Router();
            Storage = new Storage(Settings.Server.Host, Settings.Server.Name, Settings.Server.User, Settings.Server.Password, Settings.Server.Port);
            SessionManager = new SessionManager(Storage);
            await Group.LoadRightData(Storage);
            // Http.Start();
            await Listen();
        }
        private async Task Listen()
        {
            /*
            HttpListener listener = new HttpListener();
 
            listener.Prefixes.Clear();
            listener.Prefixes.Add("http://*:" + port + "/");
            listener.Prefixes.Add("https://*:443/");
            listener.Start();
            
            Console.WriteLine("Ожидание подключений...");
            */
            Router.IsRequereSecureConnection = false;
            if (Settings.Server.HttpPort == 80)
            {
                Router.IsRequereSecureConnection = true;
            }
            using (var httpServer = new HttpServer(new HttpRequestProvider()))
            {
                // Normal port 80 :
               // Console.WriteLine($"TcpListener HttpPort:{Settings.Server.HttpPort}");
                httpServer.Use(new TcpListenerAdapter(new TcpListener(IPAddress.Any, Settings.Server.HttpPort)));
               // Console.WriteLine($"ListenerSslDecorator HttpsPort:{Settings.Server.HttpsPort}");
                httpServer.Use(new ListenerSslDecorator(new TcpListenerAdapter(new TcpListener(IPAddress.Any, Settings.Server.HttpsPort)), serverCertificate));

                httpServer.Use((context, next) =>
                {
                    var adapter = new UHttpContextAdapter(context);
                    return OnGet(adapter);
                });

                httpServer.Start();
                Console.ReadLine();
            }
            /*
            while (true)
            {
                HttpListenerContext context = await listener.GetContextAsync();
                switch (context.Request.HttpMethod)
                {
                    case "GET":
                        OnGet(context);
                        break;
                    case "POST":
                        OnGet(context);
                        break;
                }
            }*/
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await ServerInit();

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Info("Stopping daemon.");
            Dispose();
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Log.Info("Disposing....");
            if(OnDispose != null)
            {
                OnDispose();
            }
        }
        public static event Action OnDispose;
        private async Task OnGet(IHttpContext e)
        {
            try
            {
                using (e.Response)
                {
                    RequestContext Context = new RequestContext();
                    await Context.Init(e, Router);
 
                    BaseController controller = null;

                    string cmd = Context.Path[1];

                    if (cmd == null)
                        cmd = "";

                    if (cmd.Length < 1)
                    {
                        cmd = "page";
                    }

                    if (!Router.Route.ContainsKey(cmd)) // если нет контроллера
                    {
                        cmd = Asset.Folder;
                    }
                    Log.Error("cmd: " + cmd);
                    int index = Array.IndexOf(Context.Path, Asset.Folder);
                    if (index != -1)
                    {
                        List<string> tmp = new List<string>(Context.Path);
                        tmp.RemoveAt(index);
                        Context.Path = tmp.ToArray();
                    }
                    try
                    {
                        controller = Router.GetController(cmd);
                        await controller.Init(Context, SessionManager);
                        await controller.Render(Context);

                    }
                    catch (Exception ex2)
                    {
                        Log.Error(ex2.ToString());
                    }
 
                    await e.Response.GetResponse(Context);
                    Log.Info(Context.Request.Request.RawUrl);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
