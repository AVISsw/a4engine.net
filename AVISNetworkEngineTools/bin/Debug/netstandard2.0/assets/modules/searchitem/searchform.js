function OpenDetail() {
  $("#myModal").modal();
}
function OpenDesc(input) {
  var descArea = $('#DescArea').html();
  descArea = fill_pattern(descArea, { Desc: input.attr('value') });
  ShowModal(descArea, "Редактироване описания");
  $('#myModal').find('.close-modal').on('click', function () {
    input.attr("value", $('#myModal').find("#DescInput").val());
    $('#myModal').find('.close-modal').off('click');
  });
}
function SaveItem(id) {
  var form = $('#' + id + "_item");
  var data = Serilaze(form);
  $.each(data, function (name, value) {
    console.log(name + '=' + value);
  });
  updateItemInfoRequest(id, data);
}


function RemoveItem(id) {
  var descArea = $('#removeItem').html();
  descArea = fill_pattern(descArea, { ItemId: id });
  ShowModal(descArea, "Редактироване описания");
}


function updateItemInfoRequest(id, data) {
  $.post(
    "/handler/itemeditor/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}


function SelectByType(typeId){
  // Hide all content class element
  $('.content').hide();

  // Search 
  $('.content').each(function () {
    $(".IntType option:selected").each(function () {
      if ($(this).val() == typeId) {
        $(this).closest('.content').show();
      }
    });
  });
}

function SelectBySlot(typeId){
  // Hide all content class element
  $('.content').hide();

  // Search 
  $('.content').each(function () {
    $(".Slot option:selected").each(function () {
      if ($(this).val() == typeId) {
        $(this).closest('.content').show();
      }
    });
  });
}

function loadItemList(callback) {
  $.post(
    "/handler/getitemlist",
    {
    },
    function (data) {
      console.log(data);
      var itemTemplate = $("#ItemTemplate").html();
      var tempItems = "";
      if (Array.isArray(data))
        data.forEach(element => {
          var temprow = fill_pattern(itemTemplate, element);
          var divRow = $(temprow);
          SetSelect(divRow.find('.TypeOfUse'), element.TypeOfUse);
          SetSelect(divRow.find('.IntType'), element.IntType);
          divRow.find('.CanSpoil').attr("checked", element.CanSpoil);
          divRow.find('.IsInfinite').attr("checked", element.IsInfinite);
          divRow.find('.Up').attr("checked", element.Up);
          divRow.find('.Premium').attr("checked", element.Premium);
          SetSelect(divRow.find('.Slot'), element.Slot);
          tempItems += divRow[0].outerHTML;
        });
      
      $("#ItemList").html(tempItems);
      $('.ExpiryDate').mask('0.00:00:00');
      if(callback)
      callback(data);
    }
  );
}

function addNewItemRequest() {
  // addItem();
  $.post(
    "/handler/itemeditor/add/",
    {},
    function (data) { loadItemList(); }
  );
}

function SaveAll(){
  if(Array.isArray(editItemList))
  editItemList.forEach(id => {
      SaveItem(id);
  });
}

function removeItemRequest(id) {
  $.post(
    "/handler/itemeditor/remove/" + id,
    {},
    function (data) { ShowModal(data, "Результат удаления"); loadItemList();}
  );
}
  var editItemList = [];
$(document).ready(function () {
  Menu();

  $('#searchInput').keyup(function () {

    // Search text
    var text = $(this).val().toLowerCase();

    // Hide all content class element
    $('.content').hide();

    // Search 
    $('.content').each(function () {
      $(this).find("input").each(function () {
        if ($(this).val().toLowerCase().indexOf("" + text + "") != -1) {
          $(this).closest('.content').show();
        }
      });
    });
  });
  $('.SelectIntType').change(function(){
    SelectByType($(this).val());
  });
  $('.SelectSlot').change(function(){
    SelectBySlot($(this).val());
  });

  loadItemList(function(){
    $('.content').find('input').change(function(){
      let elem = $(this).parent().parent();
      let itemid = elem.attr("itemid");
      console.log(elem);
      elem.css("background-color","#8eb6bea8");
      if(editItemList.indexOf(itemid) == -1){
      editItemList.push(itemid);
      }
    });

  });


});