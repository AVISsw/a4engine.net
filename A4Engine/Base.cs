﻿using AVISNetworkCommon;
using AVISNetworkEngineORM.Data;
using AVISNetworkEngineORM.DB.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace A4Engine
{
    public class Base
    {
        static NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        public string DirApp { get { return AppDomain.CurrentDomain.BaseDirectory; } }
        protected Logger Log = new Logger(Logger.LoggerType.WriteConsole);
       
        public string FileGetContents(string path)
        {
            string file_contents = "";
            try
            {
                if (File.Exists(path))
                {
                    using (TextReader tr = new StreamReader(path))
                    {
                        file_contents = tr.ReadToEnd();
                    }
                }
                else
                {
                    string er = "Метод GetFile : Не удалось найти фаил " + path;
                    file_contents = er;
                    Log.Error(er);
                }
            }
            catch (Exception ex)
            {
                string er = ex.ToString();
                file_contents = er;
                Log.Error(er);
            }
            return file_contents;
        }

        public T GetObjectByParams<T>(NameValueCollection data) where T : new()
        {

            T model = new T();
            var properties = typeof(T).GetFields();

            for (int i = 0; i < properties.Length; i++)
            {
                try
                {
                    var property = properties[i];
                    var value = data.Get(property.Name);
                    if (value != null)
                        property.SetValue(model, TypeFormat(value, property.FieldType));
                }
                catch (Exception e)
                {
                    Log.Info(e.ToString());
                }
            }
            return model;
        }
 
        public IDictionary<string, object> ToDictionary<T>(T source)
        {
            if (source == null)
                ThrowExceptionWhenSourceArgumentIsNull();

            var dictionary = new Dictionary<string, object>();
            foreach (var fieldInfo in typeof(T).GetFields()) 
                AddFieldToDictionary(fieldInfo, source, dictionary);
            return dictionary;
        }

        private static void AddFieldToDictionary(FieldInfo fieldInfo, object source, Dictionary<string, object> dictionary)
        {
            object value = baseOperation.TypeFormatToString(fieldInfo, source);
            dictionary.Add(fieldInfo.Name, value);
        }
 
        private static void ThrowExceptionWhenSourceArgumentIsNull()
        {
            throw new ArgumentNullException("source", "Unable to convert object to a dictionary. The source object is null.");
        }
        public static object TypeFormat(string value, Type type)
        {
            object result = value;

            if (type == typeof(float))
            {
                value = value.Replace(',', '.');
                result = Single.Parse(value, nfi);
            }
            if (type == typeof(double))
            {
                value = value.Replace(',', '.');
                result = Single.Parse(value, nfi);
            }
            if (type == typeof(TimeSpan))
            {
                result = TimeSpan.Parse(value);
            }
            if (type.IsEnum)
            {
                result = Enum.ToObject(type, Convert.ToInt32(value));
            }
            if (type.IsArray)
            {
                result = JsonConvert.DeserializeObject(value, type);
            }

            return Convert.ChangeType(result, type);
        }

        public byte[] BinaryFileGetContents(string path)
        {
            byte[] file_contents = new byte[0];
            try
            {
                file_contents = File.ReadAllBytes(@path);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return file_contents;
        }

        public IEnumerable<string> GetModules(string text)
        {
            string[] result = new string[1];
            Regex regex = new Regex(@"{query (.*?)/}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(text);

            foreach (Match match in matches)
            {
                yield return match.Groups[1].Value;
            }
        }

        public string FillPattern(string pattern, Dictionary<string, string> data)
        {
            Regex regex = new Regex(@"{(.*?)/}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(pattern);

            foreach (Match match in matches)
            {
                var key = match.Groups[1].Value;
                if (data.ContainsKey(key))
                {
                    pattern = pattern.Replace("{" + key + "/}", data[key]);
                }
            }

            return pattern;
        }

        public bool Request()
        {


            return false;
        }
        public Dictionary<string, string> Query(string pattern)
        {
            var result = new Dictionary<string, string>();
            var temp = pattern.Split(' ');

            foreach (string line in temp)
            {
                var temp_line = line.Split('=');
                result.Add(temp_line[0], (temp_line.Length > 1) ? temp_line[1] : null);
            }

            return result;
        }
    }
}
