class Field {
  constructor() {
    this.offsetX = 0;
    this.offsetY = 0;
    this.dragData = {
      dragged: false,
      startX: 0,
      startY: 0,
      startedOffsetX: 0,
      startedOffsetY: 0
    };
  }

  OnDrag(mouseX, mouseY) {
    if (!this.dragData.dragged) {
      this.dragData.startX = mouseX;
      this.dragData.startY = mouseY;
      this.dragData.startedOffsetX = this.offsetX;
      this.dragData.startedOffsetY = this.offsetY;
      this.dragData.dragged = true;
    }

    this.offsetX = this.dragData.startedOffsetX + mouseX - this.dragData.startX;
    this.offsetY = this.dragData.startedOffsetY + mouseY - this.dragData.startY;
  }

  OnDragStoped() {
    this.dragData.dragged = false;
  }

  OnClick() {
    //Тут можно проверить какая кнопка мыши была нажата
    //  и если правая, то вывести кастомное контекстное меню. Зачем? И без него норм
  }
}

export { Field };
