﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using A4Engine.Modules;

namespace AVISNetworkEngineTools.Modules.ControllPanel
{
    public class LogVieweModule : Module
    {

        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
          
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {
                    Console.WriteLine("User:" + User.UserData);
                    string index = FileGetContents(DirModule + "/html/index.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
            Context.Redirect("/page/auth");
            Draw("<b>Доступ запрещен!</b>");
        }

    }
}

