using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using uhttpsharp.Headers;
using uhttpsharp.Logging;

namespace uhttpsharp.RequestProviders
{
    public class HttpRequestProvider : IHttpRequestProvider
    {
        private static readonly char[] Separators = { '/' };

        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
        
        public async Task<IHttpRequest> Provide(IStreamReader reader)
        {
            // parse the http request
            var request = await reader.ReadLine().ConfigureAwait(false);

            if (request == null)
                return null;
      
            var firstSpace = request.IndexOf(' ');
            var lastSpace = request.LastIndexOf(' ');

            var tokens = new []
            {
                request.Substring(0, firstSpace),
                request.Substring(firstSpace + 1, lastSpace - firstSpace - 1),
                request.Substring(lastSpace + 1)
            };

            if (tokens.Length != 3)
            {
                return null;
            }

            
            var httpProtocol = tokens[2];
 
            var url = tokens[1];
            url = HttpUtility.UrlDecode(url);
            var uri = new Uri(url, UriKind.Relative);

            NameValueCollection headersRaw = new NameValueCollection();

            // get the headers
            string line;
           
            while (!string.IsNullOrEmpty((line = await reader.ReadLine().ConfigureAwait(false))))
            {
                string currentLine = line;
                var headerKvp = SplitHeader(currentLine);
                headersRaw.Add(headerKvp.Key, headerKvp.Value);
            }
            string verb = headersRaw.Get("_method");
            if (string.IsNullOrWhiteSpace(verb))
            {
                verb = tokens[0];
            }
            
            var httpMethod = HttpMethodProvider.Default.Provide(verb);

            int postContentLength;
            string contentLength = headersRaw.Get("content-length");
            postContentLength = Convert.ToInt32(contentLength);
            /*
            if (postContentLength > 4096)
            {
                postContentLength = 4096;
            }*/
            int loadedBytes = 0;
            MemoryStream memoryStream = new MemoryStream();
 
            while (loadedBytes < postContentLength) // 512 < 512
            {
                var postBuffer = await reader.ReadBytes(postContentLength).ConfigureAwait(false);
                memoryStream.Write(postBuffer);
                loadedBytes += postBuffer.Length;
            }
            memoryStream.Position = 0;
            return new HttpRequest(headersRaw, httpMethod, httpProtocol, uri,
                uri.OriginalString.Split(Separators, StringSplitOptions.RemoveEmptyEntries), memoryStream);
        }
 
        private KeyValuePair<string, string> SplitHeader(string header)
        {
            var index = header.IndexOf(": ", StringComparison.InvariantCultureIgnoreCase);
            return new KeyValuePair<string, string>(header.Substring(0, index), header.Substring(index + 2));
        }

    }
}