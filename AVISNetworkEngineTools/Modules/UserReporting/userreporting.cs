﻿using A4Engine.Modules;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using static AVISNetworkEngineTools.Modules.ReportViewer.ReportViewer;

namespace AVISNetworkEngineTools.Modules.UserReporting
{
    public class userreporting : Handler
    {
        public const string ReportPath = "asset/report/user";
        public const string ImageFormat = ".png";
        Logger Log;
        public override async Task Before(RequestContext Context)
        {
            Start(Context);
        }

        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }

        public override async Task Start(RequestContext Context)
        {
            var reportData = JsonConvert.DeserializeObject<UserReportData>(Context.rawParams);
            if (string.IsNullOrWhiteSpace(reportData.NickName))
                reportData.NickName = "NickName";
          
            string userPath = ReportPath + "/" + reportData.NickName +"/" + reportData.Summary;
            ReportHeaderData reportHeaderData = new ReportHeaderData() { Type = reportData.Type, Date = DateTime.Now, Name = reportData.Summary , UserName = reportData.NickName, Description = reportData.FieldByName("Description").Value };
            reportHeaderData.Hash = AVISNetworkCommon.Utility.Crypt.EncryptMD5(reportHeaderData.Date.ToString() + reportHeaderData.Name + reportHeaderData.UserName + reportHeaderData.Description);
            try
            {
                if (!Directory.Exists(userPath))
                {
                    Directory.CreateDirectory(userPath);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            try
            {
                using (StreamWriter sw = File.CreateText(userPath + "/" + "/report_header_data.json"))
                {
                    string reportHeaderJson = JsonConvert.SerializeObject(reportHeaderData);
                    if (reportHeaderJson != default)
                    {
                        sw.Write(reportHeaderJson);
                    }
                }
                using (StreamWriter sw = File.CreateText(userPath + "/" + "meta.json"))
                {
                    var deviceMeta = reportData.DeviceMetadata;
                    if (deviceMeta != null)
                    {
                        sw.Write(JsonConvert.SerializeObject(deviceMeta));
                    }
                }
                using (StreamWriter sw = File.CreateText(userPath + "/" + "logInfo.json"))
                {
                    sw.Write(reportData.LogInfo);
                }

            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            try
            {

                foreach (var screanData in reportData.Screenshots)
                {
                    var bytes = Convert.FromBase64String(screanData.DataBase64);
                    string filePath = userPath + "/" + screanData.GetHashCode()+ ImageFormat;
                    using (var imageFile = new FileStream(filePath, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            Draw("ok");
        }
    }
}
