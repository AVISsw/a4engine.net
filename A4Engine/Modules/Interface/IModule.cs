﻿using A4Engine;
using A4Engine.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4Engine.Controllers;

namespace A4Engine.Modules.Interface
{
    public interface IModule
    {
        BaseController Controller { get; set; }
        Session User { get; set; }
        SessionManager SessionManager { get; set; }
        string Alias { get; set; }
        string Name { get; }
        Task Before(RequestContext Context);

        // вызывается при создании экземпляра модуля
        Task Start(RequestContext Context);

        /*
        * Вызывается для загрузки общих ресурсов модуля, всех его экземпляров
        * */
        Task OnLoad();

    }
}
