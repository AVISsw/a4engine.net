
var yesterday = new Date();
yesterday.setDate(yesterday.getDate());
yesterday = yesterday.toISOString().slice(0, 10) + "T00:00";
var today = new Date();
today.setDate(today.getDate() +1);
today = today.toISOString().slice(0, 10) + "T00:00";
 
var model = {
  FatalCount: 0,
  ErrorCount: 0,
  WarnCount: 0,
  DebugCount: 0,
  filters: [],
  requestData: {
    StartDate: yesterday,
    EndDate: today,
    Range: 100,
    Page: 0,
    ConnectionId: -1,
    UserId: -1,
    MessageKey: [],
    RequestType: 0
  },
  requestDataLogList: []
};

$(document).ready(function () {

  Vue.component('log-list', {
    template: '#log-list',
    props: ['items']
  })
 
  new Vue({
    el: '#app-table',
    data: model,
    methods: {
      Update: function () {
        UpdateRequest(1);
      }
    }
  });
  function ReadLogs(DataLogList) {
    console.log(DataLogList);
    model.requestDataLogList = DataLogList;
    model.FatalCount = 0;
    model.ErrorCount = 0;
    model.WarnCount = 0;
    model.DebugCount = 0;

    DataLogList.forEach(element => {
      var dateTime = new Date(Date.parse(element.Time));

      let month = '' + dateTime.getMonth();
      let day = '' + dateTime.getDate();
      let year = dateTime.getFullYear();
      let hours = dateTime.getHours();
      let minutes = dateTime.getMinutes();
      let seconds = dateTime.getSeconds();

      if (month.length < 2)
        month = '0' + month;
      if (day.length < 2)
        day = '0' + day;
      element.Time = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes + ':' + seconds;
      switch (element.Type) {
        case 0:
          element.Type = "Info";
          break;
        case 1:
          element.Type = "Error";
          model.ErrorCount++;
          break;
        case 2:
          element.Type = "Done";
          break;
        case 3:
          element.Type = "Debug";
          model.DebugCount++;
          break;
        case 4:
          element.Type = "Fatal";
          model.FatalCount++;
          break;
        case 5:
          element.Type = "Warn";
          model.WarnCount++;
          break;
      }
    });
  }
  function Init(result) {
    console.log(result);
    model.filters = result;
  }

  function UpdateRequest(requestType = 1) {
    model.requestData.RequestType = requestType;
    switch (requestType) {
      case 0:
 
 
        break;
      case 1:
        model.requestData.MessageKey = [];
        model.filters.forEach(element => {
            if(element.Value){
              model.requestData.MessageKey.push(element.Name);
            }
        });
        break;
    }        
    console.log(model.filters);
    console.log(model.requestData.MessageKey);
    jsonRequestData("/handler/LogHandler", model.requestData, function (response) {

      switch (response.RequestType) {
        case 0:
          Init(response.Result);
          break;
        case 1:
          ReadLogs(response.Result);
          break;
      }
    });

  }
  UpdateRequest(0);
});