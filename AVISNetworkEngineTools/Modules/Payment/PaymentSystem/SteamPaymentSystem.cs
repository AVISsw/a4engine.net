﻿using Newtonsoft.Json;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class SteamPaymentSystem : BasePaymentSystem
    {
        static string secretKey = "64D3A258323C68424C7B1119FCA2ACFF";
        public class OrderStatus
        {
            public const string Init = "Init"; // - Заказ создан, но не авторизован пользователем.
            public const string Approved = "Approved"; // - Заказ одобрен пользователем.
            public const string Succeeded = "Succeeded";// - Заказ проведен.
            public const string Failed = "Failed";// - При заказе возникла ошибка или он не был одобрен.
            public const string Refunded = "Refunded";// - Средства за заказ возвращены и продукт должен быть отозван игрой. Возврат средств может быть инициирован пользователем.
            public const string PartialRefund = "PartialRefund";//  - Средства за один или несколько предметов в корзине возвращены. Проверьте поле itemstatus для получения информации о каждом предмете.
            public const string Chargedback = "Chargedback";//  - Заказ признан мошенническим или оспорен. Продукт должен быть отозван игрой.
            public const string RefundedSuspectedFraud = "RefundedSuspectedFraud";// - Valve вернула средства за заказ, поскольку он может быть мошенническим. Продукт должна отозвать сама игра.
            public const string RefundedFriendlyFraud = "RefundedFriendlyFraud";//- Valve вернула средства за заказ, поскольку платёж за него оспорили мошенническим способом. Продукт должна отозвать сама игра.
        }
       
        public class Status
        {
            public const string OK = "OK";
        }
        A4Engine.Utility.Index Index;
        ExchangeRatesData exchangeRates;
        public SteamPaymentSystem()
        {
            Index = new A4Engine.Utility.Index("config/payment/"+ CurrentInterface + ".index");
            exchangeRates = new ExchangeRatesData() {
                PaymentSystem = DataORM.Models.PaymentSystemType.Steam,
                Rate = 100f
            };
        }
        public override float GetPayAmount(string gameAmount)
        {
            return ReadFloat(gameAmount) / 10f;
        }

        public override bool ClientSuccess(RequestContext requestContext)
        {
            return true;
        }
        public override PaymentContext GetPaymentContext(RequestContext requestContext)
        {
            string orderid = requestContext.getParams["orderid"];
            string characterid = requestContext.getParams["characterid"];
            long amount = 0; // in cents
            ulong orderId = Convert.ToUInt64(orderid);
            long characterId = Convert.ToInt64(characterid);

            var queryResult = QueryTxn(orderId);
            if (queryResult.response.result == Status.OK)
            {
                if (queryResult.response.@params.status == OrderStatus.Approved /*|| isTest*/)
                {
                    foreach (var item in queryResult.response.@params.items)
                    {
                        amount += item.amount;
                    }
                }
                else
                {
                    throw new Exception(string.Format("Транзакция не удалась, текущий статус заказа {0} не соответвует требуему {1}", queryResult.response.@params.status, OrderStatus.Approved));
                }
            }
            else
            {
                throw new Exception("Транзакция не удалась " + queryResult.response.error.errordesc);
            }

            amount = (long)(amount / exchangeRates.Rate); // 83 / 100
            var finalizeData = FinalizeTxn(orderId);
            if (finalizeData.response.result == Status.OK)
            {
                logger.Info(string.Format("Завершен платеж Номер {0}, Сумма {1}, От {2}", orderId, amount, characterid));
                return new PaymentContext()
                {
                    Amount = amount,
                    CharacterId = characterId,
                    OrderId = orderId.ToString(),
                    PaymentSystem = exchangeRates.PaymentSystem
                };
            }
            throw new Exception("Транзакция не удалась " + finalizeData.response.error.errordesc);
        }
 
        public override string RedirectLink(RequestContext requestContext)
        {
            try
            {
                string amounts = requestContext.getParams["amount"];
                float payAmount = GetPayAmount(amounts);
                string characterid = requestContext.getParams["characterid"];
                string steamid = requestContext.getParams["steamid"];
                string lang = requestContext.getParams["lang"];
                ulong steamId = Convert.ToUInt64(steamid);
                ulong Amount = (ulong)((Convert.ToUInt64(payAmount) * exchangeRates.Rate) * 1.21f);
                if (string.IsNullOrWhiteSpace(lang))
                {
                    lang = "EN";
                }
                lang = lang.ToUpper();
                var initResult = InitTxn(steamId, Amount, lang);

                if(initResult.response.error != null)
                {
                    logger.Error("Error code {0} desc {1}", initResult.response.error.errorcode, initResult.response.error.errordesc);
                }
                logger.Info("orderid: {0}, transid: {1}, steamurl: {2}, agreements: {3}", initResult.response.@params.orderid, initResult.response.@params.transid, initResult.response.@params.steamurl, initResult.response.@params.agreements);
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
            }
            string result = @"OK";
            return result;
        }

        public override bool RequestAuthentication(RequestContext requestContext)
        {
 
            return true;
        }

        public override bool TestRequest(RequestContext requestContext)
        {
            return false;
        }
        public const ulong appId = 1648730;
        
        public string CurrentInterface
        {
            get
            {
                if (isTest)
                {
                    return "ISteamMicroTxnSandBox";
                }
                return "ISteamMicroTxn";

            }
        }
        public ulong GenerateOrderId()
        {
            Index.Increment();
            return Index.Get();
        }
        public BaseResponse<InitTxnParam> InitTxn(ulong steamId, ulong amount, string lang)
        {
            string url = "https://partner.steam-api.com/" + CurrentInterface + "/InitTxn/v3/";

            using (var webClient = new WebClient())
            {
                var pars = new NameValueCollection();
                pars.Add("key", secretKey);
                pars.Add("steamid", steamId.ToString());
                pars.Add("currency", "RUB"); //Код валюты стандарта ISO 4217. Правильный формат для каждой валюты указан в разделе
                pars.Add("appid", appId.ToString());
                pars.Add("language", lang);
                pars.Add("orderid", GenerateOrderId().ToString()); // тут надо выдумать чтоб не повторялся
                pars.Add("itemcount", "1"); //Число предметов в корзине
                pars.Add("itemid[0]", "1"); // Сторонний ID предмета.
                pars.Add("amount[0]", amount.ToString()); // Общая стоимость предметов (в центах) в платеже. Правильный формат для значения стоимости указан в разделе Поддерживаемые валюты. Обратите внимание, что переданное значение amount должно быть соответствующим переданному коду currency.
                pars.Add("description[0]", "Game rubles");
                pars.Add("qty[0]", "1"); // Число копий предмета.
 
                var response = webClient.UploadValues(url, "POST", pars);
                var result = Encoding.UTF8.GetString(response);
                logger.Info("json " + result);
                var responceObject = JsonConvert.DeserializeObject<BaseResponse<InitTxnParam>>(result);
                return responceObject;
            }
        }
        public BaseResponse<FinalizeTxnParam> FinalizeTxn(ulong orderId)
        {
            string url = "https://partner.steam-api.com/" + CurrentInterface + "/FinalizeTxn/v2/";

            using (var webClient = new WebClient())
            {
                var pars = new NameValueCollection();
                pars.Add("key", secretKey);
                pars.Add("appid", appId.ToString());
                pars.Add("orderid", orderId.ToString()); // тут надо выдумать чтоб не повторялся
 
                var response = webClient.UploadValues(url, "POST", pars);
                var result = Encoding.UTF8.GetString(response);
                logger.Info("json " + result);
                var responceObject = JsonConvert.DeserializeObject<BaseResponse<FinalizeTxnParam>>(result);
                return responceObject;
            }
        }
        public BaseResponse<GetUserInfoParam> GetUserInfo(ulong steamId, string ip)
        {
            string url = "https://partner.steam-api.com/" + CurrentInterface + "/GetUserInfo/v2";

            using (var webClient = new WebClient())
            {
                var pars = new NameValueCollection();
                pars.Add("key", secretKey);
                pars.Add("steamid", steamId.ToString());
                pars.Add("ipaddress", ip);
 
                var response = webClient.UploadValues(url, "GET", pars);
                var result = Encoding.UTF8.GetString(response);
                var responceObject = JsonConvert.DeserializeObject<BaseResponse<GetUserInfoParam>>(result);
                return responceObject;
            }
        }
        public BaseResponse<QueryTxnParam> QueryTxn(ulong orderid)
        {
            string url = string.Format("https://partner.steam-api.com/{0}/QueryTxn/v2?key={1}&appid={2}&orderid={3}",CurrentInterface, secretKey, appId.ToString(), orderid.ToString());

            using (var webClient = new WebClient())
            {
                var response = webClient.DownloadString(url);
                logger.Info("json " + response);
                var responceObject = JsonConvert.DeserializeObject<BaseResponse<QueryTxnParam>>(response);
                return responceObject;
            }
        }

        public override string ExchangeRates(RequestContext context)
        {
            return JsonConvert.SerializeObject(exchangeRates);
        }
    }
    [Serializable]
    public class BaseResponse<T>
    {
        public Response<T> response;
    }
    [Serializable]
    public class Response<T>
    {
        public string result = "";
        public T @params;
        public ResponceError error;
    }

    [Serializable]
    public class ResponceError
    {
        public int errorcode;
        public string errordesc = "";
    }
    [Serializable]
    public class GetUserInfoParam
    {
        public string state = "";
        public string country = "";
        public string currency = "";
        public string status = "";
    }
    [Serializable]
    public class InitTxnParam
    {
        public string orderid = "";
        public string transid = "";
        public string steamurl = "";
        public string agreements = "";
    }
    [Serializable]
    public class FinalizeTxnParam
    {
        public string orderid = "";
        public string transid = "";
        public string steamurl = "";
        public string agreements = "";
    }
    [Serializable]
    public class QueryTxnParam
    {
        public string orderid = "";
        public string transid = "";
        public string steamid = "";
        public string status = "";
        public string currency = "";
        public string time = "";
        public string country = "";
        public string usstate = "";
        public string timecreated = "";
 
        public Item[] items;
        public class Item
        {
            public long itemid;
            public long qty;
            public long amount;
            public long vat;
            public string itemstatus;
        }
    }
}
