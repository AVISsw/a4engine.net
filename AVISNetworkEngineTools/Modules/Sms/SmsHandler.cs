﻿using A4Engine;
using A4Engine.Modules;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AVISNetworkEngineTools.Modules.SignUp.SignUpSendPhone;

namespace AVISNetworkEngineTools.Modules.Sms
{
    public class SmsHandler : Handler
    {
        //pswd1 CfH37sprKyB0MiQRz96h
        //pswd2 fBg6rOt3R0z0hK0uzxpt

        //pswd1 test zYa1zwUC64RBJ7WRfBy3
        //pswd2 test Glk1X42LWAubZRu7o2fn
        // регистрационная информация (пароль #2)



        public static Logger Log { get; private set; }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }
 
        public override async Task Start(RequestContext Context)
        {
            // string key = Context.Path[3].ToString(); 
            //  SmsService service = new SmsService();
            //  string json = JsonConvert.SerializeObject(service.get_status("79777777777", "28"));
            //Console.WriteLine("json " + json);
            /*

             Console.WriteLine("------------send_sms--------------");
             var result = service.send_sms("79512749488", "SOTA Games, Код регистрации: 3306", 0, "", 0, 0, "SOTA Games");
             foreach (var element in result)
             {
                 Console.WriteLine("element " + element);
             }*/
 
            string message = string.Format("SOTA Games, Код регистрации: ");
            string phone = "37379373342";
            SmsAeroProject.SmsAero smsAero = new SmsAeroProject.SmsAero(Settings.Server.SmsServiceLogin, Settings.Server.SmsServicePassword);
           // string resultAuth = await smsAero.Auth();
 
            string responseJson = await smsAero.SmsSend(message, phone);
            Console.WriteLine("responseJson " + responseJson);
            SmsServiceResponse response = JsonConvert.DeserializeObject<SmsServiceResponse>(responseJson);
            if (response.success)
            {
                Log.Info(JsonConvert.SerializeObject(response.data));
            }
            if (!string.IsNullOrWhiteSpace(response.message))
            {
                Log.Info("------------send_sms_response_message--------------\n", response.message);
            }
        }

    }
}
