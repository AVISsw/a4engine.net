﻿using A4Engine.Transport;
using A4Engine.User;
using AVISNetworkEngineORM.DB;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Controllers
{
    public abstract class BaseController : Base, Interface.IController
    {
        public string DirController { get { return string.Format("{0}{1}/controllers/{2}/", AppDomain.CurrentDomain.BaseDirectory, Asset.Folder, GetType().Name.ToLower()); } }
        public string UriController { get { return string.Format("/Files/controllers/page/html/", this.GetType().Name.ToLower()); } }
        public string Alias { get; set; } = string.Empty;
        public Storage Storage { get; set; }

        public SessionManager SessionManager { set; get; }
        public string Name
        {
            get
            {
                string result = string.Empty;
                if (Alias.Length < 1)
                {
                    result = GetType().Name;
                }
                else
                {
                    result = Alias;
                }
                return result.ToLower();
            }
        }

        byte[] result = new byte[0];
        protected HttpResponseCode statusCode = 0;
        public void Draw(byte[] value, HttpResponseCode code)
        {
            result = value;
            this.statusCode = code;
        }

        public void Draw(string value, HttpResponseCode code)
        {
            result = Encoding.UTF8.GetBytes(value);
            this.statusCode = code;
        }
        public abstract Task Init(RequestContext Context, SessionManager sessionManager);

        public virtual async Task Render(RequestContext Context)
        {
            var temp = result;
            result = new byte[0];
 
            try
            {
                await Context.Request.Response.OutputStream.WriteAsync(temp, 0, temp.Length);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        internal abstract Task OnLoad();

        public void Draw(string value)
        {
            Draw(value, HttpResponseCode.Ok);
        }
    }
}
