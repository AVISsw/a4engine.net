﻿/**
 * Created by AVIS on 11.09.2018.
 * Функции для работы с шаблонами
 */
function fill_elem_assoc(idContiner, assoc_array) {
    $continer = $(idContiner);
    for (var key in assoc_array) {
        var val = assoc_array[key];
        $continer.find('*[name=' + key + ']').html(val);
        console.log(key);
    }
}
function fill_elem_assoc_val(idContiner, assoc_array) {
    $(idContiner).each(function () {
        $(this).val('twe');
    });
}
function MergeArrayObjectsByItemId(arrayA, arrayB) {
    var array = arrayA.concat(arrayB);
    var mapByItemItem = [];
    array.forEach(function (element, index) {
        if (element.ItemId) {
            if (mapByItemItem[element.ItemId])
                Object.assign(mapByItemItem[element.ItemId], element)
            else
                mapByItemItem[element.ItemId] = element;
        }
    });
    return mapByItemItem;
}

var templateList = [];

function BuildTemplateList() {
    var bodyData = $("body").html();
    var regexp = /<!--tp([\s\S]*?)-->/gmi;
    let matches = bodyData.matchAll(regexp);
    for (const match of matches) {
        console.log(match[1]);
        let name = match[1].match(/[\w]+/);
        if (!Array.isArray(name)) {
            throw new Error("template name is not defined : " + match[0]);
        } else {
            var lines = match[1].split('\n');
            lines.splice(0, 1);
            templateList[name] = lines.join('\n');
        }
    }
    console.log(matches);
}
function GetTemplateByName(templateName) {
    if (templateList.length < 1) {
        BuildTemplateList();
        console.warn("Не найдено ни одного шаблона...");
    }
    return templateList[templateName];
}
/**
 * name = ключ ассоциативного массива
 * Функция заменяет выражения типа {name/} в шаблоне,
* */
function fill_pattern(contents, assoc_array) {

    if (typeof (contents) != "string")
        throw new Error('[fill_pattern] contents: не является строкой');

    for (var key in assoc_array) {
        var val = assoc_array[key];
        contents = contents.replace(new RegExp('{' + key + '/}', 'g'), val);
    }
    return contents;
}
function clear_pattern(contents) {

    if (typeof (contents) != "string")
        throw new Error('[fill_pattern] contents: не является строкой');

    contents = contents.replace(new RegExp('{(.*?)/}', 'g'), "");

    return contents;
}

function fill_elem_json(idContiner, jsonData) {
    var assoc_array = JSON.parse(jsonData);
    fill_elem_assoc(idContiner, assoc_array);
}
function fill_elem_json_val(idContiner, jsonData) {
    var assoc_array = JSON.parse(jsonData);
    fill_elem_assoc_val(idContiner, assoc_array);
}
function itemComponent(tableWorkspace, headerTemplate, bodyTemplate) {
    var tableTemplate = $("#table_template").html();
    var headerRowTemplate = headerTemplate;
    var bodyRowTemplate = bodyTemplate;

    function renderHeaderRow(element) {
        return fill_pattern(headerRowTemplate, element);
    }

    function renderRow(element, callback) {
        if (callback)
            return callback(bodyRowTemplate, element);
        return fill_pattern(bodyRowTemplate, element);
    }

    this.renderTable = function (arItemData, renderRowFunction) {
        console.log("renderTable");
        if (!Array.isArray(arItemData)) {
            throw new Error("arItemData is not array");
        }
        if (arItemData.length < 1)
            return;

        let arItemFieldName = Object.keys(arItemData[0]);
        let header_content = renderHeaderRow(arItemFieldName);
        let body_items_continer = "";
        arItemData.forEach(element => {
            body_items_continer += renderRow(element, renderRowFunction);
        });
        resultSpace = fill_pattern(tableTemplate, {
            header_content: header_content,
            body_items_continer: body_items_continer
        });
        tableWorkspace.html(resultSpace);
        console.log("renderTable complite");
    }
}

function Menu(elem) {
    $(".left-header").toggle(
        function () {
            $(elem).hide("slide", { direction: "right" }, 1000);
        },
        function () {
            $(elem).show("slide", { direction: "right" }, 500);
        }
    );
}

function jsonRequestData(uri, postJson, callback) {
    axios.post(uri, postJson, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then((response) => {

            if (callback)
                callback(response.data);
        })
        .catch((error) => {
            console.log(error);
        });
}
function postRequestData(uri, postJson, callback) {
    axios.post(uri, postJson, {
        headers: {
            'Content-type': 'application/x-www-form-urlencoded'
        }
    })
        .then((response) => {

            if (callback)
                callback(response.data);
        })
        .catch((error) => {
            console.log(error);
        });
    }
function get_path() {
    return document.location.pathname.split("/");
}
function Serilaze(select) {
    var $data = {};
    // переберём все элементы input, textarea и select формы с id="myForm "
    select.find('input, button, textearea, select').each(function () {
        // добавим новое свойство к объекту $data
        // имя свойства – значение атрибута name элемента
        // значение свойства – значение свойство value элемента

        if (this.type == "checkbox") {
            $data[this.name] = this.checked;
        } else {
            $data[this.name] = $(this).val();
        }
    });
    return $data;
}
function DrawOptions(selectList, selectedValue = "") {
    var tempEffects = "";
    if (Array.isArray(selectList))
        selectList.forEach(element => {
            var selected = "";
            if (element.Value == selectedValue)
                selected = "selected";
            var temprow = fill_pattern('<option value="{Value/}" ' + selected + '>{Name/}</option>', element);
            tempEffects += temprow;
        });
    return tempEffects;
}

function SetSelect(htmlSelect, val) {
    if (htmlSelect) {
        var $option = htmlSelect.find('option[value="' + val + '"]');
        if ($option) {
            $option.attr('selected', true);
        }
    }
}

function ShowSelectWindow(data, callback, selected = -1) {
    var template = $("#SearchTemplate");
    var itemTemplate = $("#SearchItemTemplate");
    var tempRow = "";

    if (Array.isArray(data)) {
        data.forEach(element => {
            tempRow += fill_pattern(itemTemplate.html(), element);
        });
    }

    var body = ShowModal(template.html(), "Выбор");
    body.find(".ItemList").html(tempRow);

    body.find('.searchInput').keyup(function () {
        // Search text
        var text = $(this).val().toLowerCase();

        // Hide all content class element
        body.find('.content').hide();
        var maxCount = 15;
        var count = 0;
        // Search 
        body.find('.content').each(function () {
            $(this).find("b").each(function () {
                if ($(this).html().toLowerCase().indexOf("" + text + "") != -1) {
                    if (count < maxCount) {
                        $(this).closest('.content').show();
                        count++;
                    }
                }
            });
        });
    });

    if (selected > -1) {
        $("#radio_" + selected).attr("checked", true);
    }

    body.find('.searchInput').keyup();
    body.find('.select').click(function () {
        callback($("input[name='selectId']:checked"));
        $(".close-modal").click();
    });
}

function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}
function ShowModal(content, head = "") {
    $("#myModal").modal('show');
    $('#myModal').find(".modal-title").html(head);
    $('#myModal').find(".modal-body").html(content);
    return $('#myModal').find(".modal-body");
}
function CloseModal() {
    $("#myModal").modal('hide');
}
function md5 (d) { result = M(V(Y(X(d), 8 * d.length))); return result.toLowerCase() }; function M(d) { for (var _, m = "0123456789ABCDEF", f = "", r = 0; r < d.length; r++)_ = d.charCodeAt(r), f += m.charAt(_ >>> 4 & 15) + m.charAt(15 & _); return f } function X(d) { for (var _ = Array(d.length >> 2), m = 0; m < _.length; m++)_[m] = 0; for (m = 0; m < 8 * d.length; m += 8)_[m >> 5] |= (255 & d.charCodeAt(m / 8)) << m % 32; return _ } function V(d) { for (var _ = "", m = 0; m < 32 * d.length; m += 8)_ += String.fromCharCode(d[m >> 5] >>> m % 32 & 255); return _ } function Y(d, _) { d[_ >> 5] |= 128 << _ % 32, d[14 + (_ + 64 >>> 9 << 4)] = _; for (var m = 1732584193, f = -271733879, r = -1732584194, i = 271733878, n = 0; n < d.length; n += 16) { var h = m, t = f, g = r, e = i; f = md5_ii(f = md5_ii(f = md5_ii(f = md5_ii(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_ff(f = md5_ff(f = md5_ff(f = md5_ff(f, r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 0], 7, -680876936), f, r, d[n + 1], 12, -389564586), m, f, d[n + 2], 17, 606105819), i, m, d[n + 3], 22, -1044525330), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 4], 7, -176418897), f, r, d[n + 5], 12, 1200080426), m, f, d[n + 6], 17, -1473231341), i, m, d[n + 7], 22, -45705983), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 8], 7, 1770035416), f, r, d[n + 9], 12, -1958414417), m, f, d[n + 10], 17, -42063), i, m, d[n + 11], 22, -1990404162), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 12], 7, 1804603682), f, r, d[n + 13], 12, -40341101), m, f, d[n + 14], 17, -1502002290), i, m, d[n + 15], 22, 1236535329), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 1], 5, -165796510), f, r, d[n + 6], 9, -1069501632), m, f, d[n + 11], 14, 643717713), i, m, d[n + 0], 20, -373897302), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 5], 5, -701558691), f, r, d[n + 10], 9, 38016083), m, f, d[n + 15], 14, -660478335), i, m, d[n + 4], 20, -405537848), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 9], 5, 568446438), f, r, d[n + 14], 9, -1019803690), m, f, d[n + 3], 14, -187363961), i, m, d[n + 8], 20, 1163531501), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 13], 5, -1444681467), f, r, d[n + 2], 9, -51403784), m, f, d[n + 7], 14, 1735328473), i, m, d[n + 12], 20, -1926607734), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 5], 4, -378558), f, r, d[n + 8], 11, -2022574463), m, f, d[n + 11], 16, 1839030562), i, m, d[n + 14], 23, -35309556), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 1], 4, -1530992060), f, r, d[n + 4], 11, 1272893353), m, f, d[n + 7], 16, -155497632), i, m, d[n + 10], 23, -1094730640), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 13], 4, 681279174), f, r, d[n + 0], 11, -358537222), m, f, d[n + 3], 16, -722521979), i, m, d[n + 6], 23, 76029189), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 9], 4, -640364487), f, r, d[n + 12], 11, -421815835), m, f, d[n + 15], 16, 530742520), i, m, d[n + 2], 23, -995338651), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 0], 6, -198630844), f, r, d[n + 7], 10, 1126891415), m, f, d[n + 14], 15, -1416354905), i, m, d[n + 5], 21, -57434055), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 12], 6, 1700485571), f, r, d[n + 3], 10, -1894986606), m, f, d[n + 10], 15, -1051523), i, m, d[n + 1], 21, -2054922799), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 8], 6, 1873313359), f, r, d[n + 15], 10, -30611744), m, f, d[n + 6], 15, -1560198380), i, m, d[n + 13], 21, 1309151649), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 4], 6, -145523070), f, r, d[n + 11], 10, -1120210379), m, f, d[n + 2], 15, 718787259), i, m, d[n + 9], 21, -343485551), m = safe_add(m, h), f = safe_add(f, t), r = safe_add(r, g), i = safe_add(i, e) } return Array(m, f, r, i) } function md5_cmn(d, _, m, f, r, i) { return safe_add(bit_rol(safe_add(safe_add(_, d), safe_add(f, i)), r), m) } function md5_ff(d, _, m, f, r, i, n) { return md5_cmn(_ & m | ~_ & f, d, _, r, i, n) } function md5_gg(d, _, m, f, r, i, n) { return md5_cmn(_ & f | m & ~f, d, _, r, i, n) } function md5_hh(d, _, m, f, r, i, n) { return md5_cmn(_ ^ m ^ f, d, _, r, i, n) } function md5_ii(d, _, m, f, r, i, n) { return md5_cmn(m ^ (_ | ~f), d, _, r, i, n) } function safe_add(d, _) { var m = (65535 & d) + (65535 & _); return (d >> 16) + (_ >> 16) + (m >> 16) << 16 | 65535 & m } function bit_rol(d, _) { return d << _ | d >>> 32 - _ }

function СalculateHashes(Items) {
    let resultHash = {};
    for (let index in Items) {
        let element = Items[index];
        resultHash[element.Index] = {};
        resultHash[element.Index]
        resultHash[element.Index].Hash = md5(JSON.stringify(element));
        resultHash[element.Index].Id = element.Id;
    }
    return resultHash;
}

Date.prototype.toMysqlFormat = function () {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};
 
 