﻿using A4Engine.Modules;
using AVISNetworkEngineORM.Data;
using AVISNetworkEngineORM.DB.Interface;
using AVISNetworkEngineTools.Modules.PromoCode;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Partner
{
    public class PartnerHandler : Handler
    {
 
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override async Task OnLoad()
        {
        }

        public override async Task Start(RequestContext Context)
        {
            string action = Context.Path[3];
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");
                if (isPartner || isAdmCreater)
                {
 
                    List<InvitedData> invitedList = null;
                    string lastDaysStr = "";
                    int lastDays = 0;
                    DateTime afterDate ;
                    ResponseData response;
                    List<string> promos = null;
                    switch (action)
                    {
                        case "get_invited":
                            lastDaysStr = Context.getParams.Get("lastDays");
                            lastDays = Convert.ToInt32(lastDaysStr);
                            afterDate = DateTime.Now.AddDays(-lastDays);
 
                            InvitedResponseData invitedResponseData;
                            promos = await GetCreatedPromo(User.UserData.Id);
                            invitedList = await this.GetInvited(promos, afterDate);
                            invitedResponseData = new InvitedResponseData() { List = invitedList, Success = true };

                            Draw(JsonConvert.SerializeObject(invitedResponseData));
                            break;
                        case "get_metrix":
                            lastDaysStr = Context.getParams.Get("lastDays");
                            lastDays = Convert.ToInt32(lastDaysStr);
                            afterDate = DateTime.Now.AddDays(-lastDays);
                            promos = await GetCreatedPromo(User.UserData.Id);
                            invitedList = await this.GetInvited(promos, afterDate);
                            Draw(JsonConvert.SerializeObject(GetMetricData(invitedList, afterDate)));
                            break;
                    }
                    return;
                }
            }
            Draw(JsonConvert.SerializeObject(new ResponseData() { Success = false, Message = "Недостаточно прав для использования" }));
        }
        public async Task<List<string>> GetCreatedPromo(long createrId)
        {
            List<string> result = new List<string>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<PromoCodeData>("CreaterAccountId={0}", createrId))
            {
                while (baseData.Read)
                {
                    var promoObj = baseData.GetObject<PromoCodeData>();
                    result.Add(promoObj.Code);
                }
            }

            return result;
        }
        public async Task<bool> IsCreaterPromo(string promoCode, long createrId)
        {
            var promoData = await Controller.Storage.Get.Where<PromoCodeData>("CreaterAccountId={0} AND Code={1}", createrId, promoCode);
            return promoData != null;
        }
        public InvitedMetricData GetMetricData(List<InvitedData> elements, DateTime afterDate)
        {
            InvitedMetricData invitedMetricData = new InvitedMetricData();
            foreach (var element in elements)
            {
                invitedMetricData.DonateMoney += element.TotalAmount;
            }
            invitedMetricData.AfterDate = afterDate;
            return invitedMetricData;
        }
 
        private async Task<List<InvitedData>> GetInvited(List<string> promos, DateTime afterDate)
        {
            List<InvitedData> result = new List<InvitedData>();
            try
            {
 
                string formatedDate = baseOperation.DateTimeFormat(afterDate);
                for (int i = 0; i < promos.Count; i++)
                {
                    promos[i] = string.Format(" `accounts`.`Promo` = \"{0}\"", promos[i]);
                }
                string wherePromos = string.Join(" OR ", promos);
                if (promos.Count < 1)
                {
                    return result;
                }
                string query = string.Format(@"
            DROP TABLE
            IF
	            EXISTS tmp_payments;
            CREATE TEMPORARY TABLE `tmp_payments` SELECT
            `payment_history`.`CharacterId`,
            SUM( `payment_history`.`Amount` ) AS TotalAmount 
            FROM
	            `payment_history` 
            WHERE
	            `payment_history`.`LeadTime` >= '{1}' 
	            AND ( Test IS NULL OR Test = 0 ) 
            GROUP BY
	            `payment_history`.`CharacterId`;
 
            DROP TABLE
            IF
	            EXISTS tmp_users;
            CREATE TEMPORARY TABLE `tmp_users` SELECT
            `characters`.`Name` as CharacterName,
            `accounts`.`Id` AS 'AccId',
            `accounts`.`Promo` as Promo,
            `characters`.`Id`,
            `characters`.`Account` as Account,
            `tmp_payments`.`TotalAmount` as TotalAmount,
            `tmp_payments`.`CharacterId` as CharacterId,
            `accounts`.`RegDate` as RegDate
            FROM
	            `tmp_payments`
	             RIGHT JOIN `characters` ON `tmp_payments`.`CharacterId` = `characters`.`Id` 
	             LEFT JOIN `accounts` ON `characters`.`Account` = `accounts`.`Id` 
            WHERE
	            ({0})
	            AND `accounts`.`RegDate` >= '{1}';
	
            SELECT
	            * 
            FROM
	            tmp_users;
            ", wherePromos, formatedDate);
                Console.WriteLine("----query " + query);
                using (var data = await Controller.Storage.Query(query))
                {
                    while (data.Read)
                    {
                        var totalAmountValue = data.GetValue(5);
                        if (totalAmountValue.GetType() == typeof(DBNull))
                        {
                            totalAmountValue = 0;
                        }

                        var itemData = new InvitedData()
                        {
                            CharacterName = data.GetString(0),
                            Promo = data.GetString(2),
                            Id = data.GetInt64(3),
                            Account = data.GetInt64(4),
                            TotalAmount = Convert.ToInt64(totalAmountValue),
                            RegDate = (DateTime)data.GetValue(7)
                        };
                        result.Add(itemData);
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return result;
        }
    }
    [DBTable("temp_tab")]
    public class InvitedData
    {
        [DBField]
        public string CharacterName;
        [DBField]
        public long Account;
        [DBField]
        public string Promo;
        [DBField]
        public long Id;
        [DBField]
        public long TotalAmount;
        [DBField]
        public DateTime RegDate;
    }
    [DBTable("temp_tab")]
    public class InvitedMetricData : ResponseData
    {
        [DBField]
        public long DonateMoney;
        [DBField]
        public DateTime AfterDate;
    }
 
    public class InvitedResponseData : ResponseData
    {
        [DBField]
        public List<InvitedData> List;
    }
}
