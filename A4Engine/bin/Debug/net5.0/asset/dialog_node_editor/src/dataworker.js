var DataWorker = {
  base: this,
  opened: [],
  stTable: {},
  Export: (nodes) => {
    var exportingData = [];
    nodes.forEach((node) => {
      var connections = [];
      node.connections.forEach((connection) => {
        connections.push(connection.id);
      });
      var nodeData = {
        id: node.id,
        rect: {
          x: node.rect.x,
          y: node.rect.y,
          height: node.rect.height,
          width: node.rect.width
        },
        connections: connections,
        data: node.data
      };
      exportingData.push(nodeData);
    });
    return exportingData;
  },
  CsvParser: (str) => {
    var arr = [];
    var quote = false;  // 'true' означает, что мы находимся внутри поля, заключенного в кавычки 

    // Перебираем каждый символ, отслеживаем текущую строку и столбец (возвращенного массива) 
    for (var row = 0, col = 0, c = 0; c < str.length; c++) {
      var cc = str[c], nc = str[c + 1];       // Текущий символ, следующий символ 
      arr[row] = arr[row] || [];            // При необходимости создаем новую строку 
      arr[row][col] = arr[row][col] || '';   // При необходимости создаем новый столбец (начинаем с пустой строки) 

      // Если текущий символ - кавычка, и мы внутри
      // поле в кавычках, и следующий символ также является кавычкой,
      // добавляем кавычки в текущий столбец и пропускаем следующий символ 
      if (cc == '"' && quote && nc == '"') { arr[row][col] += cc; ++c; continue; }

      // Если это всего одна кавычка, начало / конец поля в кавычках 
      if (cc == '"') { quote = !quote; continue; }

      // Если это точка с запятой и мы не находимся в поле в кавычках, переходим к следующему столбцу 
      if (cc == ';' && !quote) { ++col; continue; }

      // Если это новая строка (CRLF) и мы не находимся в поле в кавычках, пропускаем следующий символ
      // и переходим к следующей строке и переходим к столбцу 0 этой новой строки 
      if (cc == '\r' && nc == '\n' && !quote) { ++row; col = 0; ++c; continue; }

      // Если это новая строка (LF или CR) и мы не находимся в поле в кавычках,
      // переходим к следующей строке и переходим к столбцу 0 этой новой строки 
      if (cc == '\n' && !quote) { ++row; col = 0; continue; }
      if (cc == '\r' && !quote) { ++row; col = 0; continue; }

      // В противном случае добавляем текущий символ в текущий столбец 
      arr[row][col] += cc;
    }
    return arr;
  },
  ExportJSON: (nodes) => {

    return JSON.stringify(DataWorker.Export(nodes));
  },
  Import: (data) => {
    //Текст, нам надо превратить простой список данных в список нод с их коннекшонами
    //Сначала нужно создать все ноды
    //Затем уже определеить кто куда присоединяется
  },
  DownloadData: (filename, text) => {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename + ".dlg");
    element.style.display = "none";

    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  },
  DownloadStTable: (filename, text) => {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename + ".lang");
    element.style.display = "none";

    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  },
  GetFileId: (name) => {
    let counter = 0;
    for (let file of DataWorker.opened) {
      if (file.name === name) {
        return counter;
      }
      counter++;
    }
    return -1;
  },
  GetPhrase: (code) => {
    return DataWorker.stTable[code];
  },
  SavePhrase: (code, text) => {
    DataWorker.stTable[code] = text;
  },
  GetNode: (fileName, nodeID) => {
    let fileId = DataWorker.GetFileId(fileName);
    let file = DataWorker.opened[fileId];

    for (let i in file.nodes) {
      if (file.nodes[i].id === nodeID) {
        return file.nodes[i];
      }
    }
    return false;
  },
  LoadStDialogTableFile: (files) => {
    for (let file of files) {
      let reader = new FileReader();
      reader.onload = function () {
        let name = file.name.replace(".lang", "");
        DataWorker.stTable = JSON.parse(reader.result);
        console.log(DataWorker.stTable);
        /*
        let data = DataWorker.CsvParser(reader.result);
        console.log(data);
        DataWorker.stTable = [];
        for(let key in data){
          var row = data[key];
          DataWorker.stTable[row[0]] = row[1];
        }*/
 
      };
      reader.readAsText(file, "utf-8");
    }
  },
  SaveStDialogTableFile: (fileName) => {

    for (let textCode in DataWorker.stTable) {
      let isExist = false;
      for (let key in window.editorCanvas.nodes) {
        var node = window.editorCanvas.nodes[key];
        if (textCode == node.data.text)
          isExist = true;
      }
      if (!isExist) {
        DataWorker.stTable[textCode] = undefined;
        delete DataWorker.stTable[textCode];
      }
    }
    var result = "";
 
    result = JSON.stringify(DataWorker.stTable);
    let name = "st_"+ fileName;
    DataWorker.DownloadStTable(name, result);
  },
  LoadFiles: (files) => {
    var last = "";
    for (let file of files) {
      let reader = new FileReader();
      reader.onload = function () {
        let name = file.name.replace(".dlg", "");
        let data = JSON.parse(reader.result);
        last = name;

        let nodes = [];
        for (let key in data) {
          let node;
          if (data[key].data.type === window.DialogType.Root) {
            node = window.ImportRootNode(data[key]);
          } else {
            node = window.ImportNode(data[key]);
          }
          node.tempConnections = data[key].connections;
          nodes.push(node);
        }
        nodes[0].data.name = name;

        let oldFileId = DataWorker.GetFileId(name);
        if (oldFileId >= 0) {
          DataWorker.opened[oldFileId].nodes = nodes;
        } else {
          DataWorker.opened.push({
            name: name,
            nodes: nodes
          });
        }

        for (let id in nodes) {
          var node = nodes[id];
          for (let connectionId of node.tempConnections) {
            node.connections.push(DataWorker.GetNode(name, connectionId));
          }
          delete node.tempConnections;
        }
        DataWorker.DrawLabels();

        if (last !== "") {
          window.OnDialogSelected(last);
        }
      };
      reader.readAsText(file);
    }
  },
  DrawLabels: () => {
    let filesList = document.getElementById("files_list");
    filesList.innerHTML = "";
    for (let file of DataWorker.opened) {
      filesList.innerHTML +=
        '<button class="btn_dialogue" onclick="OnDialogSelected(\'' +
        file.name +
        "')\">" +
        file.name +
        "</button>";
    }
  },
  RenameFile: (oldName, newName) => {
    var id = DataWorker.GetFileId(oldName);
    DataWorker.opened[id].name = newName;
    DataWorker.DrawLabels();
  }
};

export { DataWorker };
