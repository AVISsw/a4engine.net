﻿using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Modules.Menu
{
    public class Menu : Module
    {
        Logger Log;
        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); 
        }

        public override async Task Start(RequestContext Context)
        {
            string index = FileGetContents(DirModule + "\\html\\index.html");
            
            if(User != null)
            {
                Log.Error(JsonConvert.SerializeObject(User.UserData));
                if (User.GroupRight.Can("goTo"))
                {
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", User.UserData.Login }, { "header3", "Ты адмиин" } }));
                    Log.Error("Ты адмиин");
                     
                }
            }
            else
            {
                Log.Error("Session is null");
            }
            Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));  
        }
    }
}
