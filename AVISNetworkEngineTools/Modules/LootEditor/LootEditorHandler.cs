﻿using DataORM.Models;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using A4Engine.Modules;
using System.Threading.Tasks;
using A4Engine.Modules.ItemEditor;

namespace A4Engine.Modules.LootEditor
{
    public class LootEditorHandler : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public async Task<Dictionary<long, SpawnLootData>> GetLoots()
        {
            Dictionary<long, SpawnLootData> loots = new Dictionary<long, SpawnLootData>();
            using (var tempData = await Controller.Storage.GetAll.WhereBase<SpawnLootData>(""))
            {
                while (tempData.Read)
                {
                    SpawnLootData lootData = tempData.GetObject<SpawnLootData>();
                    loots.Add(lootData.TransformId, lootData);
                }
            }
            return loots;
        }

        public async Task<bool> AddLoot(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<SpawnLootData>(data));
                if (lastId > 0)
                    isAdd = true;
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<bool> UpdateLoot(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long transformId = Convert.ToInt64(data["TransformId"]);
                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<SpawnLootData>(data), " TransformId={0}", transformId);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveLoot(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long transformId = Convert.ToInt64(data["TransformId"]);
                isAdd = await Controller.Storage.Remove.Where<SpawnLootData>(" TransformId={0}", transformId);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<List<ChanceSpawnItems>> GetGroups()
        {
            List<ChanceSpawnItems> result = new List<ChanceSpawnItems>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ChanceSpawnItems>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ChanceSpawnItems>());
                }
            }
            return result;
        }
 
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();

            if (User != null)
            {
                string Hash = Context.postParams.Get("Hash");
                if (Hash != null)
                    Context.Request.Cookies.Update(new System.Net.Cookie("SessionHash", Hash, "/"));
                if (User.GroupRight.Can("editScene"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(await this.AddLoot(Context.postParams)));
                            break;
                        case "update":
                            Draw(JsonConvert.SerializeObject(await this.UpdateLoot(Context.postParams)));
                            break;
                        case "get":
                            Draw(JsonConvert.SerializeObject(await this.GetLoots()));
                            break;
                        case "getgroups":
                            Draw(JsonConvert.SerializeObject(await this.GetGroups()));
                            break;
                        case "remove":
                            Draw(JsonConvert.SerializeObject(await this.RemoveLoot(Context.postParams)));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }

    }
}