﻿using A4Engine.Modules;
using AVISNetworkCommon;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.QuestEditor
{
    public class QuestEditorHandler : Handler
    {
        Logger Log;
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }

        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            if (User.GroupRight.Can("quest_editor"))
            {
                switch (key)
                {
                    case "get-quest":
                        Draw(JsonConvert.SerializeObject(await this.GetQuest(Context)));
                        break;
                    case "add-quest":
                        Draw(JsonConvert.SerializeObject(await this.AddQuest(Context)));
                        break;
                    case "update-quest":
                        Draw(JsonConvert.SerializeObject(await this.UpdateQuest(Context)));
                        break;
                    case "remove-quest":
                        Draw(JsonConvert.SerializeObject(await this.RemoveQuest(Context)));
                        break;
                    case "get-quest-list":
                        Draw(JsonConvert.SerializeObject(await this.GetQuestList()));
                        break;
                    default:
                        Draw(JsonConvert.SerializeObject(
                            new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                        }
                        ));
                        break;
                }
                return;
            }
            Draw("<b>Доступ запрещен!</b>");

        }
        private async Task<Dictionary<long, ResultQuestObject>> GetQuestList()
        {
            Dictionary<long, ResultQuestObject> result = new Dictionary<long, ResultQuestObject>();

            using (var baseData = await Controller.Storage.GetAll.WhereBase<QuestData>(""))
            {
                while (baseData.Read)
                {
                    var questData = baseData.GetObject<QuestData>();
                    questData.IsDbExits = true;
                    result.Add(questData.Id, new ResultQuestObject()
                    {
                        QuestData = questData,
                        Stages = new List<QuestStageData>(),
                        Rewards = new List<QuestRewardData>()
                    });
                }
            }
            using (var baseData = await Controller.Storage.GetAll.WhereBase<QuestStageData>(""))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<QuestStageData>();
                    temp.IsDbExits = true;
                    if (result.ContainsKey(temp.QuestId))
                    {
                        result[temp.QuestId].Stages.Add(temp);
                    }
                }
            }
            using (var baseData = await Controller.Storage.GetAll.WhereBase<QuestRewardData>(""))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<QuestRewardData>();
                    temp.IsDbExits = true;
                    if (result.ContainsKey(temp.QuestId))
                    {
                        result[temp.QuestId].Rewards.Add(temp);
                    }
                }
            }

            return result;
        }
        private async Task<ResultQuestObject> UpdateQuest(RequestContext context)
        {
            try
            {
                Console.WriteLine(context.rawParams);
                ResultQuestObject finalresult = JsonConvert.DeserializeObject<ResultQuestObject>(context.rawParams);
                if (finalresult.QuestData.IsDbExits)
                {
                    bool isUpdate = await Controller.Storage.Update.Where(finalresult.QuestData, "Id={0}", finalresult.QuestData.Id);
                    if (isUpdate)
                    {
                        finalresult.QuestData.IsDbExits = isUpdate;
                    }
                }
                else
                {
                    finalresult.QuestData.Id = await Controller.Storage.Add.In(finalresult.QuestData);
                    finalresult.QuestData.IsDbExits = true;
                }

                foreach (var stageData in finalresult.Stages)
                {
                    if (stageData.IsRemove && stageData.IsDbExits)
                    {
                        bool isRemove = await Controller.Storage.Remove.Where<QuestStageData>("Id={0}", stageData.Id);
                    }
                    else
                    {
                        if (stageData.IsDbExits)
                        {
                            bool isUpdate = await Controller.Storage.Update.Where(stageData, "Id={0}", stageData.Id);
                            if (isUpdate)
                            {
                                stageData.IsDbExits = isUpdate;
                            }
                        }
                        else
                        {
                            stageData.Id = await Controller.Storage.Add.In(stageData);
                            stageData.IsDbExits = true;
                        }
                    }
                }
                for(int i = finalresult.Stages.Count - 1; i >= 0; i--)
                {
                    var stage = finalresult.Stages[i];
                    if (stage.IsRemove)
                    {
                        finalresult.Stages.RemoveAt(i);
                    }
                }

                foreach (var rewardData in finalresult.Rewards)
                {
                    if (rewardData.IsRemove && rewardData.IsDbExits)
                    {
                        bool isRemove = await Controller.Storage.Remove.Where<QuestRewardData>("Id={0}", rewardData.Id);
                    }
                    else
                    {
                        if (rewardData.IsDbExits)
                        {
                            bool isUpdate = await Controller.Storage.Update.Where(rewardData, "Id={0}", rewardData.Id);
                            if (isUpdate)
                            {
                                rewardData.IsDbExits = isUpdate;
                            }
                        }
                        else
                        {
                            rewardData.Id = await Controller.Storage.Add.In(rewardData);
                            rewardData.IsDbExits = true;
                        }
                    }
                }
                for (int i = finalresult.Rewards.Count - 1; i >= 0; i--)
                {
                    var rewardData = finalresult.Rewards[i];
                    if (rewardData.IsRemove)
                    {
                        finalresult.Rewards.RemoveAt(i);
                    }
                }
                return finalresult;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }
        private async Task<ResultQuestObject> AddQuest(RequestContext context)
        {
            QuestData questData = Controller.GetObjectByParams<QuestData>(context.postParams);
            long lastId = await Controller.Storage.Add.In(questData);
            questData.Id = lastId;

            return new ResultQuestObject() { QuestData = questData, Stages = new List<QuestStageData>() };
        }
        private async Task<bool> RemoveQuest(RequestContext context)
        {
            Console.WriteLine("Id " + context.postParams.Get("Id"));
            long questId = Convert.ToInt64(context.postParams.Get("Id"));
            bool result = await Controller.Storage.Remove.Where<QuestData>("Id={0}", questId);
            bool isRemove = await Controller.Storage.Remove.Where<QuestStageData>("QuestId={0}", questId);
            return result;
        }
        private async Task<ResultQuestObject> GetQuest(RequestContext Context)
        {
            long questId = Convert.ToInt64(Context.postParams.Get("id"));
            ResultQuestObject result = new ResultQuestObject();
            result.QuestData = await Controller.Storage.Get.Where<QuestData>("Id={0}", questId);
            result.QuestData.IsDbExits = true;

            result.Stages = new List<QuestStageData>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<QuestStageData>("QuestId={0}", questId))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<QuestStageData>();
                    temp.IsDbExits = true;
                    result.Stages.Add(temp);
                }
            }
            result.Rewards = new List<QuestRewardData>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<QuestRewardData>("QuestId={0}", questId))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<QuestRewardData>();
                    temp.IsDbExits = true;
                    result.Rewards.Add(temp);
                }
            }

            return result;
        }
        public class ResultQuestObject
        {
            public QuestData QuestData;
            public List<QuestStageData> Stages;
            public List<QuestRewardData> Rewards;
        }
    }
}
