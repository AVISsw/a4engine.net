import { Connector } from "./connector.js";

class Node {
  constructor(rect, id) {
    this.id = id;
    this.rect = rect;
    this.dragData = { dragged: false, offsetX: 0, offsetY: 0 };
    this.show = false;
    this.connector = new Connector();
    this.name = "NODE";
    this.selected = false;
    this.data = {}; //Данные ноды, заполняются из другого класса
    this.borderWidth = 3;
    this.connector.OnClick = this.OnConnectionClick.bind(this);
    this.connector.OnDragStoped = this.OnConnectorDragStopped.bind(this);

    this.connections = [];
  }
  Draw(offsetX, offsetY, context) {
    this.rect.ApplyOffset(offsetX, offsetY);
    if (this.selected) {
      //Background Border

      context.fillStyle = "yellow";
      context.fillRect(
        this.rect.relativeX - this.borderWidth,
        this.rect.relativeY - this.borderWidth,
        this.rect.width + this.borderWidth * 2,
        this.rect.height + this.borderWidth * 2
      );
    } else {
      context.fillStyle = this.rect.color;
      context.fillRect(
        this.rect.relativeX - this.borderWidth,
        this.rect.relativeY - this.borderWidth,
        this.rect.width + this.borderWidth * 2,
        this.rect.height + this.borderWidth * 2
      );
    }
    //Bacground
    context.fillStyle = "#2E2E2E";
    context.fillRect(
      this.rect.relativeX,
      this.rect.relativeY,
      this.rect.width,
      this.rect.height
    );

    context.font = "bold 15px courier";
    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = "#DCDCDC";
    context.fillText(
      this.name + "(" + this.id + ")",
      this.rect.middleX,
      this.rect.relativeY - 14
    );

    /////////////уголок/////////////////
    context.fillStyle = this.rect.color;
    context.fillRect(
      this.rect.relativeX + this.rect.width - 10,
      this.rect.relativeY + this.rect.height - 10,
      10 + this.borderWidth,
      10 + this.borderWidth
    );
    /// значек ивенты
    if (this.data) {
      // data.conditions
      //  data.events
      if (this.data.events) {
        if (this.data.events.length > 0) {
          context.drawImage(window.EventIcon,
            this.rect.relativeX + this.rect.width + 10,
            this.rect.relativeY + this.rect.height - 30,
            10 + this.borderWidth,
            10 + this.borderWidth
          );
        }
      }
      if (this.data.conditions) {
        if (this.data.conditions.length > 0) {
          context.drawImage(window.ConditionIcon,
            this.rect.relativeX + this.rect.width + 10,
            this.rect.relativeY + this.rect.height - 60,
            10 + this.borderWidth,
            10 + this.borderWidth
          );
        }
      }
    }
    ///////////////////////

    context.font = "italic 21px courier";
    context.textAlign = "left";
    context.textBaseline = "top";
    context.fillStyle = "#DCDCDC";
    var text = window.DataWorker.GetPhrase(this.data.text);
    if(text == undefined){
      text = "Такая фраза не существует";
    }
    var oneLetterWidth = context.measureText("w").width;
    var maxWidth = (this.rect.width / oneLetterWidth) >> 0;
    var textLength = context.measureText(text).width;
    var rows = (textLength / this.rect.width) >> 0;
    if (rows < textLength / this.rect.width) {
      rows++;
    }
    // draw node text
    if (rows === 0) {
       
      context.fillText(
        text,
        this.rect.relativeX + 2,
        this.rect.relativeY
      );
    }

    if (this.rect.height < 17 * rows) {
      rows = (this.rect.height / 17) >> 0;
    }

    for (let i = 0; i < rows; i++) {
      context.fillText(
        text.slice(0 + maxWidth * i, maxWidth + maxWidth * i),
        this.rect.relativeX + 2,
        this.rect.relativeY + 17 * i
      );
    }

    this.DrawConnector(offsetX, offsetY, context);
  }

  DrawConnector(offsetX, offsetY, context) {
    this.connector.ApplyOffset(this.rect, offsetX, offsetY);
    //Connector
    context.beginPath();
    context.lineWidth = 4;

    this.connections.forEach((connection) => {
      if (
        this.selected &&
        connection.id === window.rightMenu.selectedConnection
      ) {
        context.strokeStyle = "white";
        context.stroke();
        context.beginPath();
      }
      if (connection.selected) {
        context.strokeStyle = "black";
        context.stroke();
        context.beginPath();
      }

      context.moveTo(
        this.connector.rect.relativeX + this.connector.rect.width / 2,
        this.connector.rect.relativeY + this.connector.rect.height
      );
      context.lineTo(
        connection.rect.relativeX + connection.rect.width / 2,
        connection.rect.relativeY
      );
      if (
        this.selected &&
        connection.id === window.rightMenu.selectedConnection
      ) {
        context.strokeStyle = "red";
        context.stroke();
        context.beginPath();
      }
      if (connection.selected) {
        context.strokeStyle = "yellow";
        context.stroke();
        context.beginPath();
      }
    });

    this.connector.Draw(offsetX, offsetY, context);

    if (this.selected) {
      context.strokeStyle = "white";
    } else {
      context.strokeStyle = "black";
    }
    context.stroke();

    context.fillStyle = this.connector.rect.color;
    context.fillRect(
      this.connector.rect.relativeX,
      this.connector.rect.relativeY,
      this.connector.rect.width,
      this.connector.rect.height
    );
  }

  IsPointInside(x, y) {
    if (
      this.PointInsideRectangle(
        x,
        y,
        this.connector.rect.relativeX,
        this.connector.rect.relativeY,
        this.connector.rect.height,
        this.connector.rect.width
      )
    ) {
      return this.connector;
    }

    if (
      this.PointInsideRectangle(
        x,
        y,
        this.rect.relativeX,
        this.rect.relativeY,
        this.rect.height,
        this.rect.width
      )
    ) {
      return this;
    }

    return false;
  }

  IsPointOnBorder(x, y) {
    let size = 10;
    if (
      this.PointInsideRectangle(
        x,
        y,
        this.rect.relativeX + this.rect.width - size,
        this.rect.relativeY + this.rect.height - size,
        size + this.borderWidth,
        size + this.borderWidth
      )
    ) {
      return this;
    }
  }

  PointInsideRectangle(
    pointerX,
    pointerY,
    rectangleX,
    rectangleY,
    height,
    width
  ) {
    if (
      pointerX >= rectangleX &&
      pointerX <= rectangleX + width &&
      pointerY >= rectangleY &&
      pointerY <= rectangleY + height
    ) {
      return true;
    }
    return false;
  }

  OnClick() {
    //При клике, в правую колонку вывоядятся данные ноды
    if (window.mouseEventData.isDoubleClick) {
      window.dialogueProcessor.OpenSpeechEditor(this);
    }
  }

  OnSelected() {
    this.selected = true;
    window.rightMenu.WriteConnectionData(this.connections);
    window.rightMenu.DisplayData(this);
  }

  OnConnectionClick() {
    var newNode = window.CreateNode(
      this,
      this.connector.rect.middleX,
      this.connector.rect.middleY + 50
    );

    this.connections.push(newNode);
  }

  OnDrag(mouseX, mouseY) {
    if (!this.dragData.dragged) {
      this.dragData.offsetX = mouseX - this.rect.x;
      this.dragData.offsetY = mouseY - this.rect.y;
      this.dragData.dragged = true;
    }

    this.rect.x = mouseX - this.dragData.offsetX;
    this.rect.y = mouseY - this.dragData.offsetY;
  }

  OnDragStoped() {
    this.dragData.dragged = false;
  }

  OnConnectorDragStopped() {
    this.connector.connectionInProcess = false;

    window.OnCreatingConnection(this);
  }

  OnResize(x, y) {
    this.rect.width = x - this.rect.relativeX;
    this.rect.height = y - this.rect.relativeY;
  }
}

class RootNode extends Node {
  Draw(offsetX, offsetY, context) {
    this.rect.ApplyOffset(offsetX, offsetY);

    //Bacground
    context.fillStyle = this.rect.color;
    context.fillRect(
      this.rect.relativeX,
      this.rect.relativeY,
      this.rect.width,
      this.rect.height
    );

    //Text
    context.font = "bold 28px courier";
    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = "#ccc";
    context.fillText("START", this.rect.middleX, this.rect.middleY - 14);

    this.DrawConnector(offsetX, offsetY, context);
  }
  OnClick() { }
  IsPointOnBorder(x, y) { }
}

export { Node, RootNode };
