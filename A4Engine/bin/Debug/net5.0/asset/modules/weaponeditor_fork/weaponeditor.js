/*
$(document).ready(function () {
  Menu();
  $('#searchInput').keyup(function () {

    // Search text
    var text = $(this).val().toLowerCase();

    // Hide all content class element
    $('.content').hide();

    // Search 
    $('.content').each(function () {
      $(this).find("input").each(function () {
        if ($(this).val().toLowerCase().indexOf("" + text + "") != -1) {
          $(this).closest('.content').show();
        }
      });
    });
  });
});

function SelectItem(element,id) {
getItemsRequest(function(data){

  console.log(data);
  var itemTemplate = $("#ItemTemplate").html();
  var tempItems = "";
  if (Array.isArray(data)) {
    let selectData = [];
    data.forEach(element => {
      selectData[element.ItemId] = { Id: element.ItemId, Name: element.Name };
    });
    ShowSelectWindow(selectData, function (elem) {
      let id = elem.val();
      let name = selectData[id].Name;

      $(element).val(id);
      $(element).text(id + "| "+name);
      CloseModal();
    }, id);
  }
});
}
 
function SaveWeapon(id) {
  var form = $("#" + id + "_weapon");
  var data = Serilaze(form);
  data.CageItemId = "["+data.CageItemId+"]";
  console.log(data);

  $.each(data, function (name, value) {
    console.log(name + '=' + value);
  });
  updateWeaponRequest(id, data);
}


function RemoveWeapon(id) {
  var descArea = $('#removeWeapon').html();
  descArea = fill_pattern(descArea, { Id: id });
  ShowModal(descArea, "Редактироване описания");
}


function updateWeaponRequest(id, data) {
  $.post(
    "/handler/weaponeditorhandler/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function loadWeaponList(callback) {
  $.post(
    "/handler/weaponeditorhandler/getlist",
    {
    },
    function (data) {
      console.log(data);
      var itemTemplate = $("#WeaponTemplate").html();
      var tempItems = "";
      if (Array.isArray(data))
        data.forEach(element => {
 
          var temprow = fill_pattern(itemTemplate, element);
          var divRow = $(temprow);
          SetSelect(divRow.find('.ShotTypeSelect'), element.ShotTypeSelect);
          SetSelect(divRow.find('.Type'), element.Type);
          divRow.find('.SingleFireMode').attr("checked", element.SingleFireMode);
          divRow.find('.FireModeTurn').attr("checked", element.FireModeTurn);
          divRow.find('.FireModeContinuous').attr("checked", element.FireModeContinuous);

          tempItems += divRow[0].outerHTML;
        });
      $("#WeaponList").html(tempItems);
      if(callback)
      callback(data);
    }
  );
}

function addNewWeaponRequest() {
  // addItem();
  $.post(
    "/handler/weaponeditorhandler/add/",
    {},
    function (data) { loadWeaponList(); }
  );
}
function SaveAll(){
  if(Array.isArray(editItemList))
  editItemList.forEach(id => {
      SaveWeapon(id);
  });
}

function removeWeaponRequest(id) {
  $.post(
    "/handler/weaponeditorhandler/remove/" + id,
    {},
    function (data) { ShowModal(data, "Результат удаления"); loadWeaponList(); }
  );
}
var editItemList = [];
$(document).ready(function () {
  loadWeaponList(function(){
    $('.content').find('input').change(function(){
      let elem = $(this).parent().parent();
      let itemid = elem.attr("itemid");
      console.log(elem);
      elem.css("background-color","#8eb6bea8");
      if(editItemList.indexOf(itemid) == -1){
      editItemList.push(itemid);
      }
    });

  });
});
*/

$(document).ready(function () {

  Vue.component('item-component', {
    template: '#item-component-template',
    props: ['item']
  })
  Vue.component('weapon-component', {
    template: '#weapon-component-template',
    props: ['item']
  })
  Vue.component('item-list', {
    template: '#item-list',
    props: ['items']
  })
  jsonRequestData("/handler/WeaponEditorHandler/getlist", {}, function (requestDataItemList) {


    new Vue({
      el: '#app-table',
      data: {
        newTodoText: '',
        items: requestDataItemList,

      },
      methods: {
        addNewTodo: function () {
          this.todos.push({
            id: this.nextTodoId++,
            title: this.newTodoText
          })
          this.newTodoText = ''
        }
      }
    });


  });
});