﻿using A4Engine.Modules;
using AVISNetworkCommon.Utility;
using AVISNetworkEngineTools.Modules.Payment.PaymentSystem;
using DataORM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AVISNetworkEngineTools.Modules.Payment
{
    public class PaymentModule : Module
    {
 

        // SOTA_2
        //qo1B4PsWYi3eYHBW0sw8
        //Y6AZT5vs6PcAvXO0iMB2
        public override async Task OnLoad()
        {
 
        }

        public override async Task Start(RequestContext Context)
        {
            PaymentSystemType paymentSystemType = BasePaymentSystem.PaymentSystemType;
            if (Context.Path.Length > 3)
            {
                Console.WriteLine("Payment System =" + Context.Path[3]);
                paymentSystemType = Enum.Parse<PaymentSystemType>(Context.Path[3]);
            }
            User.Data.Save("PaymentSystem", paymentSystemType);
            var paymentSystem = BasePaymentSystem.Get(paymentSystemType);
            try
            {
                if (paymentSystem.isCheckRigth && paymentSystemType != PaymentSystemType.Steam)
                {
                    if (User == null)
                    {
                        Draw("Недостаточно прав!");
                        return;
                    }
                    if (!User.GroupRight.Can("checkPayment"))
                    {
                        Draw("Недостаточно прав!");
                        return;
                    }
                }
                var uri = paymentSystem.RedirectLink(Context);
                // Context.Redirect(uri, true);
                Draw(uri);
                Console.WriteLine("Redirect success");
                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}
