﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.PromoCode
{
    public class UsePromo : Handler
    {
        public Logger Log { get; private set; }

        public override Task Before(RequestContext Context)
        {
           return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);

        }
        public static async Task<PromoCodeData> СheckPromocode(string code, A4Engine.Controllers.BaseController Controller)
        {
            code = Validation(code);
            if(code == null)
                return new PromoCodeData() { Success = false, Message = "Не валидный промокод" };
            var result = await Controller.Storage.Get.Where<PromoCodeData>(" Code={0}", code);
            if (result == null)
                return new PromoCodeData() { Success = false, Message = "Данный промокод не найден" };
            result.Success = true;
            if (result.Active == false)
            {
                result.Success = false;
                result.Message = "Данный промокод не активен";
            }
            if (result.NumberOfUses < 1 && result.Limited)
            {
                result.Success = false;
                result.Message = "Данный промокод уже использован";
            }

            return result;
        }
        private static string Validation(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;
            code = code.Trim();
            return code;
        }
        public static async Task<PromoCodeData> UsePromocode(PromoCodeData codeData, AuthData user, A4Engine.Controllers.BaseController Controller)
        {
            if (codeData == null)
                return new PromoCodeData();
            if (string.IsNullOrWhiteSpace(codeData.Code))
                return new PromoCodeData();
            codeData.NumberOfUses--;
            codeData.UseTime = DateTime.Now;
            if (!await Controller.Storage.Update.Where(codeData, " Code={0}", codeData.Code))
            {
                return new PromoCodeData();
            }
            return codeData;
        }

        public override async Task Start(RequestContext Context)
        {
            Draw("");
   
        }
    }
}
