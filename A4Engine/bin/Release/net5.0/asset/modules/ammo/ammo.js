var openEditorButton;
function SaveAmmo(id) {
  var form = $('#' + id + "_ammo");
  var data = Serilaze(form);
  console.log(data);
  updateAmmoRequest(id, data);
}

function RemovAmmo(id) {
  var descArea = $('#removeammo').html();
  descArea = fill_pattern(descArea, { Id: id });
  ShowModal(descArea, "Редактироване описания");
}


function updateAmmoList(callback) {
  getItemAmmoRequest(function (data) {

    var ammoTemplate = $('#ammoTemplate').html();
    var tempammos = "";
    var types = [
      { Name: "Mesh", Value: "Mesh" },
      { Name: "SkinnedMesh", Value: "SkinnedMesh" }
    ];
    var slots = [
      { Name: "Правая рука", Value: "RightHand" },
      { Name: "Торс", Value: "Tors" },
      { Name: "Штаны", Value: "Leg" },
      { Name: "Ноги", Value: "Foot" }
    ];

    if (Array.isArray(data)){
      data= data.reverse();
      data.forEach(element => {
        if(itemList[element.ItemId]){
          console.log(itemList[element.ItemId].Name);
        element.ItemName =  element.ItemId+"| "+ itemList[element.ItemId].Name;
        }

        element.SelectType = DrawOptions(types, element.Type);
        element.SelectSlot = DrawOptions(slots, element.Slot);
        var temprow = fill_pattern(ammoTemplate, element);
        var divRow = $(temprow);
        SetSelect(divRow.find('.ProjectileType'), element.ProjectileType);
        tempammos += divRow[0].outerHTML;

      });
    }
    $("#ammoList").html(tempammos);
    if (callback)
      callback(data);
  });

}

function updateAmmoRequest(id, data) {
  $.post(
    "/handler/ammohandler/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function getItemAmmoRequest(callback) {
  $.post(
    "/handler/ammohandler/getlist/",
    {
    },
    callback
  );
}

function SelectItem(element, id) {

  getItemsRequest(function(data){
      console.log(data);
      if (Array.isArray(data)) {
        let selectData = [];
        data.forEach(element => {
          selectData[element.ItemId] = { Id: element.ItemId, Name: element.Name };
        });
        ShowSelectWindow(selectData, function (elem) {
          let id = elem.val();
          let name = selectData[id].Name;
          $(element).val(id);
          $(element).text(id + "| "+name);
          CloseModal();
        }, id);
      }
    }
  );
}

var itemList = [];
function getItemsRequest(callback) {

  $.post(
    "/handler/ammohandler/getitems/",
    {
    },
    function (data) {
      data.forEach(element => {
        itemList[element.ItemId] = element;
      });
      console.log(data);
      if (callback)
        callback(data);
    }
  );
}

function addNewAmmoRequest() {
  // addItem();
  var th = $(this);
  $.post(
    "/handler/ammohandler/add/",
    {},
    function (data) {
      console.log(data);
      updateAmmoList();
    }
  );
}

function removeAmmoRequest(id) {
  $.post(
    "/handler/ammohandler/remove/" + id,
    {},
    function (data) { updateAmmoList(); ShowModal(data, "Результат удаления"); }
  );
}

function SaveAll(){
  if(Array.isArray(editItemList))
  editItemList.forEach(id => {
      SaveAmmo(id);
  });
}

$(document).ready(function () {
  $('#searchInput').keyup(function () {

    // Search text
    var text = $(this).val().toLowerCase();

    // Hide all content class element
    $('.content').hide();

    // Search 
    $('.content').each(function () {
      $(this).find("input").each(function () {
        if ($(this).val().toLowerCase().indexOf("" + text + "") != -1) {
          $(this).closest('.content').show();
        }
      });
    });
  });
});


var editItemList = [];
$(document).ready(function () {
  Menu();
  getItemsRequest(function () {
    updateAmmoList(function () {
      $('.content').find('input').change(function () {
        let elem = $(this).parent().parent();
        let itemid = elem.attr("itemid");
        console.log(elem);
        elem.css("background-color", "#8eb6bea8");
        if (editItemList.indexOf(itemid) == -1) {
          editItemList.push(itemid);
        }
      });

    });
  });
});
