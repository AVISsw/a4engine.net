﻿using A4Engine.Modules;
using A4Engine.Modules.ItemEditor;
using AVISNetworkCommon;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.TradeList
{
    public class TradeListHandler : Handler
    {

        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public async Task<Dictionary<long, TradeListElementData>> GetTradeList(long Id)
        {
            Dictionary<long, TradeListElementData> result = new Dictionary<long, TradeListElementData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<TradeListElementData>(" TradeListId={0}", Id))
            {
                while (data.Read)
                {
                    var element = data.GetObject<TradeListElementData>();
                    result.Add(element.Id, element);
                }
            }

            return result;
        }

        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            if (User != null)
            {
                if (User.GroupRight.Can("tradeList"))
                {
                    switch (key)
                    {
                        case "update-element":
                            var json = Context.rawParams;
                            Console.WriteLine(json);
                            List<EditElement> editElements = JsonConvert.DeserializeObject<List<EditElement>>(json);
                            Draw(JsonConvert.SerializeObject(await this.UpdateItems(editElements)));
                            break;
                        case "get":
                            Draw(JsonConvert.SerializeObject(await this.GetTradeList(Context.getParams)));
                            break;
                        case "get-item-list":
                            Draw(JsonConvert.SerializeObject(await this.GetItemList()));
                            break;
                        case "get-list":
                            Draw(JsonConvert.SerializeObject(await this.GetTradeList()));
                            break;
                        case "update-list":
                            List<EditElementList> EditElementsList = JsonConvert.DeserializeObject<List<EditElementList>>(Context.rawParams);
                            Draw(JsonConvert.SerializeObject(this.UpdateList(EditElementsList)));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }

        private async Task<List<EditElementList>> UpdateList(List<EditElementList> editElementsList)
        {
            foreach (var element in editElementsList)
            {
                switch (element.Type)
                {
                    case "add":
                        await AddItem(element.Element);
                        break;
                    case "update":
                        await UpdateItem(element.Element);
                        break;
                    case "remove":
                        await RemoveItem(element.Element);
                        break;
                }
            }
            return editElementsList;
        }
        private async Task<bool> AddItem(TradeListData itemData)
        {
            long lastId = -1;
            try
            {
                lastId = await Controller.Storage.Add.In(itemData);

            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId > -1;
        }
        private async Task<bool> UpdateItem(TradeListData itemData)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Update.Where(itemData, " Id={0}", itemData.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        private async Task<bool> RemoveItem(TradeListData itemData)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<TradeListData>(" Id={0}", itemData.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        private async Task<Dictionary<long, TradeListData>> GetTradeList()
        {
            Dictionary<long, TradeListData> result = new Dictionary<long, TradeListData>();
            using (var data = await Controller.Storage.GetAll.WhereBase<TradeListData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<TradeListData>();
                    result.Add(itemData.Id, itemData);
                }
            }
            return result;
        }

        public async Task<Dictionary<long, ItemData>> GetItemList()
        {
            Dictionary<long, ItemData> result = new Dictionary<long, ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemData>();
                    result.Add(itemData.ItemId, itemData);
                }
            }

            return result;
        }
        private Task<Dictionary<long, TradeListElementData>> GetTradeList(NameValueCollection param)
        {
            long TradeListId = 0;
            try
            {
                TradeListId = Convert.ToInt64(param.Get("TradeListId"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return GetTradeList(TradeListId);
        }

        private async Task<bool> UpdateItems(List<EditElement> elements)
        {
            bool result = false;
            foreach (var element in elements)
            {
                switch (element.Type)
                {
                    case "add":
                        result = await AddItem(element.Element);
                        break;
                    case "update":
                        result = await UpdateItem(element.Element);
                        break;
                    case "remove":
                        result = await RemoveItem(element.Element);
                        break;
                }
            }
            return result;
        }

        private async Task<bool> AddItem(TradeListElementData itemData)
        {
            long lastId = -1;
            try
            {
                lastId = await Controller.Storage.Add.In(itemData);

            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId > -1;
        }
        private async Task<bool> UpdateItem(TradeListElementData itemData)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Update.Where(itemData, " Id={0}", itemData.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        private async Task<bool> RemoveItem(TradeListElementData itemData)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<TradeListElementData>(" Id={0}", itemData.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
    }
    public class EditElement
    {
        public string Type = "";
        public TradeListElementData Element;
    }
    public class EditElementList
    {
        public string Type = "";
        public TradeListData Element;
    }
}
