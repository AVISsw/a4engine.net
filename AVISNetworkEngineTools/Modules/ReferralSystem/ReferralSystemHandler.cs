﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using AVISNetworkEngineTools.Modules.PromoCode;
using DataORM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static DataORM.Models.PromoCodeData;

namespace AVISNetworkEngineTools.Modules.ReferralSystem
{
    public class ReferralSystemHandler : Handler
    {
        public Logger Log { get; private set; }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);

        }
        public Task<long> AddReferal(ReferralSystemData referralSystemData)
        {
            return Controller.Storage.Add.In(referralSystemData);
        }


        public override async Task Start(RequestContext Context)
        {
           
            string code = Context.postParams["code"];
            var promoResult = await UsePromo.СheckPromocode(code, Controller);
            if(promoResult.Type == CodeType.Referal)
            {/*
                promoResult = await UsePromo.UsePromocode(promoResult, authData, Controller);
                await AddReferal(new ReferralSystemData() { InvitedId = authData., InviterId = promoResult.UserId, Date = DateTime.Now });
                */
            }
            Draw("");
        }
    }
}
