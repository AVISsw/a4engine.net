﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using A4Engine.Modules;
using A4Engine;
using Newtonsoft.Json.Linq;
using AVISNetworkEngineORM.Data;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.AuthVk
{
   public class SignUpVk : Handler
    {
        //https://oauth.vk.com/authorize?client_id=6979592&display=page&redirect_uri=http://91.214.70.158:8888/handler/AuthVk&scope=friends&response_type=code&v=5.103
        public override Task OnLoad()
        {
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {

            return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            string code = null;
            code = Context.getParams["code"];
            string requestState = Context.getParams["state"];
            Result result = default;

            if (!string.IsNullOrWhiteSpace(code))
            {
                if (!string.IsNullOrWhiteSpace(requestState))
                {
                    // пришел запрос от клиента

                    // получаем токен, по коду от клиента
                    try
                    {
                        using (var webClient = new WebClient())
                        {
                            string url = "https://oauth.vk.com/access_token";
                            var pars = new NameValueCollection();
                            pars.Add("v", "5.103");
                            pars.Add("client_id", Settings.Server.VKAppId);
                            pars.Add("client_secret", Settings.Server.VKAppSecretKey);
                            string host = Context.Request.Request.UserHostName;
                            pars.Add("redirect_uri", string.Format("http://{0}/page/vk_confirm", host));
                            pars.Add("code", code);

                            var response = webClient.UploadValues(url, pars);
                            string responseText = Encoding.UTF8.GetString(response);
                            JObject jToken = JObject.Parse(responseText);
                            string access_token = jToken["access_token"].Value<string>();
                            long user_id = jToken["user_id"].Value<long>();

                            JObject jState = JObject.Parse(requestState);
                            string email = jState["email"].Value<string>();
                            string login = jState["login"].Value<string>();

                            if (!string.IsNullOrWhiteSpace(access_token))
                            {
                                var vkuser = await Controller.Storage.Get.Where<VKUser>("VKID={0} OR Login={1} OR EMail={2}", user_id, login, email);
                                if (vkuser != null)
                                {
                                    result = new Result() { Success = false, ErrorCode = 1, Message = "Этот аккаунт вконтакте, логин или email уже занят" };
                                }
                                else
                                {
                                    long id = await Controller.Storage.Add.In(new VKUser() { SignUpTime = DateTime.Now, EMail = email, Login = login, VKID = user_id });
                                    if (id > -1)
                                    {
                                        result = new Result() { Success = true, ErrorCode = 0, Message = "Регистрация прошла успешно!" };
                                    }
                                    else
                                    {
                                        result = new Result() { Success = false, ErrorCode = 3, Message = "Не удалось сохранить данные" };
                                    }
                                }
                            }
                            else
                            {
                                result = new Result() { Success = false, ErrorCode = 2, Message = "Не удалось авторизироваться в вконтакте" };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = new Result() { Success = false, ErrorCode = 2, Message = ex.Message };
                        Console.WriteLine(ex.ToString());
                    }
                }
                else
                {
                    result = new Result() { Success = false, ErrorCode = 4, Message = "Не передан параметр state" };
                }
            }
            else
            {
                result = new Result() { Success = false, ErrorCode = 5, Message = "Не передан параметр code" };
            }

            Draw(JsonConvert.SerializeObject(result));
        }
 
        [DBTable("vk_users")]
        public class VKUser {
            [DBField(FieldAttrkey.AutoIncrement)]
            public long Id;
            [DBField]
            public long VKID;
            [DBField]
            public string Login;
            [DBField]
            public string EMail;
            [DBField]
            public DateTime SignUpTime;
        }

    }

  
}
