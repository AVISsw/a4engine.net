﻿using A4Engine.Modules;
using AVIS.Localization;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using AVISNetworkEngineTools.Modules.PromoCode;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataORM.Models.PromoCodeData;

namespace AVISNetworkEngineTools.Modules.SignUp
{
    public class SignUpFinish : Handler
    {

        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            Log.Info("SignUpRequest");

            SignUpResult authResult = new SignUpResult();
            SetLang(Context);

            switch (SignUpSettings.SettingsData.Type)
            {
                case SignUpSettingsData.SignUpType.Close:
                    authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format(new TranslatableString("sign_up_closed")) };
                    Draw(JsonConvert.SerializeObject(authResult));
                    return;
            }


            AuthData authData = User.Data.Get<AuthData>("AuthData");

            string promocode = User.Data.Get<string>("promocode");
            bool mailConfirm = User.Data.Get<bool>("mailConfirm");
            bool phoneConfirm = User.Data.Get<bool>("phoneConfirm");

            int signUpFinishCount = User.Data.Get<int>("signUpFinishCount");

            if (signUpFinishCount > 2)
            {
                authResult = new SignUpResult() { Success = false, ErrorCode = 3, Message = string.Format(new TranslatableString("sign_up_finish_email")) };
                DrawResult(authResult);
                return;
            }
            if (!mailConfirm)
            {
                authResult = new SignUpResult() { Success = false, ErrorCode = 3, Message = string.Format(new TranslatableString("sign_up_finish_email")) };
                DrawResult(authResult);
                return;
            }
            if (SignUpSettings.SettingsData.CheckPhone)
            {
                if (!phoneConfirm)
                {
                    authResult = new SignUpResult() { Success = false, ErrorCode = 4, Message = string.Format(new TranslatableString("sign_up_finish_phone")) };
                    DrawResult(authResult);
                    return;
                }
            }
            if (authData != null)
            {
                PromoCodeData promoResult = await UsePromo.СheckPromocode(promocode, Controller);

                switch (SignUpSettings.SettingsData.Type)
                {
                    case SignUpSettingsData.SignUpType.Limited:
 
                        break;
                    case SignUpSettingsData.SignUpType.Open:
                        if (string.IsNullOrWhiteSpace(promocode))
                        {
                            promoResult.Success = true;
                        }
                        break;
                }
                Console.WriteLine(JsonConvert.SerializeObject(promoResult));
                if(promoResult.Success == false)
                {
                    authResult = new SignUpResult() { Success = promoResult.Success, ErrorCode = 15, Message = promoResult.Message };
                    DrawResult(authResult);
                    return;
                }
                bool isLimited = SignUpSettings.SettingsData.Type == SignUpSettingsData.SignUpType.Limited;
                bool isOpen = SignUpSettings.SettingsData.Type == SignUpSettingsData.SignUpType.Open;
                if ( ((promoResult.Type == CodeType.Invite && isLimited) || isOpen) && promoResult.Success)
                {
                    //User.Data.Clear();
                    signUpFinishCount++;
                    authData.Promo = promoResult.Code;
                    User.Data.Save("signUpFinishCount", signUpFinishCount);
                    long lastId = await SignUpUser(authData);
                    authData.Id = lastId;
                    await AddGroup(lastId, 1);

                    if (promoResult.Type == CodeType.Invite && promoResult.Success)
                    {
                        authResult.Success = true;
                        promoResult = await UsePromo.UsePromocode(promoResult, authData, Controller);
                        authResult.Message = new TranslatableString("sign_up_next_notify");
                    }
                }
                else
                {
                    authResult.Success = false;
                    authResult.ErrorCode = 4;
                    authResult.Message = new TranslatableString("sign_up_finish_promocode_err");
                }
            }
            else
            {
                authResult.Success = false;
                authResult.ErrorCode = 1;
                authResult.Message = " Ошибка при авторизации";
            }

            DrawResult(authResult);
        }
        void DrawResult(SignUpResult response)
        {
            Draw(JsonConvert.SerializeObject(response));
        }

        public Task<long> SignUpUser(AuthData authData)
        {
            authData.RegDate = DateTime.Now;
            return Controller.Storage.Add.In(authData);
        }

        public Task<long> AddGroup(long id, int groupId)
        {
            return Controller.Storage.ExecuteScalar("INSERT INTO user_groups (UserId, GroupId) VALUES ( {0}, {1});", id, groupId);
        }
        public struct SignUpResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}
