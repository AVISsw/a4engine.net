import { EditorCanvas } from "./editorcanvas.js";
import { RootNode } from "./node.js";

class RightMenuProcessor {
  constructor() {
    this.selectedConnection = -1;
    this.connectionSelector = document.getElementById("connectionSelector");

    this.connectionSelector.addEventListener(
      "mouseover",
      window.OnConnectionSelected
    );
  }
  DisplayData(node) {
    [].slice.call(document.getElementsByClassName("right_column")).forEach(function(item) {
      item.classList.remove("right_column");
      item.classList.add("right_column_select");
    });
 
    window.editorCanvas.SetActualScreenSize();
    window.editorCanvas.Draw();
    document.getElementById("node_id_label").innerHTML = "ID: " + node.id;
    document.getElementById("node_phrase_code").value = node.data.text;
    if (node instanceof RootNode) {
      document.getElementById("root_data").style.display = "inline";
      document.getElementById("node_data").style.display = "none";
      document.getElementById("dialogue_name_input").value = node.data.name;

      //Здесь надо вывести все данные о коннекшонах и прочем
    } else {
      document.getElementById("node_data").style.display = "inline";
      document.getElementById("root_data").style.display = "none";
      document.getElementById("type-select").value = node.data.type;
      document.getElementById("check_node_display").checked = node.data.display;

      let conditionsLayout = document.getElementById("conditionsLayout");
      conditionsLayout.innerHTML = "";
      let counter = 0;
      //CONDITIONS
      node.data.conditions.forEach((condition) => {
        let resultBlock ='<div class="condition_block">';
        resultBlock +=
          '<p><condition_label>name:  (<a href="#" onclick="return DeleteConditionById('+counter+')">del</a>)</condition_label></p>'+
          '<p><input type="text" class="textField" id="condition_name_' +counter+'" onchange="OnConditionNameChanged('+counter+')" value="'+condition.name+'"></p>';
          resultBlock +=
          '<p><condition_label>val:</condition_label></p>'+
          '<p><input type="text" class="textField" id="condition_val_'+counter+'" onchange="OnConditionValChanged('+counter+')" value="'+condition.val+'"></p>';
          resultBlock +="</div>";
        conditionsLayout.innerHTML += resultBlock;
        counter++;
      });
      counter = 0;
      //EVENTS
      let eventsLayout = document.getElementById("eventsLayout");
      eventsLayout.innerHTML = "";
      node.data.events.forEach((event) => {
        let resultBlock ='<div class="event_block">';
        resultBlock +=
          '<p><condition_label>name:  (<a href="#" onclick="return DeleteEventById(' +
          counter +
          ')">del</a>)</condition_label> </p><p><input type="text" class="textField" id="event_name_' +
          counter +
          '" onchange="OnEventNameChanged(' +
          counter +
          ')" value="' +
          event.name +
          '"></p>';
          resultBlock +=
          '<p><condition_label>val:</condition_label></p><p><input type="text" class="textField" id="event_val_' +
          counter +
          '" onchange="OnEventValChanged(' +
          counter +
          ')" value="' +
          event.val +
          '"></p>';
        counter++;
        eventsLayout.innerHTML += resultBlock;
      });
    }
  }
  WriteConnectionData(connections) {
    this.connectionSelector.innerHTML = "";
    if (connections.length < 1) {
      document.getElementById("connectionsDisplay").style.display = "none";
      this.connectionSelector.innerHTML += "empty";
    } else {
      document.getElementById("connectionsDisplay").style.display = "inline";
    }
    connections.forEach((connection) => {
      this.connectionSelector.innerHTML +=
        '<li id="' + connection.id + '"> ID ' +connection.id + '  ( <a href="#" onclick="return OnConnectionDeleting('+connection.id+')" id="'+connection.id+' ">x</a> )   ( <a href="#" onclick="return OnConnectionUp('+connection.id+')" id="'+connection.id+' ">UP</a> )  ( <a href="#" onclick="return OnConnectionDown('+connection.id+')" id="'+connection.id+' ">DOWN</a> )</li>';
    });
  }
  OnDeselect() {
    
    this.selectedConnection = -1;
    this.connectionSelector.innerHTML = "";
    [].slice.call(document.getElementsByClassName("right_column_select")).forEach(function(item) {
      item.classList.add("right_column");
      item.classList.remove("right_column_select");
    });
    window.editorCanvas.SetActualScreenSize();
    window.editorCanvas.Draw();
    document.getElementById("node_data").style.display = "none";
    document.getElementById("root_data").style.display = "none";
  }
}

export { RightMenuProcessor };
