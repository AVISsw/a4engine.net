﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.UserRights
{
    public class UserGroupHandler : Handler
    {

        Logger Log;
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }
        private async Task<List<UserGroups>> GetUserGroups()
        {
            List<UserGroups> result = new List<UserGroups>();

            using (var data = await Controller.Storage.GetAll.WhereBase<UserGroups>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<UserGroups>());
                }
            }
            return result;
        }
 
        /*
         SELECT * FROM accounts
INNER JOIN user_groups ON accounts.Id = user_groups.UserId WHERE user_groups.GroupId = 2;
             */
        private async Task<List<UserContiner>> GetUsersByGroup(long group, long limit, long offset)
        {
            List<UserContiner> result = new List<UserContiner>();

            using (var data = await Controller.Storage.Query("SELECT * FROM accounts INNER JOIN user_groups ON accounts.Id = user_groups.UserId WHERE user_groups.GroupId = {0} LIMIT {1}, {2} ;", group, offset,limit))
            {
                while (data.Read)
                {
                    var userContiner = new UserContiner();
                    userContiner.AuthData = data.GetObject<AuthData>();
                    userContiner.AuthData.Password = "";
                    userContiner.AuthData.Session = "";
                    result.Add(userContiner);
                }
            }
            return result;
        }
        private async Task<bool> AddUsersToGroup(long userId, long groupId)
        {
            var userGroups = new UserGroups() { UserId = userId, GroupId = groupId };
            userGroups.Id = await Controller.Storage.Add.In(userGroups);
            return userGroups.Id != -1;
        }
        private Task<bool> RmUsersFromGroup(long userId)
        {
            return Controller.Storage.Remove.Where<UserGroups>(" UserId={0}", userId);
        }
 
        public class UserContiner
        {
            public AuthData AuthData;
            public List<UserGroups> Groups = new List<UserGroups>();
        }
        async Task<long> GetItemCount()
        {
            using (var data = await Controller.Storage.Query("SELECT COUNT(*) FROM accounts"))
            {
                if (data.Read)
                {
                    return data.GetInt64(0);
                }
            }
            return 0;
        }
        const int limit = 1000;
        public override async Task Start(RequestContext Context)
        {
            try
            {
                string key = Context.Path[3].ToString();
                Console.WriteLine("---" + key);
                RequestData requestData = JsonConvert.DeserializeObject<RequestData>(Context.rawParams);

                if (User.GroupRight.Can("editScene"))
                {
                    switch (key)
                    {
                        case "get-users-by-group":
                            long offset = requestData.Page * limit;
                            Console.WriteLine("offset " + offset);
                            Draw(JsonConvert.SerializeObject(await this.GetUsersByGroup(requestData.GroupId, limit, offset)));
                            break;
                        case "add-users-to-group":
                            Draw(JsonConvert.SerializeObject(await this.AddUsersToGroup(requestData.UserId, requestData.GroupId)));
                            break;
                        case "rem-users-from-group":
                            Draw(JsonConvert.SerializeObject(await this.RmUsersFromGroup(requestData.UserId)));
                            break;
                        case "pages":
                            long elemntCount = await GetItemCount();
                            long pageCount = (long)Math.Ceiling((double)elemntCount / (double)limit);
                            Draw(JsonConvert.SerializeObject(new Dictionary<string, long> { { "PageCount", pageCount } }));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Draw("<b>Доступ запрещен!</b>");
        }

        [Serializable]
        public class RequestData
        {
            public long GroupId = -1;
            public long UserId = -1;
            public long Page = -1;
        }

    }
}
