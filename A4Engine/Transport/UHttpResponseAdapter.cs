﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uhttpsharp;
using uhttpsharp.Headers;

namespace A4Engine.Transport
{
    public class UHttpResponseAdapter : IHttpResponse
    {
        public string ContentType
        {
            get
            {
                return Headers["Content-Type"];
            }
            set
            {
                Headers["Content-Type"] = value;
            }
        }
        public long ContentLength64
        {
            get
            {
                string contentType = Headers["Content-Length"];
                return Convert.ToInt64(contentType);
            }
            set
            {
                Headers["Content-Length"] = value.ToString();
            }
        }

        public bool SendChunked { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string RedirectLocation { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Version ProtocolVersion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Stream OutputStream { get; private set; }

        public bool KeepAlive { get; set; }
        public NameValueCollection Headers { get; set; } = new NameValueCollection();
 
        public HttpResponseCode StatusCode { get; set; }
        public string StatusDescription { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Encoding ContentEncoding
        {
            get
            {
                return Encoding.GetEncoding(Headers["Content-Encoding"]);
            }
            set
            {
                Headers["Content-Encoding"] = value.ToString();
            }
        }

        public void Abort()
        {
            throw new NotImplementedException();
        }

        public void AddHeader(string name, string value)
        {
            throw new NotImplementedException();
        }

        public void AppendCookie(Cookie cookie)
        {
            throw new NotImplementedException();
        }

        public void AppendHeader(string name, string value)
        {
            throw new NotImplementedException();
        }

        public void Close(byte[] responseEntity, bool willBlock)
        {
            throw new NotImplementedException();
        }
   

        public void Close()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
          
        }
 
        public Task GetResponse(RequestContext requestContext)
        {
 
            _headers.Add("Date", DateTime.UtcNow.ToString("R"));
            _headers.Add("Connection", KeepAlive ? "Keep-Alive" : "Close");
            _headers.Add("Content-Type", ContentType);
            _headers.Add("Content-Length", OutputStream.Length.ToString(CultureInfo.InvariantCulture));
            _headers.Add("Server", "A4Engine");
            _headers.Add("X-Powered-By", "A4Engine");
            _headers.Add("Referrer-Policy", "no-referrer-when-downgrade");
            _headers.Add("Referer", requestContext.ServerHost);
            this.uHttpContext.Response = new HttpResponse((uhttpsharp.HttpResponseCode)((int)StatusCode), ContentType, OutputStream, KeepAlive, _headers);
            uHttpContext.Cookies = httpContext.Cookies.GetUpdates();
            return Task.CompletedTask;
        }

        public void Redirect(string url)
        {
            _headers["Location"]  = url;
            StatusCode = HttpResponseCode.TemporaryRedirect;
        }

        public void SetCookie(Cookie cookie)
        {
            throw new NotImplementedException();
        }
        NameValueCollection _headers;

        uhttpsharp.IHttpContext uHttpContext;
        IHttpContext httpContext;
        public UHttpResponseAdapter(IHttpContext context, uhttpsharp.IHttpContext uHttpContext, uhttpsharp.IHttpContext uhttpContext)
        {
            _headers = new NameValueCollection();
            httpContext = context;
           
            this.uHttpContext = uHttpContext;
            OutputStream = new MemoryStream();
        }
    }
}
