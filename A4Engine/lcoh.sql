/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : lcoh

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-14 17:26:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `Session` varchar(255) DEFAULT NULL,
  `RegIp` varchar(255) DEFAULT NULL,
  `LastIp` varchar(255) DEFAULT NULL,
  `RegMac` varchar(255) DEFAULT NULL,
  `LastMac` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', 'admin', 'f626cac9d093515f0cb0fc431c78e3c1', 'mail', 'Anton 386', '06399ffd03b6b83b7c5c5bcb0ac4d986', '', '', '', '');
INSERT INTO `accounts` VALUES ('20', 'qwerty', '7815696ecbf1c96e6894b779456d330e', 'ytykuykuy', 'jtyjty', '06399ffd03b6b83b7c5c5bcb0ac4d986', ' ', ' ', ' ', ' ');
INSERT INTO `accounts` VALUES ('22', 'qwerty2', 'f626cac9d093515f0cb0fc431c78e3c1', 'strelokzi@mail.ru', 'Иванов Иван Иванович', null, ' ', ' ', ' ', ' ');
INSERT INTO `accounts` VALUES ('23', 'asdfg3', 'f626cac9d093515f0cb0fc431c78e3c1', 'teefgergerg', 'fregtrgh', '37693cfc748049e45d87b8c7d8b9aacd', ' ', ' ', ' ', ' ');

-- ----------------------------
-- Table structure for ammo
-- ----------------------------
DROP TABLE IF EXISTS `ammo`;
CREATE TABLE `ammo` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NOT NULL,
  `Mesh` varchar(255) DEFAULT NULL,
  `PartsCount` int(11) DEFAULT '1',
  `PowerMin` float DEFAULT '1',
  `PowerMax` float DEFAULT '2',
  `ExtraRechargeTime` float DEFAULT '1',
  `Diameter` float DEFAULT '0.1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ammo
-- ----------------------------
INSERT INTO `ammo` VALUES ('1', '9', 'Bullet', '7', '-5', '-7', '1', '0.1');

-- ----------------------------
-- Table structure for cages
-- ----------------------------
DROP TABLE IF EXISTS `cages`;
CREATE TABLE `cages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) DEFAULT NULL,
  `Mesh` varchar(255) DEFAULT NULL,
  `MaxCount` int(255) DEFAULT '1',
  `AmmoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cages
-- ----------------------------
INSERT INTO `cages` VALUES ('1', '8', ' ', '12', '1');

-- ----------------------------
-- Table structure for chance_spawn_items
-- ----------------------------
DROP TABLE IF EXISTS `chance_spawn_items`;
CREATE TABLE `chance_spawn_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemId` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  `chance` float DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chance_spawn_items
-- ----------------------------
INSERT INTO `chance_spawn_items` VALUES ('1', '1', '1', '3', '0.3');
INSERT INTO `chance_spawn_items` VALUES ('2', '2', '1', '1', '0.3');
INSERT INTO `chance_spawn_items` VALUES ('3', '3', '1', '3', '0.3');

-- ----------------------------
-- Table structure for characters
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeId` int(3) DEFAULT '0',
  `Name` varchar(255) NOT NULL DEFAULT '',
  `TransformId` int(11) DEFAULT '0',
  `CharacterId` int(11) DEFAULT NULL,
  `Account` int(255) NOT NULL,
  `Exp` int(255) DEFAULT '0',
  `Money` int(255) DEFAULT '0',
  `Skin` varchar(255) NOT NULL DEFAULT '',
  `Lvl` int(255) DEFAULT '1',
  `Desc` varchar(255) DEFAULT '',
  `Walk` float DEFAULT '0',
  `BagId` int(11) DEFAULT NULL,
  `EquipmentId` int(11) DEFAULT NULL,
  `Heal` float(11,0) DEFAULT '100',
  `Stamina` float(255,0) DEFAULT NULL,
  `Fatigue` float(255,0) DEFAULT NULL,
  `Hunger` float(255,0) DEFAULT NULL,
  `Thirst` float(255,0) DEFAULT NULL,
  `Radiation` float(255,0) DEFAULT NULL,
  `Mind` float(255,0) DEFAULT NULL,
  `Bleeding` float(255,0) DEFAULT NULL,
  `Temperature` float(255,0) DEFAULT NULL,
  `Run` float(255,0) DEFAULT NULL,
  `Sprint` float(255,0) DEFAULT NULL,
  `PocketId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `transform_id` (`TransformId`),
  CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`TransformId`) REFERENCES `transform` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES ('1', '1', 'Boby', '1', '0', '1', '355', '100', 'Male_01', '3', 'Тут описание', '3', '1', '2', '25', '100', '100', '100', '100', '0', '0', '0', '37', '2', '4', '5');
INSERT INTO `characters` VALUES ('2', '1', 'Jary', '1', '0', '1', '3', '3', 'Male_01', '1', 'fff', '3', '1', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('3', '1', 'Mary', '1', '0', '1', '3', '3', 'Male_01', '2', 'ffff', '3', '1', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('4', '1', 'Lala', '1', '0', '20', '2', '3333', 'Male_01', '6', '#fffffff', '3', '1', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('5', '2', 'Zveroboy', '1', '0', '-1', '355', '3000', 'Male_01', '32', 'Первый нпс', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('6', '2', 'Test', '1', '0', '-1', '222', '222', 'Male_01', '3', 'frfrfrrg', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('7', '2', 'fff', '1', '0', '-1', '3', '3', 'Male_01', '3', 'fefeff', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('8', '2', 'fff', '1', '0', '-1', '3', '3', 'Male_01', '3', 'fefeff', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('9', '2', 'fff', '1', '0', '-1', '3', '3', 'Male_01', '3', 'fefeff', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('10', '3', 'Bot', '1', '0', '-1', '3', '100', 'Male_01', '1', '', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `characters` VALUES ('11', '2', 'Bot', '1', '0', '-1', '3', '100', 'Wolf', '1', '', '4', '3', '2', '100', null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for combine
-- ----------------------------
DROP TABLE IF EXISTS `combine`;
CREATE TABLE `combine` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Decription` varchar(255) DEFAULT NULL,
  `Recept` varchar(255) DEFAULT NULL,
  `Result` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of combine
-- ----------------------------
INSERT INTO `combine` VALUES ('3', 'Открыть консерву', 'Открыть консерву', '8,9', '5');

-- ----------------------------
-- Table structure for customization
-- ----------------------------
DROP TABLE IF EXISTS `customization`;
CREATE TABLE `customization` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) DEFAULT NULL,
  `Mesh` varchar(255) DEFAULT NULL,
  `Material` varchar(255) DEFAULT NULL,
  `Slot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customization
-- ----------------------------
INSERT INTO `customization` VALUES ('1', '', null, null, 'RightHand');
INSERT INTO `customization` VALUES ('2', null, null, null, 'RightHand');
INSERT INTO `customization` VALUES ('3', 'Mesh', 'Weapon/Pistols/Handgun_M1911A_Black', null, 'RightHand');
INSERT INTO `customization` VALUES ('4', 'SkinnedMesh', 'Сlothes/Tors/Kurtka_01', null, 'Tors');
INSERT INTO `customization` VALUES ('5', 'SkinnedMesh', 'Сlothes/Legs/Legs_01', null, 'Leg');
INSERT INTO `customization` VALUES ('6', 'SkinnedMesh', 'Сlothes/Foot/Foot_01', null, 'Foot');
INSERT INTO `customization` VALUES ('7', 'SkinnedMesh', 'Сlothes/Tors/Tors_02', null, 'Tors');

-- ----------------------------
-- Table structure for dialog_category
-- ----------------------------
DROP TABLE IF EXISTS `dialog_category`;
CREATE TABLE `dialog_category` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT 'Новая категория',
  `path` varchar(2048) NOT NULL DEFAULT '/',
  `level` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_category
-- ----------------------------
INSERT INTO `dialog_category` VALUES ('1', 'story', '/story', '0');

-- ----------------------------
-- Table structure for dialog_events
-- ----------------------------
DROP TABLE IF EXISTS `dialog_events`;
CREATE TABLE `dialog_events` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_events
-- ----------------------------
INSERT INTO `dialog_events` VALUES ('1', 'dialog_close');

-- ----------------------------
-- Table structure for dialog_filters
-- ----------------------------
DROP TABLE IF EXISTS `dialog_filters`;
CREATE TABLE `dialog_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_filters
-- ----------------------------
INSERT INTO `dialog_filters` VALUES ('1', 'Возраст');
INSERT INTO `dialog_filters` VALUES ('2', 'Имя');
INSERT INTO `dialog_filters` VALUES ('3', 'Местный');

-- ----------------------------
-- Table structure for dialog_filters_link
-- ----------------------------
DROP TABLE IF EXISTS `dialog_filters_link`;
CREATE TABLE `dialog_filters_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `filter_name` varchar(50) NOT NULL,
  `filter_value` varchar(50) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_filters_link
-- ----------------------------
INSERT INTO `dialog_filters_link` VALUES ('1', '1', 'Возраст', '65', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('2', '1', 'Имя', 'Боб', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('3', '1', 'Местный', 'Да', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('4', '1', 'Возраст', '65', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('5', '1', 'Имя', 'Боб', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('6', '1', 'Местный', 'Да', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('7', '2', 'Возраст', '65', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('8', '3', 'Respect', '1', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('9', '2', 'Respect', '1', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('10', '2', 'Возраст', '65', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('11', '4', 'Who', '1', 'npc');
INSERT INTO `dialog_filters_link` VALUES ('12', '2', 'Who', '1', 'pl');
INSERT INTO `dialog_filters_link` VALUES ('13', '3', 'Respect', '1', 'pl');

-- ----------------------------
-- Table structure for dialog_npc_answers
-- ----------------------------
DROP TABLE IF EXISTS `dialog_npc_answers`;
CREATE TABLE `dialog_npc_answers` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_npc_answers
-- ----------------------------
INSERT INTO `dialog_npc_answers` VALUES ('1', 'story', 'Привет меня зовут {name}, мне {age} лет, я живу тут всю свою жизнь');
INSERT INTO `dialog_npc_answers` VALUES ('2', 'story', 'Привет, я {name} мне  {age} лет');
INSERT INTO `dialog_npc_answers` VALUES ('3', 'story', 'Ой спасибо чувак, я думаю ты тоже очень классны!');
INSERT INTO `dialog_npc_answers` VALUES ('4', 'story', 'Сижу пержу, а вообще у нас тут тихо ; )');

-- ----------------------------
-- Table structure for dialog_player_answers
-- ----------------------------
DROP TABLE IF EXISTS `dialog_player_answers`;
CREATE TABLE `dialog_player_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT '' COMMENT '...',
  `event` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dialog_player_answers
-- ----------------------------
INSERT INTO `dialog_player_answers` VALUES ('1', 'story', 'Ты очень крутой чувак', 'say', '{key:\"Respect\",value:\"1\"}');
INSERT INTO `dialog_player_answers` VALUES ('2', 'story', 'Пока...', 'bye', ' ');
INSERT INTO `dialog_player_answers` VALUES ('3', 'story', 'Как тут у вас?', 'say', '{key:\"Who\",value:\"1\"}');

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('-1', 'Banned');
INSERT INTO `groups` VALUES ('1', 'Users');
INSERT INTO `groups` VALUES ('2', 'Admins');

-- ----------------------------
-- Table structure for group_rights
-- ----------------------------
DROP TABLE IF EXISTS `group_rights`;
CREATE TABLE `group_rights` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `GroupId` int(11) DEFAULT NULL,
  `RightId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group_rights
-- ----------------------------
INSERT INTO `group_rights` VALUES ('2', '1', '2');
INSERT INTO `group_rights` VALUES ('3', '2', '3');
INSERT INTO `group_rights` VALUES ('4', '2', '4');
INSERT INTO `group_rights` VALUES ('5', '2', '5');
INSERT INTO `group_rights` VALUES ('6', '2', '6');
INSERT INTO `group_rights` VALUES ('7', '2', '7');

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `InstanceId` int(11) NOT NULL AUTO_INCREMENT,
  `ContinerId` int(11) DEFAULT NULL,
  `ItemId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT '',
  `Count` int(11) DEFAULT '1',
  `Value` float DEFAULT '0',
  `IntType` int(11) DEFAULT '0',
  `TypeOfUse` int(1) DEFAULT '0',
  `Slot` int(11) DEFAULT '0',
  `Desc` varchar(255) DEFAULT '',
  `Key` varchar(255) DEFAULT NULL,
  `CustomizationId` int(11) DEFAULT NULL,
  `IsInfinite` tinyint(1) DEFAULT '0',
  `SizeX` int(3) DEFAULT '1',
  `SizeY` int(3) DEFAULT '1',
  `SlotX` int(3) DEFAULT '0',
  `SlotY` int(3) DEFAULT '0',
  `Wear` float DEFAULT '100',
  `Strength` float DEFAULT '0',
  `Up` tinyint(1) DEFAULT '0',
  `Premium` tinyint(1) DEFAULT '0',
  `MaxCount` int(11) DEFAULT '1',
  `Weight` float DEFAULT '1',
  `ContinerType` int(11) DEFAULT '0',
  `ExpiryDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `CanSpoil` tinyint(1) DEFAULT '0',
  `ExpiredItemId` int(11) DEFAULT NULL,
  PRIMARY KEY (`InstanceId`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('0', '1', '8', 'Магазин пистолетный', '0', '0', '3', '2', '0', 'Магазин пистолетный', 'ak47', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '8', '0', '0', '2019-07-11 13:51:18', '0', null);
INSERT INTO `items` VALUES ('1', '1', '1', 'Хлеб', '1', '20', '0', '0', '0', 'Это хлеб', 'buter_01', null, '0', '1', '1', '0', '0', '0', '1', '0', '0', '1', '0.5', '0', '2019-07-10 17:18:22', '1', '14');
INSERT INTO `items` VALUES ('2', '1', '2', 'Колбаса', '1', '66', '0', '0', '0', 'Кусок старой колбасы', 'buter_01', null, '0', '1', '1', '0', '1', '0.55', '1', '0', '0', '1', '0.5', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('3', '1', '3', 'Бронижелет', '1', '1', '1', '1', '1', 'Это бронижелет', 'mara_01', '4', '1', '2', '3', '5', '2', '1', '0.11', '0', '0', '1', '7', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('4', '2', '4', 'Тестовое оружие', '1', '20', '2', '1', '2', 'Это тестовое оружие', 'makarov_01', null, '1', '3', '2', '0', '3', '0.11', '1', '1', '0', '1', '2', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('5', '2', '5', 'Штаны', '1', '12', '1', '1', '0', 'Штаны', 'mara_01', '5', '1', '2', '3', '0', '4', '0', '0.55', '0', '1', '1', '3', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('6', '1', '6', 'Ботинки', '1', '10', '1', '1', '3', 'Ботинки', 'mara_01', '6', '1', '2', '2', '2', '0', '0.55', '0.33', '1', '1', '1', '1', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('7', '1', '7', 'Куртка', '1', '30', '1', '1', '1', 'Куртка', 'mara_01', '7', '1', '2', '3', '8', '5', '0.33', '1', '0', '0', '1', '2', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('11', '1', '10', 'Аптечка', '3', '0', '0', '0', '0', 'Обычная аптечка', 'ai_01', null, '0', '1', '1', '3', '7', '100', '0', '0', '0', '3', '1', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('12', '1', '11', 'Оптический прицел x2', '1', '0', '4', '0', '0', 'Оптический прицел x2', 'makarov_01', null, '0', '1', '1', '4', '4', '100', '0', '0', '0', '1', '0', '0', '2019-07-10 13:40:59', '0', null);
INSERT INTO `items` VALUES ('13', '1', '9', 'Патроны для пистолета', '200', '0', '5', '0', '0', 'Патроны для пистолета', '5,45x39_bp', null, '0', '1', '1', '0', '10', '100', '0', '1', '1', '200', '1', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('52', '1', '12', 'Ремонтный набор', '10', '0', '6', '-1', '0', 'Набор для ремонта предметов', 'makarov_01', null, '0', '1', '1', '2', '11', '100', '0', '0', '0', '10', '1', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `items` VALUES ('53', '1', '13', 'Глушитель', '1', '0', '7', '-1', '0', 'Глушитель', 'muzzle_nozzle', null, '0', '1', '1', '3', '3', '100', '0', '0', '0', '1', '0', '0', '2019-07-10 13:41:04', '0', null);
INSERT INTO `items` VALUES ('68', '0', '9', 'Патроны для пистолета', '8', '0', '5', '0', '0', 'Патроны для пистолета', '5,45x39_bp', '0', '0', '1', '1', '0', '0', '100', '0', '1', '1', '8', '1', '1', '2019-07-10 11:11:48', '0', '0');

-- ----------------------------
-- Table structure for item_continers
-- ----------------------------
DROP TABLE IF EXISTS `item_continers`;
CREATE TABLE `item_continers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(55) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `TileX` int(11) DEFAULT '30',
  `TileY` int(11) DEFAULT NULL,
  `Weight` float DEFAULT '-1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item_continers
-- ----------------------------
INSERT INTO `item_continers` VALUES ('1', 'Инвентарь', '0', '10', '60', '-1');
INSERT INTO `item_continers` VALUES ('2', 'Экипировка', '1', '10', '1', '-1');
INSERT INTO `item_continers` VALUES ('3', 'Лут', '2', '5', '5', '-1');
INSERT INTO `item_continers` VALUES ('4', 'Рандом лут', '2', '5', '6', '-1');
INSERT INTO `item_continers` VALUES ('5', 'Карманы', '3', '10', '1', '-1');

-- ----------------------------
-- Table structure for item_effects
-- ----------------------------
DROP TABLE IF EXISTS `item_effects`;
CREATE TABLE `item_effects` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) DEFAULT NULL,
  `PropertyName` varchar(255) DEFAULT NULL,
  `ValueName` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Key` varchar(255) DEFAULT NULL,
  `Time` float DEFAULT '-1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item_effects
-- ----------------------------
INSERT INTO `item_effects` VALUES ('1', '7', 'bulletproof', 'MaxValue', '60', 'tors', '-1');
INSERT INTO `item_effects` VALUES ('2', '7', 'bulletproof', 'Value', '60', 'tors', '-1');
INSERT INTO `item_effects` VALUES ('3', '2', 'hunger', 'Per', '-5', 'buterbrod', '20');
INSERT INTO `item_effects` VALUES ('4', '2', 'hunger', 'PerTime', '10', 'buterbrod', '20');
INSERT INTO `item_effects` VALUES ('5', '10', 'heal', 'Per', '-4', 'aptechka', '3');
INSERT INTO `item_effects` VALUES ('6', '10', 'heal', 'PerTime', '20', 'aptechka', '3');

-- ----------------------------
-- Table structure for item_template
-- ----------------------------
DROP TABLE IF EXISTS `item_template`;
CREATE TABLE `item_template` (
  `InstanceId` int(11) DEFAULT NULL,
  `ContinerId` int(11) DEFAULT NULL,
  `ItemId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT '',
  `Count` int(11) DEFAULT '1',
  `Value` float DEFAULT '0',
  `IntType` int(11) DEFAULT '0',
  `TypeOfUse` int(1) DEFAULT '0',
  `Slot` int(11) DEFAULT '0',
  `Desc` varchar(255) DEFAULT '',
  `Key` varchar(255) DEFAULT NULL,
  `CustomizationId` int(11) DEFAULT NULL,
  `IsInfinite` tinyint(1) DEFAULT '0',
  `SizeX` int(3) DEFAULT '1',
  `SizeY` int(3) DEFAULT '1',
  `SlotX` int(3) DEFAULT '0',
  `SlotY` int(3) DEFAULT '0',
  `Wear` float DEFAULT '100',
  `Strength` float DEFAULT '0',
  `Up` tinyint(1) DEFAULT '0',
  `Premium` tinyint(1) DEFAULT '0',
  `MaxCount` int(11) DEFAULT '1',
  `Weight` float DEFAULT '1',
  `ContinerType` int(11) DEFAULT '0',
  `ExpiryDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `CanSpoil` tinyint(1) DEFAULT '0',
  `ExpiredItemId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item_template
-- ----------------------------
INSERT INTO `item_template` VALUES ('0', '0', '1', 'Хлеб', '1', '20', '0', '0', '0', 'Это хлеб, просто хлеб', 'buter_01', '0', '0', '1', '1', '0', '0', '0', '1', '1', '0', '1', '0', '0', '2019-07-08 00:58:47', '1', '14');
INSERT INTO `item_template` VALUES ('2', '1', '2', 'Колбаса', '1', '66', '0', '0', '0', 'Кусок старой колбасы', 'buter_01', null, '0', '1', '1', '0', '1', '0.55', '1', '0', '0', '1', '0.5', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('3', '1', '3', 'Бронижелет', '1', '1', '1', '1', '1', 'Это бронижелет', 'mara_01', '4', '1', '2', '3', '5', '2', '1', '0.11', '0', '0', '1', '7', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('4', '2', '4', 'Тестовое оружие', '1', '20', '2', '1', '2', 'Это тестовое оружие', 'makarov_01', null, '1', '3', '2', '0', '3', '0.11', '1', '1', '0', '1', '2', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('5', '2', '5', 'Штаны', '1', '12', '1', '1', '0', 'Штаны', 'mara_01', '5', '1', '2', '3', '0', '4', '0', '0.55', '0', '1', '1', '3', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('6', '1', '6', 'Ботинки', '1', '10', '1', '1', '3', 'Ботинки', 'mara_01', '6', '1', '2', '2', '2', '0', '0.55', '0.33', '1', '1', '1', '1', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('7', '1', '7', 'Куртка', '1', '30', '1', '1', '1', 'Куртка', 'mara_01', '7', '1', '2', '3', '8', '5', '0.33', '1', '0', '0', '1', '2', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('9', '5', '8', 'Магазин пистолетный', '12', '8', '3', '2', '0', 'Магазин пистолетный', 'ak47', null, '0', '1', '1', '3', '5', '0', '1', '0', '0', '12', '0.5', '0', '2019-07-10 11:11:48', '0', null);
INSERT INTO `item_template` VALUES ('13', '0', '9', 'Патроны для пистолета', '200', '0', '5', '0', '0', 'Патроны для пистолета', '5,45x39_bp', '0', '0', '1', '1', '0', '0', '1', '0', '1', '1', '200', '1', '0', '2019-07-10 06:11:48', '0', '0');
INSERT INTO `item_template` VALUES ('11', '0', '10', 'Аптечка', '3', '0', '0', '0', '0', 'Обычная аптечка', 'ai_01', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '3', '1', '0', '2019-07-10 06:11:48', '0', '0');
INSERT INTO `item_template` VALUES ('12', '0', '11', 'Оптический прицел x2', '1', '0', '4', '0', '0', 'Оптический прицел x2', 'makarov_01', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', '2019-07-10 06:11:48', '0', '0');
INSERT INTO `item_template` VALUES ('52', '0', '12', 'Ремонтный набор', '10', '0', '6', '-1', '0', 'Набор для ремонта предметов', 'makarov_01', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '10', '1', '0', '2019-07-10 06:11:48', '0', '0');
INSERT INTO `item_template` VALUES ('53', '0', '13', 'Глушитель', '1', '0', '7', '-1', '0', 'Глушитель', 'muzzle_nozzle', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', '2019-07-10 06:11:49', '0', '0');
INSERT INTO `item_template` VALUES ('0', '0', '14', 'Испорченный хлеб', '1', '0', '0', '0', '0', 'Испорченный хлеб', 'buter_01_old', '0', '0', '1', '1', '0', '0', '1', '0', '1', '1', '1', '1', '0', '2019-07-08 06:20:38', '0', '0');

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) NOT NULL,
  `linkid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of links
-- ----------------------------
INSERT INTO `links` VALUES ('1', 'menu1', 'Главная', '/main', 'link.html', null);
INSERT INTO `links` VALUES ('5', 'menu1', 'Авторизироваться', '#login_dialog', 'link.html', 'login');
INSERT INTO `links` VALUES ('7', 'private', 'Главная', '/main', 'link.html', null);
INSERT INTO `links` VALUES ('8', 'private', 'Изменить пароль', '#reset_password', 'link.html', 'reset_password');
INSERT INTO `links` VALUES ('17', 'menu1', 'Регистрация', '/registration', 'link.html', '');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mkey` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'menu1', 'menu.html', 'main');
INSERT INTO `menu` VALUES ('2', 'private', 'menu.html', 'private');
INSERT INTO `menu` VALUES ('3', 'menu1', 'menu.html', 'post_id');
INSERT INTO `menu` VALUES ('4', 'menu1', 'menu.html', 'editor');
INSERT INTO `menu` VALUES ('5', 'menu1', 'menu.html', 'feedback');
INSERT INTO `menu` VALUES ('6', 'menu1', 'menu.html', 'catalog');
INSERT INTO `menu` VALUES ('7', 'menu1', 'menu.html', 'catalog_id');
INSERT INTO `menu` VALUES ('8', 'menu1', 'menu.html', 'finder');

-- ----------------------------
-- Table structure for muzzle_nozzle
-- ----------------------------
DROP TABLE IF EXISTS `muzzle_nozzle`;
CREATE TABLE `muzzle_nozzle` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Mesh` varchar(255) DEFAULT NULL,
  `AudioShot` varchar(255) DEFAULT NULL,
  `MuzzleFlash` varchar(255) DEFAULT NULL,
  `Power` float(255,0) DEFAULT '0',
  `ItemId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of muzzle_nozzle
-- ----------------------------
INSERT INTO `muzzle_nozzle` VALUES ('1', null, 'muzzle_nozzle', 'muzzle_nozzle', '-5', '13');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `Id` int(3) NOT NULL AUTO_INCREMENT,
  `Key` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Pattern` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('5', 'main', 'Главная', 'index.html');
INSERT INTO `pages` VALUES ('6', 'npc_answers', 'Ответы нпс', 'npc_answers.html');
INSERT INTO `pages` VALUES ('7', 'private', 'Личный кабинет', 'private.html');
INSERT INTO `pages` VALUES ('8', 'editor', 'Редактирование', 'editor.html');
INSERT INTO `pages` VALUES ('9', 'auth', 'Авторизация', 'auth.html');
INSERT INTO `pages` VALUES ('10', 'catalog', 'Каталог', 'catalog.html');
INSERT INTO `pages` VALUES ('11', 'itemeditor', 'Редактор предметов', 'itemeditor.html');
INSERT INTO `pages` VALUES ('12', 'finder', 'Поиск', 'finder.html');
INSERT INTO `pages` VALUES ('13', 'registration', 'Регистрация', 'registration.html');
INSERT INTO `pages` VALUES ('14', 'regsektion', 'Запись на кружок', 'regsektion.html');

-- ----------------------------
-- Table structure for property
-- ----------------------------
DROP TABLE IF EXISTS `property`;
CREATE TABLE `property` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(255) DEFAULT NULL,
  `Value` float DEFAULT NULL,
  `MaxValue` float(255,0) DEFAULT NULL,
  `MinValue` float DEFAULT NULL,
  `PerTime` float DEFAULT NULL,
  `Per` float DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of property
-- ----------------------------
INSERT INTO `property` VALUES ('1', 'heal', '10', '100', '0', '5', '5');
INSERT INTO `property` VALUES ('2', 'stamina', '100', '100', '0', '1', '1');
INSERT INTO `property` VALUES ('3', 'fatigue', '10', '100', '0', '10', '1');
INSERT INTO `property` VALUES ('4', 'fatigueRecoveryRateInGreenZone', '10', '100', '1', '-20', '1');
INSERT INTO `property` VALUES ('6', 'thirst', '10', '100', '0', '5', '60');
INSERT INTO `property` VALUES ('7', 'hunger', '10', '100', '0', '5', '60');
INSERT INTO `property` VALUES ('8', 'irradiation', '0', '100', '0', '0', '1');
INSERT INTO `property` VALUES ('9', 'temperature', '10', '100', '0', '5', '1');
INSERT INTO `property` VALUES ('10', 'mind', '10', '100', '0', '5', '1');
INSERT INTO `property` VALUES ('11', 'radiation', '0', '100', '0', '0', '1');
INSERT INTO `property` VALUES ('12', 'bulletproof', '0', '100', '0', '0', '0');
INSERT INTO `property` VALUES ('13', 'armor', '0', '100', '0', '0', '0');

-- ----------------------------
-- Table structure for radiation_zone
-- ----------------------------
DROP TABLE IF EXISTS `radiation_zone`;
CREATE TABLE `radiation_zone` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Power` float DEFAULT NULL,
  `TriggerId` int(11) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of radiation_zone
-- ----------------------------
INSERT INTO `radiation_zone` VALUES ('1', '100', '2', '0');

-- ----------------------------
-- Table structure for repair_table
-- ----------------------------
DROP TABLE IF EXISTS `repair_table`;
CREATE TABLE `repair_table` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RepairItemId` int(11) DEFAULT NULL,
  `ItemId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repair_table
-- ----------------------------
INSERT INTO `repair_table` VALUES ('1', '12', '3');
INSERT INTO `repair_table` VALUES ('2', '12', '7');
INSERT INTO `repair_table` VALUES ('3', '12', '4');

-- ----------------------------
-- Table structure for rights
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Right` varchar(255) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rights
-- ----------------------------
INSERT INTO `rights` VALUES ('1', 'AdminCmd', 'Команда администратора');
INSERT INTO `rights` VALUES ('2', 'AllUserCmd', 'Команда для всех пользователей');
INSERT INTO `rights` VALUES ('3', 'goTo', 'Телепорт');
INSERT INTO `rights` VALUES ('4', 'setWorldState', 'Установить состояние мира');
INSERT INTO `rights` VALUES ('5', 'setNpcStat', 'Установить стостояние нпс');
INSERT INTO `rights` VALUES ('6', 'setProperty', 'Установить свойство персонажа');
INSERT INTO `rights` VALUES ('7', 'editItem', 'Редактирование предметов');

-- ----------------------------
-- Table structure for scene
-- ----------------------------
DROP TABLE IF EXISTS `scene`;
CREATE TABLE `scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '0',
  `transformid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scene
-- ----------------------------
INSERT INTO `scene` VALUES ('1', '0', '1');

-- ----------------------------
-- Table structure for scopes
-- ----------------------------
DROP TABLE IF EXISTS `scopes`;
CREATE TABLE `scopes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) DEFAULT NULL,
  `Mesh` varchar(255) DEFAULT NULL,
  `Zoom` varchar(255) DEFAULT '0',
  `Crosshair` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scopes
-- ----------------------------
INSERT INTO `scopes` VALUES ('1', '11', '', '60,40,20', 'crosshair');

-- ----------------------------
-- Table structure for transform
-- ----------------------------
DROP TABLE IF EXISTS `transform`;
CREATE TABLE `transform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scene` varchar(50) DEFAULT NULL,
  `position` varchar(150) DEFAULT '{"x":0.0,"y":0.0,"z":0.0}',
  `rotation` varchar(150) DEFAULT '{"x":0.0,"y":0.0,"z":0.0,"w":0.0}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transform
-- ----------------------------
INSERT INTO `transform` VALUES ('1', 'MainWorld', '{\"x\":0.0,\"y\":0.0,\"z\":0.0}', '{\"x\":0.0,\"y\":0.0,\"z\":0.0}');
INSERT INTO `transform` VALUES ('2', 'MainWorld', '{\"x\":-10.0,\"y\":0.0,\"z\":-10.0}', '{\"x\":0.0,\"y\":0.0,\"z\":0.0,\"w\":0.0}');

-- ----------------------------
-- Table structure for triggers
-- ----------------------------
DROP TABLE IF EXISTS `triggers`;
CREATE TABLE `triggers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `TransformId` int(11) DEFAULT NULL,
  `Primitive` int(11) DEFAULT NULL,
  `Width` float DEFAULT '0',
  `Height` float DEFAULT '0',
  `Radius` float DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of triggers
-- ----------------------------
INSERT INTO `triggers` VALUES ('1', 'Зеленая зона', 'GreenZone', '1', '1', '10', '10', '10');
INSERT INTO `triggers` VALUES ('2', 'Участки радиационного загрязнения', 'RadiationZone', '2', '1', '10', '10', '10');

-- ----------------------------
-- Table structure for user_groups
-- ----------------------------
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `GroupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_groups
-- ----------------------------
INSERT INTO `user_groups` VALUES ('1', '1', '1');
INSERT INTO `user_groups` VALUES ('2', '1', '2');
INSERT INTO `user_groups` VALUES ('3', '20', '1');

-- ----------------------------
-- Table structure for weapons
-- ----------------------------
DROP TABLE IF EXISTS `weapons`;
CREATE TABLE `weapons` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Delay` float(9,0) DEFAULT '1',
  `BulletSpeed` float(9,0) DEFAULT '60',
  `CageItemId` int(11) DEFAULT '0',
  `AimItemId` int(11) DEFAULT NULL,
  `PowerMin` float(11,0) DEFAULT '0',
  `MuzzleFlash` varchar(255) DEFAULT '',
  `Type` int(6) DEFAULT NULL,
  `ItemId` int(11) DEFAULT NULL,
  `CustomizationId` int(11) DEFAULT NULL,
  `Slot` varchar(255) DEFAULT 'RightHand',
  `ShotType` int(11) DEFAULT '0',
  `ScatterAngle` float DEFAULT '0',
  `MuzzleNozzleItemId` int(11) DEFAULT NULL,
  `DeviceItemId` int(11) DEFAULT NULL,
  `WedgeProbability` float DEFAULT '1',
  `PowerMax` float DEFAULT '10',
  `RecoilForce` float DEFAULT '0',
  `FirstShotReturnRate` float DEFAULT '0',
  `ReturnRatio` float DEFAULT '0',
  `RechargeDuration` float DEFAULT '2',
  `ShotRange` float DEFAULT '1000',
  `SingleFireMode` tinyint(1) DEFAULT '1',
  `FireModeTurn` tinyint(1) DEFAULT '0',
  `FireModeContinuous` tinyint(1) DEFAULT '0',
  `ShotsInLine` int(11) DEFAULT '3',
  `BulletsForAShot` int(11) DEFAULT '1',
  `AudioShot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weapons
-- ----------------------------
INSERT INTO `weapons` VALUES ('3', '1', '1', null, null, '-10', '', '0', '-1', '2', 'RightHand', '0', '0', null, null, null, null, '0', null, '0', null, null, null, null, null, null, '3', null);
INSERT INTO `weapons` VALUES ('4', '1', '1', null, null, '-10', '', '0', '-2', '1', 'RightHand', '0', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '1', null);
INSERT INTO `weapons` VALUES ('5', '1', '30', '8', '11', '0', 'PistolMuzzleFire', '1', '4', '3', 'RightHand', '2', '1', '13', null, null, '-10', '9', null, '20', null, null, '1', '1', '1', null, '1', 'Pm');

-- ----------------------------
-- Table structure for weapon_modifications
-- ----------------------------
DROP TABLE IF EXISTS `weapon_modifications`;
CREATE TABLE `weapon_modifications` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `WeaponId` int(11) DEFAULT NULL,
  `ModificationId` int(11) DEFAULT NULL,
  `TypeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weapon_modifications
-- ----------------------------
INSERT INTO `weapon_modifications` VALUES ('1', '5', '1', '0');
SET FOREIGN_KEY_CHECKS=1;
