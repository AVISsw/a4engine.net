
var yesterday = new Date();
yesterday.setDate(yesterday.getDate());
yesterday = yesterday.toISOString().slice(0, 10) + "T00:00";
var today = new Date();
today.setDate(today.getDate() +1);
today = today.toISOString().slice(0, 10) + "T00:00";
 

var model = {
  SelectReport: {
    HeaderData: {
    },
    DeviceMetadata: []
  },
  Headers: []
};
$(document).ready(function () {

  Vue.component('report-list', {
    template: '#report-list',
    props: ['items'],
    methods:{
      Open: function (item) {
        console.log(item.Hash);
 
        GetReport(item.Hash);
       }
    }
  })
  function UpdateHeaders(){
    jsonRequestData("/handler/ReportViewer/get-list", {}, function (response) {
      console.log(response);
        model.Headers = response; 
    });
   }  function GetReport(hash){
    jsonRequestData("/handler/ReportViewer/get/"+hash, {}, function (response) {
      console.log(response);
        model.SelectReport = response; 
    });
   }
  new Vue({
    el: '#app-table',
    data: model,
    methods: {
      Update: function () {
        UpdateHeaders();
      }
    }
  });
  UpdateHeaders();
 
});