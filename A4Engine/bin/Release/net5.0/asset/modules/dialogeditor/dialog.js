var openEditorButton;
var groupId = get_path()[3];

function emptyDialogDataTemplate() {
  return {
    Name: "def_dialog",
    Pages: [
      {
        Id: 0,
        NpcAnswerData: [""],
        SayPlayer: [],
        Hide: false,
        Audio: ""
      }
    ],
    startPageData: function () {
      console.log(this.Pages[0]);
      return this.Pages[0];
    }
  }
}
function emptyPageDataTemplate() {
  return {
    Id: 0,
    NpcAnswerData: [""],
    SayPlayer: [],
    Hide: false,
    Audio: ""
  }
}

var dialogData;
var dialogList = [];

function GetDialogRequest(index, callback) {
  axios.get("/handler/dialoghandler/get/" + index)
    .then((response) => {
      if (callback)
        callback(response.data);
    })
    .catch((error) => {
      console.log(error);
    });
}
function GetDialogList(callback) {
  axios.post("/handler/dialoghandler/get-list")
    .then((response) => {
      if (response.data)
        dialogList = response.data;

      console.log(dialogList);
      if (callback)
        callback(dialogList);
    })
    .catch((error) => {
      console.log(error);
    });
}

function SetDialogRequest(data, callback) {
 // var copy = Object.assign({}, data);
  for (pageIndex in data.Pages) {
  //  let pageData = data.Pages[pageIndex];
   //data.Pages[pageIndex] = Object.assign({}, pageData);
    data.Pages[pageIndex].done = false;
    
    for (sayIndex in data.Pages[pageIndex].SayPlayer) {
    //  var sayData = Object.assign({}, data.Pages[pageIndex].SayPlayer[sayIndex]);
    data.Pages[pageIndex].SayPlayer[sayIndex].Children = null;
     // copy.Pages[pageIndex].SayPlayer[sayIndex] = sayData;
    }
  }  
  console.log("SetDialogRequest");
  console.log(data);
  let postJson = JSON.stringify(data)
  console.log("postJson");
  console.log(postJson);
  axios.post("/handler/dialoghandler/set", postJson, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then((response) => {

      if (callback)
        callback(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

function isValid(str) {
  if (str)
    if (("" + str).trim() != '-1')
      return true;
  return false;
}

Vue.component('page-element', {
  template: '#pageElementTemplate',
  props: ['page-data', 'rootSay', 'done'], // declare the props

  methods: {
    RemovePageHandler(pageData, rootSay) {
      dialogData.Pages.splice(pageData.Id, 1);
      if (rootSay) {
        Vue.delete(rootSay, 'Children');
        rootSay.NextPage = -1;
      }
    },
    donePage(page) {
      page.done = true;
      console.log(page);

    }, Hide(page) {
      page.Hide = !page.Hide;
      Vue.set(page, 'Hide', page.Hide);
      console.log("Hide " + page.Hide);
    },
    RemoveConditionHandler(conditionList, index) {
      conditionList.splice(index, 1);
    },
    RemoveEventHandler(eventList, index) {
      eventList.splice(index, 1);
    },
    RemovePlayerSayHandler(sayList, index) {
      sayList.splice(index, 1);
    },
    AddSayHandler(sayList) {
      let sayData = {};
      sayData.Events = [];
      sayData.Conditions = [];
      sayList.push(sayData)
 
    },
    AddPageHandler(sayData) {
      let page = emptyPageDataTemplate();
      console.log(sayData);
      if (isValid(sayData.NextPage)) {

      } else {

        if (sayData) {
          Vue.set(sayData, 'Children', page)
        }
        dialogData.Pages.push(page);
        page.Id = dialogData.Pages.indexOf(page);
        sayData.NextPage = page.Id;
      }
    },
    AddEventHandler(eventList) {
      eventList.push({})
      $( function() {
        let availableTags = [
          "exit",
          "buyer",
          "storage",
          "crstorage",
          "secondhand",
          "donatstore",
          "getquest",
          "join_in_global_faction",
          "call_global_event",
          "buy_rep",
          "get_rep",
          "save_info",
          "add_credit",
          "add_item",
          "remove_item",
        ];
        $(".EventName").autocomplete({
          source: availableTags
        });
      });
    },
    AddConditionHandler(conditionList) {
      conditionList.push({})
      $( function() {
        let availableTags = [
        "has_rented",
        "pl_check_credit",
        "npc_check_credit",
        "can_take_quest",
        "task_in_process",
        "check_rep",
        "check_faction",
        "has_info",
        "dont_has_info",
        "is_item_exist",
        "dont_is_item_exist"
      ];
      $(".ConditionName").autocomplete({
        source: availableTags
      });  });
    },
    ChangeNextPageHandler(sayData) {
      if (sayData.Children) {
        sayData.NextPage = sayData.Children.Id;
      }
    }
  }
});
var modalModule;
Vue.component('modal', {
  template: '#modal-template'
});
var name;
function SelectDialog(index) {
  console.log("select " + index);
  name = dialogList[index];
  name = name.replace("dialogs\\", "");
  name = name.replace(".json", "");
  console.log("select name " + name);
  GetDialogRequest(index, function (dataModel) {
    UpdateDialogData(dataModel);
  });
}

function UpdateDialogData(dataModel) {
  dialogData.Name = name;

  for (index in dataModel.Pages) {
    let pageData = dataModel.Pages[index];
    pageData.Id = index;
    pageData.Hide = false;
    for (index2 in pageData.SayPlayer) {
      let sayData = pageData.SayPlayer[index2];
      sayData.Id = index2;
      let childPage = dataModel.Pages[sayData.NextPage];
      if (childPage) {
        sayData.Children = childPage;

      }
    }
  }

  dialogData.Pages = dataModel.Pages;
}
function UpdateDialogs() {
  GetDialogList(function (dataModel) {
    modalModule.$data.Dialogs = dataModel.Dialogs;
  });
}
var dialogList;
var editorModule;
$(document).ready(function () {
  Menu();
  dialogData = emptyDialogDataTemplate();
  GetDialogList(function (dataModel) {
    console.log(dataModel);
    dialogList = dataModel.Dialogs;
    modalModule = new Vue({
      el: '#modalModule',
      data: {
        Type: -1,
        Title: "",
        Dialog: dialogData,
        Dialogs: dataModel.Dialogs,
        RemoveDialogName: "",
        RemoveDialogIndex: ""
      },
      methods: {
        UpdateDialogs() {
          UpdateDialogs();
        },
        SelectDialog(index) {
          SelectDialog(index);
        },
        SaveRequest(name) {
          dialogData.Name = name;
          SetDialogRequest(dialogData, function (result) {
            console.log("save result");
            console.log(result);
            UpdateDialogData(dialogData);
            UpdateDialogs();
          });
        },
        RemoveConfirm(index){
          axios.post("/handler/dialoghandler/remove/" + index)
            .then((response) => {
              if (response.data) {

                this.Dialogs = response.data.Dialogs;
                modalModule.$data.Type = -1;
                modalModule.$data.Title = "Успех";
                modalModule.$data.Type = 4;
                console.log("RemoveDialog ");
                console.log(dataModel.Dialogs);
              }
            })
            .catch((error) => {
              console.log(error);
            });
        },
        RemoveDialog(index) {
          this.RemoveDialogIndex = index;
          this.RemoveDialogName = this.Dialogs[index];
          this.Title = "Удаление";
          this.Type = 3;
        },
      }
    });

  });


  editorModule = new Vue({
    el: '#dialogContent',
    data: dialogData,
    methods: {
      SaveData() {
        modalModule.$data.Type = 2;
        modalModule.$data.Title = "Сохранить";

      },
      NewData() {
        console.log("new data");
        var newDialog = emptyDialogDataTemplate();
        dialogData.Pages = newDialog.Pages;
        dialogData.Name = newDialog.Name;
      },
      OpenData() {
        modalModule.$data.Title = "Открыть";
        modalModule.$data.Type = 1;
      }
    }
  });

 
});
 