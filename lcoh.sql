/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : lcoh

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 25/02/2020 14:41:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Session` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RegIp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LastIp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RegMac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LastMac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Birthday` date NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `Index`(`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES (1, 'admin', 'ef6f4fe2aa34b22270acfba589ff75e457626d73d49916ad643e1496849f322a', 'mail', '01447d0b96655022b211405d77982858523812490692b9d6f5873696e789ea3b', '', '127.0.0.1', '', '00-00-00-00-00-00', '0001-01-01');
INSERT INTO `accounts` VALUES (20, 'qwerty', '7815696ecbf1c96e6894b779456d330e', 'ytykuykuy', '06399ffd03b6b83b7c5cd5bcb0ac4d986', ' ', ' ', ' ', ' ', '0001-01-01');
INSERT INTO `accounts` VALUES (30, 'test', 'gr8934jk23jkf83jklwelilds3::::3556fio22kjdfijerlerjglk4j4rklng', 'strelokzi@mail.ru1', 'gr8934jk23jkf83jklwelilds3::::3556fio22kjdfijerlerjglk4j4rklng', '127.0.0.1', NULL, '00-00-00-00-00-00', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (31, 'KelevRus', '1c927eff9e9d14995741535c07b752cf5e4d5eb76f615555f9f729fe5c319018', 'papa-dog@mail.ru', 'ad94f9d09d1d711377cc80bcde139f01', '95.71.9.57', '127.0.0.1', '01-00-5E-2F-27-66', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (32, 'KelevRus1', '48c21544fd5e80b9b9b2e3bcfe5384db', 'papa-dog@mail.ru1', NULL, '95.71.114.250', NULL, '70-81-05-64-70-FF', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (34, 'test2', 'f626cac9d093515f0cb0fc431c78e3c1', 'mr.3dpro2@mail.ru', NULL, '127.0.0.1', NULL, '00-00-00-00-00-00', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (36, 'gergreg', '1c927eff9e9d14995741535c07b752cf5e4d5eb76f615555f9f729fe5c319018', 'strelokzi@mail.ru2', NULL, '127.0.0.1', NULL, '00-00-00-00-00-00', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (38, 'test4', 'ef6f4fe2aa34b22270acfba589ff75e457626d73d49916ad643e1496849f322a', 'mr.3dpro@mail.ru', '2b64bff5c677df050017757dd195060e7ce6b82a8b515d22ad50c97a9455d251', '127.0.0.1', '127.0.0.1', '00-00-00-00-00-00', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (39, 'strelokzi', 'ef6f4fe2aa34b22270acfba589ff75e457626d73d49916ad643e1496849f322a', 'strelokzi@mail.ru2', '14c9180638efd1346ff8dafe132db184e7da43688fa44dbf36f5d6f2384fd16f', '127.0.0.1', '127.0.0.1', '00-00-00-00-00-00', NULL, '0001-01-01');
INSERT INTO `accounts` VALUES (40, 'testuser2', 'ef6f4fe2aa34b22270acfba589ff75e457626d73d49916ad643e1496849f322a', 'strelokzi@mail.ru', NULL, '127.0.0.1', NULL, '01-00-5E-77-D3-01', NULL, '1998-07-11');

-- ----------------------------
-- Table structure for ammo
-- ----------------------------
DROP TABLE IF EXISTS `ammo`;
CREATE TABLE `ammo`  (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NOT NULL,
  `Mesh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PartsCount` int(11) NULL DEFAULT 1,
  `PowerMin` float NULL DEFAULT 1,
  `PowerMax` float NULL DEFAULT 2,
  `Diameter` float NULL DEFAULT 0.1,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ammo
-- ----------------------------
INSERT INTO `ammo` VALUES (1, 9, 'Bullet', 1, -5, -7, 0.1);
INSERT INTO `ammo` VALUES (3, 42, 'Bullet', 1, -15, -25, 0.1);
INSERT INTO `ammo` VALUES (4, 43, 'Bullet', 1, -20, -30, 0.1);
INSERT INTO `ammo` VALUES (5, 44, 'Bullet', 1, -60, -60, 0.1);
INSERT INTO `ammo` VALUES (6, 45, 'Bullet', 1, -5, -7, 0.1);
INSERT INTO `ammo` VALUES (7, 46, 'Bullet', 1, -5, -7, 0.1);
INSERT INTO `ammo` VALUES (8, 47, 'Bullet', 1, -10, -15, 0.1);
INSERT INTO `ammo` VALUES (9, 49, 'Bullet', 7, -3, -5, 0.1);

-- ----------------------------
-- Table structure for bans
-- ----------------------------
DROP TABLE IF EXISTS `bans`;
CREATE TABLE `bans`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CharacterId` bigint(20) NULL DEFAULT NULL,
  `AccountId` bigint(20) NULL DEFAULT NULL,
  `BanTime` datetime(0) NULL DEFAULT NULL,
  `BanDuration` datetime(0) NULL DEFAULT NULL,
  `Reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReasonType` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for battery
-- ----------------------------
DROP TABLE IF EXISTS `battery`;
CREATE TABLE `battery`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ItemId` bigint(20) NULL DEFAULT NULL,
  `Power` int(255) NULL DEFAULT NULL,
  `Type` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of battery
-- ----------------------------
INSERT INTO `battery` VALUES (1, 157, 3000, 0);

-- ----------------------------
-- Table structure for buffs
-- ----------------------------
DROP TABLE IF EXISTS `buffs`;
CREATE TABLE `buffs`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CharacterId` int(11) NULL DEFAULT NULL,
  `PropertyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ValueName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Time` time(0) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cages
-- ----------------------------
DROP TABLE IF EXISTS `cages`;
CREATE TABLE `cages`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NULL DEFAULT NULL,
  `Mesh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MaxCount` int(255) NULL DEFAULT 1,
  `AmmoItemId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ExtraRechargeTime` float NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cages
-- ----------------------------
INSERT INTO `cages` VALUES (1, 31, 'AK47', 31, '[42,43,44]', 0.5);
INSERT INTO `cages` VALUES (2, 32, 'AK47', 30, '[42,43,44]', 0.5);
INSERT INTO `cages` VALUES (3, 29, 'PM_cage', 8, '[46,45,47]', 0.5);
INSERT INTO `cages` VALUES (4, 30, 'PM_cage', 8, '[46,45,47]', 0.5);
INSERT INTO `cages` VALUES (5, 50, 'Toz', 1, '[49]', 0.5);

-- ----------------------------
-- Table structure for chance_spawn_items
-- ----------------------------
DROP TABLE IF EXISTS `chance_spawn_items`;
CREATE TABLE `chance_spawn_items`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NOT NULL,
  `GroupId` int(11) NULL DEFAULT NULL,
  `CountMin` int(11) NULL DEFAULT 1,
  `Chance` float NULL DEFAULT 1,
  `CountMax` int(11) NULL DEFAULT 2,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chance_spawn_items
-- ----------------------------
INSERT INTO `chance_spawn_items` VALUES (13, 32, 4, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (15, 17, 1, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (17, 18, 2, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (18, 31, 3, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (19, 29, 5, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (20, 30, 6, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (21, 15, 7, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (22, 16, 8, 1, 1, 2);
INSERT INTO `chance_spawn_items` VALUES (23, 55, 10000, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (24, 42, 9, 10, 1, 10);
INSERT INTO `chance_spawn_items` VALUES (25, 43, 10, 10, 1, 10);
INSERT INTO `chance_spawn_items` VALUES (26, 44, 11, 10, 1, 10);
INSERT INTO `chance_spawn_items` VALUES (27, 161, 12, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (28, 162, 13, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (29, 163, 14, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (30, 164, 15, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (31, 45, 16, 10, 1, 10);
INSERT INTO `chance_spawn_items` VALUES (32, 46, 17, 10, 1, 10);
INSERT INTO `chance_spawn_items` VALUES (33, 47, 18, 10, 10, 10);
INSERT INTO `chance_spawn_items` VALUES (34, 55, 19, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (35, 156, 10000, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (36, 157, 10000, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (37, 157, 10000, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (38, 157, 10000, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (39, 11, 20, 0, 0, 0);
INSERT INTO `chance_spawn_items` VALUES (40, 11, 21, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (41, 13, 22, 1, 1, 1);
INSERT INTO `chance_spawn_items` VALUES (42, 13, 23, 1, 1, 1);

-- ----------------------------
-- Table structure for characters
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeId` int(3) NULL DEFAULT 0,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `TransformId` int(11) NULL DEFAULT 1,
  `CharacterId` int(11) NULL DEFAULT NULL,
  `Account` int(255) NOT NULL,
  `Exp` int(255) NULL DEFAULT 0,
  `Money` bigint(255) NULL DEFAULT 0,
  `Skin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Male_01',
  `Lvl` int(255) NULL DEFAULT 1,
  `Desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MaxWalk` float NULL DEFAULT 3,
  `BagId` int(11) NULL DEFAULT NULL,
  `EquipmentId` int(11) NULL DEFAULT NULL,
  `Heal` float(11, 0) NULL DEFAULT 100,
  `Stamina` float(255, 0) NULL DEFAULT NULL,
  `Fatigue` float(255, 0) NULL DEFAULT NULL,
  `Hunger` float(255, 0) NULL DEFAULT NULL,
  `Thirst` float(255, 0) NULL DEFAULT NULL,
  `Radiation` float(255, 0) NULL DEFAULT NULL,
  `Mind` float(255, 0) NULL DEFAULT NULL,
  `Bleeding` float(255, 0) NULL DEFAULT NULL,
  `Temperature` float(255, 0) NULL DEFAULT NULL,
  `Run` float(255, 0) NULL DEFAULT 4,
  `Sprint` float(255, 0) NULL DEFAULT 4,
  `PocketId` int(11) NULL DEFAULT NULL,
  `Jump` float(255, 4) NULL DEFAULT 0.5000,
  `DonateMoney` bigint(255) NULL DEFAULT 0,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES (1, 1, '<color=purple>Dev</color><color=teal>AVIS</color>', 14791, 0, 1, 355, 8995673, 'Male_01', 3, 'Тут описание', 3, 1, 2, 100, 101, 66, 100, 100, 0, 0, 0, 37, 5, 7, 5, 4.0000, 500);
INSERT INTO `characters` VALUES (2, 1, 'Jary', 1, 0, 1, 3, 3, 'Male_01', 1, 'fff', 3, 1, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (3, 1, 'Mary', 14792, 0, 1, 3, 1020977, 'Male_01', 2, 'ffff', 3, 1, 2, 100, 101, 1, 5, 5, 0, 0, 0, 0, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (4, 1, 'Lala', 1, 0, 20, 2, 3333, 'Male_01', 6, '#fffffff', 3, 1, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (5, 2, 'Zveroboy', 1, 0, -1, 355, 3000, 'Male_01', 32, 'Первый нпс', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (6, 2, 'Test', 1, 0, -1, 222, 222, 'Male_01', 3, 'frfrfrrg', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (7, 2, 'fff', 1, 0, -1, 3, 3, 'Male_01', 3, 'fefeff', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (8, 2, 'fff', 1, 0, -1, 3, 3, 'Male_01', 3, 'fefeff', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (9, 2, 'fff', 1, 0, -1, 3, 3, 'Male_01', 3, 'fefeff', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (10, 2, 'Bot', 1, 0, -1, 3, 100, 'Male_01', 1, '  ', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (11, 2, 'Bot', 1, 0, -1, 3, 100, 'Male_01', 1, '  ', 3, 3, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 5, 4.0000, 0);
INSERT INTO `characters` VALUES (16, 1, 'Космический иван', 0, 0, 30, 0, 0, 'Male_01', 0, NULL, 3, 18, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 7, 20, 4.0000, 0);
INSERT INTO `characters` VALUES (17, 1, 'Уася Пупкин', 0, 0, 31, 0, 0, 'Male_01', 1, NULL, 3, 39, 40, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 41, 8.0000, 0);
INSERT INTO `characters` VALUES (18, 1, 'Ваня', 14789, 0, 33, 0, 0, 'Male_01', 1, NULL, 0, 42, 43, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 7, 44, 4.0000, 0);
INSERT INTO `characters` VALUES (21, 1, 'rgrger', 0, 0, 36, 0, 0, 'Male_01', 1, NULL, 0, 51, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 7, 53, 4.0000, 0);
INSERT INTO `characters` VALUES (23, 1, 'efwef efewf', 0, 0, 38, 0, 0, 'Male_01', 1, NULL, 0, 57, 58, 100, 100, 0, 0, 0, 0, 0, 0, 37, 5, 7, 59, 4.0000, 0);
INSERT INTO `characters` VALUES (24, 1, 'Вадим Горбатый', 14795, 0, 39, 0, 0, 'Male_01', 1, NULL, 0, 69, 70, 100, 101, 0, 3, 3, 0, 0, 0, 37, 5, 7, 71, 4.0000, 0);
INSERT INTO `characters` VALUES (25, 1, 'Антон Власов', 0, 0, 39, 0, 0, 'Male_01', 1, NULL, 0, 72, 73, 100, 100, 0, 0, 0, 0, 0, 0, 37, 5, 7, 74, 4.0000, 0);
INSERT INTO `characters` VALUES (26, 1, 'додик бывалый', 14793, 0, 39, 0, 0, 'Male_01', 1, NULL, 0, 75, 76, 100, 101, 0, 2, 2, 0, 0, 0, 37, 5, 7, 77, 4.0000, 0);
INSERT INTO `characters` VALUES (27, 1, 'Витек Бледный', 14796, 0, 39, 0, 10000, 'Male_01', 1, 'Высокий, бледный, вес около 70 кг', 0, 78, 79, 100, 101, 0, 1, 1, 0, 0, 0, 37, 5, 7, 80, 4.0000, 0);
INSERT INTO `characters` VALUES (28, 1, 'Валера Кислый', 14794, 0, 39, 0, 0, 'Male_01', 1, 'Валера я такой валера)', 0, 81, 82, 100, 101, 0, 1, 1, 0, 0, 0, 37, 5, 7, 83, 4.0000, 0);

-- ----------------------------
-- Table structure for combine
-- ----------------------------
DROP TABLE IF EXISTS `combine`;
CREATE TABLE `combine`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Decription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Recept` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of combine
-- ----------------------------
INSERT INTO `combine` VALUES (3, 'Открыть консерву', 'Открыть консерву', '8,9', '5');

-- ----------------------------
-- Table structure for currency_rate
-- ----------------------------
DROP TABLE IF EXISTS `currency_rate`;
CREATE TABLE `currency_rate`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Currency` int(255) NULL DEFAULT -1,
  `InCreditTermsForAUnit` float(255, 0) NULL DEFAULT 1,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of currency_rate
-- ----------------------------
INSERT INTO `currency_rate` VALUES (1, 0, 1);
INSERT INTO `currency_rate` VALUES (2, 1, 5);

-- ----------------------------
-- Table structure for customization
-- ----------------------------
DROP TABLE IF EXISTS `customization`;
CREATE TABLE `customization`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Mesh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Material` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Slot` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customization
-- ----------------------------
INSERT INTO `customization` VALUES (2, 'Mesh', NULL, NULL, 'RightHand');
INSERT INTO `customization` VALUES (3, 'Mesh', 'Handgun_M1911A_Black', NULL, 'RightHand');
INSERT INTO `customization` VALUES (4, 'SkinnedMesh', 'lv_01_armor_m1', NULL, 'Tors');
INSERT INTO `customization` VALUES (5, 'SkinnedMesh', 'lv_03_pants_m2', NULL, 'Leg');
INSERT INTO `customization` VALUES (6, 'SkinnedMesh', 'lv_01_boots', NULL, 'Foot');
INSERT INTO `customization` VALUES (7, 'SkinnedMesh', 'lv_02_armor_m2', NULL, 'Tors');
INSERT INTO `customization` VALUES (13, 'Mesh', 'null', '', 'RightHand');
INSERT INTO `customization` VALUES (15, 'Mesh', 'AK_47', '', 'RightHand');
INSERT INTO `customization` VALUES (16, 'Mesh', 'def_pm', '', 'RightHand');
INSERT INTO `customization` VALUES (17, 'Mesh', 'TOZ_120U', 'null', 'RightHand');
INSERT INTO `customization` VALUES (18, 'Mesh', 'TOZ_120', 'null', 'RightHand');
INSERT INTO `customization` VALUES (20, NULL, NULL, NULL, NULL);
INSERT INTO `customization` VALUES (23, 'SkinnedMesh', 'null', 'null', 'GasMask');
INSERT INTO `customization` VALUES (24, 'SkinnedMesh', 'Gazmak', 'null', 'Head');
INSERT INTO `customization` VALUES (25, 'Mesh', 'knife_00', 'null', 'RightHand');
INSERT INTO `customization` VALUES (26, NULL, NULL, NULL, NULL);
INSERT INTO `customization` VALUES (27, 'Mesh', 'AK_47', 'null', 'RightHand');
INSERT INTO `customization` VALUES (28, 'Mesh', 'AK_47', 'null', 'RightHand');
INSERT INTO `customization` VALUES (29, 'Mesh', 'def_pm', 'null', 'RightHand');
INSERT INTO `customization` VALUES (30, 'Mesh', 'def_pm', 'null', 'RightHand');

-- ----------------------------
-- Table structure for dialog_category
-- ----------------------------
DROP TABLE IF EXISTS `dialog_category`;
CREATE TABLE `dialog_category`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Новая категория',
  `path` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/',
  `level` int(9) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_category
-- ----------------------------
INSERT INTO `dialog_category` VALUES (1, 'story', '/story', 0);

-- ----------------------------
-- Table structure for dialog_events
-- ----------------------------
DROP TABLE IF EXISTS `dialog_events`;
CREATE TABLE `dialog_events`  (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_events
-- ----------------------------
INSERT INTO `dialog_events` VALUES (1, 'dialog_close');

-- ----------------------------
-- Table structure for dialog_filters
-- ----------------------------
DROP TABLE IF EXISTS `dialog_filters`;
CREATE TABLE `dialog_filters`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`, `name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_filters
-- ----------------------------
INSERT INTO `dialog_filters` VALUES (1, 'Возраст');
INSERT INTO `dialog_filters` VALUES (2, 'Имя');
INSERT INTO `dialog_filters` VALUES (3, 'Местный');

-- ----------------------------
-- Table structure for dialog_filters_link
-- ----------------------------
DROP TABLE IF EXISTS `dialog_filters_link`;
CREATE TABLE `dialog_filters_link`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `filter_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `filter_value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_filters_link
-- ----------------------------
INSERT INTO `dialog_filters_link` VALUES (1, 1, 'Возраст', '65', 'npc');
INSERT INTO `dialog_filters_link` VALUES (2, 1, 'Имя', 'Боб', 'npc');
INSERT INTO `dialog_filters_link` VALUES (3, 1, 'Местный', 'Да', 'npc');
INSERT INTO `dialog_filters_link` VALUES (4, 1, 'Возраст', '65', 'pl');
INSERT INTO `dialog_filters_link` VALUES (5, 1, 'Имя', 'Боб', 'pl');
INSERT INTO `dialog_filters_link` VALUES (6, 1, 'Местный', 'Да', 'pl');
INSERT INTO `dialog_filters_link` VALUES (7, 2, 'Возраст', '65', 'npc');
INSERT INTO `dialog_filters_link` VALUES (8, 3, 'Respect', '1', 'npc');
INSERT INTO `dialog_filters_link` VALUES (9, 2, 'Respect', '1', 'pl');
INSERT INTO `dialog_filters_link` VALUES (10, 2, 'Возраст', '65', 'pl');
INSERT INTO `dialog_filters_link` VALUES (11, 4, 'Who', '1', 'npc');
INSERT INTO `dialog_filters_link` VALUES (12, 2, 'Who', '1', 'pl');
INSERT INTO `dialog_filters_link` VALUES (13, 3, 'Respect', '1', 'pl');

-- ----------------------------
-- Table structure for dialog_npc_answers
-- ----------------------------
DROP TABLE IF EXISTS `dialog_npc_answers`;
CREATE TABLE `dialog_npc_answers`  (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_npc_answers
-- ----------------------------
INSERT INTO `dialog_npc_answers` VALUES (1, 'story', 'Привет меня зовут {name}, мне {age} лет, я живу тут всю свою жизнь');
INSERT INTO `dialog_npc_answers` VALUES (2, 'story', 'Привет, я {name} мне  {age} лет');
INSERT INTO `dialog_npc_answers` VALUES (3, 'story', 'Ой спасибо чувак, я думаю ты тоже очень классны!');
INSERT INTO `dialog_npc_answers` VALUES (4, 'story', 'Сижу пержу, а вообще у нас тут тихо ; )');

-- ----------------------------
-- Table structure for dialog_player_answers
-- ----------------------------
DROP TABLE IF EXISTS `dialog_player_answers`;
CREATE TABLE `dialog_player_answers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '...',
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dialog_player_answers
-- ----------------------------
INSERT INTO `dialog_player_answers` VALUES (1, 'story', 'Ты очень крутой чувак', 'say', '{key:\"Respect\",value:\"1\"}');
INSERT INTO `dialog_player_answers` VALUES (2, 'story', 'Пока...', 'bye', ' ');
INSERT INTO `dialog_player_answers` VALUES (3, 'story', 'Как тут у вас?', 'say', '{key:\"Who\",value:\"1\"}');

-- ----------------------------
-- Table structure for flashlights
-- ----------------------------
DROP TABLE IF EXISTS `flashlights`;
CREATE TABLE `flashlights`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Range` float(255, 0) NULL DEFAULT 10,
  `SpotAngle` float(255, 0) NULL DEFAULT 75,
  `Intensity` float(255, 0) NULL DEFAULT 6,
  `Color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#FFFFFF',
  `isVisible` tinyint(1) NULL DEFAULT NULL,
  `Filter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Texture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ConsumptionOn` int(11) NULL DEFAULT NULL,
  `SoundOn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SoundOff` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ItemId` bigint(20) NULL DEFAULT NULL,
  `ConsumptionOff` int(255) NULL DEFAULT NULL,
  `BatteryType` int(255) NULL DEFAULT NULL,
  `PowerLimit` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flashlights
-- ----------------------------
INSERT INTO `flashlights` VALUES (1, 10, 75, 6, '#FAFAD2', 1, NULL, 'Flashlights/Texture/flash_light_mask_01', 10, NULL, NULL, 156, 1, 0, 300);

-- ----------------------------
-- Table structure for group_rights
-- ----------------------------
DROP TABLE IF EXISTS `group_rights`;
CREATE TABLE `group_rights`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `GroupId` int(11) NULL DEFAULT NULL,
  `RightId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_rights
-- ----------------------------
INSERT INTO `group_rights` VALUES (2, 1, 2);
INSERT INTO `group_rights` VALUES (3, 2, 3);
INSERT INTO `group_rights` VALUES (4, 2, 4);
INSERT INTO `group_rights` VALUES (5, 2, 5);
INSERT INTO `group_rights` VALUES (6, 2, 6);
INSERT INTO `group_rights` VALUES (7, 2, 7);
INSERT INTO `group_rights` VALUES (8, 2, 8);
INSERT INTO `group_rights` VALUES (9, 2, 9);
INSERT INTO `group_rights` VALUES (10, 2, 10);
INSERT INTO `group_rights` VALUES (11, 2, 11);
INSERT INTO `group_rights` VALUES (12, 2, 12);
INSERT INTO `group_rights` VALUES (13, 2, 13);
INSERT INTO `group_rights` VALUES (14, 2, 14);
INSERT INTO `group_rights` VALUES (15, 2, 15);
INSERT INTO `group_rights` VALUES (16, 2, 16);
INSERT INTO `group_rights` VALUES (17, 2, 17);
INSERT INTO `group_rights` VALUES (18, 2, 1);
INSERT INTO `group_rights` VALUES (19, 2, 18);

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (-1, 'Banned');
INSERT INTO `groups` VALUES (1, 'Users');
INSERT INTO `groups` VALUES (2, 'Admins');

-- ----------------------------
-- Table structure for item_continers
-- ----------------------------
DROP TABLE IF EXISTS `item_continers`;
CREATE TABLE `item_continers`  (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Type` int(11) NULL DEFAULT NULL,
  `TileX` int(11) NULL DEFAULT 30,
  `TileY` int(11) NULL DEFAULT NULL,
  `MaxWeight` float NULL DEFAULT -1,
  `ItemInstanceId` bigint(11) NULL DEFAULT NULL,
  `CodeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_continers
-- ----------------------------
INSERT INTO `item_continers` VALUES (-1, 'Хранилище комиссионки', 0, 10, 100, 10000000, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (1, 'Инвентарь', 0, 10, 5, 50, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (2, 'Экипировка', 1, 20, 1, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (3, 'Лут', 2, 10, 15, -1, NULL, 'container_metal_small');
INSERT INTO `item_continers` VALUES (4, 'Рандом лут', 2, 10, 15, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (5, 'Карманы', 3, 10, 1, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (18, 'Сумка', 0, 5, 5, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (19, 'Экипировка', 1, 20, 1, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (20, 'Карманы', 3, 10, 1, -1, NULL, 'def_continer');
INSERT INTO `item_continers` VALUES (34, 'Сумка', 0, 10, 30, 60, 304, 'def_continer');
INSERT INTO `item_continers` VALUES (36, 'Сумка', 0, 10, 30, 60, 545, 'def_continer');
INSERT INTO `item_continers` VALUES (37, 'Сумка', 0, 10, 30, 60, 599, 'def_continer');
INSERT INTO `item_continers` VALUES (38, 'Сумка', 0, 10, 30, 60, 600, 'def_continer');
INSERT INTO `item_continers` VALUES (39, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (40, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (41, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (42, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (43, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (44, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (51, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (52, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (53, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (54, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (55, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (56, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (57, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (58, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (59, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (67, 'Ящик сталкера расширенный', 2, 10, 8, 150, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (69, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (70, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (71, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (72, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (73, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (74, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (75, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (76, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (77, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (78, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (79, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (80, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (81, 'Сумка', 0, 10, 5, 50, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (82, 'Экипировка', 1, 20, 1, -1, 0, 'def_continer');
INSERT INTO `item_continers` VALUES (83, 'Карманы', 3, 10, 1, -1, 0, 'def_continer');

-- ----------------------------
-- Table structure for item_effects
-- ----------------------------
DROP TABLE IF EXISTS `item_effects`;
CREATE TABLE `item_effects`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NULL DEFAULT NULL,
  `PropertyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ValueName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Time` float(11, 0) NULL DEFAULT -1,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_effects
-- ----------------------------
INSERT INTO `item_effects` VALUES (1, 7, 'bulletproof', 'MaxValue', '60', 'tors', -1);
INSERT INTO `item_effects` VALUES (2, 7, 'bulletproof', 'Value', '60', 'tors', -1);
INSERT INTO `item_effects` VALUES (4, 2, 'hunger', 'PerTime', '-10', 'buterbrod', 20);
INSERT INTO `item_effects` VALUES (5, 10, 'heal', 'Per', '-4', 'aptechka', 10);
INSERT INTO `item_effects` VALUES (6, 10, 'heal', 'PerTime', '20', 'aptechka', 10);
INSERT INTO `item_effects` VALUES (7, 52, 'irradiation', 'PerTime', '-99', 'gasmask', -1);
INSERT INTO `item_effects` VALUES (8, 33, 'heal', 'Value', '50', 'aptechka', -1);
INSERT INTO `item_effects` VALUES (9, 34, 'heal', 'PerTime', '10', 'aptechka', 20);
INSERT INTO `item_effects` VALUES (10, 35, 'hunger', 'Value', '50', 'buterbrod', 0);
INSERT INTO `item_effects` VALUES (11, 36, 'heal', 'PerTime', '10', 'buterbrod', 50);
INSERT INTO `item_effects` VALUES (12, 38, NULL, NULL, NULL, NULL, 0);
INSERT INTO `item_effects` VALUES (13, 40, NULL, NULL, NULL, NULL, 0);
INSERT INTO `item_effects` VALUES (16, 88, 'thirst', 'Value', '-100', 'сanned', -1);
INSERT INTO `item_effects` VALUES (17, 88, 'hunger', 'Value', '-100', 'сanned', -1);

-- ----------------------------
-- Table structure for item_template
-- ----------------------------
DROP TABLE IF EXISTS `item_template`;
CREATE TABLE `item_template`  (
  `InstanceId` int(11) NULL DEFAULT NULL,
  `ContinerId` int(11) NULL DEFAULT NULL,
  `ItemId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `Count` int(11) NULL DEFAULT 1,
  `PocketsCount` int(11) NULL DEFAULT 0,
  `IntType` int(11) NULL DEFAULT 0,
  `TypeOfUse` int(1) NULL DEFAULT 0,
  `Slot` int(11) NULL DEFAULT 0,
  `Desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomizationId` int(11) NULL DEFAULT NULL,
  `IsInfinite` tinyint(1) NULL DEFAULT 0,
  `SizeX` int(3) NULL DEFAULT 1,
  `SizeY` int(3) NULL DEFAULT 1,
  `SlotX` int(3) NULL DEFAULT 0,
  `SlotY` int(3) NULL DEFAULT 0,
  `Wear` float(4, 0) NULL DEFAULT 1,
  `Strength` float(4, 0) NULL DEFAULT 1,
  `Up` tinyint(1) NULL DEFAULT 0,
  `Premium` tinyint(1) NULL DEFAULT 0,
  `Personal` tinyint(1) NULL DEFAULT 0,
  `MaxCount` int(11) NULL DEFAULT 1,
  `Weight` float NULL DEFAULT 1,
  `ContinerType` int(11) NULL DEFAULT 0,
  `ExpiryDate` time(0) NULL DEFAULT NULL,
  `CanSpoil` tinyint(1) NULL DEFAULT 0,
  `ExpiredItemId` int(11) NULL DEFAULT NULL,
  `ItemContext` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Skin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InteractionTime` float NULL DEFAULT NULL,
  `WearMax` float(4, 0) NULL DEFAULT NULL,
  `StrengthMax` float(4, 0) NULL DEFAULT NULL,
  `Price` float(10, 2) NULL DEFAULT NULL,
  `DonatedPrice` float(10, 2) NULL DEFAULT NULL,
  `Filter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ItemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 173 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_template
-- ----------------------------
INSERT INTO `item_template` VALUES (0, 0, 1, 'Батон хлеба', 6, 0, 0, 0, 0, 'Батон хлеба. Вполне пригоден для употребления. Правда немного черствый.', 'spi_bread_wite_01', 0, 0, 2, 1, 0, 0, 100, 100, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_bread_wite_01', 0, 100, 100, 0.00, 0.00, 'null');
INSERT INTO `item_template` VALUES (0, 0, 2, 'Полбатона хлеба', 3, 0, 0, 0, 0, 'Полбатона хлеба. Вполне пригоден для употребления. Правда немного черствый.', 'spi_bread_wite_01_half', 0, 0, 1, 1, 0, 0, 100, 100, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_bread_wite_01_half', 0, 100, 100, 0.00, 0.00, 'null');
INSERT INTO `item_template` VALUES (3, 0, 3, 'Бронижелет', 1, 0, 1, 1, 1, 'Это бронижелет', 'armor_00', 4, 1, 2, 3, 0, 0, 1, 1, 0, 0, NULL, 1, 7, 0, '00:00:00', 0, 0, '3', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 4, 'Тестовое оружие', 1, 0, 2, 1, 2, 'Это тестовое оружие', 'pm_gm', 3, 0, 3, 2, 0, 0, 1, 1, 1, 0, 0, 1, 2, 0, '00:00:00', 0, 0, 'weapons', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (5, 0, 5, 'Штаны', 1, 0, 1, 1, 0, 'Штаны', 'pants_00', 5, 1, 2, 3, 0, 0, 1, 1, 0, 1, NULL, 1, 3, 0, '00:00:00', 0, 0, '3', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (6, 0, 6, 'Ботинки', 1, 0, 1, 1, 3, 'Ботинки', 'boots_00', 6, 1, 2, 2, 0, 0, 1, 1, 1, 1, NULL, 1, 1, 0, '00:00:00', 0, 0, '5', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (7, 0, 7, 'Куртка', 1, 0, 1, 1, 1, 'Куртка', 'armor_00', 7, 1, 2, 3, 0, 0, 1, 1, 0, 0, NULL, 1, 2, 0, '00:00:00', 0, 0, '3', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 8, 'Магазин ак 47', 0, 0, 3, 2, 0, 'Магазин автоматный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 30, 0.5, 0, '00:00:00', 0, 0, 'cages', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 9, 'Патроны для ак47', 200, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 200, 1, 0, '00:00:00', 0, 0, 'ammos', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 10, 'Батон ржаного хлеба*', 6, 0, 0, 2, 0, 'Батон ржаного хлеба с плесенью. Очень черствый. Почти весь покрыт плесенью. ', 'spi_bread_black_02', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 3, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_bread_black_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (12, 0, 11, 'Оптический прицел x2', 1, 0, 4, 0, 0, 'Оптический прицел x2', 'pso_1_gm', 0, 0, 1, 1, 0, 0, 100, 100, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, '0', 'spi_pso_01', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (52, 1, 12, 'Ремонтный набор', 10, 0, 6, -1, 0, 'Набор для ремонта предметов', 'makarov_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, NULL, 10, 1, 0, '12:48:24', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (53, 0, 13, 'Глушитель', 1, 0, 7, -1, 0, 'Глушитель', 'muzzle_nozzle', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, 'silencers', 'spi_pbs_01', 0, 1, 1, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 14, 'АКСУ 74', 1, 0, 2, 1, 4, '', 'ak47_gm', 15, 0, 5, 2, 0, 0, 1, 1, 0, 0, 0, 0, 3, 0, '00:00:00', 0, 0, 'weapons', 'null', 0, 1, 1, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 150, 150, 0, 0, 0, 0, 0.65, 0, '00:00:00', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `item_template` VALUES (0, 0, 16, 'Пм [GM] - Премиум', 1, 0, 2, 1, 2, 'Пм[GM] - Премиум', 'spi_pm_01', 30, 0, 3, 2, 0, 0, 150, 150, 0, 1, 0, 0, 0.65, 0, '00:00:00', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `item_template` VALUES (0, 0, 17, 'AK-47 [GM]', 1, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 0, 0, 100, 100, 0, 0, 0, 0, 4.3, 0, '00:00:00', 0, 0, 'weapons', 'spi_ak47', 0, 100, 100, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `item_template` VALUES (0, 0, 18, 'AK-47 [GM] - Премиум', 1, 0, 2, 1, 4, '', 'ak47_gm', 28, 0, 5, 2, 0, 0, 150, 150, 0, 1, 0, 0, 3.7, 0, '00:00:00', 0, 0, 'weapons', 'spi_ak47', 0, 150, 150, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `item_template` VALUES (0, 0, 19, 'ТОЗ 120 [GM]', 1, 0, 2, 1, 4, '', 'toz_120_gm', 18, 0, 5, 2, 0, 0, 100, 100, 0, 0, 0, 0, 3.4, 0, '00:00:00', 0, 0, 'weapons', 'null', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 20, 'ТОЗ 120 [GM] - Премиум', 0, 0, 2, 1, 4, '', 'toz_120_gm', 0, 0, 5, 2, 0, 0, 150, 150, 0, 1, 0, 0, 3, 0, '00:00:00', 0, 0, 'weapons', 'null', 0, 150, 150, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 21, 'Нож [GM]', 0, 0, 2, 1, 5, 'Нож армейский. Обыкновенный. Есть почти у каждого сталкера.', 'sti_knife_01', 0, 0, 2, 2, 0, 0, 100, 100, 0, 0, 0, 0, 0.1, 0, '00:00:00', 1, 0, 'weapons', 'spi_knife_01', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 22, 'Нож [GM] - Премиум', 0, 0, 2, 1, 5, 'Нож армейский, заточенный. Есть далеко не у всех сталкеров.', 'sti_knife_01', 0, 0, 2, 2, 0, 0, 150, 150, 0, 1, 0, 0, 0.1, 0, '00:00:00', 0, 0, 'weapons', 'spi_knife_01', 0, 150, 150, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 23, 'Шлем [GM]', 0, 0, 1, 1, 0, '', 'helmet_00', 0, 0, 2, 2, 0, 0, 100, 100, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, '5', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 24, 'Куртка [GM]', 0, 0, 1, 1, 1, '', 'armor_00', 0, 0, 2, 3, 0, 0, 100, 100, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, '3', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 25, 'Сумка [GM]', 1, 3, 8, 1, 0, '', 'bag_00', 0, 1, 2, 3, 0, 0, 100, 100, 0, 0, 0, 1, 2, 0, '00:00:00', 0, 0, '3', 'null', 1, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 26, 'Перчатки [GM]', 0, 0, 1, 1, 0, '', 'gloves_00', 0, 0, 2, 2, 0, 0, 100, 100, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, '5', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 27, 'Штаны [GM]', 0, 0, 1, 1, 0, '', 'pants_00', 0, 0, 2, 3, 0, 0, 100, 100, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, '3', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 28, 'Ботинки [GM]', 0, 0, 1, 1, 3, '', 'boots_00', 0, 0, 2, 2, 0, 0, 100, 100, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, '5', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 29, 'Магазин ПМ [GM]', 0, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 0, 0, 25, 25, 0, 0, 0, 8, 0.08, 0, '00:00:00', 0, 0, 'cages', 'spi_pm_01_cage', 5, 25, 25, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 30, 'Магазин ПМ [GM] - Премиум', 0, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 0, 0, 50, 50, 0, 1, 0, 8, 0.07, 0, '00:00:00', 0, 0, 'cages', 'spi_pm_01_cage', 5, 50, 50, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 31, 'Магазин АК-47 [GM]', 0, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 50, 50, 0, 0, 0, 30, 0.26, 0, '00:00:00', 0, 0, 'cages', 'spi_ak47_cage', 5, 50, 50, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 32, 'Магазин АК-47 [GM] - Премиум', 0, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 75, 0, 1, 0, 30, 0.2, 0, '00:00:00', 0, 0, 'cages', 'spi_ak47_cage', 5, 75, 75, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 33, 'Аптечка [GM]', 2, 0, 18, 2, 0, '', 'med_kit_00', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'null', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 34, 'Аптечка [GM] - Премиум', 3, 0, 18, 2, 0, '', 'med_kit_00', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'null', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 35, 'Батон хлеба*', 6, 0, 0, 0, 0, 'Батон хлеба с плесенью. Очень черствый. Почти весь покрыт плесенью. ', 'spi_bread_wite_02', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_bread_wite_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 36, 'Полбатона хлеба*', 3, 0, 0, 0, 0, 'Полбатона хлеба с плесенью. Очень черствый. Почти весь покрыт плесенью. ', 'spi_bread_wite_02_half', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_bread_wite_02_half', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 37, 'Полбатона ржаного хлеба*', 3, 0, 0, -1, 0, 'Полбатона ржаного хлеба с плесенью. Очень черствый. Почти весь покрыт плесенью. ', 'spi_bread_black_02_half', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_bread_black_02_half', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 38, 'ПБС -1 [GM]', 0, 0, 7, 0, 0, '', 'pbs_1_gm', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, '00:00:00', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 39, 'ПБС -1 [GM] - Премиум', 0, 0, 7, 0, 0, '', 'pbs_1_gm', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, NULL, 0, 0, 0, '00:00:00', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 40, 'ПСО -1 [GM]', 0, 0, 4, 0, 0, '', 'pso_1_gm', 0, 0, 1, 1, 0, 0, 100, 100, 0, 0, 0, 0, 0, 0, '00:00:00', 0, 0, 'null', 'null', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 41, 'ПСО -1 [GM] - Премиум', 0, 0, 4, 0, 0, '', 'pso_1_gm', 0, 0, 1, 1, 0, 0, 100, 100, 0, 1, 0, 0, 0, 0, '00:00:00', 0, 0, 'null', 'null', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 42, 'Патрон 7,62х39 - ПС', 250, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 0, '00:00:00', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 43, 'Патрон 7,62х39 - БП ', 250, 0, 5, 0, 0, '', '7-62x39_bp', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 0, '00:00:00', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 44, 'Патрон 7,62х39 - Премиум', 250, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 250, 0.012, 0, '00:00:00', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 45, 'Патрон 9х18 - ПС', 50, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.01, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 46, 'Патрон 9х18 - БП', 50, 0, 5, 0, 0, '', '9x18_bp', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.01, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 47, 'Патрон 9х18 - Премиум', 50, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 49, 'Дробь 12x70', 50, 0, 5, -1, 0, 'Патроны для дробовикуа', '12x70_0', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.025, 0, '00:00:00', 0, 0, 'ammos', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 50, 'Патронтаж', 0, 0, 3, 2, 0, '', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 1, 0, '00:00:00', 0, 0, 'cages', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 51, 'Противогаз', 1, 0, 9, 1, 10, 'Противогаз', 'gas_mask_gm', 23, 0, 2, 2, 0, 0, 0, 0, 0, 0, NULL, 1, 0.5, 0, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 52, 'Фильтр', 1, 0, 10, 1, 9, '', 'gas_filter_gm', 24, 0, 1, 2, 0, 0, 0, 0, 0, 0, NULL, 1, 0.5, 0, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 53, 'Счетчик гейгера', 1, 0, 11, 1, 8, '', 'doz_pripiat_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'device', 'doz_pripiat_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 54, 'Нож Армейский', 1, 0, 2, 1, 5, 'Нож армейский. Обыкновенный. Есть почти у каждого сталкера.', 'sti_knife_01', 25, 0, 2, 2, 0, 0, 100, 100, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'weapons', 'spi_knife_01', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 55, 'КПК', 1, 0, 12, 1, 12, '', 'spi_pda_default_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'devices', 'spi_dpa_01_low', 0, 0, 0, 0.00, 0.00, 'Оружие/Тест/Пистолеты');
INSERT INTO `item_template` VALUES (NULL, 0, 57, 'Карта приватности одежды', 1, 0, 14, -1, 0, 'Позволяет сделать предмет экипировки личным', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, '7', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 58, 'Карта приватности оружия', 1, 0, 14, -1, 0, '', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, '7', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 59, 'Карта приватности мелочи', 1, 0, 14, -1, 0, '', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, '7', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 60, 'Батон ржаного хлеба', 6, 0, 0, 0, 0, 'Батон ржаного хлеба. Вполне пригоден для употребления. Правда немного черствый.', 'spi_bread_black_01', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_bread_black_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 61, 'Полбатона ржаного хлеба', 3, 0, 0, 0, 0, 'Полбатона ржаного хлеба. Вполне пригоден для употребления. Правда немного черствый.', 'spi_bread_black_01_half', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_bread_black_01_half', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 62, 'Колбаса полукопченая', 6, 0, 0, 0, 0, 'Колбаса полукопченая. Пригодня для употребления.', 'spi_sausage_a', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 63, 'Колбаса докторская', 6, 0, 0, 0, 0, 'Колбаса докторская. Пригодня для употребления.', 'spi_sausage_b', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 64, 'Кусок колбасы полукопченой', 3, 0, 0, 0, 0, 'Кусок колбасы полукопченой. Пригодня для употребления.', 'spi_sausage_half_a', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.15, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_half_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 65, 'Кусок колбаса докторской', 3, 0, 0, 0, 0, 'Кусок колбаса докторской. Пригодня для употребления.', 'spi_sausage_half_b', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_half_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 66, 'Колбаса полукопченая*', 6, 0, 0, 0, 0, 'Испорченная полукопченая колбаса. Источает мерзкий запах. Есть такое не рекомендуется.', 'spi_sausage_old_a', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_old_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 67, 'Колбаса докторская*', 6, 0, 0, 0, 0, 'Испорченная докторская колбаса. Источает мерзкий запах. Есть такое не рекомендуется.', 'spi_sausage_old_b', 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_old_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 68, 'Кусок колбасы полукопченой*', 3, 0, 0, 0, 0, 'Кусок испорченной полукопчёной колбасы. Источает мерзкий запах. Есть такое не рекомендуется.', 'spi_sausage_old_half_a', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.15, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_old_half_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 69, 'Кусок колбаса докторской*', 3, 0, 0, 0, 0, 'Кусок испорченной докторской колбасы. Источает мерзкий запах. Есть такое не рекомендуется.', 'spi_sausage_old_half_b', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_sausage_old_half_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 71, 'Ломтик черного хлеба', 1, 0, 0, 0, 0, 'Ломтик ржаного хлеба. Если добавить колбасы, получится вкусный бутерброд.', 'spi_slice_black_bread', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 72, 'Ломтик черного хлеба*', 1, 0, 0, 0, 0, 'Ломтик испорченного ржаного хлеба. Есть такое не рекомендуется.', 'spi_slice_black_bread_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 73, 'Ломтик хлеба', 1, 0, 0, 0, 0, 'Ломтик хлеба. Если добавить колбасы, получится вкусный бутерброд.', 'spi_slice_wite_bread', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 74, 'Ломтик хлеба*', 1, 0, 0, 0, 0, 'Ломтик испорченного хлеба. Есть такое не рекомендуется.', 'spi_slice_wite_bread_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 75, 'Бутерброд', 1, 0, 0, 0, 0, 'Вкусный бутерброд с полукопченой колбасой.', 'spi_slice_black_bread_sausage_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread_sausage_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 76, 'Бутерброд*', 1, 0, 0, 0, 0, 'Испорченный бутерброд с полукопченой колбасой.', 'spi_slice_black_bread_sausage_01_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread_sausage_01_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 77, 'Бутерброд', 1, 0, 0, 0, 0, 'Вкусный бутерброд с докторской колбасой.', 'spi_slice_black_bread_sausage_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread_sausage_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 78, 'Бутерброд*', 1, 0, 0, 0, 0, 'Испорченный бутерброд с докторской колбасой.', 'spi_slice_black_bread_sausage_02_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_black_bread_sausage_02_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 79, 'Бутерброд', 1, 0, 0, 0, 0, 'Вкусный бутерброд из ржаного хлеба с полукопченой колбасой.', 'spi_slice_wite_bread_sausage_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread_sausage_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 80, 'Бутерброд*', 1, 0, 0, 0, 0, 'Испорченный бутерброд из ржаного хлеба с полукопченой колбасой.', 'spi_slice_wite_bread_sausage_01_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread_sausage_01_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 81, 'Бутерброд', 1, 0, 0, 0, 0, 'Вкусный бутерброд из ржаного хлеба с докторской колбасой.', 'spi_slice_wite_bread_sausage_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread_sausage_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 82, 'Бутерброд*', 1, 0, 0, 0, 0, 'Испорченный бутерброд из ржаного хлеба с докторской колбасой.', 'spi_slice_wite_bread_sausage_02_bad', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'food', 'spi_slice_wite_bread_sausage_02_bad', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 83, 'Молоко в бутылке', 6, 0, 0, 0, 0, 'Молоко в бутылке. Странно что не испортилось.', 'spi_milk_01', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_milk_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 84, 'Молоко в пакете', 3, 0, 0, 0, 0, 'Молоко в пакете. Странно что не испортилось.', 'spi_milk_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_milk_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 85, 'Кефир в бутылке', 6, 0, 0, 0, 0, 'Кефир в бутылке. Странно что не испортился.', 'spi_kefir_01', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.8, 0, '00:00:00', 0, 0, 'food', 'spi_kefir_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 86, 'Кефир в пакете', 3, 0, 0, 0, 0, 'Кефир в пакете. Странно что не испортился.', 'spi_kefir_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.4, 0, '00:00:00', 0, 0, 'food', 'spi_kefir_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 87, 'Йогурт', 3, 0, 0, 0, 0, 'Йогурт в бутылке. Странно что не испортился.', 'spi_yogurt_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_yogurt_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 88, 'Большая консерва', 1, 0, 0, 0, 0, 'Тушенная говядина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_01', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'details', 'spi_con_big_01', 0, 1, 1, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 89, 'Большая консерва', 1, 0, 15, -1, 0, 'Тушенная свинина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_02', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'details', 'spi_con_big_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 90, 'Большая консерва', 1, 0, 15, -1, 0, 'Гречневая каша с тушенной свининой. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_03', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'details', 'spi_con_big_03', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 91, 'Большая консерва', 1, 0, 15, -1, 0, 'Тушенная оленина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_04', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'details', 'spi_con_big_04', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 92, 'Средняя консерва', 1, 0, 15, -1, 0, 'Голец, лосось натуральный, дальневосточный. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 93, 'Средняя консерва', 1, 0, 15, -1, 0, 'Тушенка солдатская, натуральная. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 94, 'Средняя консерва', 1, 0, 15, -1, 0, 'Салака, тушки. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_03', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_03', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 95, 'Средняя консерва', 1, 0, 15, -1, 0, 'Говядина тушенная, высший сорт. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_04', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_04', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 96, 'Средняя консерва', 1, 0, 15, -1, 0, 'Каша гречневая с тушенной говядиной. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_05', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_05', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 97, 'Средняя консерва', 1, 0, 15, -1, 0, 'Горошек зеленый. Можно использовать как ингридиент в приготовлении салатов. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_06', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_06', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 98, 'Средняя консерва', 1, 0, 15, -1, 0, 'икра кабачковая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_07', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_07', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 99, 'Средняя консерва', 1, 0, 15, -1, 0, 'Куропатка в бульоне. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_08', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'details', 'spi_con_med_08', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 100, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Салака, тушки. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_01', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 101, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Горбуша. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_02', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 102, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Килька в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_03', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_03', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 103, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Тушенка солдатская, из говядины. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_04', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_04', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 104, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Килька в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_05', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_05', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 105, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Паштет. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_06', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_06', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 106, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Судак в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_07', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_07', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 107, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Каша перловая со свининой. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_08', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_08', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 108, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Икра черная. Осетровая, зернистая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_09', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_09', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 109, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Печень трески. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_10', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_10', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 110, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Лещь в томатном соусе.  Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_11', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_11', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 111, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Икра кабачковая баклажанная. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_12', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_12', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 112, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Каша гречневая с тушенной говядиной. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_13', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_13', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 113, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Икра красная лососевая зернистая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_14', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_14', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 114, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Ряпушка. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_15', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_15', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 115, 'Маленькая консерва', 1, 0, 15, -1, 0, 'Шпроты в масле. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_16', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'details', 'spi_con_small_16', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 116, 'Большая консерва', 3, 0, 0, 0, 0, 'Тушенная говядина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_01_open', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'food', 'spi_con_big_01_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 117, 'Большая консерва', 3, 0, 0, 0, 0, 'Тушенная свинина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_02_open', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'food', 'spi_con_big_02_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 118, 'Большая консерва', 3, 0, 0, 0, 0, 'Гречневая каша с тушенной свининой. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_03_open', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'food', 'spi_con_big_03_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 119, 'Большая консерва', 3, 0, 0, 0, 0, 'Тушенная оленина. Если не открывать, останется свежей еще очень долго. ', 'spi_con_big_04_open', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'food', 'spi_con_big_04_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 120, 'Средняя консерва', 2, 0, 0, 0, 0, 'Голец, лосось натуральный, дальневосточный. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_01_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_01_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 121, 'Средняя консерва', 2, 0, 0, 0, 0, 'Тушенка солдатская, натуральная. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_02_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_02_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 122, 'Средняя консерва', 2, 0, 0, 0, 0, 'Салака, тушки. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_03_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_03_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 123, 'Средняя консерва', 2, 0, 0, 0, 0, 'Говядина тушенная, высший сорт. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_04_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_04_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 124, 'Средняя консерва', 2, 0, 0, 0, 0, 'Каша гречневая с тушенной говядиной. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_05_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_05_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 125, 'Средняя консерва', 2, 0, 0, 0, 0, 'Горошек зеленый. Можно использовать как ингридиент в приготовлении салатов. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_06_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_06_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 126, 'Средняя консерва', 2, 0, 0, 0, 0, 'икра кабачковая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_07_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_07_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 127, 'Средняя консерва', 2, 0, 0, 0, 0, 'Куропатка в бульоне. Если не открывать, останется свежей еще очень долго. ', 'spi_con_med_08_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.3, 0, '00:00:00', 0, 0, 'food', 'spi_con_med_08_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 128, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Салака, тушки. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_01_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_01_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 129, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Горбуша. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_02_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_02_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 130, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Килька в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_03_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_03_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 131, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Тушенка солдатская, из говядины. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_04_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_04_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 132, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Килька в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_05_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_05_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 133, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Паштет. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_06_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_06_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 134, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Судак в томатном соусе. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_07_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_07_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 135, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Каша перловая со свининой. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_08_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_08_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 136, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Икра черная. Осетровая, зернистая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_09_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_09_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 137, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Печень трески. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_10_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_10_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 138, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Лещь в томатном соусе.  Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_11_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_11_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 139, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Икра кабачковая баклажанная. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_12_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_12_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 140, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Каша гречневая с тушенной говядиной. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_13_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_13_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 141, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Икра красная лососевая зернистая. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_14_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_14_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 142, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Ряпушка. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_15_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_15_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 143, 'Маленькая консерва', 1, 0, 0, 0, 0, 'Шпроты в масле. Если не открывать, останется свежей еще очень долго. ', 'spi_con_small_16_open', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.1, 0, '00:00:00', 0, 0, 'food', 'spi_con_small_16_open', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 153, 'Большая пустая консерва', 1, 0, 15, 0, 0, 'Большая пустая консерва. Внутри кроме грязи на стенках ничего нет.', 'spi_con_big', 0, 0, 1, 2, 0, 0, 1, 1, 0, 0, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'details', 'spi_con_big', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 154, 'Средняя пустая консерва', 1, 0, 15, 0, 0, 'Средняя пустая консерва. Внутри кроме грязи на стенках ничего нет.', 'spi_con_med', 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'details', 'spi_con_med', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 155, 'Маленькая пустая консерва', 0, 0, 0, 0, 0, 'Маленькая пустая консерва. Внутри кроме грязи на стенках ничего нет.', 'spi_con_small', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '00:00:00', 0, 0, 'details', 'spi_con_small', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (NULL, 0, 156, 'Фонарик', 1, 0, 16, 1, 11, '', 'spi_flashlight_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, 'devices', 'devices_01', 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 158, 'Стерильный бинт', 1, 0, 19, 2, 0, 'Бинт стерильный. Поможет остановить легкое кровотечение.', 'spi_bandage_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'med_kit', 'spi_bandage_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 159, 'Перевязочный комплект', 2, 0, 19, 2, 0, 'Бинт стерильный с перекисью водорода. Позволит обработать рану и остановить среднее кровотечение.', 'spi_bandage_02', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0.02, 0, '00:00:00', 0, 0, 'med_kit', 'spi_bandage_02', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 160, 'Перевязочный комплект', 3, 0, 19, 2, 0, 'Резиновый эластичный жгут, бинт стерильный с перекисью водорода. Позволит обработать рану и остановить тяжелое кровотечение.', 'spi_bandage_03', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3, 0.03, 0, '00:00:00', 0, 0, 'med_kit', 'spi_bandage_03', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 161, 'АИ', 1, 0, 18, 2, 0, 'Аптечка индивидуальная.', 'spi_med_small_a', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0.2, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_small_a', 0, 0, 0, 100.00, 100.00, NULL);
INSERT INTO `item_template` VALUES (0, 0, 162, 'АИ-Н', 2, 0, 18, 2, 0, 'Аптечка индивидуальная научная.', 'spi_med_small_b', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0.17, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_small_b', 0, 0, 0, 200.00, 200.00, NULL);
INSERT INTO `item_template` VALUES (0, 0, 163, 'АИ-А', 3, 0, 18, 2, 0, 'Аптечка индивидуальная армейская.', 'spi_med_small_c', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3, 0.15, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_small_c', 0, 0, 0, 300.00, 300.00, NULL);
INSERT INTO `item_template` VALUES (0, 0, 164, 'АИ-АС', 4, 0, 18, 2, 0, 'Аптечка индивидуальная, армейская специальная.', 'spi_med_small_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 4, 0.1, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_small_prem', 0, 0, 0, 0.00, 300.00, NULL);
INSERT INTO `item_template` VALUES (0, 0, 165, 'АПП', 1, 0, 18, 0, 0, 'Аптечка первой помощи, индивидуальная.', 'spi_med_mid_a', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0.5, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_mid_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 166, 'АПП-Н', 2, 0, 18, 0, 0, 'Аптечка первой помощи, научная.', 'spi_med_mid_b', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0.45, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_mid_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 167, 'АПП-А', 3, 0, 18, 0, 0, 'Аптечка первой помощи, армейская.', 'spi_med_mid_c', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 3, 0.4, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_mid_c', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 168, 'АПП-АС', 4, 0, 18, 0, 0, 'Аптечка первой помощи, армейская специальная.', 'spi_med_mid_prem', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 4, 0.35, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_mid_prem', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 169, 'ИПП', 1, 0, 18, 0, 0, 'Индивидуальный перевязочный пакет.', 'spi_med_big_a', 0, 0, 2, 3, 0, 0, 0, 0, 0, 0, 0, 1, 2.5, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_big_a', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 170, 'ИПП-Н', 2, 0, 18, 0, 0, 'Индивидуальный перевязочный пакет, научный.', 'spi_med_big_b', 0, 0, 2, 3, 0, 0, 0, 0, 0, 0, 0, 2, 2.25, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_big_b', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 171, 'ИПП-А', 3, 0, 18, 0, 0, 'Индивидуальный перевязочный пакет, армейский.', 'spi_med_big_c', 0, 0, 2, 3, 0, 0, 0, 0, 0, 0, 0, 3, 2, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_big_c', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `item_template` VALUES (0, 0, 172, 'ИПП-АС', 4, 0, 18, 0, 0, 'Индивидуальный перевязочный пакет, армейский специальный.', 'spi_med_big_prem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, '00:00:00', 0, 0, 'med_kit', 'spi_med_big_prem', 0, 0, 0, 15000.00, 5000.00, NULL);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `InstanceId` int(11) NOT NULL AUTO_INCREMENT,
  `ContinerId` int(11) NULL DEFAULT NULL,
  `ItemId` int(11) NULL DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `Count` int(11) NULL DEFAULT 1,
  `PocketsCount` int(11) NULL DEFAULT 0,
  `IntType` int(11) NULL DEFAULT 0,
  `TypeOfUse` int(1) NULL DEFAULT 0,
  `Slot` int(11) NULL DEFAULT 0,
  `Desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomizationId` int(11) NULL DEFAULT NULL,
  `IsInfinite` tinyint(1) NULL DEFAULT 0,
  `SizeX` int(3) NULL DEFAULT 1,
  `SizeY` int(3) NULL DEFAULT 1,
  `SlotX` int(3) NULL DEFAULT 0,
  `SlotY` int(3) NULL DEFAULT 0,
  `Wear` float NULL DEFAULT 100,
  `Strength` float NULL DEFAULT 0,
  `Up` tinyint(1) NULL DEFAULT 0,
  `Premium` tinyint(1) NULL DEFAULT 0,
  `Personal` tinyint(1) NULL DEFAULT 0,
  `MaxCount` int(11) NULL DEFAULT 1,
  `Weight` float NULL DEFAULT 1,
  `ContinerType` int(11) NULL DEFAULT 0,
  `ExpiryDate` time(0) NULL DEFAULT NULL,
  `CanSpoil` tinyint(1) NULL DEFAULT 0,
  `ExpiredItemId` int(11) NULL DEFAULT NULL,
  `ItemContext` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Skin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InteractionTime` float NULL DEFAULT NULL,
  `WearMax` float(4, 0) NULL DEFAULT NULL,
  `StrengthMax` float(4, 0) NULL DEFAULT NULL,
  `Price` float(10, 2) NULL DEFAULT NULL,
  `DonatedPrice` float(10, 2) NULL DEFAULT NULL,
  `Filter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`InstanceId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1358 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (133, 156, 8, 'Магазин пистолетный', 0, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 0.5, 1, '16:20:37', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (134, 169, 8, 'Магазин пистолетный', 0, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 0.5, 1, '16:35:15', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (135, 31, 8, 'Магазин пистолетный', 0, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 0.5, 0, '16:03:32', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (136, 31, 8, 'Магазин пистолетный', 0, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 12, 0.5, 0, '16:03:24', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (139, 133, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 12, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (147, 4, 5, 'Штаны', 1, 12, 1, 1, 0, 'Штаны', 'mara_01', 5, 1, 2, 3, 0, 0, 0, -106.23, 0, 1, 0, 1, 3, 0, '16:04:41', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (150, 134, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 12, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (155, 104, 11, 'Оптический прицел x2', 1, 0, 4, 0, 0, 'Оптический прицел x2', 'makarov_01', 0, 0, 1, 1, 0, 5, 1, 0, 0, 0, 0, 1, 0, 1, '09:03:07', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (162, 31, 8, 'Магазин пистолетный', 0, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 30, 0.5, -1, '06:11:48', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (215, 135, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 12, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (216, 136, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 12, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (252, 326, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (254, 162, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 30, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (256, 104, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (260, 259, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 30, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (261, 256, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 100, 0, 1, 1, 0, 30, 1, 1, '12:53:31', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (263, 326, 8, 'Магазин пистолетный', 27, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (265, 302, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (274, 4, 9, 'Патроны для пистолета', 200, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 200, 1, 0, '12:48:28', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (275, 4, 2, 'Колбаса', 1, 66, 0, 0, 0, 'Кусок старой колбасы', 'buter_01', 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0.5, 0, '12:48:39', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (276, 4, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 0, '12:48:32', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (277, 4, 9, 'Патроны для пистолета', 200, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 200, 1, 0, '12:48:28', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (279, 4, 9, 'Патроны для пистолета', 200, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 200, 1, 0, '12:48:28', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (297, 252, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '12:48:28', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (298, 265, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '12:48:28', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (308, 575, 8, 'Магазин пистолетный', 13, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '23:26:02', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (331, 607, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '29:30:02', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (336, 445, 8, 'Магазин пистолетный', 2, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (340, 305, 8, 'Магазин пистолетный', 20, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '24:00:45', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (343, 576, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '24:13:03', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (350, 336, 9, 'Патроны для пистолета', 2, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '12:48:28', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (352, 340, 9, 'Патроны для пистолета', 20, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '12:48:28', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (428, 326, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '24:49:08', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (432, 392, 9, 'Патроны для пистолета', 28, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '24:34:57', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (433, 421, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '24:34:57', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (434, 427, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '24:34:57', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (450, 448, 49, 'Дробь 12x70', 1, 0, 5, -1, 0, 'Патроны для дробовикуа', '12x70_0', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, '16:16:06', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (453, 451, 50, 'Патронтаж', 11, 0, 3, 2, 0, '', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 1, 1, '16:20:34', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (454, 453, 49, 'Дробь 12x70', 11, 0, 5, -1, 0, 'Патроны для дробовикуа', '12x70_0', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 12, 1, 1, '16:20:31', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (473, 326, 11, 'Оптический прицел x2', 1, 0, 4, 0, 0, 'Оптический прицел x2', 'pso_1_gm', 0, 0, 1, 1, 0, 3, 1, 0, 0, 0, 0, 1, 0, 1, '17:13:25', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (504, 263, 9, 'Патроны для пистолета', 27, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '28:27:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (505, 308, 9, 'Патроны для пистолета', 13, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '28:27:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (569, 343, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '24:42:09', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (584, 428, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '25:09:59', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (588, 596, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 3, 1, 0, 0, 0, 0, 30, 0.5, 1, '22:52:09', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (604, 627, 11, 'Оптический прицел x2', 1, 0, 4, 0, 0, 'Оптический прицел x2', 'pso_1_gm', 0, 0, 1, 1, 3, 2, 1, 0, 0, 0, 0, 1, 0, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (608, 331, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:04:58', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (619, 612, 45, 'Патрон 9х18 - ПС', 0, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '14:57:38', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (622, 598, 29, 'Магазин ПМ [GM]', 8, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 2, 1, 25, 0, 0, 0, 0, 8, 0.08, 1, '15:21:19', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (623, 622, 46, 'Патрон 9х18 - БП', 8, 0, 5, 0, 0, '', '9x18_bp', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 8, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (624, 620, 45, 'Патрон 9х18 - ПС', 8, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 3, 1, 0, 0, 0, 0, 0, 8, 0, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (635, 706, 9, 'Патроны для пистолета', 20, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 200, 1, 1, '20:01:54', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (641, 628, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (652, 314, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:04:18', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (671, 354, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 5, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (674, 316, 9, 'Патроны для пистолета', 22, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '12:28:18', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (680, 627, 8, 'Магазин пистолетный', 30, 8, 3, 2, 0, 'Магазин пистолетный', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 30, 0.5, 1, '12:48:32', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (686, 680, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 4, 2, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (687, 338, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 5, 2, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (688, 588, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 3, 3, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (689, 676, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 4, 2, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (690, 338, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 5, 2, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (691, 596, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 6, 2, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (693, 680, 9, 'Патроны для пистолета', 24, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 8, 2, 1, 0, 1, 1, 0, 30, 1, 1, '10:04:37', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (695, 316, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (696, 680, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (697, 676, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (698, 680, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (699, 588, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (700, 338, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '10:25:50', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (701, 680, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (702, 676, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (703, 338, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (710, 338, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (711, 680, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (712, 588, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (714, 338, 9, 'Патроны для пистолета', 6, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (718, 676, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (719, 706, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (720, 338, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (730, 338, 9, 'Патроны для ак47', 3, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 2, 2, 1, 0, 1, 1, 0, 200, 1, 1, '00:00:00', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (731, 338, 9, 'Патроны для ак47', 6, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 3, 2, 1, 0, 1, 1, 0, 200, 1, 1, '17:16:05', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (732, 338, 9, 'Патроны для ак47', 9, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 4, 2, 1, 0, 1, 1, 0, 200, 1, 1, '34:32:36', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (734, 338, 9, 'Патроны для ак47', 12, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 5, 2, 1, 0, 1, 1, 0, 200, 1, 1, '69:06:06', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (735, 338, 9, 'Патроны для ак47', 15, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 6, 2, 1, 0, 1, 1, 0, 200, 1, 1, '86:23:21', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (736, 676, 9, 'Патроны для ак47', 3, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 3, 2, 1, 0, 1, 1, 0, 200, 1, 1, '00:00:00', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (738, 338, 9, 'Патроны для ак47', 0, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '34:52:38', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (739, 588, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (741, 680, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (742, 676, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (743, 721, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '20:01:54', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (744, 706, 9, 'Патроны для пистолета', 0, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (749, 680, 9, 'Патроны для ак47', 0, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (750, 676, 9, 'Патроны для ак47', 0, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (753, 680, 9, 'Патроны для ак47', 6, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 1, 3, 1, 0, 1, 1, 0, 200, 1, 1, '00:00:00', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (754, 680, 9, 'Патроны для ак47', 3, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 2, 3, 1, 0, 1, 1, 0, 200, 1, 1, '00:00:00', 0, 0, '8', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (755, 680, 9, 'Патроны для ак47', 0, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (756, 588, 9, 'Патроны для ак47', 0, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (769, 723, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 6, 2, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (770, 680, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (771, 706, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (772, 721, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (773, 676, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (774, 338, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (787, 588, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '16:42:59', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (788, 783, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 3, 1, 0, 1, 1, 0, 30, 1, 1, '17:36:28', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (789, 354, 9, 'Патроны для ак47', 30, 0, 5, 0, 0, 'Патроны для ак 47', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '16:42:59', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (791, 676, 9, 'Патроны для пистолета', 30, 0, 5, 0, 0, 'Патроны для пистолета', '5,45x39_bp', 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 30, 1, 1, '11:13:46', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (810, 827, 31, 'Магазин АК-47 [GM]', 18, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 2, 0, 50, 0, 0, 0, 0, 30, 0.26, 1, '00:00:00', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (814, 810, 42, 'Патрон 7,62х39 - ПС', 0, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 30, 0.016, 1, '15:46:02', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (817, 810, 42, 'Патрон 7,62х39 - ПС', 0, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 30, 0.016, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (823, 810, 42, 'Патрон 7,62х39 - ПС', 12, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (828, 825, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 0, 0, 1, 0, 30, 0.2, 1, '14:37:55', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (829, 828, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (830, 810, 42, 'Патрон 7,62х39 - ПС', 18, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (831, 40, 18, 'AK-47 [GM] - Премиум', 30, 0, 2, 1, 4, '', 'ak47_gm', 28, 0, 5, 2, 0, 0, 149.6, 0.291008, 0, 1, 0, 250, 3.7, 0, '11:45:24', 0, 0, '2', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (833, 831, 31, 'Магазин АК-47 [GM]', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 50, 0, 0, 0, 0, 30, 0.26, 1, '11:46:45', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (834, 832, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 0, 0, 1, 0, 30, 0.2, 1, '11:46:53', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (836, 833, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '11:47:30', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (855, 853, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 6, 0, 75, 75, 0, 1, 0, 30, 0.2, 1, '16:41:02', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (857, 855, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '16:41:31', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (858, 861, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 75, 0, 1, 0, 30, 0.2, 1, '17:38:01', 0, 0, '0', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (865, 858, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '17:55:59', 0, 0, '0', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (897, 894, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 75, 0, 1, 0, 30, 0.2, 1, '01:33:32', 0, 0, '0', 'sti_ak47_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (898, 897, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '01:33:15', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (944, 922, 31, 'Магазин АК-47 [GM]', 10, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 50, 50, 0, 0, 0, 30, 0.26, 1, '13:06:38', 0, 0, 'cages', 'spi_ak47_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (945, 944, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:07:14', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (947, 834, 42, 'Патрон 7,62х39 - ПС', 30, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '14:10:59', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (950, 39, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 250, 0.012, 0, '00:00:00', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (954, 955, 30, 'Магазин ПМ [GM] - Премиум', 7, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 8, 0, 50, 50, 0, 1, 0, 8, 0.07, 1, '00:00:00', 0, 0, 'cages', 'spi_pm_01_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (956, 39, 45, 'Патрон 9х18 - ПС', 25, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0.01, 0, '16:08:05', 0, 0, 'ammos', 'null', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (957, 954, 45, 'Патрон 9х18 - ПС', 7, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '16:08:05', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (959, 40, 15, 'Пм [GM]', 7, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 150, 149.803, 0, 0, 0, 50, 0.65, 0, '16:24:56', 0, 0, 'weapons', 'spi_pm_01', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (960, 959, 29, 'Магазин ПМ [GM]', 7, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 0, 0, 25, 25, 0, 0, 0, 8, 0.08, 1, '16:25:11', 0, 0, 'cages', 'spi_pm_01_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (961, 960, 45, 'Патрон 9х18 - ПС', 7, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 5, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '16:08:05', 0, 0, NULL, NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (965, 41, 156, 'Фонарик', 1690, 0, 16, 2, 0, '', 'null', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0.01, 0, '11:04:07', 0, 0, 'details', 'spi_con_small', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (967, 41, 29, 'Магазин ПМ [GM]', 6, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 0, 0, 25, 25, 0, 0, 0, 8, 0.08, 0, '00:00:00', 0, 0, 'cages', 'spi_pm_01_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (968, 1297, 32, 'Магазин АК-47 [GM] - Премиум', 10, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, -1, -1, 75, 75, 0, 1, 0, 30, 0.2, 1, '17:46:55', 0, 0, 'cages', 'spi_ak47_cage', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (974, 967, 45, 'Патрон 9х18 - ПС', 6, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 3, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '16:08:05', 0, 0, 'ammos', NULL, NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (999, 42, 17, 'AK-47 [GM]', 10, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 0, 0, 100, 99.8071, 0, 0, 0, 250, 4.3, 0, '09:51:56', 0, 0, 'weapons', 'spi_ak47', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1000, 999, 32, 'Магазин АК-47 [GM] - Премиум', 10, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, -1, -1, 75, 75, 0, 1, 0, 30, 0.2, 1, '09:52:01', 0, 0, 'cages', 'spi_ak47_cage', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1001, 1000, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 6, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '09:52:05', 0, 0, 'ammos', 'Cube', NULL, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1004, 1039, 42, 'Патрон 7,62х39 - ПС', 22, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1008, 1173, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, -1, -1, 75, 75, 0, 1, 0, 30, 0.2, 1, '00:00:00', 0, 0, 'cages', 'spi_ak47_cage', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1013, 1109, 30, 'Магазин ПМ [GM] - Премиум', 8, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, 5, 0, 50, 50, 0, 1, 0, 8, 0.07, 1, '20:57:45', 0, 0, 'cages', 'spi_pm_01_cage', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1014, 1009, 43, 'Патрон 7,62х39 - БП ', 10, 0, 5, 0, 0, '', '7-62x39_bp', 0, 0, 1, 1, 3, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1018, 1039, 44, 'Патрон 7,62х39 - Премиум', 28, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1021, 1013, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '20:59:20', 0, 0, 'ammos', NULL, 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1023, 1022, 45, 'Патрон 9х18 - ПС', 8, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '20:59:24', 0, 0, 'ammos', NULL, 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1028, 1282, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, -1, -1, 75, 75, 0, 1, 0, 30, 0.2, 1, '09:55:52', 0, 0, 'cages', 'spi_ak47_cage', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1030, 1012, 30, 'Магазин ПМ [GM] - Премиум', 8, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, -1, -1, 50, 50, 0, 1, 0, 8, 0.07, 1, '00:00:00', 0, 0, 'cages', 'spi_pm_01_cage', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1037, 67, 162, 'АИ-Н', 1, 0, 18, 2, 0, 'Аптечка индивидуальная научная.', 'spi_med_small_b', 0, 0, 1, 1, 2, 1, 0, 0, 0, 0, 0, 2, 0.17, 0, '09:56:13', 0, 0, 'med_kit', 'spi_med_small_b', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1042, 1030, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1099, 1039, 44, 'Патрон 7,62х39 - Премиум', 20, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 3, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:23:47', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1100, 1028, 43, 'Патрон 7,62х39 - БП ', 10, 0, 5, 0, 0, '', '7-62x39_bp', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:23:51', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1101, 1039, 42, 'Патрон 7,62х39 - ПС', 24, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1103, 1095, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:41:50', 0, 0, 'ammos', 'Cube', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1104, 1095, 44, 'Патрон 7,62х39 - Премиум', 6, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 3, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:42:00', 0, 0, 'ammos', 'Cube', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1105, 1095, 43, 'Патрон 7,62х39 - БП ', 26, 0, 5, 0, 0, '', '7-62x39_bp', 0, 0, 1, 1, 3, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:42:07', 0, 0, 'ammos', 'Cube', 0, 100, 100, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1108, 1175, 30, 'Магазин ПМ [GM] - Премиум', 8, 0, 3, 2, 0, '', 'cage_pm_gm', 0, 0, 1, 1, -1, -1, 50, 50, 0, 1, 0, 8, 0.07, 1, '09:40:01', 0, 0, 'cages', 'spi_pm_01_cage', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1111, 1108, 45, 'Патрон 9х18 - ПС', 6, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 5, 2, 0, 0, 0, 0, 0, 50, 0.01, 1, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1112, 1108, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 4, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1115, 1108, 46, 'Патрон 9х18 - БП', 2, 0, 5, 0, 0, '', '9x18_bp', 0, 0, 1, 1, 7, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1118, 1095, 42, 'Патрон 7,62х39 - ПС', 12, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 8, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1119, 1095, 44, 'Патрон 7,62х39 - Премиум', 16, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 9, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:42:00', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1120, 1108, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 6, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1121, 1108, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 6, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1124, 1108, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 4, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1126, 1108, 47, 'Патрон 9х18 - Премиум', 6, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1127, 1108, 45, 'Патрон 9х18 - ПС', 2, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1128, 1108, 47, 'Патрон 9х18 - Премиум', 2, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1129, 1028, 44, 'Патрон 7,62х39 - Премиум', 26, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '13:42:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1130, 1108, 47, 'Патрон 9х18 - Премиум', 6, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1131, 1108, 47, 'Патрон 9х18 - Премиум', 2, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1132, 1108, 47, 'Патрон 9х18 - Премиум', 6, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1133, 1108, 47, 'Патрон 9х18 - Премиум', 2, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1134, 1108, 47, 'Патрон 9х18 - Премиум', 6, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1135, 1174, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1136, 1108, 47, 'Патрон 9х18 - Премиум', 4, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1138, 1039, 42, 'Патрон 7,62х39 - ПС', 18, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1139, 1095, 44, 'Патрон 7,62х39 - Премиум', 30, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1140, 1028, 44, 'Патрон 7,62х39 - Премиум', 30, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1141, 1039, 44, 'Патрон 7,62х39 - Премиум', 4, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1147, 1095, 42, 'Патрон 7,62х39 - ПС', 16, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1148, 1101, 42, 'Патрон 7,62х39 - ПС', 12, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1149, 1039, 42, 'Патрон 7,62х39 - ПС', 20, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1150, 1039, 42, 'Патрон 7,62х39 - ПС', 4, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, -1, -1, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1151, 1008, 44, 'Патрон 7,62х39 - Премиум', 30, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1152, 1039, 42, 'Патрон 7,62х39 - ПС', 4, 0, 5, 0, 0, '', '7-62x39_ps', 0, 0, 1, 1, 3, 0, 0, 0, 0, 0, 0, 250, 0.016, 1, '13:41:48', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1154, 1314, 44, 'Патрон 7,62х39 - Премиум', 30, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 250, 0.012, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1158, 1114, 45, 'Патрон 9х18 - ПС', 4, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 7, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, NULL, NULL, NULL);
INSERT INTO `items` VALUES (1160, 1108, 47, 'Патрон 9х18 - Премиум', 8, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 50, 0.008, 1, '09:40:08', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1161, 1108, 45, 'Патрон 9х18 - ПС', 8, 0, 5, 0, 0, '', '9x18_ps', 0, 0, 1, 1, 8, 0, 0, 0, 0, 0, 0, 50, 0.01, 1, '00:00:00', 0, 0, 'ammos', NULL, 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1175, 2, 16, 'Пм [GM] - Премиум', 8, 0, 2, 1, 2, 'Пм[GM] - Премиум', 'spi_pm_01', 30, 0, 3, 2, 2, 0, 150, 98.98, 0, 1, 0, 8, 0.65, 0, '18:47:34', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1176, 1173, 13, 'Глушитель', 1, 0, 7, -1, 0, 'Глушитель', 'muzzle_nozzle', 0, 0, 1, 1, -1, -1, 1, 1, 0, 0, 0, 1, 0, 1, '09:53:30', 0, 0, 'silencers', 'spi_pbs_01', 0, 1, 1, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1224, 2, 55, 'КПК', 1, 0, 12, 1, 12, '', 'spi_pda_default_01', 0, 0, 2, 2, 12, 0, 0, 0, 0, 1, 0, 1, 0.01, 0, '17:15:07', 0, 0, 'devices', 'spi_dpa_01_low', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1226, 67, 47, 'Патрон 9х18 - Премиум', 10, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 2, 0, 0, 0, 0, 1, 0, 50, 0.008, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1232, 67, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '10:02:01', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1234, 67, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '10:02:20', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1264, 67, 47, 'Патрон 9х18 - Премиум', 10, 0, 5, 0, 0, '', '9x18_prem', 0, 0, 1, 1, 2, 2, 0, 0, 0, 1, 0, 50, 0.008, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1265, 67, 46, 'Патрон 9х18 - БП', 10, 0, 5, 0, 0, '', '9x18_bp', 0, 0, 1, 1, 9, 1, 0, 0, 0, 0, 0, 50, 0.01, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1273, 67, 157, 'Пальчиковая батарейка', 2999, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 9, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '20:26:19', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1275, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1276, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1278, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1281, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1282, 67, 17, 'AK-47 [GM]', 30, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 5, 2, 100, 99.9, 0, 0, 1, 30, 4.3, 0, '13:52:14', 0, 0, 'weapons', 'spi_ak47', 0, 100, 100, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `items` VALUES (1283, 3, 55, 'КПК', 1, 0, 12, 1, 12, '', 'spi_pda_default_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'devices', 'spi_dpa_01_low', 0, 0, 0, 0.00, 0.00, 'Оружие/Тест/Пистолеты');
INSERT INTO `items` VALUES (1286, 3, 55, 'КПК', 1, 0, 12, 1, 12, '', 'spi_pda_default_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'devices', 'spi_dpa_01_low', 0, 0, 0, 0.00, 0.00, 'Оружие/Тест/Пистолеты');
INSERT INTO `items` VALUES (1287, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1292, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1296, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1299, 5, 32, 'Магазин АК-47 [GM] - Премиум', 10, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 75, 0, 1, 0, 30, 0.2, 0, '12:16:57', 0, 0, 'cages', 'spi_ak47_cage', 5, 75, 75, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1300, 1299, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, -1, -1, 0, 0, 0, 1, 0, 250, 0.012, 1, '12:17:01', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1301, 968, 43, 'Патрон 7,62х39 - БП ', 10, 0, 5, 0, 0, '', '7-62x39_bp', 0, 0, 1, 1, 1, 2, 0, 0, 0, 0, 0, 250, 0.016, 1, '12:17:06', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1303, 67, 17, 'AK-47 [GM]', 1, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 5, 1, 100, 100, 0, 0, 0, 0, 4.3, 0, '18:28:44', 0, 0, 'weapons', 'spi_ak47', 0, 100, 100, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `items` VALUES (1306, 3, 18, 'AK-47 [GM] - Премиум', 1, 0, 2, 1, 4, '', 'ak47_gm', 28, 0, 5, 2, 0, 0, 150, 150, 0, 1, 0, 0, 3.7, 0, '00:00:00', 0, 0, 'weapons', 'spi_ak47', 0, 150, 150, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `items` VALUES (1307, 67, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 7, 0, 150, 150, 0, 0, 0, 0, 0.65, 0, '00:00:00', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1308, 67, 17, 'AK-47 [GM]', 1, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 3, 3, 100, 100, 0, 0, 0, 0, 4.3, 0, '10:08:00', 0, 0, 'weapons', 'spi_ak47', 0, 100, 100, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `items` VALUES (1309, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1310, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1311, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1312, 3, 156, 'Фонарик', 1, 0, 16, 1, 11, '', 'spi_flashlight_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '00:00:00', 0, 0, 'devices', 'devices_01', 0, 0, 0, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1313, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1314, 1317, 32, 'Магазин АК-47 [GM] - Премиум', 30, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 0, 0, 75, 75, 0, 1, 0, 30, 0.2, 1, '00:00:00', 0, 0, 'cages', 'spi_ak47_cage', 5, 75, 75, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1315, 3, 55, 'КПК', 1, 0, 12, 1, 12, '', 'spi_pda_default_01', 0, 0, 2, 2, 0, 0, 0, 0, 0, 1, 0, 1, 0.01, 0, '00:00:00', 0, 0, 'devices', 'spi_dpa_01_low', 0, 0, 0, 0.00, 0.00, 'Оружие/Тест/Пистолеты');
INSERT INTO `items` VALUES (1316, 3, 157, 'Пальчиковая батарейка', 3000, 0, 17, 0, 0, '', 'details_01', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 3000, 0, 0, '00:00:00', 0, 0, 'details', 'details_01', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1317, 2, 17, 'AK-47 [GM]', 30, 0, 2, 1, 4, '', 'ak47_gm', 27, 0, 5, 2, 4, 0, 100, 99.8, 0, 0, 0, 30, 4.3, 0, '17:14:27', 0, 0, 'weapons', 'spi_ak47', 0, 100, 100, 2000.00, 2000.00, 'Оружие/Автоматические Винтовки');
INSERT INTO `items` VALUES (1318, 1, 32, 'Магазин АК-47 [GM] - Премиум', 0, 0, 3, 2, 0, '', 'cage_ak47_gm', 0, 0, 1, 1, 8, 0, 75, 75, 0, 1, 0, 30, 0.2, 0, '00:00:00', 0, 0, 'cages', 'spi_ak47_cage', 5, 75, 75, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1319, 1, 44, 'Патрон 7,62х39 - Премиум', 10, 0, 5, 0, 0, '', '7-62x39_prem', 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 250, 0.012, 0, '00:00:00', 0, 0, 'ammos', 'Cube', 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1324, 3, 15, 'Пм [GM]', 0, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 3, 0, 150, 150, 0, 0, 0, 0, 0.65, 0, '16:14:58', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1327, 3, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0.65, 0, '16:51:13', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1330, 3, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0.65, 0, '16:53:59', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1333, 3, 15, 'Пм [GM]', 0, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 1, 0.9, 0, 0, 0, 0, 0.65, 0, '17:41:23', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1337, 3, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 1, 1, 0, 0, 0, 0, 0.65, 0, '17:46:55', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1351, 3, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 0, 0, 100, 100, 0, 0, 0, 0, 0.65, 0, '11:29:48', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1354, 1, 15, 'Пм [GM]', 1, 0, 2, 1, 2, 'Пм', 'spi_pm_01', 16, 0, 3, 2, 2, 0, 100, 100, 0, 0, 0, 0, 0.65, 0, '09:55:58', 0, 0, 'weapons', 'spi_pm_01', 0, 150, 150, 300.00, 300.00, 'Оружие/Пистолеты');
INSERT INTO `items` VALUES (1356, 1, 3, 'Бронижелет', 1, 0, 1, 1, 1, 'Это бронижелет', 'armor_00', 4, 1, 2, 3, 7, 0, 1, 1, 0, 0, 0, 1, 7, 0, '12:54:39', 0, 0, '3', NULL, 0, 100, 100, 0.00, 0.00, NULL);
INSERT INTO `items` VALUES (1357, 2, 7, 'Куртка', 1, 0, 1, 1, 1, 'Куртка', 'armor_00', 7, 1, 2, 3, 1, 0, 1, 1, 0, 0, 0, 1, 2, 0, '13:01:09', 0, 0, '3', NULL, 0, 100, 100, 0.00, 0.00, NULL);

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `body` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `linkid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of links
-- ----------------------------
INSERT INTO `links` VALUES (1, 'menu1', 'Главная', '/main', 'link.html', NULL);
INSERT INTO `links` VALUES (5, 'menu1', 'Авторизироваться', '#login_dialog', 'link.html', 'login');
INSERT INTO `links` VALUES (7, 'private', 'Главная', '/main', 'link.html', NULL);
INSERT INTO `links` VALUES (8, 'private', 'Изменить пароль', '#reset_password', 'link.html', 'reset_password');
INSERT INTO `links` VALUES (17, 'menu1', 'Регистрация', '/registration', 'link.html', '');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'menu1', 'menu.html', 'main');
INSERT INTO `menu` VALUES (2, 'private', 'menu.html', 'private');
INSERT INTO `menu` VALUES (3, 'menu1', 'menu.html', 'post_id');
INSERT INTO `menu` VALUES (4, 'menu1', 'menu.html', 'editor');
INSERT INTO `menu` VALUES (5, 'menu1', 'menu.html', 'feedback');
INSERT INTO `menu` VALUES (6, 'menu1', 'menu.html', 'catalog');
INSERT INTO `menu` VALUES (7, 'menu1', 'menu.html', 'catalog_id');
INSERT INTO `menu` VALUES (8, 'menu1', 'menu.html', 'finder');

-- ----------------------------
-- Table structure for muzzle_nozzle
-- ----------------------------
DROP TABLE IF EXISTS `muzzle_nozzle`;
CREATE TABLE `muzzle_nozzle`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Mesh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AudioShot` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MuzzleFlash` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Power` float(255, 0) NULL DEFAULT 0,
  `ItemId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of muzzle_nozzle
-- ----------------------------
INSERT INTO `muzzle_nozzle` VALUES (1, 'silencer_all_Ak_01', 'ak_47_silencer', 'muzzle_nozzle', -5, 13);

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `Id` int(3) NOT NULL AUTO_INCREMENT,
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (5, 'main', 'Главная', 'index.html');
INSERT INTO `pages` VALUES (6, 'looteditor', 'Ответы нпс', 'looteditor.html');
INSERT INTO `pages` VALUES (7, 'weaponeditor', 'Личный кабинет', 'weaponeditor.html');
INSERT INTO `pages` VALUES (8, 'editor', 'Редактирование', 'editor.html');
INSERT INTO `pages` VALUES (9, 'auth', 'Авторизация', 'auth.html');
INSERT INTO `pages` VALUES (10, 'signup', 'Регистрация', 'signup.html');
INSERT INTO `pages` VALUES (11, 'itemeditor', 'Редактор предметов', 'itemeditor.html');
INSERT INTO `pages` VALUES (12, 'ammoedtior', 'Редактор патронов', 'ammoedtior.html');
INSERT INTO `pages` VALUES (13, 'registration', 'Регистрация', 'registration.html');
INSERT INTO `pages` VALUES (14, 'regsektion', 'Запись на кружок', 'regsektion.html');
INSERT INTO `pages` VALUES (15, 'test_page', 'Страница, для тестирования модулей', 'test_page.html');
INSERT INTO `pages` VALUES (16, 'promo_creator', 'Страница генерации промокодов', 'promocode_creator.html');
INSERT INTO `pages` VALUES (17, 'signup_finish', 'Страница завершения регистрации', 'signup_finish.html');
INSERT INTO `pages` VALUES (18, 'dialog_editor', 'Редактор диалогов', 'dialog.html');
INSERT INTO `pages` VALUES (19, 'trade_list', 'Редактор списк торговли', 'trade_list.html');
INSERT INTO `pages` VALUES (20, 'vk_confirm', 'Страница подтверждения регистрации', 'vk_confirm.html');

-- ----------------------------
-- Table structure for private_card_table
-- ----------------------------
DROP TABLE IF EXISTS `private_card_table`;
CREATE TABLE `private_card_table`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `PrivatedItemTypes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ItemId` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of private_card_table
-- ----------------------------
INSERT INTO `private_card_table` VALUES (1, '[1,8,9,10]', 57);
INSERT INTO `private_card_table` VALUES (2, '[2]', 58);
INSERT INTO `private_card_table` VALUES (3, '[0,3,4,5,6,7,11,12,13]', 59);

-- ----------------------------
-- Table structure for promocode
-- ----------------------------
DROP TABLE IF EXISTS `promocode`;
CREATE TABLE `promocode`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Type` int(255) NULL DEFAULT NULL,
  `isUsed` tinyint(1) NULL DEFAULT NULL,
  `UserId` bigint(22) NULL DEFAULT NULL,
  `UseTime` time(0) NULL DEFAULT NULL,
  `CreateTime` time(0) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of promocode
-- ----------------------------
INSERT INTO `promocode` VALUES (1, '322JAXDDA', 0, 1, 40, '14:54:54', '00:44:29');

-- ----------------------------
-- Table structure for property
-- ----------------------------
DROP TABLE IF EXISTS `property`;
CREATE TABLE `property`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Value` float NULL DEFAULT NULL,
  `MaxValue` float(255, 0) NULL DEFAULT NULL,
  `MinValue` float NULL DEFAULT NULL,
  `PerTime` float NULL DEFAULT NULL,
  `Per` float NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of property
-- ----------------------------
INSERT INTO `property` VALUES (1, 'heal', 100, 100, 0, 0.08333, 1);
INSERT INTO `property` VALUES (2, 'stamina', 100, 100, 0, 1, 1);
INSERT INTO `property` VALUES (3, 'fatigue', 10, 100, 0, 0.1, 60);
INSERT INTO `property` VALUES (4, 'fatigueRecoveryRateInGreenZone', 10, 100, 1, -20, 1);
INSERT INTO `property` VALUES (6, 'thirst', 10, 100, 0, 0.023, 1);
INSERT INTO `property` VALUES (7, 'hunger', 10, 100, 0, 0.023, 1);
INSERT INTO `property` VALUES (8, 'irradiation', 0, 100, -100, 0, 1);
INSERT INTO `property` VALUES (9, 'temperature', 10, 100, 0, 0, 1);
INSERT INTO `property` VALUES (10, 'mind', 100, 100, 0, 5, 1);
INSERT INTO `property` VALUES (11, 'radiation', 0, 100, 0, 0, 1);
INSERT INTO `property` VALUES (12, 'bulletproof', 0, 100, 0, 0, 0);
INSERT INTO `property` VALUES (13, 'armor', 0, 100, 0, 0, 0);
INSERT INTO `property` VALUES (14, 'bleeding', 0, 100, 0, 0, 1);
INSERT INTO `property` VALUES (15, 'poisoning', 0, 100, 0, 0, 1);

-- ----------------------------
-- Table structure for radiation_zone
-- ----------------------------
DROP TABLE IF EXISTS `radiation_zone`;
CREATE TABLE `radiation_zone`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Power` float NULL DEFAULT NULL,
  `TriggerId` int(11) NULL DEFAULT NULL,
  `Type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of radiation_zone
-- ----------------------------
INSERT INTO `radiation_zone` VALUES (1, 100, 0, 0);

-- ----------------------------
-- Table structure for recept_condition_item
-- ----------------------------
DROP TABLE IF EXISTS `recept_condition_item`;
CREATE TABLE `recept_condition_item`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ReceptId` bigint(20) NULL DEFAULT NULL,
  `ItemId` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '[]',
  `Count` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of recept_condition_item
-- ----------------------------
INSERT INTO `recept_condition_item` VALUES (1, 0, '[3,1]', 1);
INSERT INTO `recept_condition_item` VALUES (2, 0, '[2,4]', 1);

-- ----------------------------
-- Table structure for recept_result_item
-- ----------------------------
DROP TABLE IF EXISTS `recept_result_item`;
CREATE TABLE `recept_result_item`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ReceptId` bigint(20) NULL DEFAULT NULL,
  `ItemId` int(11) NULL DEFAULT NULL,
  `Count` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of recept_result_item
-- ----------------------------
INSERT INTO `recept_result_item` VALUES (1, 0, 15, 1);

-- ----------------------------
-- Table structure for recepts
-- ----------------------------
DROP TABLE IF EXISTS `recepts`;
CREATE TABLE `recepts`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CraftType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Price` bigint(10) NULL DEFAULT 0,
  `SkillId` int(255) NULL DEFAULT NULL,
  `SuccessExp` float(255, 0) NULL DEFAULT NULL,
  `ChanceLowSkill` float(255, 0) NULL DEFAULT NULL,
  `ChanceHighSkill` float(255, 0) NULL DEFAULT NULL,
  `Desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Filter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Duration` float(255, 0) NULL DEFAULT 0,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of recepts
-- ----------------------------
INSERT INTO `recepts` VALUES (0, 'weapon', 0, 0, 500, 30, 90, 'Тестовый рецепт', 'Оружие', 0);

-- ----------------------------
-- Table structure for repair_table
-- ----------------------------
DROP TABLE IF EXISTS `repair_table`;
CREATE TABLE `repair_table`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RepairItemId` int(11) NULL DEFAULT NULL,
  `ItemId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of repair_table
-- ----------------------------
INSERT INTO `repair_table` VALUES (1, 12, 3);
INSERT INTO `repair_table` VALUES (2, 12, 7);
INSERT INTO `repair_table` VALUES (3, 12, 4);
INSERT INTO `repair_table` VALUES (4, 12, 17);
INSERT INTO `repair_table` VALUES (5, 12, 18);
INSERT INTO `repair_table` VALUES (6, 12, 31);
INSERT INTO `repair_table` VALUES (7, 12, 32);

-- ----------------------------
-- Table structure for rights
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rights
-- ----------------------------
INSERT INTO `rights` VALUES (1, 'AdminCmd', 'Команда администратора');
INSERT INTO `rights` VALUES (2, 'AllUserCmd', 'Команда для всех пользователей');
INSERT INTO `rights` VALUES (3, 'goTo', 'Телепорт');
INSERT INTO `rights` VALUES (4, 'setWorldState', 'Установить состояние мира');
INSERT INTO `rights` VALUES (5, 'setNpcStat', 'Установить стостояние нпс');
INSERT INTO `rights` VALUES (6, 'setProperty', 'Установить свойство персонажа');
INSERT INTO `rights` VALUES (7, 'editItem', 'Редактирование предметов');
INSERT INTO `rights` VALUES (8, 'getItem', 'Получить экземпляр предмета');
INSERT INTO `rights` VALUES (9, 'editScene', 'Редактирование карты');
INSERT INTO `rights` VALUES (10, 'createPromo', 'Генерация промокодов');
INSERT INTO `rights` VALUES (11, 'ban', 'Блокировка игроков');
INSERT INTO `rights` VALUES (12, 'kick', 'Выкинуть игрока');
INSERT INTO `rights` VALUES (13, 'editDialogs', 'Редактирование диалогов');
INSERT INTO `rights` VALUES (14, 'godmod', 'Бессмертие');
INSERT INTO `rights` VALUES (15, 'tradeList', 'Покупка/Продажа редактор');
INSERT INTO `rights` VALUES (16, 'notification', 'Уведомления от имени сервера');
INSERT INTO `rights` VALUES (17, 'isClientGM', 'Привелегии администратора на клиенте');
INSERT INTO `rights` VALUES (18, 'unBan', 'Разбанить');

-- ----------------------------
-- Table structure for scene
-- ----------------------------
DROP TABLE IF EXISTS `scene`;
CREATE TABLE `scene`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NULL DEFAULT 0,
  `transformid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of scene
-- ----------------------------
INSERT INTO `scene` VALUES (1, 0, 1);

-- ----------------------------
-- Table structure for scopes
-- ----------------------------
DROP TABLE IF EXISTS `scopes`;
CREATE TABLE `scopes`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) NULL DEFAULT NULL,
  `Mesh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Zoom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `Crosshair` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of scopes
-- ----------------------------
INSERT INTO `scopes` VALUES (10, 11, 'pso_1_gm', '[60,40,20]', 'pso-1');

-- ----------------------------
-- Table structure for second_hand_loots
-- ----------------------------
DROP TABLE IF EXISTS `second_hand_loots`;
CREATE TABLE `second_hand_loots`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `OwnerId` bigint(20) NULL DEFAULT NULL,
  `ContinerId` bigint(20) NULL DEFAULT NULL,
  `SaleEndDate` datetime(0) NULL DEFAULT NULL,
  `Type` int(255) NULL DEFAULT NULL,
  `ItemInstanceId` bigint(20) NULL DEFAULT NULL,
  `Price` bigint(10) NULL DEFAULT NULL,
  `Currency` int(255) NULL DEFAULT NULL,
  `OwnerName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for spawn_loot
-- ----------------------------
DROP TABLE IF EXISTS `spawn_loot`;
CREATE TABLE `spawn_loot`  (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Type` int(11) NULL DEFAULT NULL,
  `TransformId` int(11) NULL DEFAULT NULL,
  `ChanceSpawnGroupId` int(11) NULL DEFAULT NULL,
  `Skin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spawn_loot
-- ----------------------------
INSERT INTO `spawn_loot` VALUES (8, 'Лут', 3, 24, 1, 'Bag');
INSERT INTO `spawn_loot` VALUES (10, 'Точка спауна лута', 1, 20, 1, 'Bag');
INSERT INTO `spawn_loot` VALUES (11, 'Лут группа', 1, 26, 1, 'Bag');

-- ----------------------------
-- Table structure for storage_contract
-- ----------------------------
DROP TABLE IF EXISTS `storage_contract`;
CREATE TABLE `storage_contract`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `OwnerId` bigint(20) NULL DEFAULT NULL,
  `StorageId` bigint(20) NULL DEFAULT NULL,
  `ContinerId` bigint(20) NULL DEFAULT NULL,
  `ContractDateEnd` datetime(0) NULL DEFAULT NULL,
  `Type` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of storage_contract
-- ----------------------------
INSERT INTO `storage_contract` VALUES (3, 1, 1, 67, '2020-02-18 15:45:54', 0);

-- ----------------------------
-- Table structure for trade_list
-- ----------------------------
DROP TABLE IF EXISTS `trade_list`;
CREATE TABLE `trade_list`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trade_list
-- ----------------------------
INSERT INTO `trade_list` VALUES (1, 'Первый список');
INSERT INTO `trade_list` VALUES (2, 'Второй список');

-- ----------------------------
-- Table structure for trade_list_items
-- ----------------------------
DROP TABLE IF EXISTS `trade_list_items`;
CREATE TABLE `trade_list_items`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ItemId` bigint(20) NULL DEFAULT NULL,
  `Currency` int(255) NULL DEFAULT NULL,
  `TradeListId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trade_list_items
-- ----------------------------
INSERT INTO `trade_list_items` VALUES (1, 42, 0, 1);
INSERT INTO `trade_list_items` VALUES (2, 17, 1, 1);
INSERT INTO `trade_list_items` VALUES (3, 11, 0, 1);
INSERT INTO `trade_list_items` VALUES (4, 15, 1, 1);
INSERT INTO `trade_list_items` VALUES (13, 88, 0, 0);
INSERT INTO `trade_list_items` VALUES (14, 1, 0, 0);

-- ----------------------------
-- Table structure for transform
-- ----------------------------
DROP TABLE IF EXISTS `transform`;
CREATE TABLE `transform`  (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `Scene` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'MainWorld',
  `Position` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '{\"x\":0.0,\"y\":0.0,\"z\":0.0}',
  `Rotation` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '{\"x\":0.0,\"y\":0.0,\"z\":0.0}',
  `Type` bigint(11) NULL DEFAULT 0,
  `GroupId` bigint(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14797 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transform
-- ----------------------------
INSERT INTO `transform` VALUES (14791, 'MainWorld', '{\"x\":544.1496,\"y\":521.029968,\"z\":700.188}', '{\"x\":7.21438932,\"y\":279.220154,\"z\":0.0}', 0, NULL);
INSERT INTO `transform` VALUES (14792, 'MainWorld', '{\"x\":543.262756,\"y\":521.0522,\"z\":690.17804}', '{\"x\":44.13598,\"y\":304.0,\"z\":0.0}', 0, NULL);
INSERT INTO `transform` VALUES (14793, 'MainWorld', '{\"x\":543.27,\"y\":521.0746,\"z\":720.950439}', '{\"x\":10.1040382,\"y\":187.538422,\"z\":0.0}', 0, NULL);
INSERT INTO `transform` VALUES (14794, 'MainWorld', '{\"x\":540.8129,\"y\":521.06,\"z\":738.5224}', '{\"x\":19.4639969,\"y\":116.300163,\"z\":0.0}', 0, NULL);
INSERT INTO `transform` VALUES (14795, 'MainWorld', '{\"x\":554.946533,\"y\":521.66,\"z\":704.3981}', '{\"x\":30.398407,\"y\":274.8998,\"z\":0.0}', 0, NULL);
INSERT INTO `transform` VALUES (14796, 'MainWorld', '{\"x\":536.7775,\"y\":521.029968,\"z\":739.572}', '{\"x\":15.62401,\"y\":90.99996,\"z\":0.0}', 0, NULL);

-- ----------------------------
-- Table structure for triggers
-- ----------------------------
DROP TABLE IF EXISTS `triggers`;
CREATE TABLE `triggers`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Type` int(255) NULL DEFAULT NULL,
  `TransformId` int(11) NULL DEFAULT NULL,
  `Primitive` int(11) NULL DEFAULT NULL,
  `Width` float NULL DEFAULT 0,
  `Height` float NULL DEFAULT 0,
  `Radius` float NULL DEFAULT 0,
  `Length` float NULL DEFAULT NULL,
  `GroupId` int(11) NULL DEFAULT -1,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of triggers
-- ----------------------------
INSERT INTO `triggers` VALUES (1, 'Зеленая зона', 1, 1, 0, 10, 10, 10, 0, 0);
INSERT INTO `triggers` VALUES (2, 'Тестовая аномалия', 3, 23, 1, 0, 0, 5, 0, 0);
INSERT INTO `triggers` VALUES (13, 'Участки радиационного загрязнения', 2, 2, 1, 10, 10, 10, 10, -1);

-- ----------------------------
-- Table structure for user_groups
-- ----------------------------
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NULL DEFAULT NULL,
  `GroupId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_groups
-- ----------------------------
INSERT INTO `user_groups` VALUES (1, 1, 1);
INSERT INTO `user_groups` VALUES (2, 1, 2);
INSERT INTO `user_groups` VALUES (3, 20, 1);
INSERT INTO `user_groups` VALUES (4, 22, 1);
INSERT INTO `user_groups` VALUES (5, 22, 2);
INSERT INTO `user_groups` VALUES (6, 28, 1);
INSERT INTO `user_groups` VALUES (7, 29, 1);
INSERT INTO `user_groups` VALUES (8, 30, 1);
INSERT INTO `user_groups` VALUES (9, 31, 1);
INSERT INTO `user_groups` VALUES (10, 31, 2);
INSERT INTO `user_groups` VALUES (11, 30, 2);
INSERT INTO `user_groups` VALUES (12, 32, 1);
INSERT INTO `user_groups` VALUES (13, 33, 1);
INSERT INTO `user_groups` VALUES (14, 34, 1);
INSERT INTO `user_groups` VALUES (15, 35, 1);
INSERT INTO `user_groups` VALUES (16, 36, 1);
INSERT INTO `user_groups` VALUES (17, 37, 1);
INSERT INTO `user_groups` VALUES (18, 38, 1);
INSERT INTO `user_groups` VALUES (20, 39, 1);
INSERT INTO `user_groups` VALUES (21, 40, 1);

-- ----------------------------
-- Table structure for vk_users
-- ----------------------------
DROP TABLE IF EXISTS `vk_users`;
CREATE TABLE `vk_users`  (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `VKID` bigint(20) NULL DEFAULT NULL,
  `Login` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SignUpTime` date NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for weapon_modifications
-- ----------------------------
DROP TABLE IF EXISTS `weapon_modifications`;
CREATE TABLE `weapon_modifications`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `WeaponId` int(11) NULL DEFAULT NULL,
  `ModificationId` int(11) NULL DEFAULT NULL,
  `TypeId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of weapon_modifications
-- ----------------------------
INSERT INTO `weapon_modifications` VALUES (1, 5, 1, 0);

-- ----------------------------
-- Table structure for weapons
-- ----------------------------
DROP TABLE IF EXISTS `weapons`;
CREATE TABLE `weapons`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Delay` float(9, 2) NULL DEFAULT 1.00,
  `BulletSpeed` float(9, 2) NULL DEFAULT 60.00,
  `CageItemId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '[]',
  `AimItemId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '[]',
  `PowerMin` float(11, 2) NULL DEFAULT 0.00,
  `MuzzleFlash` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `Type` int(6) NULL DEFAULT NULL,
  `ItemId` int(11) NULL DEFAULT NULL,
  `CustomizationId` int(11) NULL DEFAULT NULL,
  `Slot` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'RightHand',
  `ShotTypeSelect` int(11) NULL DEFAULT 0,
  `ScatterAngle` float NULL DEFAULT 0,
  `MuzzleNozzleItemId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '[]',
  `DeviceItemId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '[]',
  `WedgeProbability` float NULL DEFAULT 1,
  `PowerMax` float NULL DEFAULT 10,
  `RecoilForce` float NULL DEFAULT 0,
  `FirstShotReturnRate` float NULL DEFAULT 0,
  `ReturnRatio` float NULL DEFAULT 0,
  `RechargeDuration` float NULL DEFAULT 2,
  `ShotRange` float NULL DEFAULT 1000,
  `SingleFireMode` tinyint(1) NULL DEFAULT 1,
  `FireModeTurn` tinyint(1) NULL DEFAULT 0,
  `FireModeContinuous` tinyint(1) NULL DEFAULT 0,
  `ShotsInLine` int(11) NULL DEFAULT 3,
  `BulletsForAShot` int(11) NULL DEFAULT 1,
  `AudioShot` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'default',
  `AmmoMaxCount` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of weapons
-- ----------------------------
INSERT INTO `weapons` VALUES (3, 100.00, 1.00, '[]', '[]', -10.00, '', 0, -1, 0, 'RightHand', 1, 0, '[]', '[]', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 'null', 0);
INSERT INTO `weapons` VALUES (4, 100.00, 1.00, '[]', '[]', -10.00, '', 0, -2, 0, 'RightHand', 1, 0, '[]', '[]', 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 'null', 0);
INSERT INTO `weapons` VALUES (6, 100.00, 100.00, '[8]', '[11]', 0.00, 'PistolMuzzleFire', 2, 14, 0, 'RightHand', 2, 0.5, '[13]', '[0]', 0, -8, 3, 0, 10, 1, 150, 1, 1, 1, 3, 1, 'Pm', 0);
INSERT INTO `weapons` VALUES (9, 100.00, 0.00, '[]', '[]', 0.00, '', 0, 0, 0, 'RightHand', 1, 0, '[]', '[]', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'default', 0);
INSERT INTO `weapons` VALUES (10, 100.00, 50.00, '[29,30]', '[]', 0.00, 'MuzzleFlash_Pistol_single', 3, 15, 0, 'RightHand', 2, 0.2, '[13]', '[]', 0.1, 0, 5, 0, 10, 1, 300, 1, 0, 0, 0, 0, 'Pm', 0);
INSERT INTO `weapons` VALUES (11, 100.00, 50.00, '[29,30]', '[]', 0.00, 'MuzzleFlash_Pistol_single', 3, 16, 0, 'RightHand', 2, 0.2, '[13]', '[]', 0.1, 0, 5, 0, 10, 1, 300, 1, 0, 0, 0, 0, 'Pm', 0);
INSERT INTO `weapons` VALUES (12, 600.00, 200.00, '[31,32]', '[11]', -10.00, 'rifle', 4, 17, 0, 'RightHand', 2, 0.01, '[13]', '[]', 0.01, -15, 3, 0, 0, 1, 300, 1, 1, 1, 3, 0, 'ak_47', 0);
INSERT INTO `weapons` VALUES (13, 600.00, 150.00, '[31,32]', '[11]', -10.00, 'rifle', 4, 18, 0, 'RightHand', 2, 0, '[13]', '[]', 0.01, -15, 1, 0, 0, 1, 300, 1, 1, 1, 3, 0, 'ak_47', 0);
INSERT INTO `weapons` VALUES (14, 100.00, 30.00, '[50]', '[]', -5.00, 'PistolMuzzleFire', 3, 19, 0, 'RightHand', 2, 1, '[]', '[]', 0.5, -5, 9, 0, 20, 1, 100, 1, 0, 0, 0, 1, 'default', 0);
INSERT INTO `weapons` VALUES (15, 100.00, 50.00, '[50]', '[]', -5.00, 'PistolMuzzleFire', 3, 20, 0, 'RightHand', 2, 1, '[]', '[]', 0.1, -7, 8, 0, 10, 1, 100, 1, 0, 0, 0, 1, 'default', 0);
INSERT INTO `weapons` VALUES (16, 100.00, 30.00, '[]', '[]', -10.00, '', 2, 54, 0, 'RightHand', 1, 0, '[]', '[]', 0, -25, 9, 0, 20, 1, 2.5, 1, 0, 0, 0, 0, 'default', 0);
INSERT INTO `weapons` VALUES (28, 100.00, 70.00, '[29]', '[11]', 0.00, 'PistolMuzzleFire', 3, 4, 0, 'RightHand', 2, 1, '[13]', '[]', 0.1, 0, 9, 0, 20, 1, 150, 1, 1, 1, 3, 1, 'Pm', 0);

SET FOREIGN_KEY_CHECKS = 1;
