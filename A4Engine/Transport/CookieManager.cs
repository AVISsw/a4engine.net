﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Transport
{
    public class CookieManager : ICookieManager
    {
        Dictionary<string, ICookie> cookies = new Dictionary<string, ICookie>();
        public CookieManager(CookieCollection cookies)
        {
            foreach(Cookie cookie in cookies)
            {
                ICookie cookieElement;
                if (!this.cookies.TryGetValue(cookie.Name, out cookieElement))
                {
                    this.cookies.Add(cookie.Name, new CookieElement() { Cookie = cookie });
                }
            } 
        }

        public Cookie this[string cookieName] => Get(cookieName);

        public Cookie Get(string cookieName)
        {
            ICookie cookie;
            if(cookies.TryGetValue(cookieName, out cookie))
            {
               return cookie.Cookie;
            }
            return null;
        }

        public CookieCollection GetUpdates()
        {
            CookieCollection cookiesResult = new CookieCollection();
            foreach(var element in cookies.Values)
            {
                if (element.Updated)
                {
                    cookiesResult.Add(element.Cookie);
                }
            }
            Console.WriteLine("Updated Cookie " + cookiesResult.Count);
            return cookiesResult;
        }

        public void Remove(string cookieName)
        {
            var cookie = cookies[cookieName];
            cookie.Updated = true;
            cookie.Cookie.Expired = true;
            cookie.Cookie.Expires = new DateTime();
        }

        public void Update(Cookie cookieValue)
        {
            ICookie cookie;
            if (cookies.TryGetValue(cookieValue.Name, out cookie))
            {
                cookie.Updated = true;
                cookie.Cookie = cookieValue;
            }
            else
            {
                cookies.Add(cookieValue.Name, new CookieElement() { Updated = true, Cookie = cookieValue });
            }
        }
    }
    public class CookieElement : ICookie
    {
        public bool Updated { get; set; }
        public Cookie Cookie { get; set; }
    }
}
