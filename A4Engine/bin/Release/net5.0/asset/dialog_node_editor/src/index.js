import { EditorCanvas } from "./editorcanvas.js";
import { Rect } from "./rect.js";
import { Node, RootNode } from "./node.js";
import { RightMenuProcessor } from "./rightmenu.js";
import { DialogueProcessor } from "./dialogueprocessor.js";
import { DialogueTestWindow } from "./dialoguetestwindow.js";
import { DataWorker } from "./dataworker.js";

var counter = 0;
var editorCanvas;
window.mouseEventData = {};
window.dialogueProcessor = {};
window.rightMenu = {};
window.testWindow = {};
window.DataWorker = DataWorker;
window.DialogType = {
  Root: 0,
  NPC: 1,
  Player: 2
};
window.editorCanvas = editorCanvas;
var EventIcon = new Image(20, 20);
EventIcon.src = '/dialog_node_editor/icon/evIco.png';
window.EventIcon = EventIcon

var ConditionIcon = new Image(20, 20);
ConditionIcon.src = '/dialog_node_editor/icon/cnIco.png';
window.ConditionIcon = ConditionIcon
var updaterInterval;

function AddCounter(nodeId) {
  if (nodeId > counter) {
    counter = nodeId;
  }
  counter++;
}

class MouseEventCustomData {
  constructor() {
    //Этот блок частично лишний и сделан для удобства чтения
    this.cursorType = "default"; //default (поле), select, resize
    this.currentActionType = "none"; //none, select, resize, drag, dragOffset

    this.mouseDown = false;
    this.isDoubleClick = false;
    this.objUnderCursor = null;

    this.x = 0;
    this.y = 0;
    this.timestamp = 0;
    this.canvasBlocked = false;
  }
  Down(x, y) {
    this.mouseDown = true;

    //cursorType определяет что под курсором - поле, нода или граница ноды
    if (this.cursorType === "default") {
      this.currentActionType = "none";
    }

    if (this.cursorType === "resize") {
      this.currentActionType = "resize";
    }

    if (this.cursorType === "select") {
      this.currentActionType = "select";

      this.x = x;
      this.y = y;

      var newTimestamp = Date.now();
      if (newTimestamp - this.timestamp < 200) {
        this.isDoubleClick = true;
        this.timestamp = 0;
      } else {
        this.isDoubleClick = false;
        this.timestamp = newTimestamp;
      }

      this.ProcessSelection(this.objUnderCursor);
    }
  }
  Move(x, y) {
    this.x = x;
    this.y = y;

    if (this.currentActionType === "select") {
      this.isClick = false;
      this.currentActionType = "drag";
    }

    if (this.currentActionType === "none" && this.mouseDown) {
      this.currentActionType = "dragOffset";
    }

    if (this.currentActionType === "drag") {
      this.objUnderCursor.OnDrag(x, y);
    }

    if (this.currentActionType === "resize") {
      this.objUnderCursor.OnResize(x, y);
    }

    if (this.currentActionType === "dragOffset") {
      editorCanvas.field.OnDrag(x, y);
    }

    this.Over(x, y);
  }
  Up() {
    if (this.canvasBlocked) {
      return;
    }
    if (this.currentActionType === "select") {
      this.objUnderCursor.OnClick();
    } else if (
      this.currentActionType === "drag" ||
      this.currentActionType === "resize"
    ) {
      this.objUnderCursor.OnDragStoped();
    } else if (this.currentActionType === "dragOffset") {
      editorCanvas.field.OnDragStoped();
    }

    if (this.currentActionType === "none") {
      if (editorCanvas.CheckPointInsideCanvas(this.x, this.y)) {
        window.rightMenu.OnDeselect();
        this.DeselectSelectedNode();
      }
 
    }

    this.mouseDown = false;
    this.currentActionType = "none";
  }
  Over(x, y) {
    if (this.currentActionType === "none") {
      //Если действий не выполняется
      let obj = editorCanvas.CheckPointOnNodeBorder(x, y); //Граница ноды, за которую можно тягать
      if (obj) {
        this.objUnderCursor = document.body.style.cursor = "nw-resize";
        this.cursorType = "resize";
        this.objUnderCursor = obj;
        return;
      }
      obj = editorCanvas.CheckPointInsideNode(x, y); //Либо нода, либо коннекшен
      if (obj) {
        document.body.style.cursor = "pointer";
        this.cursorType = "select";
        this.objUnderCursor = obj;
        return;
      }
      //Field
      document.body.style.cursor = "auto";
      this.cursorType = "default";
      this.objUnderCursor = null;
    }
  }
  RessetCursor() { }
  ProcessSelection(obj) {
    this.DeselectSelectedNode();

    editorCanvas.selected = obj;
    obj.OnSelected();
  }
  DeselectSelectedNode() {
    if (editorCanvas.selected) {
      editorCanvas.selected.selected = false;
    }
  }
}

function Init() {
  //Инициализация компонентов
  editorCanvas = new EditorCanvas(640, 480);
  editorCanvas.Init();

  window.mouseEventData = new MouseEventCustomData();
  window.rightMenu = new RightMenuProcessor();
  window.dialogueProcessor = new DialogueProcessor(editorCanvas);
  window.testWindow = new DialogueTestWindow();
  window.editorCanvas = editorCanvas;
  document.getElementById("body").onmousemove = OnMouseMove;
  editorCanvas.canvas.onmousedown = OnMouseDown;
  document.getElementById("body").onmouseup = OnMouseUp;

  document.getElementById("body").onkeyup = KeyUp;

  updaterInterval = setInterval(Update, 1000 / 50);

  //Останавливает апдейтер во избежания очередного зависания.
  //После отсановки все еще можно сохранить данные, но продолжить работу
  //  можно только после нажатия F5
  window.onerror = function (message, source, lineno, colno, error) {
    clearInterval(updaterInterval);
  };
  /*
 window.addEventListener("keyup", ({ key }) => {
   if (key === "Delete") {
     if (!(editorCanvas.selected instanceof RootNode)) {
       if (editorCanvas.selected instanceof Node) {
         editorCanvas.DeleteNode(editorCanvas.selected);
       }
     }
   }
 });
 */
}
function KeyUp(e) {
 
  if (editorCanvas.CheckPointInsideCanvas(window.mouseEventData.x, window.mouseEventData.y)) {
    if (e.key === "Delete") {
      if (!(editorCanvas.selected instanceof RootNode)) {
        if (editorCanvas.selected instanceof Node) {
          editorCanvas.DeleteNode(editorCanvas.selected);
        }
      }
    }
  }
}
window.RessetOffset = function () {
  editorCanvas.field.offsetX = 0;
  editorCanvas.field.offsetY = 0;
};

window.CreateRoot = function () {
  if (editorCanvas.nodes.length > 0) {
    return;
  }

  var node = new RootNode(
    new Rect("green", editorCanvas.rect.width / 2 - 45, 10, 90, 50),
    counter
  );
  AddCounter(0);
  console.log("counter " + counter);
  editorCanvas.AddNode(node);

  window.dialogueProcessor.AfterRootNodeCreated(node);
  window.mouseEventData.ProcessSelection(node);

  DataWorker.opened.push({ name: node.data.name, nodes: editorCanvas.nodes });
  DataWorker.DrawLabels();
  return node;
};

window.CreateNode = function (parent, x, y) {
  x -= editorCanvas.field.offsetX;
  y -= editorCanvas.field.offsetY;
  var node = new Node(new Rect("#581845", x - 60, y, 200, 100), counter);
  AddCounter(0);
  console.log("counter " + counter);
  editorCanvas.AddNode(node);
  window.dialogueProcessor.AfterNodeCreated(node, parent);
  node.data.text = editorCanvas.nodes[0].data.name + "_" + node.id;

  window.mouseEventData.ProcessSelection(node);

  return node;
};

window.ImportNode = (nodeData) => {
  let x = nodeData.rect.x;
  let y = nodeData.rect.y;
  let height = nodeData.rect.height;
  let width = nodeData.rect.width;

  let node = new Node(new Rect("#581845", x, y, width, height), nodeData.id);
  node.data = nodeData.data;
  AddCounter(nodeData.id);
  console.log("counter " + counter);
  window.dialogueProcessor.AfterNodeImported(node);

  return node;
};

window.ImportRootNode = (nodeData) => {
  var x = nodeData.rect.x;
  var y = nodeData.rect.y;
  var node = new RootNode(new Rect("green", x, y, 90, 50), nodeData.id);
  node.data = nodeData.data;
  AddCounter(nodeData.id);
  console.log("counter " + counter);

  window.dialogueProcessor.AfterRootImported(node);

  return node;
};

window.OnCreatingConnection = function (firstNode) {
  var mouseX = window.mouseEventData.x;
  var mouseY = window.mouseEventData.y;
  if (editorCanvas.CheckPointInsideCanvas(mouseX, mouseY)) {
    var obj = editorCanvas.CheckPointInsideNode(mouseX, mouseY);
    if (obj instanceof Node && !(obj instanceof RootNode)) {
      if (obj.id === firstNode.id) {
        return;
      }
      if (firstNode.connections.indexOf(obj) !== -1) {
        return;
      }
      firstNode.connections.push(obj);
    } else {
      let newNode = this.CreateNode(firstNode, mouseX, mouseY);
      newNode.rect.y -= 50;
      firstNode.connections.push(newNode);
    }
  }
};

function OnMouseDown(e) {
  var mouseX = e.clientX - editorCanvas.canvas.offsetLeft;
  var mouseY = e.clientY - editorCanvas.canvas.offsetTop;

  window.mouseEventData.Down(mouseX, mouseY);
  return false;
}

function OnMouseMove(e) {
  var mouseX = e.clientX - editorCanvas.canvas.offsetLeft;
  var mouseY = e.clientY - editorCanvas.canvas.offsetTop;
  window.mouseEventData.Move(mouseX, mouseY);
  window.testWindow.OnMouseMove(e);
  return false;
}

function OnMouseUp() {
  window.mouseEventData.Up();
  window.testWindow.OnMouseUp();

}

window.OnConnectionSelected = function (event) {
  var id = event.target.id;
  if (id !== "connectionSelector") {
    window.rightMenu.selectedConnection = parseInt(id, 10);
  }
};

window.OnStopingConnectionSelection = function () {
  window.rightMenu.selectedConnection = -1;
};

window.OnConnectionDeleting = function (id) {
  let node = editorCanvas.selected;
  editorCanvas.DeleteNodeConnection(node, id);
  window.rightMenu.WriteConnectionData(node.connections);
  return false;
};
window.OnConnectionUp = function (id) {
  //let node = window.mouseEventData.obj;
  let node = editorCanvas.selected;
  console.log(node);
  editorCanvas.UpNodeConnection(node, id);
  window.rightMenu.WriteConnectionData(node.connections);
  return false;
};
window.OnConnectionDown = function (id) {
  //let node = window.mouseEventData.obj;
  let node = editorCanvas.selected;
  console.log(node);
  editorCanvas.DownNodeConnection(node, id);
  window.rightMenu.WriteConnectionData(node.connections);
  return false;
};

function Update() {
  editorCanvas.Draw();
}

window.LoadDialogues = () => {
  var file_input = document.getElementById("file-input");
  file_input.click();
};
window.LoadStDialogTable = () => {
  var file_input = document.getElementById("file-st-dialog-table-input");
  file_input.click();
};
window.OnChooseStDialogTable = (event) => {
  if (event.target.files.length > 0) {
    DataWorker.LoadStDialogTableFile(event.target.files);
  }
};
window.OnChooseFiles = (event) => {
  if (event.target.files.length > 0) {
    DataWorker.LoadFiles(event.target.files);
  }
};

window.OpenTestWindow = () => {
  var data = DataWorker.Export(editorCanvas.nodes);
  //Сначала старт сервера, потом открытие новой страницы
  window.testWindow.ShowWindow(data);
};

window.SaveFile = () => {
  if (editorCanvas.nodes.length > 0) {
    var data = DataWorker.ExportJSON(editorCanvas.nodes);
    DataWorker.DownloadData(editorCanvas.nodes[0].data.name, data);
  }
};
window.SaveStDialogTable = () => {
  if (editorCanvas.nodes.length > 0) {
    DataWorker.SaveStDialogTableFile(editorCanvas.nodes[0].data.name);
  }
};

window.OnDialogSelected = (name) => {
  var id = DataWorker.GetFileId(name);
  var nodes = DataWorker.opened[id].nodes;
  editorCanvas.nodes = nodes;
  window.rightMenu.OnDeselect();
};

window.OnRootNameChanged = () => {
  var name = document.getElementById("dialogue_name_input").value;
  DataWorker.RenameFile(editorCanvas.nodes[0].data.name, name);
  editorCanvas.nodes[0].data.name = name;
};

window.ClearCanvasNodes = () => {
  editorCanvas.nodes = [];
  window.rightMenu.OnDeselect();
};

Init();
