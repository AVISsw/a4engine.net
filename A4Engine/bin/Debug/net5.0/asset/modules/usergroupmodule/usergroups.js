
var moduleData = {
    UserId: -999,
    GroupId: -1,
    Users: {},
    Groups: {},
    UserName: ""
}; 
function selectPage(page) {
    console.log("selectPage " + page);
    jsonRequestData("/handler/UserGroupHandler/get-users-by-group", { GroupId: moduleData.GroupId, UserId: -1, Page: page }, function (requestDataItemList) {
        console.log(requestDataItemList);
        moduleData.Users = requestDataItemList;
    });
}
$(window).on('load', function () {

    var tradeListModule = new Vue({
        el: '#groupList',
        data: {
            Type: -1,
            Title: "",
            moduleData: moduleData
        },
        methods: {
            SelectGroup(index, item) {
                moduleData.GroupId = item.Id;
                jsonRequestData("/handler/UserGroupHandler/get-users-by-group", { GroupId: moduleData.GroupId, UserId: -1, Page: currentPage }, function (requestDataItemList) {
                    console.log(requestDataItemList);
                    moduleData.Users = requestDataItemList;
                });
            }
        }
    });

    var tradeListModule2 = new Vue({
        el: '#userList',
        data: {
            Type: -1,
            search: "",
            moduleData: moduleData
        },
        methods: {
            AddUser() {

                jsonRequestData("/handler/UserGroupHandler/add-users-to-group", { GroupId: moduleData.GroupId, UserId: moduleData.UserId }, function (requestDataItemList) {
                    jsonRequestData("/handler/UserGroupHandler/get-users-by-group", { GroupId: moduleData.GroupId, UserId: -1, Page: currentPage }, function (requestDataItemList) {
                        console.log(requestDataItemList);
                        moduleData.Users = requestDataItemList;
                    });
                });
            },
            RmUser(userId) {

                jsonRequestData("/handler/UserGroupHandler/rem-users-from-group", { GroupId: moduleData.GroupId, UserId: userId }, function (requestDataItemList) {
                    jsonRequestData("/handler/UserGroupHandler/get-users-by-group", { GroupId: moduleData.GroupId, UserId: -1, Page: currentPage }, function (requestDataItemList) {
                        console.log(requestDataItemList);
                        moduleData.Users = requestDataItemList;
                    });
                });
            },
            SelectGroup(index, item) {
                moduleData.GroupId = item.Id;
            }
        },
        computed: {
            searchf: function () {
                var name = this.search;
                let result = Object.values(moduleData.Users).filter(function (elem) {
                    if (name === '') return true;
                    else return elem.AuthData.Login.indexOf(name) > -1 || elem.AuthData.Email.indexOf(name) > -1;
                });
                return result;
            }
        }
    });
    jsonRequestData("/handler/GroupRightHandler/get-groups", { GroupId: -1, Rights: null, GroupRights: null }, function (requestDataItemList) {
        console.log(requestDataItemList);
        moduleData.Groups = requestDataItemList;
    });
    $.post(
        "/handler/UserGroupHandler/pages",
        {},
        function (data) {
            for (i = 0; i < data.PageCount; i++) {
                $("#pages").append('<li><a href="#page" onclick=\"selectPage(' + i + ')\">' + i + '</a></li>');
            }
        }
    );

});