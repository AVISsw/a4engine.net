
$(document).ready(function () {
  var model = { monitoringData: { IsServerOnline: true, TotalConnection: 99999, Online: 0 } };
  var monitoringModule = null;
  monitoringModule = new Vue({
    el: '#app-monitoring',
    data: model,
    methods: {
      Restart(){
        jsonRequestData("/handler/Monitoring/restart", {}, function (requestData) {
 
          alert(requestData);
        });
      },
      UpdateCache(){
        jsonRequestData("/handler/Monitoring/update_cache", {}, function (requestData) {
 
          alert(requestData);
        });
      }
    }
  });

  function Update() {
    jsonRequestData("/handler/Monitoring/get", {}, function (requestData) {
      console.log(requestData);
      model.monitoringData = requestData;
    });
  }
 let timerId = setInterval(() => Update(), 2000);
 Update();
});