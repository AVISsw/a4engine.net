﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using A4Engine.Modules;

namespace A4Engine.Modules.Ammo
{
    public class Ammo : Module
    {
        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    string index = FileGetContents(DirModule + "/form.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
            Context.Redirect("/page/auth");
            Draw("<b>Доступ запрещен!</b>");
        }
    }
}
