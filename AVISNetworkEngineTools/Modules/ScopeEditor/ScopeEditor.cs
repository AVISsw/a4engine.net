﻿using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using A4Engine.Modules;
using AItemData = A4Engine.Modules.ItemEditor.ItemData;
using DataORM.Models;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.ScopeEditor
{
    public class ScopeEditor : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override async Task Before(RequestContext Context)
        {
           await Start(Context);
        }


        public async Task<long> AddTemplate<T>(NameValueCollection data) where T : new()
        {
            long lastId = -1;
            try
            {
                lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<T>(data));
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId;
        }
        public async Task<List<AItemData>> GetByType(ItemType itemType)
        {
            List<AItemData> result = new List<AItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<AItemData>("IntType={0}", (int)itemType))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<AItemData>());
                }
            }
            return result;
        }

        public async Task<bool> UpdateTemplate<T>(int id, NameValueCollection data) where T : new()
        {
            bool isAdd = false;
            try
            {
                Console.WriteLine(data.Get("ItemId"));
                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<T>(data), " ItemId={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveTemplate<T>(int id) where T : new()
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<T>(" ItemId={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public override async Task Start(RequestContext Context)
        {

            string key = Context.Path[3].ToString();
            int id = 0;

            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(await this.AddTemplate<ScopeData>(Context.postParams)));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.UpdateTemplate<ScopeData>(id, Context.postParams)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveTemplate<ScopeData>(id)));
                            break;
                        case "getlist":
                            Draw(JsonConvert.SerializeObject(await this.GetList()));
                            break;
                        case "getitems":
                            Draw(JsonConvert.SerializeObject(await this.GetByType(ItemType.OpticalSights)));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
            return;
        }

        private async Task<List<ScopeData>> GetList()
        {
            List<ScopeData> result = new List<ScopeData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ScopeData>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ScopeData>());
                }
            }
            return result;
        }
    }
}
