﻿using A4Engine.Controllers;
using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineORM.Data;
using AVISNetworkEngineORM.DB;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataORM.Models.PromoCodeData;

namespace AVISNetworkEngineTools.Modules.PromoCode
{
    public class CreatePromo : A4Engine.Modules.Handler
    {
        public static long[] PromoSpawnGroup = new long[] { -2020, -2021, -2022 };
        public Logger Log { get; private set; }
        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        async Task<int> GenerateCode(int count, CodeType typeCode)
        {
            var result = new PromoCodeData();
            int generated = 0;
            for (int i = 0; i < count; i++)
            {
                result.Code = RandomString(16);
                result.Type = typeCode;
                result.CreateTime = DateTime.Now;

                long lastId = await Controller.Storage.Add.In(result);
                if (lastId > -1)
                {
                    generated++;
                }
            }
            return generated;
        }

        private string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }

        public override async Task Start(RequestContext Context)
        {
            string action = Context.Path[3];
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");
                if (isPartner || isAdmCreater)
                {
                    string promo = "";
                    string lastDaysStr = "";
                    int lastDays = 0;
                    DateTime afterDate;
                    ResponseData response;
                    CreatePromoData createPromoData;
                    switch (action)
                    {
                        case "create_promo":


                            createPromoData = JsonConvert.DeserializeObject<CreatePromoData>(Context.rawParams);
                            response = await GeneratePromo(Controller, createPromoData, isAdmCreater, isPartner, User.UserData.Id);
                            Draw(JsonConvert.SerializeObject(response));
                            break;
                        case "update_promo":
                            createPromoData = JsonConvert.DeserializeObject<CreatePromoData>(Context.rawParams);
                            response = await UpdatePromoCode(createPromoData.PromoCodeData);
                            Draw(JsonConvert.SerializeObject(response));
                            break;
                        case "remove_promo":
                            promo = Context.Path[4];
                            response = await RemovePromo(promo);
                            Draw(JsonConvert.SerializeObject(response));
                            break;
                        case "get_created_promo":
                            var createdPromoList = await GetCreatedPromo(Controller, User.UserData.Id);
                            Draw(JsonConvert.SerializeObject(new PromoList() { Success = true, List = createdPromoList }));
                            break;

                    }
                    return;
                }
            }
            Draw(JsonConvert.SerializeObject(new ResponseData() { Success = false, Message = "Недостаточно прав для использования" }));
        }
        public static async Task<ResponseData> GeneratePromo(BaseController Controller, CreatePromoData createPromoData, bool isAdmCreater, bool isPartner, long userId)
        {
            createPromoData.PromoCodeData.CreaterAccountId = userId; // usedata
            if (isAdmCreater)
            {
                try
                {
                    var promoCodeResult = await CreatePromo.CreatePromoCode(Controller, createPromoData.PromoCodeData);
                    switch (createPromoData.PromoCodeData.Type)
                    {
                        case PromoCodeData.CodeType.StartPromo:
                        case PromoCodeData.CodeType.BonusItem:
                            await Controller.Storage.Remove.Where<AddItemByPromo>("PromoId={0}", promoCodeResult.Id);
                            await Controller.Storage.Add.In(new AddItemByPromo() { PromoId = promoCodeResult.Id, ItemSpawnGroup = createPromoData.ItemSpawnGroup });
                            break;
                    }
                    return new ResponseData() { Success = true };
                }
                catch (Exception ex)
                {
                    return new ResponseData() { Success = false, Message = ex.Message };
                }
            }
            else
            if (isPartner)
            {
                switch (createPromoData.PromoCodeData.Type)
                {
                    case PromoCodeData.CodeType.StartPromo:
                        try
                        {
                            if (!PromoSpawnGroup.Contains(createPromoData.ItemSpawnGroup))
                            {
                                return new ResponseData() { Success = false, Message = "Нельзя создать промокод с данной группой спауна, это не предусмотрено администратором" };
                            }
                            var createdList = await GetCreatedPromo(Controller, userId);
                            if (createdList.Count > 5)
                            {
                                return new ResponseData() { Success = false, Message = "Вы создали максимальное количество промокодов" };
                            }
                            var promoCodeResult = await CreatePromo.CreatePromoCode(Controller, createPromoData.PromoCodeData);
                            await Controller.Storage.Remove.Where<AddItemByPromo>("PromoId={0}", promoCodeResult.Id);
                            await Controller.Storage.Add.In(new AddItemByPromo() { PromoId = promoCodeResult.Id, ItemSpawnGroup = createPromoData.ItemSpawnGroup });
                            return new ResponseData() { Success = true };
                        }
                        catch (Exception ex)
                        {
                            return new ResponseData() { Success = false, Message = ex.Message };
                        }
                }
            }

            return new ResponseData() { Success = false, Message = "Вы не можете создавать промокод данного типа" };
        }

        public static async Task<List<PromoCodeData>> GetCreatedPromo(BaseController Controller, long createrId)
        {
            return await Controller.Storage.GetAll.Where<PromoCodeData>("CreaterAccountId={0}", createrId);
        }

        private async Task<ResponseData> RemovePromo(string promo)
        {
            bool success = false;
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");

                if (isAdmCreater)
                {
                    success = await Controller.Storage.Remove.Where<PromoCodeData>("Code={0}", promo);
                    return new ResponseData() { Success = success };
                }
                else if (isPartner)
                {
                    var response = new ResponseData() { Success = success };
                    success = await Controller.Storage.Remove.Where<PromoCodeData>("Code={0} AND CreaterAccountId={1}", promo, User.UserData.Id);
                    if (!success)
                    {
                        response.Message = "Не удалось удалить данный промокод " + promo;
                    }
                    return response;
                }
            }
            return new ResponseData() { Success = false, Message = "Вы не можете удалить данный промокод" };
        }
        public async Task<ResponseData> UpdatePromoCode(PromoCodeData promoCodeData)
        {
            bool success = false;
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");

                if (isAdmCreater)
                {
                    success = await Controller.Storage.Update.Where(promoCodeData, "Code={0}", promoCodeData.Code);
                    return new ResponseData() { Success = success };
                }
                else if (isPartner)
                {
                    var response = new ResponseData() { Success = success };
                    success = await Controller.Storage.Update.Where(promoCodeData, "Code={0} AND CreaterAccountId={1}", promoCodeData.Code, User.UserData.Id);
                    if (!success)
                    {
                        response.Message = "Не удалось удалить данный промокод " + promoCodeData.Code;
                    }
                    return response;
                }
            }
            return new ResponseData() { Success = false, Message = "Вы не можете удалить данный промокод" };
        }
        public static async Task<PromoCodeData> CreatePromoCode(BaseController controller, PromoCodeData promoCodeData)
        {
            promoCodeData.CreateTime = DateTime.Now;
            PromoCodeData findedPromo = await controller.Storage.Get.Where<PromoCodeData>("Code={0}", promoCodeData.Code);
            if (findedPromo != null)
                throw new Exception("Промокд с таким кодом уже существует");
            promoCodeData.Id = await controller.Storage.Add.In(promoCodeData);
            return promoCodeData;
        }

    }
    [DBTable("temp_tab")]
    public class CreatePromoData
    {
        [DBField]
        public PromoCodeData PromoCodeData;
        [DBField]
        public long ItemSpawnGroup;
    }
    [DBTable("add_items_by_promo")]
    public class AddItemByPromo
    {
        [DBField]
        public long Id;
        [DBField]
        public long PromoId;
        [DBField]
        public long ItemSpawnGroup;
    }
    [DBTable("temp_tab")]
    public class PromoList : ResponseData
    {
        public List<PromoCodeData> List = new List<PromoCodeData>();
    }
    public class ResponseData
    {
        public bool Success = true;
        public string Message;
    }
    public struct GenerateResult
    {
        public bool Success;
        public long SuccesCount;
    }
    public struct Error
    {
        public bool Success;
        public int ErrorCode;
        public string Message;
    }

}
