﻿using A4Engine;
using A4Engine.Modules;
using AVIS.Localization;
using AVISNetworkCommon;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.SignUp
{
    public class SignUpSettings : Handler
    {
        Logger Log;
        public static SignUpSettingsData SettingsData;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            SettingsData = new SignUpSettingsData()
            {
                Type = SignUpSettingsData.SignUpType.Open,
                CheckPhone = false,
                Notify = ""
            };

 
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
              return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            SetLang(Context);
            SettingsData.Notify = new TranslatableString("notify");
            Draw(JsonConvert.SerializeObject(SettingsData));
        }
 
    }
}
