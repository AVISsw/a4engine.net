﻿using Newtonsoft.Json;
using Qiwi.BillPayments.Client;
using Qiwi.BillPayments.Model;
using Qiwi.BillPayments.Model.In;
using Qiwi.BillPayments.Model.Out;
using Qiwi.BillPayments.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class QiwiPaymentSystem:BasePaymentSystem
    {
        //public 
        //48e7qUxn9T7RyYE1MVZswX1FRSbE6iyCj2gCRwwF3Dnh5XrasNTx3BGPiMsyXQFNKQhvukniQG8RTVhYm3iP3eDZNJ96XHG61x3eTn4CyFPWkAphYJvbBue5AZXUjvfSG77DobuYAVByqq1nUxxdkkmtpnR4NyarxgzqxGUGmpbA14PYhjQ4ueQcEgoye

        //secret
        //eyJ2ZXJzaW9uIjoiUDJQIiwiZGF0YSI6eyJwYXlpbl9tZXJjaGFudF9zaXRlX3VpZCI6IjBhODdnZi0wMCIsInVzZXJfaWQiOiI3OTUxMjc0OTQ4OCIsInNlY3JldCI6ImE4NWFmZWZlM2U3MzNjMjMwYzE3OTNjOWU2MzVmOGVkYTJlMjVkMWEzZjM2YzUyYzA2ODhiZjY4N2U3NWQ5MjEifX0=
        static string secretKey = "eyJ2ZXJzaW9uIjoiUDJQIiwiZGF0YSI6eyJwYXlpbl9tZXJjaGFudF9zaXRlX3VpZCI6InZxZjg3dy0wMCIsInVzZXJfaWQiOiI3OTIwODcyMzY5MyIsInNlY3JldCI6IjRhZGFmNjZhMjliZmZiM2ViZDE0NDNiMmI3ZjQxYTFhZjRhMjMyNDA0ODkyNTQwNjk0NzM2YmRjMGZmMDE3Y2EifX0=";
        public QiwiPaymentSystem()
        {
 
           
        }
        public override bool ClientSuccess(RequestContext requestContext)
        {
            return true;
        }

        public override string ExchangeRates(RequestContext context)
        {
            throw new NotImplementedException();
        }

        public override float GetPayAmount(string gameAmount)
        {
            return ReadFloat(gameAmount) / 10f;
        }

        public override PaymentContext GetPaymentContext(RequestContext requestContext)
        {
            var settings = new JsonSerializerSettings() { Formatting = Formatting.Indented };
            Notification notification = JsonConvert.DeserializeObject<Notification>(requestContext.rawParams, settings);

            return new PaymentContext() {
                Amount = (int)notification.Bill.Amount.ValueDecimal,
                CharacterId = Convert.ToInt32(notification.Bill.Customer.Account),
                OrderId = notification.Bill.BillId,
                PaymentSystem = DataORM.Models.PaymentSystemType.Qiwi
            };
        }

        public override string RedirectLink(RequestContext Context)
        {
            float payAmount = GetPayAmount(Context.getParams.Get("amount"));
            string out_summ = GetOrderTotal(payAmount);
            long characterId = Convert.ToInt64(Context.getParams.Get("characterid"));

            var client = BillPaymentsClientFactory.Create(
                secretKey: secretKey
            );
            var bill = client.CreateBill(
               info: new CreateBillInfo
               {
                   BillId = Guid.NewGuid().ToString(),
                   Amount = new MoneyAmount
                   {
                       ValueString = out_summ,
                       CurrencyEnum = CurrencyEnum.Rub
                   },
                   Comment = "Пополнение баланса в игре S.O.T.A 2 ",
                   ExpirationDateTime = DateTime.Now.AddDays(45),
                   Customer = new Customer
                   {
                       Account = characterId.ToString()
                   },
                   SuccessUrl = new Uri("https://sota-games-server.ru/page/payment_success")
               }
           );
            string result = @"
            <script type=""text/javascript"">
            window.location = '"+ bill.PayUrl.AbsoluteUri + @"';
            </script>
            ";
            return result;
        }

        public override bool RequestAuthentication(RequestContext requestContext)
        {
 
            string hash = requestContext.Request.Request.Headers["X-Api-Signature-SHA256"];
            var settings = new JsonSerializerSettings() { Formatting = Formatting.Indented };
            Notification notification = JsonConvert.DeserializeObject<Notification>(requestContext.rawParams, settings);

            return BillPaymentsUtils.CheckNotificationSignature(hash, notification, secretKey);
        }

        public override bool TestRequest(RequestContext requestContext)
        {
            return false;
        }
    }
    [ComVisible(true)]
    [DataContract]
    public class sotaCustomer : Customer
    {
        [ComVisible(true)]
        [DataMember(Name = "character_id")]
        public string CharacterId { get; set; }
    }
}
