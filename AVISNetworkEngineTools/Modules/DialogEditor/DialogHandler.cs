﻿using A4Engine.Modules;
 
using MessagePack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.DialogEditor
{
    public class DialogHandler : Handler
    {
        const string targetDirectory = "dialogs";
        DialogsList dialogsList = new DialogsList();
        public override async Task Before(RequestContext Context)
        {
           await Start(Context);
        }

        public override async Task OnLoad()
        {
            UpdateDialogsList();
        }

        private void UpdateDialogsList()
        {
            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }
            dialogsList.Dialogs = Directory.GetFiles(targetDirectory, "*.json");
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();

            switch (key)
            {
                case "get-list":
                    UpdateDialogsList();
                    Draw(JsonConvert.SerializeObject(dialogsList));
                    break;
                case "get":
                    get(Context);
                    break;
                case "set":
                    set(Context);
                    break;
                case "remove":
 
                    int index = Convert.ToInt32(Context.Path[4]);

                    if (dialogsList.Dialogs.Length > index)
                    {
                        string path = dialogsList.Dialogs[index];

                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                    UpdateDialogsList();
                    Draw(JsonConvert.SerializeObject(dialogsList));
                    break;
            }
        }

        private void set(RequestContext context)
        {
    
            try
            {
                context.rawParams = context.rawParams.Trim();
                Console.WriteLine("----------------------");
                Console.WriteLine(context.rawParams);

                DialogData dialogPage = JsonConvert.DeserializeObject<DialogData>(context.rawParams);
                string path = targetDirectory + "/" + dialogPage.Name + ".json";
                string dir = Path.GetDirectoryName(path);
                Console.WriteLine("dir " + dir);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                for (int i = 0; i < dialogPage.Pages.Count; i++)
                {
                    var page = dialogPage.Pages[i];
                    page.Id = i;
                    for (int j = 0; j < page.SayPlayer.Count; j++)
                    {
                        var say = page.SayPlayer[j];
                        say.Id = j;
                    }
                }
                string result = JsonConvert.SerializeObject(dialogPage);
                File.WriteAllText(path, result);
                Draw(result);
            }
            catch(Exception e)
            {
                Draw(e.ToString());
            }
    
            UpdateDialogsList();
        }

        private void get(RequestContext Context)
        {
            UpdateDialogsList();
            int index = Convert.ToInt32(Context.Path[4]);

            if (dialogsList.Dialogs.Length > index)
            {
                string path = dialogsList.Dialogs[index];

                if (File.Exists(path))
                {
                    using (StreamReader sr = File.OpenText(path))
                    {
                        try
                        {
                            string json = sr.ReadToEnd().Trim();
                            DialogData dialogs = JsonConvert.DeserializeObject<DialogData>(json);
                            Draw(JsonConvert.SerializeObject(dialogs));
                        }
                        catch(Exception e)
                        {
                            Draw(e.ToString());
                        }
                    }
                    return;
                }
            }
            Draw("{}");
        }
    }

    public class DialogsList
    {
        public string[] Dialogs = new string[] { };
    }

    [System.Serializable]
    [MessagePackObject]
    public class DialogData
    {
        [Key(0)]
        public List<DialogPage> Pages = new List<DialogPage>();
        [Key(1)]
        public string Name = "";
    }
    [System.Serializable]
    [MessagePackObject]
    public class DialogPage
    {
        [Key(0)]
        public long Id;
        [Key(1)]
        public List<string> NpcAnswerData = new List<string>();
        [Key(2)]
        public List<SayPlayerData> SayPlayer = new List<SayPlayerData>();
        [Key(3)]
        public bool Show = false;
        [Key(4)]
        public string Audio;
    }
    [MessagePackObject]
    public class DialogInstanceData
    {
        [Key(0)]
        public DialogPage DialogPageData;
        [Key(1)]
        public string NpcName = "";
        [Key(2)]
        public string Photo = "";
    }
    [System.Serializable]
    [MessagePackObject]
    public class SayPlayerData
    {
        [Key(0)]
        public long Id;
        [Key(1)]
        public string Text;
        [Key(2)]
        public List<DialogEventData> Events;
        [Key(3)]
        public List<DialogConditionData> Conditions;
        [Key(4)]
        public int NextPage = -1;
    }
    [System.Serializable]
    [MessagePackObject]
    public class DialogEventData
    {
        [Key(0)]
        public long Id;
        [Key(1)]
        public string EventName;
        [Key(2)]
        public string EventValue;
    }
    [System.Serializable]
    [MessagePackObject]
    public class DialogConditionData
    {
        [Key(0)]
        public long Id;
        [Key(1)]
        public string ConditionName;
        [Key(2)]
        public string ConditionValue;

    }
}
