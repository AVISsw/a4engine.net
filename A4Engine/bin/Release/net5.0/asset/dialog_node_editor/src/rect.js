class Rect {
  constructor(color, x, y, width, height) {
    this.color = color;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.middleX = width / 2 + x;
    this.middleY = height / 2 + y;
    this.relativeX = x;
    this.relativeY = y;
  }
  ApplyOffset(x, y) {
    this.relativeX = this.x + x;
    this.relativeY = this.y + y;

    this.middleX = this.width / 2 + this.relativeX;
    this.middleY = this.height / 2 + this.relativeY;
  }
}

export { Rect };
