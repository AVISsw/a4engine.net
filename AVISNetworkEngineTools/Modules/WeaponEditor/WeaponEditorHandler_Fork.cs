﻿using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using A4Engine.Modules;
using System.Linq;
using System.Threading.Tasks;

namespace A4Engine.Modules.WeaponEditor
{
    class WeaponEditorHandler_Fork : ItemEditor.ItemEditor
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }


        public async Task<bool> AddWeaponTemplate(NameValueCollection data)
        {
            long lastItemId = await this.AddItemTemplate(data);
            bool isAdd = false;
            try
            {
                long lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<WeaponData>(data));
                if (lastId > 0)
                    isAdd = true;
                isAdd = lastItemId > -1 && isAdd;
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
 
        public async Task<bool> UpdateWeaponTemplate(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                var weaponData = Controller.GetObjectByParams<WeaponData>(data);
                isAdd = await Controller.Storage.Update.Where(weaponData, " Id={0}", weaponData.Id);
                isAdd = isAdd && await this.UpdateItemTemplate(weaponData.ItemId, data);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveWeaponTemplate(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                var weaponData = Controller.GetObjectByParams<WeaponData>(data);
                isAdd = await Controller.Storage.Remove.Where<WeaponData>(" Id={0}", weaponData.Id);
                isAdd = isAdd && await this.RemoveItemTemplate(weaponData.ItemId);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public override async Task Start(RequestContext Context)
        {

            string key = Context.Path[3].ToString();
            int id = 0;

            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(this.AddWeaponTemplate(Context.postParams)));
                            break;
                        case "update":
                            Draw(JsonConvert.SerializeObject(this.UpdateWeaponTemplate(Context.postParams)));
                            break;
                        case "remove":
                            Draw(JsonConvert.SerializeObject(this.RemoveWeaponTemplate(Context.postParams)));
                            break;
                        case "getlist":
                            Draw(JsonConvert.SerializeObject(this.GetWeaponList()));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
            ;
        }

        private async Task<List<Dictionary<string, object>>> GetWeaponList()
        {
            Dictionary<long,WeaponData> weaponList = new Dictionary<long, WeaponData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<WeaponData>(""))
            {
                while (data.Read)
                {
                    var weaponData = data.GetObject<WeaponData>();
                    weaponList.Add(weaponData.ItemId, weaponData);
                }
            }
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>("IntType={0}", (int)ItemType.Weapons))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemEditor.ItemData>();
                    IDictionary<string, object> resultWeaponm = new Dictionary<string, object>();
                    if (weaponList.ContainsKey(itemData.ItemId))
                    {
                        var weaponData = weaponList[itemData.ItemId];
                        resultWeaponm = Controller.ToDictionary(weaponData);
                    }
                    IDictionary<string, object> resultItem = Controller.ToDictionary(itemData);
                    var resultElement = resultItem.Union(resultWeaponm).ToDictionary(pair => pair.Key, pair => pair.Value);
                     
                    result.Add(resultElement);
                }
            }
            return result;
        }
    }
}