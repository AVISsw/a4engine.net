﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using A4Engine.Modules;
using A4Engine;
using static AVISNetworkEngineTools.Modules.AuthVk.SignUpVk;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.AuthVk
{
    public class checkVkConfirm : Handler
    {
 
        public override Task OnLoad()
        {
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {

           return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            Result result = default;
            string email = null;
            email = Context.getParams["email"];
            string login = null;
            login = Context.getParams["login"];

            VKUser vkuser = await Controller.Storage.Get.Where<VKUser>(" Login={0} AND EMail={1}", login, email);
            if (vkuser != null)
            {
                result = new Result() { Success = true, ErrorCode = 0, Message = "Все ок, регистрируем" };
            }
            else
            {
                result = new Result() { Success = false, ErrorCode = 1, Message = "Аккаунт вк не привязан" };
            }

            Draw(JsonConvert.SerializeObject(result));

        }
    }
}
