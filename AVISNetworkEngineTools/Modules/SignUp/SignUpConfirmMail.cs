﻿using A4Engine.User;
using AVISNetworkCommon.Utility;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using A4Engine.Modules;
using AVISNetworkEngineTools.Modules.PromoCode;
using AVISNetworkEngineTools.Modules.SignUp;
using System.Threading.Tasks;
using AVIS.Localization;

namespace A4Engine.Modules.SignUp
{
    public class SignUpConfirmMail : Handler
    {

        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
         return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            Log.Info("SignUpConfirmMail");

            SignUpResult authResult = new SignUpResult();

            switch (SignUpSettings.SettingsData.Type)
            {
                case DataORM.Models.SignUpSettingsData.SignUpType.Close:
                    authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format(new TranslatableString("sign_up_closed")) };
                    Draw(JsonConvert.SerializeObject(authResult));
                    return;
            }

            SetLang(Context);

            AuthData authData = User.Data.Get<AuthData>("AuthData");
            string code = User.Data.Get<int>("SignUpCode").ToString();
            DateTime confirmLastRequest = User.Data.Get<DateTime>("SignUpConfirmLastRequest");
            DateTime сonfirmEndRequest = User.Data.Get<DateTime>("SignUpEndRequest");

            if (сonfirmEndRequest > DateTime.Now)
            {
                if (confirmLastRequest.AddSeconds(20) < DateTime.Now)
                {
                    confirmLastRequest = DateTime.Now;
                    User.Data.Save("SignUpConfirmLastRequest", confirmLastRequest);
 
                    string requestCode = Context.postParams["code"];
 
                    if (requestCode.Length < 4)
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 3;
                        authResult.Message = new TranslatableString("sign_up_email_code_length");
                    }
                    else
                    if (code.Length < 6)
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 2;
                        authResult.Message = new TranslatableString("sign_up_email_code_gen_err");
                    }
                    else
                    if (authData != null && code == requestCode)
                    {
                        User.Data.Save("mailConfirm", true);
                        // все ок
                        authResult.Success = true;
                        authResult.ErrorCode = 0;
                        authResult.Message = new TranslatableString("sign_up_email_code_finish");
                    }
                    else
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 1;
                        authResult.Message = new TranslatableString("sign_up_email_code_err");
                    }
                }
                else
                {
                    TimeSpan deltaTime = confirmLastRequest.AddSeconds(20) - DateTime.Now;
                    authResult = new SignUpResult() { Success = false, ErrorCode = 2, Message = string.Format(new TranslatableString("sign_up_email_code_wait"), deltaTime.TotalSeconds) };
                }
            }
            else
            {
                authResult = new SignUpResult() { Success = false, ErrorCode = 4, Message = string.Format(new TranslatableString("sign_up_email_code_wait_err")) };
            }
            Draw(JsonConvert.SerializeObject(authResult));
        }

 
        public struct SignUpResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}
