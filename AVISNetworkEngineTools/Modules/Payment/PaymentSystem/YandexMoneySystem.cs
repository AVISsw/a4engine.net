﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Yandex.Checkout.V3;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class YandexMoneySystem:BasePaymentSystem
    {
        static readonly Client _client = new Client("501156", "test_As0OONRn1SsvFr0IVlxULxst5DBIoWi_tyVaezSRTEI");
        public YandexMoneySystem()
        {
          
        }

        public override bool ClientSuccess(RequestContext requestContext)
        {
            return true;
        }

        public override string ExchangeRates(RequestContext context)
        {
            throw new NotImplementedException();
        }

        public override float GetPayAmount(string gameAmount)
        {
            throw new NotImplementedException();
        }

        public override PaymentContext GetPaymentContext(RequestContext requestContext)
        {
            throw new NotImplementedException();
        }

        public override string RedirectLink(RequestContext Context)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string out_summ = GetOrderTotal(Context.getParams.Get("amount"));
            long characterId = Convert.ToInt64(Context.getParams.Get("characterid"));

            // 1. Создайте платеж и получите ссылку для оплаты
            decimal amount = decimal.Parse(out_summ, CultureInfo.InvariantCulture.NumberFormat);
            var newPayment = new NewPayment
            {
                Amount = new Amount { Value = amount, Currency = "RUB" },
                Confirmation = new Confirmation { Type = ConfirmationType.Redirect, ReturnUrl = "https://sota-games-server.ru/page/payment_success" },
                Metadata = new Dictionary<string, string>() { { "CharacterId", characterId.ToString() } }
            };
            Yandex.Checkout.V3.Payment payment = _client.CreatePayment(newPayment);

            // 2. Перенаправьте пользователя на страницу оплаты
            string url = payment.Confirmation.ConfirmationUrl;
            return url;
        }
 
        public override bool RequestAuthentication(RequestContext context)
        {
            // Чтобы получить это уведомление, нужно указать адрес этой страницы
            // в настройках магазина (https://kassa.yandex.ru/my/tunes).
            try
            {
                logger.Info($"Confirm: Request.HttpMethod={context.Request.Request.HttpMethod}, Request.ContentType={context.Request.Request.ContentType}, Request.InputStream has {context.Request.Request.ContentLength64} bytes");
                Message message = Client.ParseMessage(context.Request.Request.HttpMethod, context.Request.Request.ContentType, context.Request.Request.InputStream);
                Yandex.Checkout.V3.Payment payment = message?.Object;
                if (message?.Event == Event.PaymentWaitingForCapture && payment.Paid)
                {
                    logger.Info($"Got message: payment.id={payment.Id}, payment.paid={payment.Paid}");

                    // 4. Подтвердите готовность принять платеж
                   var result = _client.CapturePayment(payment.Id);
                   logger.Info("result.Status " + result.Status.ToString());
                   return true;
                }
            }
            catch (Exception exception)
            {
                logger.Info(exception.ToString());
            }
            return false;
        }

        public override bool TestRequest(RequestContext requestContext)
        {
            throw new NotImplementedException();
        }
    }
}
