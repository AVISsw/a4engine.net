﻿using A4Engine.User;
using AVISNetworkCommon;
using AVISNetworkEngineORM.DB;
using System.Collections.Generic;
using System.Text;
using A4Engine.Modules;
using System.Threading.Tasks;
using System;
using A4Engine.Transport;

namespace A4Engine.Controllers.Page
{
    public class Page : BaseController
    {
        Logger Log = new Logger(Logger.LoggerType.WriteConsole);
        Dictionary<string, PageData> pages = new Dictionary<string, PageData>();
        
        public override async Task Init(RequestContext Context, SessionManager sessionManager)
        {
            Storage = new Storage(Settings.Server.Host, Settings.Server.Name, Settings.Server.User, Settings.Server.Password, Settings.Server.Port);
            SessionManager = sessionManager;


            string key = Context.Path.Length > 2 ? Context.Path[2] : Settings.Server.DefaultPage;
            string index;
            PageData pageData = GetPage(key);
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (pageData != null)
            {
                index = FileGetContents(DirController + pageData.Pattern);
                Log.Info(DirController + pageData.Pattern);
                var modulesData = GetModules(index);
                result.Add("title", pageData.Name);
                foreach (var moduleData in modulesData)
                {
                    var queryData = Query(moduleData);
                    Context.moduleParams = queryData;
                    string moduleName = queryData["module"];
                    Module module = Context.Router.GetModule(moduleName);
                    if (module != null)
                    {
                        module.Controller = this;
                        module.SessionManager = SessionManager;
                        module.User = SessionManager.GetSessionByCookie(Context);
                        await module.Before(Context);
                        result.Add("query " + moduleData, module.Render());
                    }
                    else
                    {
                        result.Add("query " + moduleData, "Module not found");
                    }
                }
 
                Context.Request.Response.ContentType = "text/html";
                Context.Request.Response.StatusCode = HttpResponseCode.Ok;
                Context.Request.Response.ContentEncoding = Encoding.UTF8;
            }
            else
            {
                Context.Request.Response.ContentType = "text/html";
                Context.Request.Response.StatusCode = HttpResponseCode.NotFound;
                Context.Request.Response.ContentEncoding = Encoding.UTF8;
                index = FileGetContents(DirController + "error_404.html");
            }
            Draw(FillPattern(index, result));
            if (!string.IsNullOrWhiteSpace(Context.RedirectHeader))
            {
                Context.Request.Response.Redirect(Context.RedirectHeader);
            }
        }

        public PageData GetPage(string key)
        {
            if (pages.ContainsKey(key))
                return pages[key];
            return null;
        }

        internal override async Task OnLoad()
        {
            Storage = new Storage(Settings.Server.Host, Settings.Server.Name, Settings.Server.User, Settings.Server.Password, Settings.Server.Port);
            using (var data = await Storage.GetAll.WhereBase<PageData>(""))
            {
                while (data.Read)
                {
                    var pageData = data.GetObject<PageData>();
                    pages.Add(pageData.Key, pageData);
                }
            }
        }
    }
}
