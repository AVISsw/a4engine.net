﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace AVISNetworkEngineTools.Modules.CraftEdtior
{
    public class ItemModsHandler : Handler
    {
        Logger Log;
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }
        public async Task<List<ItemTemplateData>> GetItemList()
        {
            List<ItemTemplateData> result = new List<ItemTemplateData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemTemplateData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemTemplateData>();
                    result.Add(itemData);
                }
            }

            return result;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            if (User.GroupRight.Can("editScene"))
            {
                switch (key)
                {
                    case "update-item-mods":
                        Draw(JsonConvert.SerializeObject(await this.UpdateMod(Context)));
                        break;
                    case "remove-all-mod-by-item":
                        Draw(JsonConvert.SerializeObject(await this.RemoveMod(Context)));
                        break;
                    case "get-item-mods":
                        Draw(JsonConvert.SerializeObject(await this.GetItemMods()));
                        break;
                    case "get-item-list":
                        Draw(JsonConvert.SerializeObject(await this.GetItemList()));
                        break;
                    default:
                        Draw(JsonConvert.SerializeObject(
                            new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                        }
                        ));
                        break;
                }
                return;
            }
            Draw("<b>Доступ запрещен!</b>");

        }

        private async Task<ResultItemModsObject> GetItemMods()
        {
            ResultItemModsObject result = new ResultItemModsObject();
            try
            {
 

                result.ModTemplates = new List<ItemModTemplate>();
                using (var baseData = await Controller.Storage.GetAll.WhereBase<ItemModTemplate>(""))
                {
                    while (baseData.Read)
                    {
                        var temp = baseData.GetObject<ItemModTemplate>();
                        temp.IsDbExits = true;
                        result.ModTemplates.Add(temp);
                    }
                }
                List<ModEffectTemplate> ModEffectTemplates = new List<ModEffectTemplate>();
                using (var baseData = await Controller.Storage.GetAll.WhereBase<ModEffectTemplate>(""))
                {
                    while (baseData.Read)
                    {
                        var temp = baseData.GetObject<ModEffectTemplate>();
                        temp.IsDbExits = true;
                        ModEffectTemplates.Add(temp);
                    }
                }

                List<ModConditionItemData> ModConditionItemItems = new List<ModConditionItemData>();
                using (var baseData = await Controller.Storage.GetAll.WhereBase<ModConditionItemData>(""))
                {
                    while (baseData.Read)
                    {
                        var temp = baseData.GetObject<ModConditionItemData>();
                        temp.IsDbExits = true;
                        ModConditionItemItems.Add(temp);
                    }
                }

                foreach (var modTemplate in result.ModTemplates)
                {
                    foreach (var modEffect in ModEffectTemplates)
                    {
                        if (modTemplate.Id == modEffect.ModTemplateId)
                        {
                            if (modTemplate.Effects == null)
                            {
                                modTemplate.Effects = new List<ModEffectTemplate>();
                            }
                            modTemplate.Effects.Add(modEffect);
                        }
                    }
                    foreach (var modConditionItem in ModConditionItemItems)
                    {
                        if (modTemplate.Id == modConditionItem.ModId)
                        {
                            if(modTemplate.RequireItemId == null)
                            {
                                modTemplate.RequireItemId = new List<ModConditionItemData>();
                            }
                            modTemplate.RequireItemId.Add(modConditionItem);
                        }
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
 
            return result;
        }

        private async Task<ResultItemModsObject> UpdateMod(RequestContext context)
        {
            ResultItemModsObject finalresult = JsonConvert.DeserializeObject<ResultItemModsObject>(context.rawParams);
            List<ModEffectTemplate> ModEffectTemplates = new List<ModEffectTemplate>();
            List<ModConditionItemData> ModConditionItemItems = new List<ModConditionItemData>();
            for (int i = finalresult.ModTemplates.Count - 1; i >= 0; i--)
            {
                var modTemplate = finalresult.ModTemplates[i];
                if (modTemplate.IsRemove && modTemplate.IsDbExits)
                {
                    bool isRemove = await Controller.Storage.Remove.Where<ItemModTemplate>("Id={0}", modTemplate.Id);
                    if (isRemove)
                    {
                        modTemplate.IsRemove = false;
                        finalresult.ModTemplates.RemoveAt(i);
                    }
                }
                else
                {
                    if (modTemplate.IsDbExits)
                    {
                        bool isUpdate = await Controller.Storage.Update.Where(modTemplate, "Id={0}", modTemplate.Id);
                        modTemplate.IsDbExits = true;
                    }
                    else
                    {
                        modTemplate.Id = await Controller.Storage.Add.In(modTemplate);
                        modTemplate.IsDbExits = true;
                    }
                }
                foreach(var effect in modTemplate.Effects)
                {
                    effect.ModTemplateId = modTemplate.Id;
                }
                foreach (var itemData in modTemplate.RequireItemId)
                {
                    itemData.ModId = modTemplate.Id;
                }
                ModEffectTemplates.AddRange(modTemplate.Effects);
                ModConditionItemItems.AddRange(modTemplate.RequireItemId);
            }
            for (int i = ModEffectTemplates.Count - 1; i >= 0; i--)
            {
                var modEffectTemplate = ModEffectTemplates[i];
                if (modEffectTemplate.IsRemove && modEffectTemplate.IsDbExits)
                {
                    bool isRemove = await Controller.Storage.Remove.Where<ModEffectTemplate>("Id={0}", modEffectTemplate.Id);
                    if (isRemove)
                    {
                        modEffectTemplate.IsRemove = false;
                        ModEffectTemplates.RemoveAt(i);
                    }
                }
                else
                {
                    if (modEffectTemplate.IsDbExits)
                    {
                        bool isUpdate = await Controller.Storage.Update.Where(modEffectTemplate, "Id={0}", modEffectTemplate.Id);
                        if (isUpdate)
                        {
                            modEffectTemplate.IsDbExits = true;
                        }
                    }
                    else
                    {
                        modEffectTemplate.Id = await Controller.Storage.Add.In(modEffectTemplate);
                        modEffectTemplate.IsDbExits = true;
                    }
                }
            }
            for (int i = ModConditionItemItems.Count - 1; i >= 0; i--)
            {
                var modConditionItem = ModConditionItemItems[i];
                if (modConditionItem.IsRemove && modConditionItem.IsDbExits)
                {
                    bool isRemove = await Controller.Storage.Remove.Where<ModConditionItemData>("Id={0}", modConditionItem.Id);
                    if (isRemove)
                    {
                        modConditionItem.IsRemove = false;
                        ModEffectTemplates.RemoveAt(i);
                    }
                }
                else
                {
                    if (modConditionItem.IsDbExits)
                    {
                        bool isUpdate = await Controller.Storage.Update.Where(modConditionItem, "Id={0}", modConditionItem.Id);
                        if (isUpdate)
                        {
                            modConditionItem.IsDbExits = true;
                        }
                    }
                    else
                    {
                        modConditionItem.Id = await Controller.Storage.Add.In(modConditionItem);
                        modConditionItem.IsDbExits = true;
                    }
                }
            }
            return finalresult;
        }
 
        private async Task<bool> RemoveMod(RequestContext context)
        {
            Console.WriteLine("Id " + context.postParams.Get("Id"));
            long modId = Convert.ToInt64(context.postParams.Get("Id"));
            bool result = await Controller.Storage.Remove.Where<ItemModTemplate>("Id={0}", modId);
            result &= await Controller.Storage.Remove.Where<ModEffectTemplate>("ModTemplateId={0}", modId);
            return result;
        }
        private async Task<ResultItemModsObject> GetModData(long itemId)
        {
            ResultItemModsObject result = default;
            result.ItemId = itemId;
            result.ModTemplates = new List<ItemModTemplate>();
            List<long> idList = new List<long>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ItemModTemplate>("ItemId={0}", itemId))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<ItemModTemplate>();
                    temp.IsDbExits = true;
                    result.ModTemplates.Add(temp);
                    idList.Add(temp.Id);
                }
            }
            var ModEffectTemplates = new List<ModEffectTemplate>();
            string andWhere = "";
            andWhere += string.Join(" OR ModTemplateId =", idList);
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ModEffectTemplate>(andWhere))
            {
                if (baseData.Read)
                {
                    var temp = baseData.GetObject<ModEffectTemplate>();
                    temp.IsDbExits = true;
                    ModEffectTemplates.Add(temp);
                }
            }
            return result;
        }
        public class ResultItemModsObject
        {
            public long ItemId;
            public List<ItemModTemplate> ModTemplates;
        }
    } 
}
