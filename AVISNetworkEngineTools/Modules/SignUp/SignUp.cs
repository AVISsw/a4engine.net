﻿using A4Engine.User;
using AVISNetworkCommon.Utility;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using A4Engine.Modules;
using AVISNetworkEngineTools.Modules.PromoCode;
using AVISNetworkEngineTools.Modules;
using AVISNetworkEngineTools.Modules.SignUp;
using System.Threading.Tasks;
using DataORM.Models;
using static DataORM.Models.PromoCodeData;
using AVIS.Localization;
using System.Globalization;

namespace A4Engine.Modules.SignUp
{
    public class SignUp : Handler
    {
        Random rand = new Random();
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
              return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            try
            {

                Log.Info("SignUpRequest");
                SignUpResult authResult = new SignUpResult();
                authResult.Success = true;
                AuthData authData = new AuthData();
                SetLang(Context);

                string culture = Context.postParams["culture"];
                if (string.IsNullOrWhiteSpace(culture))
                {
                    culture = "ru-RU";
                }
                authData.Password = Crypt.EncryptSHA256(Context.postParams["password"].Trim());
                authData.Login = Context.postParams["login"];
                authData.Email = Context.postParams["email"];

                authData.Login = authData.Login.ToLower().Trim();
                authData.Email = authData.Email.ToLower().Trim();

                DateTime confirmLastRequest = User.Data.Get<DateTime>("SignUpMailLastRequest");
                TimeSpan deltaTime = confirmLastRequest.AddSeconds(20) - DateTime.Now;
                if (deltaTime.TotalSeconds <= 0)
                {
                    confirmLastRequest = DateTime.Now;
                    User.Data.Save("SignUpMailLastRequest", confirmLastRequest);
                    string birthdayString = Context.postParams["birthday"];
                    try
                    {
                        authData.Birthday = DateTime.Parse(birthdayString, new CultureInfo(culture));
                    }
                    catch (Exception ex)
                    {
                       // authResult.Success = false;
                        authResult.Message += new TranslatableString("sign_up_error_date");
                    }
                    try
                    {
                        authData.RegIp = Context.Request.Request.RemoteEndPoint.Address.ToString();
                        await Task.Factory.StartNew(delegate
                        {
                            authData.RegMac = Network.ConvertIpToMAC(Context.Request.Request.RemoteEndPoint.Address);
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Не удалось получить данные об ip юзера");
                    }

                    var findAccount = await FindAccountByData(authData);
                    string promocode = "";
                    try
                    {
                        promocode = Context.postParams["promocode"];
                    }
                    catch
                    {
                        Console.WriteLine("Промокод не указан ");
                    }
                    Console.WriteLine("1promocode " + promocode);
                    PromoCodeData promoResult = await UsePromo.СheckPromocode(promocode, Controller);
                    Console.WriteLine("2promocode " + promocode);
                    switch (SignUpSettings.SettingsData.Type)
                    {
                        case SignUpSettingsData.SignUpType.Open:
                            promoResult.Limited = false;
                            if (string.IsNullOrWhiteSpace(promocode))
                            {
                                promoResult.Success = true;
                            }
                            promoResult.Type = CodeType.Invite;
                            break;
                    }
                    Console.WriteLine("3promocode " + promocode);
                    Console.WriteLine("promoResult " + JsonConvert.SerializeObject(promoResult));
                    if (promoResult.Success == false)
                    {
                        authResult = new SignUpResult() { Success = promoResult.Success, ErrorCode = 15, Message = promoResult.Message };
                        Draw(JsonConvert.SerializeObject(authResult));
                        return;
                    }

                    User.Data.Save("promocode", promocode);

                    if (string.IsNullOrWhiteSpace(authData.Email))
                    {
                        authResult.Success = false;
                        authResult.Message += new TranslatableString("sign_up_error_email");
                    }

                    if (promoResult.Type != CodeType.Invite && promoResult.Success)
                    {
                        authResult.Message += new TranslatableString("sign_up_error_invite");
                        authResult.Success = false;
                    }

                    if (findAccount != null)
                    {
                        authResult.Success = false;
                        if (!string.IsNullOrWhiteSpace(findAccount.Email))
                        {
                            if (findAccount.Email.ToLower() == authData.Email)
                            {
                                authResult.Message += "Аккаунт с таким Email уже зарегистрирован \n";
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(findAccount.Login))
                        {
                            if (findAccount.Login.ToLower() == authData.Login)
                            {
                                authResult.Message += "Аккаунт с таким логином уже зарегистрирован \n";
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(findAccount.Email))
                        {
                            if (findAccount.Login.ToLower() == authData.Phone)
                            {
                                authResult.Message += "Аккаунт с таким телефоном уже зарегистрирован \n";
                            }
                        }
                    }

                    if (authResult.Success == true)
                    {
                        int code = rand.Next(100000, 999999);
                        Log.Info(JsonConvert.SerializeObject(authData));
                        Mail.Get().SendEmail(authData.Email, new TranslatableString("sign_up_email_title"), string.Format(new TranslatableString("sign_up_email_body"), code));
                        authResult.Success = true;
                        if (string.IsNullOrWhiteSpace(authResult.Message))
                        {
                            authResult.Message = new TranslatableString("sign_up_next_notify");
                        }
                        authData.Promo = promoResult.Code;
                        User.Data.Save("SignUpCode", code);
                        User.Data.Save("AuthData", authData);
                        User.Data.Save("SignUpEndRequest", DateTime.Now.AddMinutes(15));
                        User.Data.Save("SignUpSendPhoneEndRequest", DateTime.Now.AddMinutes(30));
                        User.Data.Save("SignUpConfirmPhoneEndRequest", DateTime.Now.AddMinutes(45));
                        DateTime SignUpEndRequest = User.Data.Get<DateTime>("SignUpEndRequest");
                        DateTime SignUpSendPhoneEndRequest = User.Data.Get<DateTime>("SignUpSendPhoneEndRequest");

                    }
                }
                else
                {
                    authResult = new SignUpResult() { Success = false, ErrorCode = 2, Message = string.Format(new TranslatableString("sign_up_wait_email"), (int)deltaTime.TotalSeconds) };
                }
                Console.WriteLine("authResult " + JsonConvert.SerializeObject(authResult));
                Draw(JsonConvert.SerializeObject(authResult));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public Task<AuthData> FindAccountByData(AuthData authData)
        {
            return Controller.Storage.Get.Where<AuthData>(" Login={0} OR Email={1}", authData.Login, authData.Email);
        }
        public Task<CharacterData> FindCharacterByName(string name)
        {
            return Controller.Storage.Get.Where<CharacterData>("Name={0}", name);
        }



        public struct SignUpResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}
