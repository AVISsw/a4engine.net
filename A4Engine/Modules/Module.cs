﻿using A4Engine;
using A4Engine.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4Engine.Controllers;
using A4Engine.Controllers.Interface;
using A4Engine.Modules.Interface;


namespace A4Engine.Modules
{
    public abstract class Module : Base, IModule
    {
        public string DirModule { get { return string.Format("{0}"+Asset.Folder+"/modules/{1}", AppDomain.CurrentDomain.BaseDirectory, this.GetType().Name.ToLower()); } }
        public string UriModule { get { return string.Format("/files/modules/{0}", this.GetType().Name.ToLower()); } }
        public BaseController Controller { get; set; }
        public SessionManager SessionManager { get; set; }
        public Session User { get; set; }
        public string Alias { get; set; } = string.Empty;
        public string Name
        {
            get
            {
                string result = string.Empty;
                if (Alias.Length < 1)
                {
                    result = GetType().Name;
                }
                else
                {
                    result = Alias;
                }
                return result.ToLower();
            }
        }
        protected string result_data = string.Empty;

        public Module()
        {

        }

        public async Task Before(RequestContext Context)
        {
            await this.Start(Context);
        }

        // вызывается при создании экземпляра модуля
        public abstract Task Start(RequestContext Context);

        /*
        * Вызывается для загрузки общих ресурсов модуля, всех его экземпляров
        * */
        public abstract Task OnLoad();

        protected virtual void Draw(string result)
        {
            result_data += result;
        }

        public string Render()
        {
            var temp = result_data;
            result_data = string.Empty;
            return temp;
        }
    }
}