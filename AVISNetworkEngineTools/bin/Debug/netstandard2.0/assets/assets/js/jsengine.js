﻿/**
 * Created by AVIS on 11.09.2018.
 * Функции для работы с шаблонами
 */
function fill_elem_assoc(idContiner, assoc_array) {
    $continer = $(idContiner);
    for (var key in assoc_array) {
        var val = assoc_array[key];
        $continer.find('*[name=' + key + ']').html(val);
        console.log(key);
    }
}
function fill_elem_assoc_val(idContiner, assoc_array) {
    $(idContiner).each(function () {
        $(this).val('twe');
    });
}

/**
 * name = ключ ассоциативного массива
 * Функция заменяет выражения типа {name/} в шаблоне,
* */
function fill_pattern(contents, assoc_array) {

    if (typeof (contents) != "string")
        throw '[fill_pattern] contents: не является строкой';

    for (var key in assoc_array) {
        var val = assoc_array[key];
        contents = contents.replace(new RegExp('{' + key + '/}', 'g'), val);
    }
    return contents;
}
function clear_pattern(contents) {

    if (typeof (contents) != "string")
        throw '[fill_pattern] contents: не является строкой';

    contents = contents.replace(new RegExp('{(.*?)/}', 'g'), "");

    return contents;
}

function fill_elem_json(idContiner, jsonData) {
    var assoc_array = JSON.parse(jsonData);
    fill_elem_assoc(idContiner, assoc_array);
}
function fill_elem_json_val(idContiner, jsonData) {
    var assoc_array = JSON.parse(jsonData);
    fill_elem_assoc_val(idContiner, assoc_array);
}

function Menu(elem) {
    $(".left-header").toggle(
      function () {
        $(elem).hide("slide", { direction: "right" }, 1000);
      },
      function () {
        $(elem).show("slide", { direction: "right" }, 500);
      }
    );
  }
  
function get_path() {
    return document.location.pathname.split("/");
}
function Serilaze(select) {
    var $data = {};
    // переберём все элементы input, textarea и select формы с id="myForm "
    select.find('input, button, textearea, select').each(function () {
        // добавим новое свойство к объекту $data
        // имя свойства – значение атрибута name элемента
        // значение свойства – значение свойство value элемента

        if (this.type == "checkbox") {
            $data[this.name] = this.checked;
        } else {
            $data[this.name] = $(this).val();
        }
    });
    return $data;
}
function DrawOptions(selectList, selectedValue = "") {
    var tempEffects = "";
    if (Array.isArray(selectList))
        selectList.forEach(element => {
            var selected = "";
            if (element.Value == selectedValue)
                selected = "selected";
            var temprow = fill_pattern('<option value="{Value/}" ' + selected + '>{Name/}</option>', element);
            tempEffects += temprow;
        });
    return tempEffects;
}

function SetSelect(htmlSelect, val) {
    if (htmlSelect) {
        var $option = htmlSelect.find('option[value="' + val + '"]');
        if ($option) {
            $option.attr('selected', true);
        }
    }
}

function ShowSelectWindow(data,callback, selected=-1) {
    var template = $("#SearchTemplate");
    var itemTemplate = $("#SearchItemTemplate");
    var tempRow = "";

    if (Array.isArray(data)) {
        data.forEach(element => {
            tempRow += fill_pattern(itemTemplate.html(), element);
        });
    }

    var body = ShowModal(template.html(), "Выбор");
    body.find(".ItemList").html(tempRow);

    body.find('.searchInput').keyup(function () {
        // Search text
        var text = $(this).val().toLowerCase();

        // Hide all content class element
        body.find('.content').hide();
        var maxCount = 15;
        var count = 0;
        // Search 
        body.find('.content').each(function () {
            $(this).find("b").each(function () {
                if ($(this).html().toLowerCase().indexOf("" + text + "") != -1) {
                    if(count < maxCount){
                    $(this).closest('.content').show();
                    count++;
                    }
                }
            });
        });
    });

    if(selected > -1){
        $("#radio_"+selected).attr("checked",true);
    }

    body.find('.searchInput').keyup();
    body.find('.select').click(function () {
        callback($("input[name='selectId']:checked"));
        $(".close-modal").click();
    });
}

function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}
function ShowModal(content, head = "") {
    $("#myModal").modal('show');
    $('#myModal').find(".modal-title").html(head);
    $('#myModal').find(".modal-body").html(content);
    return $('#myModal').find(".modal-body");
}
function CloseModal() {
    $("#myModal").modal('hide');
}

Date.prototype.toMysqlFormat = function () {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};