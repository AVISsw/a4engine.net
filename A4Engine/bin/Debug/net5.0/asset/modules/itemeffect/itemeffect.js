
function SaveEffect(id) {
  var form = $('#' + id + "_effect");
  var data = Serilaze(form);
  updateEffectRequest(id, data);
}

function RemoveEffect(id) {
  var descArea = $('#removeEffect').html();
  descArea = fill_pattern(descArea, { ItemId: id });
  ShowModal(descArea, "Редактироване описания");
}
var propertylist = [];
function OpenEffectEditor(id) {
  $.post(
    "/handler/effecteditor/getpropertylist",
    {},
    function (datapropertylist) {
 
      propertylist = datapropertylist;
      var EffectEditorTemplate = $('#EffectEditor').html();
      EffectEditorTemplate = fill_pattern(EffectEditorTemplate, { ItemId: id });
      ShowModal(EffectEditorTemplate, "Редактор эффектов");
      updateEffectList(id);
    }
  );
}
function isEmpty(str) {
  if (str != null && typeof str !== "undefined") {
    str = str.trim();
  }
  return !str;
}
function updateEffectList(id) {
  getItemEffectRequest(id, function (data) {
    var EffectTemplate = $('#EffectTemplate').html();
    var tempEffects = "";
    var options = [];
    if (Array.isArray(propertylist)) {
      propertylist.forEach(element => {
        if (isEmpty(element.Name)) {
          element.Name = element.Key;
        }
        options.push({ Name: element.Name, Value: element.Key });
      });
    }

    if (Array.isArray(data))
      data.forEach(element => {
        var fields = [
          {
            Name: "Абсолютное значение",
            Value: "Value"
          },
          {
            Name: "Мин.Значение",
            Value: "MinValue"
          },
          {
            Name: "Макс.Значение",
            Value: "MaxValue"
          },
          {
            Name: "Скорость изменения / сек.",
            Value: "PerTime"
          }
        ];
        console.log(element);
        element.SelectPropertyName = DrawOptions(options, element.PropertyName);
        element.SelectValueName = DrawOptions(fields, element.ValueName);
        
        var temprow = fill_pattern(EffectTemplate, element);
        var divRow = $(temprow);
        divRow.find('.Permanent').attr("checked", element.Permanent);
        divRow.find('.InstanceEffect').attr("checked", element.InstanceEffect);
        tempEffects += divRow[0].outerHTML;
      });
    $("#EffectList").html(tempEffects);
  });
}

function updateEffectRequest(id, data) {
  $.post(
    "/handler/effecteditor/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function getItemEffectRequest(id, callback) {
  $.post(
    "/handler/effecteditor/get/" + id,
    {
    },
    callback
  );
}


function addNewEffectRequest(itemId) {
  // addItem();
  $.post(
    "/handler/effecteditor/add/",
    { ItemId: itemId },
    function (data) { updateEffectList(itemId); }
  );
}

function removeEffectRequest(id) {
  $.post(
    "/handler/effecteditor/remove/" + id,
    {},
    function (data) { ShowModal(data, "Результат удаления"); }
  );
}
