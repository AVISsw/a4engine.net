﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace AVISNetworkEngineTools.Modules.CraftEdtior
{
    public class CraftEditorHandler : Handler
    {

        Logger Log;
        public override Task Before(RequestContext Context)
        {
           return Start(Context);  
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }
        public async Task<Dictionary<long, ItemTemplateData>> GetItemList()
        {
            Dictionary<long, ItemTemplateData> result = new Dictionary<long, ItemTemplateData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemTemplateData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemTemplateData>();
                    result.Add(itemData.ItemId, itemData);
                }
            }

            return result;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            if (User.GroupRight.Can("editScene"))
            {
                switch (key)
                {
                    case "get-recept":
                        Draw(JsonConvert.SerializeObject(await this.GetRecept(Context)));
                        break;
                    case "add-recept":
                        Draw(JsonConvert.SerializeObject(await this.AddRecept(Context)));
                        break;
                    case "update-recept":
                        Draw(JsonConvert.SerializeObject(await this.UpdateRecept(Context)));
                        break;
                    case "remove-recept":
                        Draw(JsonConvert.SerializeObject(await this.RemoveRecept(Context)));
                        break;
                    case "get-recept-list":
                        Draw(JsonConvert.SerializeObject(await this.GetReceptList()));
                        break;
                    case "get-item-list":
                        Draw(JsonConvert.SerializeObject(await this.GetItemList()));
                        break;
                    default:
                        Draw(JsonConvert.SerializeObject(
                            new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                        }
                        ));
                        break;
                }
                return;
            }
            Draw("<b>Доступ запрещен!</b>");

        }

        private async Task<List<ResultReceptObject>> GetReceptList()
        {
            Dictionary<long, ResultReceptObject> result = new Dictionary<long, ResultReceptObject>();

            using (var baseData = await Controller.Storage.GetAll.WhereBase<ReceptData>(""))
            {
                while (baseData.Read)
                {
                    var ReceptData = baseData.GetObject<ReceptData>();
                    ReceptData.IsDbExits = true;
                    result.Add(ReceptData.Id, new ResultReceptObject()
                    {
                        ReceptData = ReceptData,
                        Result = new List<ReceptResultItemData>(),
                        Conditions = new List<ReceptConditionItemData>()
                    });
                }
            }
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ReceptConditionItemData>(""))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<ReceptConditionItemData>();
                    temp.IsDbExits = true;
                    if (result.ContainsKey(temp.ReceptId))
                    {
                        result[temp.ReceptId].Conditions.Add(temp);
                    }
                }
            }
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ReceptResultItemData>(""))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<ReceptResultItemData>();
                    temp.IsDbExits = true;
                    if (result.ContainsKey(temp.ReceptId))
                    {
                        result[temp.ReceptId].Result.Add(temp);
                    }
                }
            }

            return result.Values.ToList();
        }

        private async Task<ResultReceptObject> UpdateRecept(RequestContext context)
        {
            ResultReceptObject finalresult = JsonConvert.DeserializeObject<ResultReceptObject>(context.rawParams);
    
            if (finalresult.ReceptData.IsDbExits)
            {
                bool isUpdate = await Controller.Storage.Update.Where(finalresult.ReceptData, "Id={0}", finalresult.ReceptData.Id);
                if (isUpdate)
                {
                    finalresult.ReceptData.IsDbExits = isUpdate;
                }
            }
            else
            {
                finalresult.ReceptData.Id = await Controller.Storage.Add.In(finalresult.ReceptData);
                finalresult.ReceptData.IsDbExits = true;
            }
            for (int i = finalresult.Conditions.Count - 1; i >= 0; i--)
            {
                var condition = finalresult.Conditions[i];
                if (condition.IsRemove && condition.IsDbExits)
                {
                    bool isRemove = await Controller.Storage.Remove.Where<ReceptConditionItemData>("Id={0}", condition.Id);
                    if (isRemove)
                    {
                        condition.IsRemove = false;
                        finalresult.Conditions.RemoveAt(i);
                    }
                }
                else
                {
                    if (condition.IsDbExits)
                    {
                        bool isUpdate = await Controller.Storage.Update.Where(condition, "Id={0}", condition.Id);
                        if (isUpdate)
                        {
                            condition.IsDbExits = true;
                        }
                    }
                    else
                    {
                        condition.Id = await Controller.Storage.Add.In(condition);
                        condition.IsDbExits = true;
                    }
                }

            }
            for (int i = finalresult.Result.Count - 1; i >= 0; i--)
            {
                var result = finalresult.Result[i];
                if (result.IsRemove&& result.IsDbExits)
                {
                    bool isRemove = await Controller.Storage.Remove.Where<ReceptResultItemData>("Id={0}", result.Id);
                    if (isRemove)
                    {
                        result.IsRemove = false;
                        finalresult.Result.RemoveAt(i);
                    }
                }
                else
                {
                    if (result.IsDbExits)
                    {
                        bool isUpdate = await Controller.Storage.Update.Where(result, "Id={0}", result.Id);
                        if (isUpdate)
                        {
                            result.IsDbExits = true;
                        }
                    }
                    else
                    {
                        result.Id = await Controller.Storage.Add.In(result);
                        result.IsDbExits = true;
                    }
                }
            }
            return finalresult;
        }
        private async Task<ResultReceptObject> AddRecept(RequestContext context)
        {
            ReceptData receptData = Controller.GetObjectByParams<ReceptData>(context.postParams);
            long lastId = await Controller.Storage.Add.In(receptData);
            receptData.Id = lastId;

            return new ResultReceptObject() { ReceptData = receptData, Conditions = new List<ReceptConditionItemData>(), Result = new List<ReceptResultItemData>() };
        }
        private async Task<bool> RemoveRecept(RequestContext context)
        {
            Console.WriteLine("Id " + context.postParams.Get("Id"));
            long receptId = Convert.ToInt64(context.postParams.Get("Id"));
            bool result = await Controller.Storage.Remove.Where<ReceptData>("Id={0}", receptId);

            return result;
        }
        private async Task<ResultReceptObject> GetRecept(RequestContext Context)
        {
            long receptId = Convert.ToInt64(Context.postParams.Get("id"));
            ResultReceptObject result = default;
            result.ReceptData = await Controller.Storage.Get.Where<ReceptData>("Id={0}", receptId);
            result.ReceptData.IsDbExits = true;

            result.Conditions = new List<ReceptConditionItemData>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ReceptConditionItemData>("ReceptId={0}", receptId))
            {
                while (baseData.Read)
                {
                    var temp = baseData.GetObject<ReceptConditionItemData>();
                    temp.IsDbExits = true;
                    result.Conditions.Add(temp);
                }
            }
            result.Result = new List<ReceptResultItemData>();
            using (var baseData = await Controller.Storage.GetAll.WhereBase<ReceptResultItemData>("ReceptId={0}", receptId))
            {
                if (baseData.Read)
                {
                    var temp = baseData.GetObject<ReceptResultItemData>();
                    temp.IsDbExits = true;
                    result.Result.Add(temp);
                }
            }
            return result;
        }
        public class ResultReceptObject
        {
            public ReceptData ReceptData;
            public List<ReceptConditionItemData> Conditions;
            public List<ReceptResultItemData> Result;
        }
    }
}
