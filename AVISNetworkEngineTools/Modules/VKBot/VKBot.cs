﻿using A4Engine.Modules;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Abstractions;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Utils;

namespace AVISNetworkEngineTools.Modules.VKBot
{
    public class VKBot : Handler
    {
        public VKBot()
        {
            instance = this;
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }
        long adminId = 147269118;
        string AccessToken = "20a145ee99e0dabca27da6407e1e6d0ddc901bb2b2ae8d7efd2020f9cc017abea7001a17694b354d3ad22";
        string Secret = "fek22gQsvhp4f21gbnj7ecvzkoi";
        string GroupUrl = "https://vk.com/s_o_t_a";
        private VkApi _vkApi;
        Random rand;
        List<VeteranData> Veterans;
        static VKBot instance;
        public override async Task OnLoad()
        {

            rand = new Random();
            Console.WriteLine("Load VKBOT-----------------------------------");
            try
            {
                _vkApi = new VkApi();
                _vkApi.Authorize(new ApiAuthParams { AccessToken = AccessToken });

                Console.WriteLine($"VKBOT IsAuthorized: {_vkApi.IsAuthorized}");
                _vkApi.Messages.Send(new MessagesSendParams
                {
                    RandomId = rand.Next(),
                    PeerId = adminId,
                    Message = "Запустил веб сервер!"
                });
                WebPanel.Index.OnDispose += Index_OnDispose;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Monitoring.Monitoring.GameServerStartNotify += Monitoring_GameServerStartNotify;
            Monitoring.Monitoring.GameServerOffNotify += Monitoring_GameServerOffNotify;
            Console.WriteLine($"11111111111111======= Начал загрузку список ветеранов");
            try
            {
                Veterans = new List<VeteranData>();
                using (var textReader = File.OpenText("veteran_list.txt.txt"))
                {
                    while (!textReader.EndOfStream)
                    {
                        string line = textReader.ReadLine();
                        string[] data = line.Split(";");
                        Veterans.Add(new VeteranData()
                        {
                            Login = data[0].Trim().ToLower(),
                            Email = data[1].Trim().ToLower(),
                            Password = data[2].Trim().ToLower(),
                            LastEntering = DateTime.Parse(data[3])
                        });
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine($"111111======= Загружен список ветеранов в количестве: {Veterans.Count}");

        }
        async Task<string> GetVeteranPromocode(Message message)
        {
            string login = "";
            string email = "";
            string password = "";
            string text = message.Body;

            string[] rows = text.Split("\n");
            foreach (var row in rows)
            {
                if (string.IsNullOrWhiteSpace(row))
                    continue;
                string[] param_list = row.Split(";");
                if (param_list.Length < 2)
                    continue;
                for (int i = 0; i < param_list.Length; i++)
                {
                    param_list[i] = param_list[i].Trim().ToLower();
                }
                string key = param_list[0];
                string val = param_list[1];
    
                switch (key)
                {
                    case "логин":
                        login = val;
                        break;
                    case "почта":
                        email = val;
                        break;
                    case "пароль":
                        password = val;
                        break;
                }
            }
            var veteranData = CheckVeteran(login, email, password);
            bool isVeteran = veteranData != null;
            bool not_login = string.IsNullOrWhiteSpace(login) && string.IsNullOrWhiteSpace(email);
            bool not_password = string.IsNullOrWhiteSpace(password);

            string textStatus = "Не удалось найти в базе";

            if (not_login)
            {
                textStatus = "Необходимо ввести логин (логин;мой_логин) или почту (почта;моя_почта@mail.ru)";
            }
            if (not_password)
            {
                textStatus = "Необходимо ввести пароль (пароль;мой_пароль)";
            }

            if (isVeteran)
            {
                textStatus = "Ветеран";
            }
            string result = $"Статус:{textStatus}";
            if (isVeteran)
            {
                string promocode = AVISNetworkCommon.Utility.Crypt.EncryptMD5(veteranData.Login + veteranData.Email + veteranData.Password);
                var response = await PromoCode.CreatePromo.GeneratePromo(Controller, new PromoCode.CreatePromoData()
                {
                    PromoCodeData = new DataORM.Models.PromoCodeData()
                    {
                        Code = promocode,
                        Type = DataORM.Models.PromoCodeData.CodeType.BonusItem,
                        Limited = true,
                        NumberOfUses = 1,
                        Active = true,
                        Message = "Промокод с бонусом для ветеранов"
                    },
                    ItemSpawnGroup = -333
                }, true, true, -1);
                if (response.Success)
                {
                    result += $"\n Промокод:{promocode}";
                    File.AppendAllText("config/vk_notify_list.txt", message.UserId.ToString()+ "\n", Encoding.UTF8);
                }
                else
                {
                    result += $"\n Промокод:{promocode}";
                    result += $"\n Сообщение:{response.Message}";
                }
            }
            return result;
        }
        VeteranData CheckVeteran(string login, string email, string password)
        {
            foreach (var veteran in Veterans)
            {
                if ((veteran.Email == email || veteran.Login == login) && veteran.Password == password)
                {
                    return veteran;
                }
            }
            return null;
        }
        public static void SendMsgAdmVK(string message)
        {
            instance._vkApi.Messages.Send(new MessagesSendParams
            {
                RandomId = instance.rand.Next(),
                PeerId = instance.adminId,
                Message = message
            });
        }
        private void Monitoring_GameServerOffNotify()
        {
            _vkApi.Messages.Send(new MessagesSendParams
            {
                RandomId = rand.Next(),
                PeerId = adminId,
                Message = "Игровой сервер выключен!"
            });
        }

        private void Monitoring_GameServerStartNotify()
        {
            _vkApi.Messages.Send(new MessagesSendParams
            {
                RandomId = rand.Next(),
                PeerId = adminId,
                Message = "Игровой сервер запущен!"
            });
        }

        private void Index_OnDispose()
        {
            _vkApi.Messages.Send(new MessagesSendParams
            {
                RandomId = rand.Next(),
                PeerId = adminId,
                Message = "Веб сервер завершил работу!"
            });
        }

        public override async Task Start(RequestContext Context)
        {
            try
            {
                Updates updates = JsonConvert.DeserializeObject<Updates>(Context.rawParams);
                if (updates.Secret == Secret)
                {
                    switch (updates.Type)
                    {
                        // Если это уведомление для подтверждения адреса
                        case "confirmation":
                            // Отправляем строку для подтверждения 
                            Draw("10508008");
                            return;
                        case "message_new":
                            {
                                // Десериализация
                                var msg = Message.FromJson(new VkResponse(updates.Object));

                                Console.WriteLine($"vkmsg user[{msg.UserId}]: { msg.Body}");
                                // Отправим в ответ полученный от пользователя текст
                                string reciveText = "";
                                if (Has(msg.Body, "привет", "ку", "здарова ", "хай", "hi", "hello"))
                                {
                                    reciveText = "Приветствую тебя сталкер!";
                                }
                                if (Has(msg.Body, "скачать", "ссылка", "скачать?", "ссылка?"))
                                {
                                    reciveText = @"Скачай игру, введи промокод VK при регистрации, и получи стартовый набор!
Ссылка для скачивания игры: https://www.sota-games-server.ru/sota2_updater.exe";
                                }
                                if (Has(msg.Body, "промокод", "промокод?"))
                                {
                                    reciveText = @"Введи промокод VK при регистрации, и получи стартовый набор!";
                                }
                                if (Has(msg.Body, "системные", "системники", "системные?", "системники?"))
                                {
                                    reciveText = @"
СИСТЕМНЫЕ ТРЕБОВАНИЯ
МИНИМАЛЬНЫЕ:
Требуются 64-разрядные процессор и операционная система
ОС: Windows 7
Процессор: Core 2 duo
Оперативная память: 4 GB ОЗУ
Видеокарта: AMD Radeon R7 250 2GB
DirectX: Версии 11
Сеть: Широкополосное подключение к интернету
Место на диске: 5 GB
Дополнительно: Минимальные системные требования уточняются.
РЕКОМЕНДОВАННЫЕ:
Требуются 64-разрядные процессор и операционная система
ОС: Windows 10
Процессор: AMD Ryzen 5 1600 или лучше.
Оперативная память: 8 GB ОЗУ
Видеокарта: AMD Radeon R9 290 или лучше.
DirectX: Версии 11
Сеть: Широкополосное подключение к интернету
Место на диске: 5 GB";
                                }
                                if (Has(msg.Body, "ветеран"))
                                {
                                    reciveText = @"Приветствуем тебя сталкер. Для того чтобы получить код ветерана, нам необходимо убедится в этом.
Отправь сообщение по форме:

Я_ВЕТЕРАН
логин;мой_логин
пароль;мой_пароль

Если у тебя вместо логина почта то:

Я_ВЕТЕРАН
почта;моя_почта@mail.ru
пароль;мой_пароль

Обратите внимание, нужно вводить данные для входа в S.O.T.A 1";
                                    reciveText = "Акция завершилась!";
                                }
                                if (Has(msg.Body, "я_ветеран"))
                                {
                                    reciveText = "Акция завершилась!";
                                //    reciveText = await GetVeteranPromocode(msg);
                                }
                                if (msg.Body.StartsWith("/"))
                                {
                                    reciveText = "";
                                    Monitoring.Monitoring.Cmd(msg.Body);
                                }

                                if (!string.IsNullOrWhiteSpace(reciveText))
                                {
                                    _vkApi.Messages.Send(new MessagesSendParams
                                    {
                                        RandomId = rand.Next(),
                                        PeerId = msg.UserId,
                                        Message = reciveText
                                    });
                                }
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Draw("ok");
        }

        bool Has(string msg, params string[] keys)
        {
            msg = msg.ToLower();
            string[] words = msg.Split(' ', '\n');
            foreach (var key in keys)
            {
                if (words.Contains(key))
                {
                    return true;
                }
            }
            return false;
        }
        [Serializable]
        public class Updates
        {
            /// <summary>
            /// Тип события
            /// </summary>
            [JsonProperty("type")]
            public string Type { get; set; }

            /// <summary>
            /// Объект, инициировавший событие
            /// Структура объекта зависит от типа уведомления
            /// </summary>
            [JsonProperty("object")]
            public JObject Object { get; set; }

            /// <summary>
            /// ID сообщества, в котором произошло событие
            /// </summary>
            [JsonProperty("group_id")]
            public long GroupId { get; set; }

            /// <summary>
            /// Секретный ключ для проверки запроса
            /// </summary>
            [JsonProperty("secret")]
            public string Secret { get; set; }
        }
        [Serializable]
        public class VeteranData
        {
            public string Login;
            public string Email;
            public string Password;
            public DateTime LastEntering;
        }
    }
}
