﻿using A4Engine.Controllers;
using AVISNetworkCommon;
using AVISNetworkCommon.Utility;
using DataORM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class PaymentResult
    {
        public PaymentAwaitingProcessing PaymentAwaitingProcessing;
        public bool Success;
        public string Message;
    }
    public class PaymentContext
    {
        public string OrderId;
        public long Amount;
        public long CharacterId;
        public PaymentSystemType PaymentSystem;
    }
    public class ExchangeRatesData
    {
        public float Rate;
        public PaymentSystemType PaymentSystem;
    }


    public abstract class BasePaymentSystem
    {
        public bool isTest = false;
        public bool isCheckRigth = false;

        public static PaymentSystemType PaymentSystemType = PaymentSystemType.Qiwi;
        public Logger logger;
        public float ReadFloat(string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                return 0;

            val = val.Replace(".", ",");
            return Convert.ToSingle(val);
        }
        public abstract float GetPayAmount(string gameAmount);
        /// <summary>
        /// Получить ссылку на страницу оплаты
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract string RedirectLink(RequestContext context);
 
 
        /// <summary>
        /// Получить текующую платежную систему
        /// </summary>
        /// <returns></returns>
        public static BasePaymentSystem Get(PaymentSystemType paymentSystemType)
        {
            BasePaymentSystem PaymentSystem = null;
            if (PaymentSystem == null)
            {
                switch (paymentSystemType)
                {
                    case PaymentSystemType.Robokassa:
                        PaymentSystem = new RobokassaPaymentSystem();
                        break;
                    case PaymentSystemType.Paymaster:
                        PaymentSystem = new PayMasterPaymentSystem();
                        break;
                    case PaymentSystemType.Qiwi:
                        PaymentSystem = new QiwiPaymentSystem();
                        break;
                    case PaymentSystemType.Yandex:
                        PaymentSystem = new YandexMoneySystem();
                        break;
                    case PaymentSystemType.Steam:
                        PaymentSystem = new SteamPaymentSystem();
                        break;
                }
                PaymentSystem.logger = new Logger(Logger.LoggerType.Full, "/payment");
            }
            return PaymentSystem;
        }
        public string GetOrderTotal(string amount)
        {
            float floatAmount = Convert.ToSingle(amount);

           return GetOrderTotal(floatAmount);
        }
        public string GetOrderTotal(float floatAmount)
        {
            return Math.Round(floatAmount, 2).ToString("0.00",
                    System.Globalization.CultureInfo.GetCultureInfo("en-US"));
        }
        public abstract PaymentContext GetPaymentContext(RequestContext requestContext);
        /// <summary>
        /// Валидация запроса от имени платежной системы при принятии уведомления оплатеже
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns></returns>
        public abstract bool RequestAuthentication(RequestContext requestContext);
        /// <summary>
        /// Валидация итоговой страницы показываемой клиенту
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns></returns>
        public abstract bool ClientSuccess(RequestContext requestContext); 
        public abstract bool TestRequest(RequestContext requestContext);
        public async Task<PaymentResult> AddAwaitingProcessing(BaseController Controller, string paymentId,long characterId, long amount, PaymentSystemType paymentSystemType, bool test)
        {
            PaymentAwaitingProcessing paymentAwaitingProcessing = new PaymentAwaitingProcessing();
            paymentAwaitingProcessing.Amount = amount;
            paymentAwaitingProcessing.InvId = paymentId;
            paymentAwaitingProcessing.CharacterId = characterId;
            paymentAwaitingProcessing.PaymentSystem = paymentSystemType;
            paymentAwaitingProcessing.Time = DateTime.Now;
            paymentAwaitingProcessing.Test = test;

            var checkPaymentAwaitingProcessing = await Controller.Storage.Get.Where<PaymentAwaitingProcessing>("InvId={0} AND PaymentSystem={1}", paymentAwaitingProcessing.InvId, (int)paymentAwaitingProcessing.PaymentSystem);
            var checkPaymentHistory = await Controller.Storage.Get.Where<PaymentHistory>("InvId={0} AND PaymentSystem={1}", paymentAwaitingProcessing.InvId, (int)paymentAwaitingProcessing.PaymentSystem);
            PaymentResult paymentResult = new PaymentResult()
            {
                PaymentAwaitingProcessing = checkPaymentAwaitingProcessing
            };
            if (checkPaymentAwaitingProcessing == null && checkPaymentHistory == null)
            {
                await Controller.Storage.Add.In(paymentAwaitingProcessing);
                paymentResult.Success = true;
                paymentResult.Message = "order_num :" + paymentAwaitingProcessing.InvId + ";Summ :" + paymentAwaitingProcessing.Amount + ";Date :" + DateTime.Now.ToString();
            }
            else
            {
                paymentResult.Success = false;
                paymentResult.Message = "Повторное подтверждение заказа :" + paymentAwaitingProcessing.InvId + ";Summ :" + paymentAwaitingProcessing.Amount + ";Date :" + DateTime.Now.ToString();
            }
            return paymentResult;
        }

        public abstract string ExchangeRates(RequestContext context);
    }
}
