﻿using System;
using System.Xml.Serialization;
using System.IO;
 
using System.Collections.Generic;
using AVIS.Localization;
using AVISNetworkCommon;

namespace A4Engine
{
    public class Settings
    {
        private Settings()
        {

        }
        public static SettingsData Server;
        public static void Init()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(SettingsData));

            try
            {
                using (FileStream fs = new FileStream("server.settings", FileMode.Open))
                {
                    Server = (SettingsData)formatter.Deserialize(fs);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Server = new SettingsData();
                Server.Host = "localhost";
                Server.Password = "test";
                Server.Name = "lcoh";
                Server.Port = 3306;
                Server.User = "root";
                Server.FileHost = "http://localhost";
                Server.HttpPort = 80;
                Server.HttpsPort = 443;
                Server.Assembly = new List<string>() { "AVISNetworkEngineTools.dll" };

                using (FileStream fs = new FileStream("server.settings", FileMode.Create))
                {
                    formatter.Serialize(fs, Server);
                }
            }
 
            Localization.Get().Init(new LocalizationLogger(), "config/lang");
 
            foreach (var current_lang in Localization.Get().GetLangList())
            {
                Localization.Get().AddLang(current_lang);
                Console.WriteLine("Loaded " + current_lang);
            }
        }

        private class LocalizationLogger : IDebug
        {
            Logger logger;
            public LocalizationLogger()
            {
                logger = new Logger(Logger.LoggerType.WriteConsole);
            }
            public void Info(string text, params object[] arg)
            {
                logger.Info(text, arg);
            }

            public void Log(string text, params object[] arg)
            {
                logger.Info(text, arg);
            }

            public void LogError(string text, params object[] arg)
            {
                logger.Error(text, arg);
            }
        }
    }
    [Serializable]
    public class SettingsData
    {
        public int HttpPort;
        public int HttpsPort;
        public string Host;
        public string Name;
        public int Port;
        public string User;
        public string Password;

        // smtp
        public string SmtpHost;
        public int SmtpPort;
        public string senderEmail;
        public string senderName;
        public string senderPassword;

        public string FileHost = "http://localhost";
        public string VKAppId = "";
        public string VKAppSecretKey = "";
        public string VKAppServiceKey = "";

        public string SmsServiceLogin = "";
        public string SmsServicePassword = "";

        public List<string> Assembly;
        public int MaxSmsCountForUser = 3;
        public string EnterNotify = "Добро пожаловать!";
        public string DefaultPage = "public";
        public string SslCertificate = "cert\\sota_server.pfx";
        public string SslPassword = "";
    }
}
