﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Monitoring
{
    public class MonitoringModule : Module
    {

        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {

            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    string index = FileGetContents(DirModule + "/html/index.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
           
            Draw("<b>Доступ запрещен!</b>");
        }

    }
}
