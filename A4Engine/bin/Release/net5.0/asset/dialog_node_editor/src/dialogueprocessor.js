class DialogueProcessor {
  constructor(editor) {
    this.overlayOpened = false;

    window.OnSpeechEditorButtonCancel = () => {
      this.OnSpeechEditorClosed(false);
    };
    window.OnSpeechEditorButtonSave = () => {
      this.OnSpeechEditorClosed(true);
    };
    this.overlay = document.getElementById("overlay");
    this.speechBox = document.getElementById("speechBox");
    window.AddConditionToNode = this.AddConditionToNode.bind(this);
    window.AddEventToNode = this.AddEventToNode.bind(this);
    window.OnNodeTypeChanged = this.OnNodeTypeChanged.bind(this);

    window.DeleteConditionById = this.DeleteConditionById.bind(this);
    window.OnConditionNameChanged = this.OnConditionNameChanged.bind(this);
    window.OnConditionValChanged = this.OnConditionValChanged.bind(this);

    window.DeleteEventById = this.DeleteEventById.bind(this);
    window.OnEventNameChanged = this.OnEventNameChanged.bind(this);
    window.OnEventValChanged = this.OnEventValChanged.bind(this);

    window.OnPhraseCodeValChanged = this.OnPhraseCodeValChanged.bind(this);

    window.OnNodeDisplayChanged = () => {
      var checked = document.getElementById("check_node_display").checked;
      this.editor.selected.data.display = checked;
    };
    this.editor = editor;
  }
  AfterRootNodeCreated(rootNode) {
    rootNode.data = {
      type: window.DialogType.Root,
      name: "new_dialogue_" + parseInt(Math.random() * 1000, 10)
    };
  }
  AfterNodeCreated(newNode, parentNode) {
    newNode.data = {
      text: "",
      conditions: [],
      events: [],
      audio: "",
      display: true
    };
    if (parentNode.data.type === window.DialogType.NPC) {
      newNode.name = "Player";
      newNode.data.type = window.DialogType.Player;
      newNode.rect.color = "#000066";
    } else {
      newNode.name = "NPC";
      newNode.data.type = window.DialogType.NPC;
      newNode.rect.color = "#581845";
    }
  }
  AfterNodeImported(newNode) {
    if (newNode.data.type === window.DialogType.NPC) {
      newNode.name = "NPC";
      newNode.rect.color = "#581845";
    } else {
      newNode.name = "player";
      newNode.rect.color = "#000066";
    }
  }
  AfterRootImported(root) {}
  OpenSpeechEditor(node) {
    this.overlay.style.visibility = "visible";
    this.overlayOpened = true;
    this.speechBox.value = window.DataWorker.GetPhrase(this.editor.selected.data.text);
    window.mouseEventData.canvasBlocked = true;
  }
  OnSpeechEditorClosed(save) {
    this.overlay.style.visibility = "hidden";
    this.overlayOpened = false;
    if (save) { 
      window.DataWorker.SavePhrase(this.editor.selected.data.text, this.speechBox.value)
      //this.editor.selected.data.textContent = this.speechBox.value;
    }
    window.mouseEventData.canvasBlocked = false;
  }
  OnNodeTypeChanged() {
    var node = this.editor.selected;
    var val = document.getElementById("type-select").value;
    node.data.type = val;

    if (val === window.DialogType.Player) {
      node.name = "Player";
      node.rect.color = "#000066";
    } else {
      node.name = "NPC";
      node.rect.color = "#581845";
    }
  }
  AddConditionToNode() {
    this.editor.selected.data.conditions.push({ name: "", val: "" });
    window.rightMenu.DisplayData(this.editor.selected);
  }
  DeleteConditionById(id) {
    this.editor.selected.data.conditions.splice(id, 1);
    window.rightMenu.DisplayData(this.editor.selected);
    return false;
  }
  OnConditionNameChanged(id) {
    if (this.editor.selected.data === undefined) return;
    let name = document.getElementById("condition_name_" + id).value;
    this.editor.selected.data.conditions[parseInt(id, 10)].name = name;
  }
  OnConditionValChanged(id) {
    if (this.editor.selected.data === undefined) return;
    let val = document.getElementById("condition_val_" + id).value;
    this.editor.selected.data.conditions[parseInt(id, 10)].val = val;
  }
  AddEventToNode() {
    this.editor.selected.data.events.push({ name: "", val: "" });
    window.rightMenu.DisplayData(this.editor.selected);
  }
  DeleteEventById(id) {
    this.editor.selected.data.events.splice(id, 1);
    window.rightMenu.DisplayData(this.editor.selected);
    return false;
  }
  OnEventNameChanged(id) {
    if (this.editor.selected.data === undefined) return;
    let name = document.getElementById("event_name_" + id).value;
    this.editor.selected.data.events[parseInt(id, 10)].name = name;
  }
  OnEventValChanged(id) {
    if (this.editor.selected.data === undefined) return;
    let val = document.getElementById("event_val_" + id).value;
    this.editor.selected.data.events[parseInt(id, 10)].val = val;
  }  
  OnPhraseCodeValChanged(val){
 
    /*
    let val = document.getElementById("event_val_" + id).value;  
    */
    this.editor.selected.data.text = val;
  }
}

export { DialogueProcessor };
