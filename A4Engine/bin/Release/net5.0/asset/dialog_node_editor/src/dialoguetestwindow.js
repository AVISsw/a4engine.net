class DialogueTestWindow {
  constructor() {
    this.windowBody = document.getElementById("windowBody");
    this.header = document.getElementById("windowHeader");
    this.move = false;
    this.header.onmousedown = (evnt) => {
      let currentPosX = this.windowBody.offsetLeft;
      let currentPosY = this.windowBody.offsetTop;
      this.offsetX = evnt.clientX - currentPosX;
      this.offsetY = evnt.clientY - currentPosY;
      this.move = true;
    };
    this.dialogueCahsField = document.getElementById("dialogueCash");
    this.variantsField = document.getElementById("dialogueVariants");
    this.conditionField = document.getElementById("conditionsField");
    this.dialogueData = [];
    this.globalConditions = {};

    window.OnClickVariant = this.OnClickVariant.bind(this);
    window.OnGlobalConditionChange = this.OnGlobalConditionChange.bind(this);
    window.CloseDialogueWindow = () => {
      this.windowBody.style.display = "none";
    };
    window.OnDialogTestStarted = () => {
      this.variantsField.innerHTML = "";
      if (this.dialogueData.length > 0) {
        this.ProcessNode(this.dialogueData[0]);
      }
    };
  }
  GetNodeById(id) {
    var node = this.dialogueData[0];
    let counter = 0;

    for (let data of this.dialogueData) {
      if (data.id === id) {
        node = this.dialogueData[counter];
        break;
      }
      counter++;
    }
    return node;
  }
  ShowWindow(data) {
    this.dialogueData = data;
    this.globalConditions = {};
    this.windowBody.style.display = "block";
    this.dialogueCahsField.innerHTML = '<div class="aligner"></div>';
    this.conditionField.innerHTML = "";
    this.variantsField.innerHTML =
      '<button class="variantButton" onclick="OnDialogTestStarted()">Start dialog</button>';
    if (data.length > 0) {
      for (let node of data) {
        if (node.data.type !== window.DialogType.Root) {
          var conditions = node.data.conditions;
          for (let condition of conditions) {
            let full_name = condition.name + "_" + condition.val;
            if (!this.globalConditions.hasOwnProperty(full_name)) {
              this.globalConditions[full_name] = {
                val: condition.val,
                currentVal: false
              };
            }
          }
        }
      }
      this.ShowConditions();
      //Надо же еще обратную связь сделать
    }
  }
  ProcessNode(node) {
    let connectedNode;
    for (let id of node.connections) {
      connectedNode = this.GetNodeById(id);

      var go = true;
      for (let condition of connectedNode.data.conditions) {
        let full_name = condition.name + "_" + condition.val;
        if (!this.globalConditions[full_name].currentVal) {
          go = false;
        }
      }
      if (go) {
        if (connectedNode.data.type === window.DialogType.NPC) {
          this.DisplayNPCData(connectedNode);
          break;
        } else {
          this.ShowPlayerVariant(connectedNode);
        }
      }
      console.log(go + " " + connectedNode.data.type);
    }
  }
  DisplayNPCData(node) {
    this.dialogueCahsField.innerHTML +=
      '<div class="speechBox"><p><h4>' +
      node.data.type +
      "</h4></p><p>" +
      node.data.text +
      "</p></div>";
    this.ScrollToEnd();
    this.ProcessNode(node);
  }
  DisplayPlayerData(node) {
    //В конкретном случае разница в обводке только
    this.dialogueCahsField.innerHTML +=
      '<div class="speechBox2"><p><h4>' +
      node.data.type +
      "</h4></p><p>" +
      node.data.text +
      "</p></div>";
    this.ScrollToEnd();
  }
  ShowConditions() {
    for (let name of Object.keys(this.globalConditions)) {
      this.conditionField.innerHTML +=
        "<p> " +
        name +
        ' <input type="checkbox" value="' +
        this.globalConditions[name].currentVal +
        '" onchange="OnGlobalConditionChange(\'' +
        name +
        '\')" id="checkbox_' +
        name +
        '"> </p>';
    }
  }
  ShowPlayerVariant(node) {
    this.variantsField.innerHTML +=
      '<button class="variantButton" onclick="OnClickVariant(' +
      node.id +
      ')">' +
      node.data.text +
      "</button>";
  }
  OnClickVariant(id) {
    this.variantsField.innerHTML = "";

    id = parseInt(id, 10);
    var node = this.GetNodeById(id);
    this.DisplayPlayerData(node);
    this.ProcessNode(node);
  }
  OnMouseMove(evnt) {
    if (this.move) {
      this.windowBody.style.left = evnt.clientX - this.offsetX;
      this.windowBody.style.top = evnt.clientY - this.offsetY;
    }
  }
  OnMouseUp() {
    this.move = false;
  }
  OnGlobalConditionChange(name) {
    var checkbox = document.getElementById("checkbox_" + name);
    this.globalConditions[name].currentVal = checkbox.checked;
  }
  ScrollToEnd() {
    var elem = this.dialogueCahsField;
    elem.scrollTop = elem.scrollHeight;
  }
}
//onmousedown="StartWindowDragging()" onmousemove="ProcessWindowDragging()" onmouseup="StopWindowDragging()

export { DialogueTestWindow };
