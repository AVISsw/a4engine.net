console.log("quest editor init");
$(window).on('load', function () {
    console.log("quest editor load");
    jsonRequestData("/handler/QuestEditorHandler/get-quest-list", {}, function (requestDataReceptList) {
        console.log(requestDataReceptList);
        var firstElem;
        for (var i in requestDataReceptList) {
            firstElem = requestDataReceptList[i];
            break;
        }
        itemListModule = new Vue({
            el: '#itemListModule',
            data: {
                Items: requestDataReceptList,
                Quest: firstElem,
                selectItemData: firstElem,
                newInitHandler: '',
                newCompliteHandler: '',
                newFailHandler: '',
                newCancelHandler: '',
                newCompliteCondition: '',
                newFailCondition: '',
                ItemName: ''
            },
            methods: {
                SelectQuest(item) {
                    this.Quest = Object.assign({}, item);
                    console.log(this.Quest);
                }, Clone(item) {
                    var id = this.Quest.QuestData.Id;

                    this.Quest = JSON.parse(JSON.stringify(item));
                    this.Quest.QuestData.Id = id;
                    this.Quest.QuestData.CacheId = id;
                    this.Quest.Stages.forEach(element => {
                        element.Id = -1;
                        element.CacheId = -1;
                        element.IsDbExits = false;
                        element.QuestId = id;
                    });
                },
                AddQuest() {
                    var items = this.Items;
                    postRequestData("/handler/QuestEditorHandler/add-quest", {}, function (responseData) {
                        console.log(responseData);
                        console.log(items);
                        Vue.set(items, responseData.Id, responseData);
                        this.Quest = responseData;
                    });
                }, AddStage() {
                    console.log(this.Quest);
                    if (!this.Quest.Stages) {
                        this.Quest.Stages = [];
                    }
                    this.Quest.Stages.push({
                        CancelConditions: [],
                        CompliteConditions: [],
                        RequiredStagesId: [],
                        Description: "",
                        FailConditions: [],
                        Id: -1,
                        IsDbExits: false,
                        IsRemove: false,
                        IsRequired: true,
                        OnCancel: [],
                        OnComplite: [],
                        OnFail: [],
                        OnInit: [],
                        QuestId: this.Quest.QuestData.Id,
                        Title: ""
                    });
                }, RmStage(index, condition) {
                    console.log(condition);
                    condition.IsRemove = true;
                },
                AddReward() {
                    if (!this.Quest.Rewards) {
                        this.Quest.Rewards = [];
                    }
                    this.Quest.Rewards.push({
                        Id: -1,
                        QuestId: this.Quest.QuestData.Id,
                        Name: "default",
                        EventName: "default_event_name",
                        ItemId:-1,
                        Count:1,
                        Money:0,
                        Rep:0,
                        FactionId:-1,
                        IsDbExits: false,
                        IsRemove: false
                    });
                },
                 RmReward(index, condition) {
                    console.log(condition);
                    condition.IsRemove = true;
                },
                SaveQuest(Quest) {
                    console.log(this.Quest);
                    console.log("post request save");
                    this.Quest.Stages.forEach(element => {
                        console.log(element);
                        if (!Array.isArray(element.OnInit)) {
                            element.ItemId = element.ItemId.split(",");
                        }
                        if(element.RequiredStagesId == 'undefined'){
                            element.RequiredStagesId = '';
                        }                       
                        if(element.RequiredStagesId == null){
                            element.RequiredStagesId = '';
                        }
                        console.log("element.RequiredStagesId " + element.RequiredStagesId);
                        if (element.RequiredStagesId.length > 0) {
                            if (typeof element.RequiredStagesId == 'string')
                                element.RequiredStagesId = element.RequiredStagesId.split(",");
                        } else {
                            element.RequiredStagesId = [];
                        }
                    });

                    postRequestData("/handler/QuestEditorHandler/update-quest", this.Quest, function (responseData) {
                        itemListModule.$data.Quest = responseData;
                         
                    });
                },
                RemoveQuest(Quest) {
                    var items = this.Items;
                    console.log(Quest.QuestData);
                    $.post(
                        "/handler/QuestEditorHandler/remove-quest",
                        { "Id": Quest.QuestData.Id },
                        function (responseData) {
                            Vue.delete(items, Quest.QuestData.Id);
                        }
                    );
                },
                AddInitQuestHandler(handler, questData) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!questData.OnInit) {
                        questData.OnInit = [];
                    }
                    questData.OnInit.push(value);
                    this.newQuestInitHandler = "";
                },
                AddCreateQuestHandler(handler, questData) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!questData.OnCreate) {
                        questData.OnCreate = [];
                    }
                    questData.OnCreate.push(value);
                    this.newQuestCreateHandler = "";
                },
                RemoveCreateQuestHandler(index, questData) {
                    questData.OnCreate.splice(index, 1);
                },
                RemoveInitQuestHandler(index, questData) {
                    questData.OnInit.splice(index, 1);
                },
                AddCompliteQuestHandler(handler, questData) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!questData.OnComplite) {
                        questData.OnComplite = [];
                    }
                    questData.OnComplite.push(value);
                    this.newQuestCompliteHandler = "";
                },
                RemoveCompliteQuestHandler(index, questData) {
                    questData.OnComplite.splice(index, 1);
                },
                AddFailQuestHandler(handler, questData) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!questData.OnFail) {
                        questData.OnFail = [];
                    }
                    questData.OnFail.push(value);
                    this.newQuestFailHandler = "";
                },
                RemoveFailQuestHandler(index, questData) {
                    questData.OnFail.splice(index, 1);
                },
                AddCancelQuestHandler(handler, questData) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!questData.OnCancel) {
                        questData.OnCancel = [];
                    }
                    questData.OnCancel.push(value);
                    this.newQuestCancelHandler = "";
                },
                RemoveCancelQuestHandler(index, questData) {
                    questData.OnCancel.splice(index, 1);
                },
                AddInitHandler(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!stage.OnInit) {
                        stage.OnInit = [];
                    }
                    stage.OnInit.push(value);
                    this.newInitHandler = "";
                },
                RmInitHandler(index, stage) {
                    stage.OnInit.splice(index, 1);
                },
                RmCreateHandler(index, stage) {
                    stage.OnCreate.splice(index, 1);
                },
                AddCreateHandler(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!stage.OnCreate) {
                        stage.OnCreate = [];
                    }
                    stage.OnCreate.push(value);
                    this.newCreateHandler = "";
                }, 
                RmCompliteHandler(index, stage) {
                    stage.OnCreate.splice(index, 1);
                },
                AddCompliteHandler(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!stage.OnComplite) {
                        stage.OnComplite = [];
                    }
                    stage.OnComplite.push(value);
                    this.newCompliteHandler = "";
                },
                RmCompliteHandler(index, stage) {
                    stage.OnComplite.splice(index, 1);
                },
                AddFailHandler(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!stage.OnFail) {
                        stage.OnFail = [];
                    }
                    stage.OnFail.push(value);
                    this.newFailHandler = "";
                },
                RmFailHandler(index, stage) {
                    stage.OnFail.splice(index, 1);
                },
                AddCancelHandler(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    } if (!stage.OnCancel) {
                        stage.OnCancel = [];
                    }
                    stage.OnCancel.push(value);
                    this.newCancelHandler = "";
                },
                RmCancelHandler(index, stage) {
                    stage.OnCancel.splice(index, 1);
                },
                AddCompliteCondition(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    }
                    if (!stage.CompliteConditions) {
                        stage.CompliteConditions = [];
                    }
                    stage.CompliteConditions.push(value);
                    this.newCompliteCondition = "";
                },
                RemoveCompliteCondition(index, stage) {
                    stage.CompliteConditions.splice(index, 1);
                },
                AddFailCondition(handler, stage) {
                    var value = handler && handler.trim();
                    if (!value) {
                        return;
                    }
                    if (!stage.FailConditions) {
                        stage.FailConditions = [];
                    }
                    stage.FailConditions.push(value);
                    this.newFailCondition = "";
                },
                RemoveFailCondition(index, stage) {
                    stage.FailConditions.splice(index, 1);
                }
            },
            computed: {
                filteredList: function () {
                    var name = this.ItemName;      
                    let result = Object.values(this.Items).filter(function (elem) {
                        if (name === '') return true;
                        else return elem.Title.indexOf(name) > -1;
                    });
                    return result.reverse();
                }
            }
        });

    });
});
console.log("quest editor");