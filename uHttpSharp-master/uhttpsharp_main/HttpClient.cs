/*
 * Copyright (C) 2011 uhttpsharp project - http://github.com/raistlinthewiz/uhttpsharp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System.Text;
using System.Net;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using uhttpsharp.Clients;
using uhttpsharp.Headers;
using uhttpsharp.RequestProviders;
using uhttpsharp.Logging;

namespace uhttpsharp
{
    internal sealed class HttpClientHandler
    {
        private const string CrLf = "\r\n";
        private static readonly byte[] CrLfBuffer = Encoding.UTF8.GetBytes(CrLf);

        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
        
        private readonly IClient _client;
        private readonly Func<IHttpContext, Task> _requestHandler;
        private readonly IHttpRequestProvider _requestProvider;
        private readonly EndPoint _remoteEndPoint;
        private DateTime _lastOperationTime;
 
        public HttpClientHandler(IClient client, Func<IHttpContext, Task> requestHandler, IHttpRequestProvider requestProvider)
        {
            _remoteEndPoint = client.RemoteEndPoint;
            _client = client;
            _requestHandler = requestHandler;
            _requestProvider = requestProvider;
            
            Logger.InfoFormat("Got Client {0}", _remoteEndPoint);
            Task.Factory.StartNew(Process);
            UpdateLastOperationTime();
        }

        private async Task<Stream> InitializeStream()
        {
            if (Client is ClientSslDecorator)
            {
                await ((ClientSslDecorator)Client).AuthenticateAsServer().ConfigureAwait(false);
                _client.SecureConnection = true;
            }

            //   _stream = new BufferedStream(_client.Stream, 8096);
           return _client.Stream;
        }

        private async void Process()
        {
            try
            {
                var _stream = await InitializeStream();

                while (_client.Connected)
                {
                    // TODO : Configuration.
                    var limitedStream = new NotFlushingStream(new LimitedStream(_stream));
                    var streamReader = new MyStreamReader(limitedStream);
                    /*
                    StringBuilder headerBuf = new StringBuilder();
                    byte[] content = new byte[1024];
          
                    while (true)
                    {
                        int count = await _stream.ReadAsync(content, 0, content.Length);
                        await limitedStream.WriteAsync(content, 0, count);
                        string line = Encoding.ASCII.GetString(content,0 ,count);
                        headerBuf.Append(line);
                        Console.WriteLine(string.Format("{0} < {1}",count, content.Length));
                        if (count < content.Length   headerBuf.ToString().Contains("\r\n\r\n")) // 483 < 512
                        {
 
                            break;
                        }
                    }
                    string header = headerBuf.ToString();
                    Console.WriteLine(header);
                    //    return;
                    limitedStream.Position = 0;
               */
                    var request = await _requestProvider.Provide(streamReader).ConfigureAwait(false);

                    if (request != null)
                    {
                        UpdateLastOperationTime();

                        var context = new HttpContext(request, _client.RemoteEndPoint);
                        context.SecureConnection = _client.SecureConnection;
                        Logger.InfoFormat("{1} : Got request {0}", request.Uri, _client.RemoteEndPoint);
                        await _requestHandler(context).ConfigureAwait(false);

                        if (context.Response != null)
                        {
                            var streamWriter = new StreamWriter(_stream) { AutoFlush = false };
                            streamWriter.NewLine = "\r\n";
                            await WriteResponse(context, streamWriter).ConfigureAwait(false);
                            await limitedStream.ExplicitFlushAsync().ConfigureAwait(false);
                            if (!request.KeepAliveConnection() || context.Response.CloseConnection)
                            {
                                _client.Close();
                            }
                        }
                        UpdateLastOperationTime();
                    }
                    else
                    {
                        _client.Close();
                    }
                }
            }
            catch (Exception e)
            {
                // Hate people who make bad calls.
                Logger.WarnException(string.Format("Error while serving : {0} , {1}", _remoteEndPoint, e), e);
     
            }
            finally
            {
                _client?.Close();
            }

            Logger.InfoFormat("Lost Client {0}", _remoteEndPoint);
        }
        public string ToCookieData(CookieCollection cookies)
        {
            StringBuilder builder = new StringBuilder();
            Console.WriteLine("-------------- start to cookie ----------");
            try
            {
                foreach (Cookie cookie in cookies)
                {
                    builder.AppendFormat("Set-Cookie: {0}={1};", cookie.Name, cookie.Value);
                    if (!string.IsNullOrWhiteSpace(cookie.Path))
                    {
                        builder.AppendFormat(" Path={0};", cookie.Path);
                    }
                    if (!string.IsNullOrWhiteSpace(cookie.Domain))
                        builder.AppendFormat(" Domain={0};", cookie.Domain);

                    builder.AppendFormat(" Expires={0};", cookie.Expires);
                    builder.AppendFormat("{0}", Environment.NewLine);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(builder.ToString());
            return builder.ToString();
        }
        private async Task WriteResponse(HttpContext context, StreamWriter writer)
        {
            IHttpResponse response = context.Response;

            // Headers
            await writer.WriteLineAsync(string.Format("HTTP/1.1 {0} {1}",
                (int)response.ResponseCode,
                response.ResponseCode))
                .ConfigureAwait(false);

            foreach (string header in response.Headers)
            {
                await writer.WriteLineAsync(string.Format("{0}: {1}", header, response.Headers[header])).ConfigureAwait(false);
            }

            // Cookies

            if (context.Cookies.Count > 0)
            {
                await writer.WriteAsync(ToCookieData(context.Cookies))
                    .ConfigureAwait(false);
            }

            // Empty Line
            await writer.WriteLineAsync().ConfigureAwait(false);
            writer.Flush();
            try
            {
                await response.WriteBody(writer).ConfigureAwait(false);
                await writer.FlushAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public IClient Client
        {
            get { return _client; }
        }

        public void ForceClose()
        {
            _client.Close();
        }

        public DateTime LastOperationTime
        {
            get
            {
                return _lastOperationTime;
            }
        }

        private void UpdateLastOperationTime()
        {
            // _lastOperationTime = DateTime.Now;
        }

    }

    internal class NotFlushingStream : Stream
    {
        private readonly Stream _child;
        public NotFlushingStream(Stream child)
        {
            _child = child;
        }


        public void ExplicitFlush()
        {
            _child.Flush();
        }

        public Task ExplicitFlushAsync()
        {
            return _child.FlushAsync();
        }

        public override void Flush()
        {
            // _child.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _child.Seek(offset, origin);
        }
        public override void SetLength(long value)
        {
            _child.SetLength(value);
        }
        public override int Read(byte[] buffer, int offset, int count)
        {
            return _child.Read(buffer, offset, count);
        }

        public override int ReadByte()
        {
            return _child.ReadByte();
        }
        public override void Write(byte[] buffer, int offset, int count)
        {
            _child.Write(buffer, offset, count);
        }
        public override void WriteByte(byte value)
        {
            _child.WriteByte(value);
        }
        public override bool CanRead
        {
            get { return _child.CanRead; }
        }
        public override bool CanSeek
        {
            get { return _child.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _child.CanWrite; }
        }
        public override long Length
        {
            get { return _child.Length; }
        }
        public override long Position
        {
            get { return _child.Position; }
            set { _child.Position = value; }
        }
        public override int ReadTimeout
        {
            get { return _child.ReadTimeout; }
            set { _child.ReadTimeout = value; }
        }
        public override int WriteTimeout
        {
            get { return _child.WriteTimeout; }
            set { _child.WriteTimeout = value; }
        }
    }

    public static class RequestHandlersAggregateExtensions
    {

        public static Func<IHttpContext, Task> Aggregate(this IList<IHttpRequestHandler> handlers)
        {
            return handlers.Aggregate(0);
        }

        private static Func<IHttpContext, Task> Aggregate(this IList<IHttpRequestHandler> handlers, int index)
        {
            if (index == handlers.Count)
            {
                return null;
            }

            var currentHandler = handlers[index];
            var nextHandler = handlers.Aggregate(index + 1);
            
            return context => currentHandler.Handle(context, () => nextHandler(context));
        }


    }
}
