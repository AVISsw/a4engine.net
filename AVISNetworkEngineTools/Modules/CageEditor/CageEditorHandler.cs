﻿using A4Engine.Modules;
using A4Engine.Modules.ItemEditor;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using ItemData = A4Engine.Modules.ItemEditor.ItemData;
using ItemType = A4Engine.Modules.ItemEditor.ItemType;

namespace AVISNetworkEngineTools.Modules.CageEditor
{
    public class CageEditorHandler : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }


        public async Task<long> Add(CageData data)
        {
            long lastId = -1;
            try
            {

                lastId = await Controller.Storage.Add.In(data);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId;
        }

        public async Task<bool> Update(CageData data)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Update.Where(data, " Id={0}", data.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> Remove(CageData data)
        { 
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<CageData>(" Id={0}", data.Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<List<ItemData>> GetItemsByType()
        {
            List<ItemData> result = new List<ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>("IntType={0} OR IntType={1}", (int)ItemType.Ammo, (int)ItemType.Grenade))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ItemData>());
                }
            }
            return result;
        }

        public async Task<List<CageData>> GetList()
        {
            List<CageData> result = new List<CageData>();

            try
            {
                using (var data = await Controller.Storage.GetAll.WhereBase<CageData>(""))
                {
                    while (data.Read)
                    {
                        result.Add(data.GetObject<CageData>());
                    }
                }
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return result;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            CageData templateData = null;
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            templateData = JsonConvert.DeserializeObject<CageData>(Context.rawParams);
                            Draw(JsonConvert.SerializeObject(
                             new Dictionary<string, object>() { { "Id", await this.Add(templateData) } }
                                ));
                            break;
                        case "update":
                            templateData = JsonConvert.DeserializeObject<CageData>(Context.rawParams);
                            Draw(JsonConvert.SerializeObject(await this.Update(templateData)));
                            break;
                        case "remove":
                            templateData = JsonConvert.DeserializeObject<CageData>(Context.rawParams);
                            Draw(JsonConvert.SerializeObject(await this.Remove(templateData)));
                            break;
                        case "getlist":
                            Draw(JsonConvert.SerializeObject(await this.GetList()));
                            break;
                        case "getitems":
                            Draw(JsonConvert.SerializeObject(await this.GetItemsByType()));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }
    }
}
