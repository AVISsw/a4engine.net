﻿using A4Engine.Modules;
using AVIS.Localization;
using AVISNetworkCommon;
using AVISNetworkCommon.Utility;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.RestorePassword
{
    public class RestorePassword : Handler
    {
        Random rand = new Random();
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context); 
        }


        public override async Task Start(RequestContext Context)
        {
 
            SignUpResult authResult = new SignUpResult();
            SetLang(Context);
            string key = Context.Path[3].ToString();
            Log.Info("RestorePassword "+ key);
            try
            {
                switch (key)
                {
                    case "find":
                        authResult = await Find(Context.postParams);
                        break;
                    case "confirm":
                        authResult = await Confirm(Context.postParams);
                        break;
                    case "change":
                        authResult = await Change(Context.postParams);
                        break;
                }
            }catch(Exception ex)
            {
                authResult.ErrorCode = 331;
                authResult.Message = new TranslatableString("restore_password_unknown_error");
                Log.Error(ex.ToString());
            }
            Console.WriteLine(JsonConvert.SerializeObject(authResult));
            Draw(JsonConvert.SerializeObject(authResult));
        }

        private async Task<SignUpResult> Change(NameValueCollection postParams)
        {
            bool changePswd = User.Data.Get<bool>("СhangePswd");
            User.Data.Save("СhangePswd", false);
            string password = postParams["password"].Trim();
            if (changePswd)
            {
                var account = User.Data.Get<AuthData>("accountData");
                account.Password = Crypt.EncryptSHA256(password);
                bool result = await Controller.Storage.Update.Where(account, "Id = {0}", account.Id); 
                if (result == false)
                {
                    return new SignUpResult() { Success = false, ErrorCode = 1, Message = new TranslatableString("restore_password_faile_save_password")
                };
                }
                else
                {
                    return new SignUpResult() { Success = true, ErrorCode = 0, Message = new TranslatableString("restore_password_success") };
                }
            }
            return new SignUpResult() { Success = false, ErrorCode = 2, Message = new TranslatableString("restore_password_not_confirm") };

        }

        private async Task<SignUpResult> Confirm(NameValueCollection postParams)
        {
            int confirmCount = User.Data.Get<int>("RestorePswdConfirmCount");
            DateTime confirmLastRequest = User.Data.Get<DateTime>("RestorePswdConfirmLastRequest");

            int originalCode = User.Data.Get<int>("RestorePswdCode");
            int requestCode = Convert.ToInt32(postParams["code"]);
            DateTime RestorePswdConfirmEndRequest = User.Data.Get<DateTime>("RestorePswdConfirmEndRequest");

            if (RestorePswdConfirmEndRequest > DateTime.Now)
            {
                if (confirmLastRequest.AddSeconds(20) < DateTime.Now)
                {
                    confirmLastRequest = DateTime.Now;
                    User.Data.Save("RestorePswdConfirmLastRequest", confirmLastRequest);
                    if (confirmCount < 6)
                    {
                        confirmCount++;
                        User.Data.Save("RestorePswdConfirmCount", confirmCount);

                        if (originalCode == requestCode)
                        {
                            User.Data.Save("СhangePswd", true);
                            return new SignUpResult() { Success = true, ErrorCode = 0 };
                        }
                        return new SignUpResult() { Success = true, ErrorCode = 3, Message = new TranslatableString("restore_password_confirm_err_code") };
                    }
                    else
                    {
                        return new SignUpResult() { Success = false, ErrorCode = 1, Message = new TranslatableString("restore_password_confirm_max_err") };
                    }
                }
                else
                {
                    TimeSpan deltaTime = confirmLastRequest.AddSeconds(20) - DateTime.Now;
                    return new SignUpResult() { Success = false, ErrorCode = 2, Message = string.Format(new TranslatableString("restore_password_confirm_interval"), deltaTime.TotalSeconds) };
                }
            }
            else
            {
                return new SignUpResult() { Success = false, ErrorCode = 4, Message = string.Format(new TranslatableString("restore_password_confirm_timeout")) };

            }
        }

        private async Task<SignUpResult> Find(NameValueCollection postParams)
        {
            AuthData account = null;
            string mail = postParams["mail"];
            if (!string.IsNullOrWhiteSpace(mail))
            {
                account = await FindAccount(mail);
                if (account != null)
                {
                    User.Data.Save("accountData", account);
                    int code = rand.Next(100000, 999999);
                    Log.Info(JsonConvert.SerializeObject(account));
                    Mail.Get().SendEmail(account.Email, new TranslatableString("restore_password_email_title"), string.Format(new TranslatableString("restore_password_email_body"), code));
                    User.Data.Save("RestorePswdCode", code);
                    User.Data.Save("RestorePswdConfirmEndRequest", DateTime.Now.AddMinutes(15));
                    return new SignUpResult() { Success = true, Message = new TranslatableString("restore_password_email_next_notify") };
                }
            }
            return new SignUpResult() { Success = false, ErrorCode = 1, Message = new TranslatableString("restore_password_email_not_found") };
        }
        public Task<AuthData> FindAccount(string email)
        {
            return Controller.Storage.Get.Where<AuthData>("Email={0}", email);
        }
        [Serializable]
        public struct SignUpResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}
