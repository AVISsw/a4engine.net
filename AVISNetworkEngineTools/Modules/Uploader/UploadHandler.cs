﻿using A4Engine.Modules;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Uploader
{
    public class UploadHandler : Handler
    {
        string filePath = null;

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }
        Logger Log;
        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }
        public override async Task Start(RequestContext Context)
        {
            Console.WriteLine($"Start Start");
            try
            {
                string Hash = Context.getParams.Get("Hash");
                Console.WriteLine($"Hash {Hash}");
                var user = SessionManager.GetSession(Hash);
                Console.WriteLine($"User {user.UserData.Login}");
                if (user.GroupRight.Can("uploadFile"))
                {
                    var files = Context.FormData.Files.GetEnumerator();

                    while (files.MoveNext())
                    {
                        try
                        {
                            filePath = A4Engine.Controllers.Asset.Folder+"/ServerContent/" + files.Current.FileName;
                            string dir = Path.GetDirectoryName(filePath);
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }
                            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
                            {
                                fs.Write(files.Current.FileData);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                            Draw(JsonConvert.SerializeObject(new UploadResult() { Success = false, ErrorCode = 2, Message = e.Message }));
                            return;
                        }
                    }
                    Draw(JsonConvert.SerializeObject(new UploadResult() { Success = true, ErrorCode = 0, Message = "OK" }));
                    return;
                }
                else
                {
                    Console.WriteLine("Error right");
                    Draw(JsonConvert.SerializeObject(new UploadResult() { Success = false, ErrorCode = 1, Message = "Недостаточно прав" }));
                    return;
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Draw(JsonConvert.SerializeObject(new UploadResult() { Success = false, ErrorCode = 4, Message = ex.Message }));
            }
        }

        public struct UploadResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}