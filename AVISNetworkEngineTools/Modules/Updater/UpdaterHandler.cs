﻿using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using AVISNetworkEngineData.Structs;
using AVISNetworkEngineORM.DB.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using A4Engine.Modules;
using System.IO;
using A4Engine;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Updater
{
    public class UpdaterHandler : Handler
    {
        string filePath = null;
        string folder = "game";
        string hostPath = Settings.Server.FileHost;
        
        public override async Task Before(RequestContext Context)
        {
           await Start(Context);
        }
        Logger Log;
        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }
        public override async Task Start(RequestContext Context)
        {
            CheckVersionResultData checkVersionResultData = new CheckVersionResultData();
            try
            {
                string folderName = Context.Path[3];
                switch (folderName)
                {
                    case "game":
                        folder = "game";
                        break;
                    case "launcher":
                        folder = "launcher";
                        break;
                }
                filePath = hostPath + "/" + folder + "/" + "server_version.json";
                VersionData versionData = new VersionData();
                versionData = Get();
                versionData.FileHost = hostPath;

                string hash = Context.Path[4];
                checkVersionResultData.Success = true;
                checkVersionResultData.Code = 0;
                checkVersionResultData.CurrentVersion = hash == versionData.Hash;
                Console.WriteLine(string.Format("Проверка версии {0} == {1}", versionData.Hash, hash));
                switch (folderName)
                {
                    case "game":
                      //  checkVersionResultData.CurrentVersion = false;
                        checkVersionResultData.VersionData = versionData;
                        break;
                    case "launcher":
                       // checkVersionResultData.CurrentVersion = false;
                        checkVersionResultData.VersionData = versionData;
                        break;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("===============================");
                Console.WriteLine(e.ToString());
                checkVersionResultData.Message = e.ToString();
                checkVersionResultData.Success = false;
                checkVersionResultData.Code = -1;
            }
            Draw(JsonConvert.SerializeObject(checkVersionResultData).ToString());
        }
        private void Save(VersionData data)
        {
            try
            {
                string text = JsonConvert.SerializeObject(data);
                using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    fs.Write(Encoding.UTF8.GetBytes(text));
                }
            }
            catch (Exception e)
            {

            }
        }
 
        private VersionData Get()
        {
            try
            {
                /*
                byte[] buffer;
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    int length = (int)fs.Length;  // get file length
                    buffer = new byte[length];
                    fs.Read(buffer);
                }
                return JsonConvert.DeserializeObject<VersionData>(Encoding.UTF8.GetString(buffer));
                */
                return JsonConvert.DeserializeObject<VersionData>(Request(filePath));
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            return null;
        }
    }
    public class CheckVersionResultData
    {
        public bool Success;
        public int Code;
        public bool CurrentVersion;
        public VersionData VersionData;
        public string Message;
    }
    public class VersionData {
        public long Version;
        public string FileHost;
        public string GameName;
        public string UpdateHandler;
        public string NewsHandler;
        public string GameExe;
        public string PublisherUrl;
        public string Hash;
        public Dictionary<string, FileData> Files { get; set; } = new Dictionary<string, FileData>() { };
    }
    public class FileData
    {
        public string Hash;
    }
}
