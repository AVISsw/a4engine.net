﻿using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AVISNetworkEngineTools.Modules.ItemEditor
{
    public class ItemFilterList
    {
        ItemFilterListData itemFilterList;
 
        public ItemFilterList(string lang)
        {
            Init(lang);
        }

        private string path;
 
        public void Init(string lang)
        {
            path = string.Format("config/lang/{0}", lang);
            string file = path + "/item_filter_list.json";
            string tempData = File.ReadAllText(file);
            itemFilterList = JsonConvert.DeserializeObject<ItemFilterListData>(tempData);
        }
        public string GetName(int index)
        {
            string result = "empty";
            itemFilterList.List.TryGetValue(index, out result);
            return result;
        }
        public Dictionary<int, string> GetList()
        {
            return itemFilterList.List;
        }
    }
 
}
