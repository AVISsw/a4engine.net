/*
 * Copyright (C) 2011 uhttpsharp project - http://github.com/raistlinthewiz/uhttpsharp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using uhttpsharp.Headers;

namespace uhttpsharp
{
    [DebuggerDisplay("{Method} {OriginalUri,nq}")]
    internal class HttpRequest : IHttpRequest
    {
        private NameValueCollection _headers;
        private HttpMethods _method;
        private readonly string _protocol;
        private readonly Uri _uri;
        private readonly string[] _requestParameters;

        Stream content;
        public HttpRequest(NameValueCollection headers, HttpMethods method, string protocol, Uri uri, string[] requestParameters, Stream content)
        {
            _headers = headers;
            _method = method;
            _protocol = protocol;
            _uri = uri;
            _requestParameters = requestParameters;
            this.content = content;
        }
        public bool KeepAliveConnection()
        {
            string value = _headers.Get("connection");
            return !string.IsNullOrWhiteSpace(value)
                && value.Equals("Keep-Alive", StringComparison.InvariantCultureIgnoreCase);
        }

        public HttpMethods Method
        {
            get { return _method; }
        }

        public string Protocol
        {
            get { return _protocol; }
        }

        public Uri Uri
        {
            get { return _uri; }
        }

        public string[] RequestParameters
        {
            get { return _requestParameters; }
        }

        public Stream ContentStream => content;

        public NameValueCollection Headers =>_headers;
    }

    public interface IHttpRequest
    {
        NameValueCollection Headers { get; }

        HttpMethods Method { get; }

        string Protocol { get; }

        Uri Uri { get; }

        string[] RequestParameters { get; }
        Stream ContentStream { get; }
 
        bool KeepAliveConnection();
    }

    public interface IHttpPost
    {

        byte[] Raw {get;}

        IHttpHeaders Parsed {get;}

    }

    public sealed class HttpRequestParameters
    {
        private readonly string[] _params;

        private static readonly char[] Separators = { '/' };

        public HttpRequestParameters(Uri uri)
        {
            var url = uri.OriginalString;
            _params = url.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
        }

        public IList<string> Params
        {
            get { return _params; }
        }
    }
}