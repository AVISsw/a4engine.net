﻿using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using AVISNetworkEngineORM.DB;
using AVISNetworkEngineORM.DB.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
 using Microsoft.Extensions.Configuration;
 using Microsoft.Extensions.DependencyInjection;
 using Microsoft.Extensions.Hosting;
using uhttpsharp;
using uhttpsharp.RequestProviders;
using uhttpsharp.Listeners;
using System.Net.Sockets;
using System.Net;
using A4Engine.Transport;
using System.IO;
using Qiwi.BillPayments.Model;

namespace WebPanel
{
    class Program
    {
 
        static async Task Main(string[] args)
        {
 
            var builder = new HostBuilder()

                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                   
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddOptions();
                    var ser = services.AddSingleton<IHostedService, Index>();
                });

            await builder.RunConsoleAsync();

            Console.ReadLine();
        }
 
    }

    public class WebServer {

        public WebServer(int port)
        {
 
            /* create a TcpListener to listen for netweork requests on the provided
             * port number at the lookedup host address and start listening */
            TcpListener listener = new TcpListener(
                Dns.GetHostAddresses("localhost")[0], port);
            listener.Start();
            Console.WriteLine("Web server listening on port {0}", port);

            /* main body of the web server, this will listen for requests, 
             * open a socket with the client when a request is received 
             * and spawn a process thread for accepting the request and 
             * return to listen for the next request */
            while (true)
            {
                TcpClient soc = listener.AcceptTcpClient();
                new Task(delegate ()
                {
                    AcceptRequest(soc);
                }).Start();
            }
        }
        public void AcceptRequest(TcpClient socket)
        {
            if (socket.Connected)
            {
                /* create a buffer for the header and read in data from the
                 * client 1024 bytes at a time, ending only after a full
                 * header has been received */
                StringBuilder headerBuf = new StringBuilder();

                byte[] content = new byte[9024];
                while (true)
                {
                    var stream = socket.GetStream();
       
                    stream.Read(content, 0, content.Length);

                    headerBuf.Append(Encoding.ASCII.GetString(content));

                    /* if the data read thus far contains the header termination
                     * string, then we have seen the entire header and can get 
                     * on with it */
                    if (headerBuf.ToString().Contains("\r\n\r\n"))
                    {
                        break;
                    }
                }
                string header = headerBuf.ToString();

                /* generally this kind of debug statement would not be left 
                 * in the code as it adds unneccessary overhead to processing
                 * the request */
                Console.WriteLine(header);
 

            }
            socket.Close(); // always make sure to close network and file handles!!
        }
    }

}
