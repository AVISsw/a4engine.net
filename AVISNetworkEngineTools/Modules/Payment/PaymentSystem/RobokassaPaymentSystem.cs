﻿using AVISNetworkCommon.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class RobokassaPaymentSystem : BasePaymentSystem
    {
        public string mrh_pass1 = "CfH37sprKyB0MiQRz96h";
        public string mrh_pass2 = "fBg6rOt3R0z0hK0uzxpt";
        public string test_mrh_pass1 = "zYa1zwUC64RBJ7WRfBy3";
        public string test_mrh_pass2 = "Glk1X42LWAubZRu7o2fn";
        public string mrh_login = "S.O.T.A-2";

        public RobokassaPaymentSystem()
        {
            if (isTest)
            {
                mrh_pass1 = test_mrh_pass1;
                mrh_pass2 = test_mrh_pass2;
            }
        }
        public override bool ClientSuccess(RequestContext requestContext)
        {
            return RequestAuthentication(requestContext);
        }
        public class RobokassaPaymentContext : PaymentContext
        {
            public RobokassaPaymentContext(string OutSum, string IndId, string Shp_CharacterId)
            {
                Amount = Convert.ToInt64(OutSum);
                CharacterId = Convert.ToInt64(Shp_CharacterId);
                OrderId = IndId;
                PaymentSystemType = PaymentSystem = BasePaymentSystem.PaymentSystemType;
            }
        }
        public override bool TestRequest(RequestContext requestContext)
        {
            return isTest;
        }
        public override string RedirectLink(RequestContext Context)
        {
            // number of order
            var inv_id = 0;

            // описание заказа
            // order description
            var inv_desc = "Приобритение игровых рублей";

            // сумма заказа
            // sum of order
            long out_summ = Convert.ToInt64(Context.getParams.Get("amount"));

            // тип товара
            // code of goods
            long Shp_CharacterId = Convert.ToInt64(Context.getParams.Get("characterid"));

            // предлагаемая валюта платежа
            // default payment e-currency
            var in_curr = "";

            // язык
            // language
            var culture = "ru";

            var crc = Crypt.EncryptSHA256(mrh_login + ":" + out_summ + ":" + inv_id + ":" + mrh_pass1 + ":Shp_CharacterId=" + Shp_CharacterId);

            string uri = string.Format("https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin={0}&OutSum={1}&InvId={2}&Description={3}&Shp_CharacterId={4}&IncCurrLabel={5}&SignatureValue={6}&Culture={7}&IsTest={8}",
                mrh_login,
                out_summ,
                inv_id,
                HttpUtility.UrlEncode(inv_desc),
                Shp_CharacterId,
                in_curr,
                crc,
                culture,
                 Convert.ToInt32(isTest)
                );
            return uri;
        }

        public override bool RequestAuthentication(RequestContext Context)
        {
            string remoteHash = Context.postParams.Get("SignatureValue");
            if (string.IsNullOrWhiteSpace(remoteHash))
            {
                throw new Exception("SignatureValue is empty");
            }
            var inv_id = 0;
            // сумма заказа
            // sum of order
            long out_summ = Convert.ToInt64(Context.getParams.Get("amount"));
            // тип товара
            // code of goods
            long Shp_CharacterId = Convert.ToInt64(Context.getParams.Get("characterid"));
            var currentHash = Crypt.EncryptSHA256(mrh_login + ":" + out_summ + ":" + inv_id + ":" + mrh_pass1 + ":Shp_CharacterId=" + Shp_CharacterId);
            return currentHash.ToUpper() == remoteHash.ToUpper();
        }

        public override PaymentContext GetPaymentContext(RequestContext requestContext)
        {
            return new RobokassaPaymentContext(requestContext.getParams.Get("OutSum"), requestContext.postParams.Get("InvId"), requestContext.getParams.Get("Shp_CharacterId"));
        }

        public override string ExchangeRates(RequestContext context)
        {
            throw new NotImplementedException();
        }

        public override float GetPayAmount(string gameAmount)
        {
            throw new NotImplementedException();
        }
    }
}
 
