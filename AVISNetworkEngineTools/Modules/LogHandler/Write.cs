﻿using A4Engine.Modules;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.LogHandler
{
    public class Write : Handler
    {
        static List<LogObject> Logs = new List<LogObject>();
        static Logger Log;
        const string _dir = "remotelogs";
        const string _path = "/logfile";
        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
 
            if (!Directory.Exists(_dir))
            {
                Directory.CreateDirectory(_dir);
            }
            if (!File.Exists(Path))
            {
                File.CreateText(Path);
            }
        }

        public static string GetPath(DateTime dateTime)
        {
            var result = _dir + _path;
            result = result + dateTime.ToString("_MM_dd_yyyy") + ".log";
            return result;
        }
        static public string Path
        {
            get {
                
                return GetPath(DateTime.Now);
            }
        } 
        public override async Task Before(RequestContext Context)
        {
            Start(Context);
        }
        static internal Queue<LogObject> getLog(string raw)
        {
            try
            {
                return JsonConvert.DeserializeObject<Queue<LogObject>>(raw);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return new Queue<LogObject>();
        }

        public override async Task Start(RequestContext Context)
        {
            Log.Info("WriteLog");
            string Hash = Context.getParams.Get("Hash");
            if (!string.IsNullOrWhiteSpace(Hash))
            {
                Hash = Hash.Trim();
                Log.Info("Hash " + Hash);
                var session = SessionManager.GetSession(Hash);

                if (session != null)
                {
                    if (session.GroupRight.Can("editItem"))
                    {
                        string tempData = string.Empty;
                        var logs = getLog(Context.rawParams);
                        while (logs.Count > 0)
                        {
                            var log = logs.Dequeue();
                            LogHandler.Logs.Add(log);
                            tempData += JsonConvert.SerializeObject(log) + "\n";
                        }
                        using (StreamWriter sw = File.AppendText(Path))
                        {
                            sw.WriteLine(tempData);
                        }

                        Draw("" + 200);
                    }
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }
    }
}
