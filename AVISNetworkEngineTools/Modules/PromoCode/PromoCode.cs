﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.PromoCode
{
   public class PromoCode : Module
    {
        public override async Task OnLoad()
        {
            
        }

        public override async Task Start(RequestContext Context)
        {
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");
                if (isPartner || isAdmCreater)
                {
                    string index = FileGetContents(DirModule + "/form.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
            Draw("У вас недостаточно прав!");
            return;
        }
    }
}
