﻿using A4Engine.Modules;
using AVIS.Localization;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static A4Engine.Modules.SignUp.SignUpConfirmMail;

namespace AVISNetworkEngineTools.Modules.SignUp
{
    public class SignUpConfirmPhone : Handler
    {

        Logger Log;
        Random rand;
        public override Task OnLoad()
        {
 
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }
 
        public override async Task Start(RequestContext Context)
        {
            Log.Info("SignUpConfirmPhone");
            SetLang(Context);
            SignUpResult authResult = new SignUpResult();

            switch (SignUpSettings.SettingsData.Type)
            {
                case DataORM.Models.SignUpSettingsData.SignUpType.Close:
                    authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format(new TranslatableString("sign_up_closed")) };
                    Draw(JsonConvert.SerializeObject(authResult));
                    return;
            }
 
            DateTime confirmLastRequest = User.Data.Get<DateTime>("SignUpConfirmPhoneLastRequest");
            DateTime сonfirmEndRequest = User.Data.Get<DateTime>("SignUpConfirmPhoneEndRequest");

            if (сonfirmEndRequest > DateTime.Now)
            {
                TimeSpan deltaTime = confirmLastRequest.AddSeconds(20) - DateTime.Now;
                if (deltaTime.TotalSeconds <= 0)
                {
                    confirmLastRequest = DateTime.Now;
                    User.Data.Save("SignUpConfirmPhoneLastRequest", confirmLastRequest);

                    string requestCode = Context.postParams["code"];

                    AuthData authData = User.Data.Get<AuthData>("AuthData");
                    string code = User.Data.Get<int>("confirmPhoneCode").ToString();
 
                    if (requestCode.Length < 4)
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 3;
                        authResult.Message = "Неверная длинная кода подтверждения";
                    }
                    else
                    if (code.Length < 6)
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 2;
                        authResult.Message = " Ошибка при генерации кода подтверждения";
                    }
                    else
                    if (code == requestCode)
                    {
                        User.Data.Save("phoneConfirm", true);
                        // все ок
                        authResult.Success = true;
                        authResult.ErrorCode = 0;
                        authResult.Message = "Все ок";
                    }
                    else
                    {
                        authResult.Success = false;
                        authResult.ErrorCode = 1;
                        authResult.Message = " Ошибка при авторизации";
                    }
                }
                else
                {
                    authResult = new SignUpResult() { Success = false, ErrorCode = 2, Message = string.Format("Вы можете повторить попытку через {0} секунд", (int)deltaTime.TotalSeconds) };
                }
            }
            else
            {
                authResult = new SignUpResult() { Success = false, ErrorCode = 4, Message = string.Format("Время ожидания запроса истекло...") };
            }
            Draw(JsonConvert.SerializeObject(authResult));
        }

 
    }
}
