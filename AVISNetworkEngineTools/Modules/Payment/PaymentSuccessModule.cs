﻿using A4Engine.Modules;
using AVISNetworkCommon.Utility;
using AVISNetworkEngineTools.Modules.Payment.PaymentSystem;
using DataORM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Payment
{
    public class PaymentSuccessModule : Module
    {
        Random random;
        public override async Task OnLoad()
        {
            random = new Random();
        }
 
        public override async Task Start(RequestContext Context)
        {
            // HTTP parameters: OutSum, InvId, SignatureValue, Shp_item
            string thank = Thanks[random.Next(Thanks.Count)];
            try
            {
                PaymentSystemType paymentSystemType = User.Data.Get<PaymentSystemType>("PaymentSystem");
                var paymentSystem = BasePaymentSystem.Get(paymentSystemType);
                try
                {
                    var paymentContext = paymentSystem.GetPaymentContext(Context);
                    if (!paymentSystem.ClientSuccess(Context))
                    {
                        Draw("Ошибка валидации\n");
                        return;
                    }
                    Draw("Платеж №" + paymentContext.OrderId + " прошел успешно! <br>" + thank);
                    return;
                }
                catch (Exception ex)
                {
                    Log.Info(ex.ToString());
                }
                Draw("Платеж прошел успешно! <br>" + thank);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
                Draw("Ошибка валидации\n");
            }
        }
        public List<string> Thanks = new List<string>() {
            "Спасибо за поддержку!",
            "Большое спасибо игра развивается благодаря вашей поддержке!",
            "Больше спасибо за поддержку!",
            "Благодарим за поддержку!"
        };
    }
}
