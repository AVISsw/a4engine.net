import { Rect } from "./rect.js";

class Connector {
  constructor() {
    this.rect = new Rect("#ccc", 0, 0, 30, 16);
    this.connectionInProcess = false;
  }

  ApplyOffset(baseRect, offsetX, offsetY) {
    this.rect.x = baseRect.x + baseRect.width / 2 - this.rect.width / 2;
    this.rect.y = baseRect.y + baseRect.height - this.rect.height / 2;

    this.rect.ApplyOffset(offsetX, offsetY);

    this.rect.middleX = this.rect.relativeX + this.rect.width / 2;
    this.rect.middleY = this.rect.relativeY + this.rect.height;
  }

  OnDrag(mouseX, mouseY) {
    this.connectionInProcess = true;
  }

  Draw(offsetX, offsetY, context) {
    if (this.connectionInProcess) {
      context.moveTo(this.rect.middleX, this.rect.middleY);
      context.lineTo(window.mouseEventData.x, window.mouseEventData.y);
    }
  }
  OnSelected() {}
}

export { Connector };
