﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Utility
{
    public class Index
    {
        ulong index = 0;
        string path;
        public Index(string path)
        {
            this.path = path;
            var dir = Path.GetDirectoryName(path);
            if (!string.IsNullOrWhiteSpace(dir))
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            using (var fileStrem = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (var fileStreamReader = new StreamReader(fileStrem))
                {
                    var fileData = fileStreamReader.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(fileData))
                    {
                        index = Convert.ToUInt64(fileData);
                    }
                }
            }
        }

        public ulong Get()
        {
            return index;
        }
        public void Increment()
        {
            index++;
            using (var fileStrem = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (var fileStreamWriter = new StreamWriter(fileStrem))
                {
                    fileStreamWriter.Write(index.ToString());
                    fileStreamWriter.Flush();
                }
            }
        }
    }
}
