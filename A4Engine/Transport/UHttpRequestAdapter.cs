﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Transport
{
    public class UHttpRequestAdapter : IHttpRequest
    {
        public bool IsSecureConnection { get { return uhttpContext.SecureConnection; } }

        public string UserHostAddress => throw new NotImplementedException();

        public string UserAgent { get { return Headers.Get("User-Agent"); } }

        public Uri UrlReferrer => throw new NotImplementedException();

        public Uri Url => uhttpRequest.Uri;

        public TransportContext TransportContext => throw new NotImplementedException();

        public string ServiceName => throw new NotImplementedException();

        public Guid RequestTraceIdentifier => throw new NotImplementedException();

        public IPEndPoint RemoteEndPoint
        {
            get {  return (IPEndPoint) uhttpContext.RemoteEndPoint; }
        }

        public string RawUrl => Url.OriginalString;

        public NameValueCollection QueryString { get; set; }

        public Version ProtocolVersion => throw new NotImplementedException();

        public IPEndPoint LocalEndPoint => throw new NotImplementedException();

        public bool KeepAlive => throw new NotImplementedException();

        public bool IsWebSocketRequest => throw new NotImplementedException();

        public string UserHostName { get; private set; }

        public string[] UserLanguages => throw new NotImplementedException();

        public bool IsAuthenticated => throw new NotImplementedException();

        public Stream InputStream { get; private set; }

        public HttpMethods HttpMethod { get; private set; }

        public NameValueCollection Headers { get { return uhttpRequest.Headers; } }
        public bool HasEntityBody
        {
            get
            {
                return ContentLength64 > 0;
            }
        }
        public CookieCollection Cookies { get; set; }

        public string ContentType
        {
            get;
            private set;
        }
        public long ContentLength64
        {
            get;
            private set;
        }

        public Encoding ContentEncoding
        {
            get;
            private set;
        }
        public int ClientCertificateError => throw new NotImplementedException();

        public string[] AcceptTypes => throw new NotImplementedException();

        public bool IsLocal
        {
            get
            {
                return UserHostName.IndexOf("localhost") != -1 ||
                    UserHostName.IndexOf("128.0.0.1") != -1 ||
                    UserHostName.IndexOf("192.168.") != -1;
            }
        }

        string IHttpRequest.HttpMethod => uhttpRequest.Protocol;

        public IAsyncResult BeginGetClientCertificate(AsyncCallback requestCallback, object state)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 EndGetClientCertificate(IAsyncResult asyncResult)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 GetClientCertificate()
        {
            throw new NotImplementedException();
        }

        public Task<X509Certificate2> GetClientCertificateAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }
        public HttpResponseCode ResponseCode;

        private uhttpsharp.IHttpRequest uhttpRequest;
        private IHttpContext HttpContext;

        private Encoding GetEncoding(string str)
        {
            str = str.Trim().ToLower();
            if (str.IndexOf("utf-8") != -1)
            {
                return Encoding.UTF8;
            }

            return Encoding.ASCII;
        }
        uhttpsharp.IHttpContext uhttpContext;
        public UHttpRequestAdapter(IHttpContext HttpContext, uhttpsharp.IHttpContext uhttpContext, uhttpsharp.IHttpRequest uHttpRequest)
        {
            this.uhttpContext = uhttpContext;
            this.uhttpRequest = uHttpRequest;
            this.HttpContext = HttpContext;
            InputStream = uhttpRequest.ContentStream;
            try
            {
                var strContentType = Headers["Content-Type"];
                if (string.IsNullOrWhiteSpace(strContentType))
                    strContentType = "text/html; charset=utf-8";

                string[] splitable = strContentType.Split(";");
                ContentType = strContentType;

                string encodingStr = "utf-8";
                if (splitable.Length > 1)
                    encodingStr = splitable[1];

                ContentEncoding = GetEncoding(encodingStr);

                string contentLength = Headers["Content-Length"];
                ContentLength64 = Convert.ToInt64(contentLength);

                UserHostName = Headers["Host"];
 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
