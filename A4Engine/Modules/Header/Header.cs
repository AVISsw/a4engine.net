﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Modules.Header
{
    public class Header : Module
    {
        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            string index = FileGetContents(DirModule + "/form.html");
            Draw(FillPattern(index, new Dictionary<string, string> { }));
            return;
        }
    }
}
