$(document).ready(function () {
  
  var itemList = [];

  Vue.component('continer-component', {
    template: '#continer-component-template',
    props: ['item'],
    methods:{
      Update: function (item) {
        if( typeof(item["RepairsItemId"]) == "string")
        item["RepairsItemId"] = item["RepairsItemId"].split(",");
        console.log(item);
        jsonRequestData("/handler/RepairHandler/update", item, function (requestDataItemList) {
        });
      },
      Remove: function (item) {
 
        jsonRequestData("/handler/RepairHandler/remove", item , function (requestDataItemList) {

          item.Id = requestDataItemList.Id;
          var index = itemList.indexOf(item);
          itemList.splice(index,1);
        });
      }
    }
  })
  Vue.component('item-list', {
    template: '#item-list',
    props: ['items']
  })
  jsonRequestData("/handler/RepairHandler/getlist", {}, function (requestDataItemList) {
    requestDataItemList= requestDataItemList.reverse();
  itemList = requestDataItemList;
  console.log(requestDataItemList);
    new Vue({
      el: '#app-table',
      data: {
        newTodoText: '',
        items: requestDataItemList
      },
      methods: {
        Add: function () {
          var item = {
            Mesh: "",
            ItemId: -1,
            PercentChangeWear: 0,
            RepairsItemId: "",
            ChangeWear: 0,
            PercentChangeStrength: 0,
            ChangeStrength: 0
          };   

          jsonRequestData("/handler/RepairHandler/add", item , function (requestDataItemList) {
 
            item.Id = requestDataItemList.Id;
            itemList.push(item);
          });
        }
      }
    });
  });


});