﻿using MessagePack;
using AVISNetworkCommon;
using AVISNetworkEngineORM.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using A4Engine.Modules;

using System.Web;
using AVISNetworkEngineData.Models;
using System.Threading.Tasks;
using AVISNetworkEngineTools.Modules.ItemEditor;

namespace A4Engine.Modules.ItemEditor
{
    public class ItemEditor : Handler
    {
        Logger Log;
        public ItemFilterList itemFilterList;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            itemFilterList = new ItemFilterList("ru");
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public async Task<List<ItemData>> GetItemTemplate(int id)
        {
            List<ItemData> result = new List<ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>(" item_template.ItemId={0}", id))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ItemData>());
                }
            }
            return result;
        }

        public async Task<long> AddItemTemplate(NameValueCollection data)
        {
            long lastId = -1;
            try
            {
               lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<ItemData>(data));

            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId;
        }

        public async Task<bool> UpdateItemTemplate(int id, NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                var itemData = Controller.GetObjectByParams<ItemData>(data);
                itemData.StrengthMax = itemData.Strength;
                isAdd = await Controller.Storage.Update.Where(itemData, " ItemId={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveItemTemplate(int id)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<ItemData>(" ItemId={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<string> UpdateItemName()
        {
            string result = "";
            try
            {
                Console.WriteLine("UpdateItemName");
                Dictionary<string, string> resultDic = new Dictionary<string, string>();
                using (var fileWriter = new StreamWriter(File.Create("item_names.lang"), Encoding.UTF8))
                {
                    Console.WriteLine("item_names.lang");
                    using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>(""))
                    {
                        Console.WriteLine("ItemData.ItemData");
                        while (data.Read)
                        {
                            var itemData = data.GetObject<ItemData>();
                            string keyName = itemData.Key + "_" + itemData.ItemId + "_name";
                            string keyDesc = itemData.Key + "_" + itemData.ItemId + "_desc";
                            resultDic.Add(keyName, itemData.Name);
                            resultDic.Add(keyDesc, itemData.Desc);
                        }
                    }
                    result = JsonConvert.SerializeObject(resultDic);
                    fileWriter.Write(result);
                    fileWriter.Flush();
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return result;
        }
        public override async Task Start(RequestContext Context)
        {

            string key = Context.Path[3].ToString();
            int id = 0;

            if (User != null)
            {
                if (User.GroupRight.Can("editItem")) {

                    Console.WriteLine("get_loccccc key " + key);

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(await this.AddItemTemplate(Context.postParams)));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.UpdateItemTemplate(id, Context.postParams)));
                            break;
                        case "get":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.GetItemTemplate(id)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveItemTemplate(id)));
                            break;
                        case "get-filter-list":
                            Draw(JsonConvert.SerializeObject(this.itemFilterList.GetList()));
                            break;
                        case "get-loc":
                            Console.WriteLine("get_loccccc");
                            string result = await UpdateItemName();
                            Draw(result);
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
            // Draw(JsonConvert.SerializeObject(this.GetItemTemplate(id)));
        }
    }
    [MessagePackObject]
    [DBTable("item_template")]
    public class ItemData
    {
        [Key(0)]
        public long InstanceId;
        [Key(1), DBField]
        public long ContinerId;
        [Key(2), DBField(FieldAttrkey.AutoIncrement)]
        public int ItemId;
        [Key(3), DBField]
        public string Name;
        [Key(4), DBField]
        public int Count;
        [Key(5), DBField]
        public int PocketsCount;
        [Key(6), DBField]
        public ItemType IntType;
        [Key(7), DBField]
        public EquipmentContiner EquipmentContiner;
        [Key(8), DBField]
        public EquipmentnSlot Slot;
        [Key(9), DBField]
        public string Desc;
        [Key(10), DBField]
        public string Key;
        [Key(11)]
        public List<string> IconLayers;
        [Key(12), DBField]
        public int CustomizationId;
        [Key(13), DBField]
        public bool IsInfinite;
        [Key(14), DBField]
        public int SizeX;
        [Key(15), DBField]
        public int SizeY;
        [Key(16), DBField]
        public int SlotX;
        [Key(17), DBField]
        public int SlotY;
        [Key(18), DBField]
        public float Wear;
        [Key(19), DBField]
        public float Strength;
        [Key(20), DBField]
        public float StrengthMax;
        [Key(21), DBField]
        public bool Up;
        [Key(22), DBField]
        public bool Premium;
        [Key(23), DBField]
        public bool Personal;
        [Key(24), DBField]
        public int MaxCount;
        [Key(25), DBField]
        public float Weight;
        [Key(26), DBField]
        public int ContinerType;
        [Key(26), DBField]
        public TimeSpan ExpiryDate;
        [Key(27), DBField]
        public bool CanSpoil;
        [Key(28), DBField]
        public int ExpiredItemId;
        [Key(29), DBField]
        public string ItemContext;
        [Key(30)]
        public SortedDictionary<ItemType, int[]> SelectItems = new SortedDictionary<ItemType, int[]>();
        [Key(31)]
        public SortedDictionary<ItemType, List<long>> ChildItems = new SortedDictionary<ItemType, List<long>>();
        [Key(32), DBField]
        public string Skin;
        [Key(33)]
        public float TotalWeight;
        [IgnoreMember]
        public bool ExistsInDb = false;
        [Key(34), DBField]
        public bool Stackable = false;
        [Key(35), DBField]
        public float InteractionTime = 0.0f;
        [Key(36), DBField]
        public float Price = 0.0f;
        [Key(37), DBField]
        public float DonatedPrice = 0.0f;
        [Key(38), DBField]
        public string Filter = "";
        [Key(39), DBField]
        public bool IsUsed = false;
        [Key(40), DBField]
        public bool Completed = false;
        [Key(41), DBField]
        public bool InProcess = false;
        [Key(42), DBField]
        public int SkillId;
        [Key(43), DBField]
        public int SkillLevel;
        [Key(44), DBField]
        public int UsedItemId = -1;
    }
    public enum ContinerType
    {
        Continer = 0,
        Item = 1
    }
    public enum ItemType
    {
        Provision = 0,
        Armor = 1,
        Weapons = 2,
        Cages = 3,
        OpticalSights = 4,
        Ammo = 5,
        Repair = 6,
        MuzzleNozzle = 7,
        Bag = 8,
        GasMask = 9,
        Filter = 10,
        Geiger = 11,
        Pda = 12,
        Device = 13,
        PrivatCard = 14,
        Details = 15,
        Flashlight = 16,
        Battery = 17,
        MedKit = 18,
        Bandages = 19,
        Grenade = 20
    }

}
