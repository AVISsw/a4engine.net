﻿using DataORM.Models;
using AVISNetworkCommon;
using AVISNetworkEngineORM.DB;
using AVISNetworkEngineORM.DB.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine
{
    public class Rights
    {
        public AccountGroupData accountGroupData { get; set; }

        public string where = "";
        public Rights()
        {
            accountGroupData = new AccountGroupData();
        }

        public bool Can(string right)
        {
            return accountGroupData.Rights.ContainsValue(right);
        }

        public void LoadData()
        {
            accountGroupData.Rights.Add(0, "AllUserCmd");

        }
    }


    public class Group
    {
        public static Dictionary<int, Rights> groups = new Dictionary<int, Rights>();
        public static Dictionary<long, string> rights = new Dictionary<long, string>();
        List<int> groupsId = new List<int>();
        IStorage Storage;
        public Group(IStorage storage,long UserId)
        {
            Storage = storage;
            var loadDataTask = LoadData(UserId);
            loadDataTask.Wait();             
        }

        public bool MemberGroup(int id)
        {
            return groupsId.Contains(id);
        }


        public bool Can(int groupId, string right)
        {
            if (groups.ContainsKey(groupId))
            {
                return groups[groupId].Can(right);
            }
            return false;
        }

        public bool Can(string right)
        {
            foreach (int groupId in groupsId)
            {
                if (Can(groupId, right))
                    return true;
            }
            return false;
        }

        public async Task LoadData(long UserId)
        {
            using (baseData rights_data = await Storage.Query("SELECT * FROM user_groups WHERE UserId={0}", UserId))
            {
                while (rights_data.Read)
                {
                    groupsId.Add(rights_data.GetInt(2));
                    Console.WriteLine("UserId {0}, right {1}", UserId.ToString(), rights_data.GetInt(2).ToString());
                }
            }
        }

        public static async Task LoadRightData(Storage storage)
        {

            using (baseData rights_data = await storage.Query("SELECT * FROM rights"))
            {
                while (rights_data.Read)
                {
                    rights.Add(Convert.ToInt64(rights_data.GetValue(0)), rights_data.GetString(1));
                }
            }


            using (baseData rights_data = await storage.Query("SELECT * FROM group_rights"))
            {
                while (rights_data.Read)
                {
                    int groupId = rights_data.GetInt(1);
                    long rightId = Convert.ToInt64(rights_data.GetValue(2));
                    if (!groups.ContainsKey(groupId))
                    {
                        groups.Add(groupId, new Rights());
                        groups[groupId].accountGroupData.Rights.Add(2, "AllUserCmd");
                    }
                    if (!groups[groupId].accountGroupData.Rights.ContainsKey(rightId))
                        groups[groupId].accountGroupData.Rights.Add(rightId, rights[rightId]);
                    Console.WriteLine("rightId {0}, right {1}", rightId.ToString(), rights[rightId].ToString());
                }
            }
        }
    }
}
