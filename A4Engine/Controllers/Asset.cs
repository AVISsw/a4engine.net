﻿using A4Engine.Transport;
using A4Engine.User;
using AVISNetworkCommon;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace A4Engine.Controllers
{

    public class Asset : BaseController
    {
        Hashtable Contents = new Hashtable();
        Logger Log = new Logger(Logger.LoggerType.WriteConsole);
        public const string Folder = "asset";
        public Asset()
        {
            SetContents();
        }

        public override async Task Init(RequestContext Context, SessionManager sessionManager)
        {
            // запилить выбор фаила по пути из аддресной строки
            string stringPath = "/"+ Folder + "/";
 
            for(int i = 1; i < Context.Path.Count(); i++)
            {
                string value = Context.Path[i];
                stringPath += "/"+ value;
            }
            stringPath = stringPath.Remove(0,1);
 
            Context.Request.Response.ContentType = GetContent(stringPath);
            Context.Request.Response.StatusCode = HttpResponseCode.NotFound;

            if (File.Exists(stringPath))
            {
                Context.Request.Response.StatusCode = HttpResponseCode.Ok;
                var fileStream = new FileStream(stringPath, FileMode.Open, FileAccess.Read);
                fileStream.CopyTo(Context.Request.Response.OutputStream);
                fileStream.Close();
            }
        }
        public static async Task WriteFile(FileStream fileInputStream, RequestContext Context)
        {
            try
            {
                Context.Request.Response.SendChunked = true;
                
                byte[] buffer = new byte[1024 * 4];
                int nbytes;
                while ((nbytes = await fileInputStream.ReadAsync(buffer, 0, buffer.Length)) > 0)
                {
                    await Context.Request.Response.OutputStream.WriteAsync(buffer, 0, nbytes);
                    Context.Request.Response.OutputStream.Flush();
                } 
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                fileInputStream.Close();
            }
        }

        public string GetContent(string file_path)
        {
            string ext = "";
            int dot = file_path.LastIndexOf(".");
            if (dot >= 0)
                ext = file_path.Substring(dot, file_path.Length - dot).ToUpper();
            if (Contents[ext] == null)
                return "application/" + ext;
            else
                return (string)Contents[ext];
        }

        protected void SetContents()
        {
            Contents.Add("", "application/unknown");
            Contents.Add(".HTML", "text/html");
            Contents.Add(".HTM", "text/html");
            Contents.Add(".TXT", "text/plain");
            Contents.Add(".CSS", "text/css");
            Contents.Add(".GIF", "image/gif");
            Contents.Add(".JPG", "image/jpeg");
            Contents.Add(".VUE", "application/javascript");
            Contents.Add(".JS", "application/javascript");
        }
        public override async Task Render(RequestContext Context)
        {
           
        }
        internal override Task OnLoad()
        {

            return Task.FromResult(0);
        }
    }
}
