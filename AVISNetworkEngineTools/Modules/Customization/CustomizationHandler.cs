﻿using System;
using System.Collections.Generic;
using A4Engine.Modules;

using System.Text;
using System.Collections.Specialized;
using AVISNetworkCommon;
using Newtonsoft.Json;
using AVISNetworkCommon.Data.Models;
using System.Threading.Tasks;

namespace A4Engine.Modules.Customization
{
    class CustomizationHandler : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context); 
        }

        public async Task<List<CustomizationData>> GetItem(int id)
        {
            List<CustomizationData> result = new List<CustomizationData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<CustomizationData>(" Id={0}", id))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<CustomizationData>());
                }
            }
            return result;
        }


        public async Task<long> AddCustomization(NameValueCollection data)
        {
            List<CustomizationData> result = new List<CustomizationData>();
            long lastId = -1;
            try
            {
                lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<CustomizationData>(data));
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId;
        }

        public async Task<bool> UpdateCustomization(int id, NameValueCollection data)
        {
            List<CustomizationData> result = new List<CustomizationData>();
            bool isAdd = false;
            try
            {

                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<CustomizationData>(data), " Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveCustomization(int id)
        {
            List<CustomizationData> result = new List<CustomizationData>();
            bool isAdd = false;
            try
            {

                isAdd = await Controller.Storage.Remove.Where<CustomizationData>(" Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            int id = 0;
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(
                             new Dictionary<string, object>() { { "Id", await this.AddCustomization(Context.postParams) } }
                                ));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.UpdateCustomization(id, Context.postParams)));
                            break;
                        case "getitem":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.GetItem(id)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveCustomization(id)));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }

    }
}
