﻿
using A4Engine.Modules.ItemEditor;
using DataORM.Models;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using A4Engine.Modules;
using System.Threading.Tasks;

namespace A4Engine.Modules.LootEditor
{
    public class ChanceSpawnItemsHandler : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public async Task<Dictionary<long, Dictionary<long, ChanceSpawnItems>>> GetAllGroups()
        {
            Dictionary<long, Dictionary<long, ChanceSpawnItems>> groups = new Dictionary<long, Dictionary<long, ChanceSpawnItems>>();
         
            using (var tempData = await Controller.Storage.GetAll.WhereBase<ChanceSpawnItems>(""))
            {
                while (tempData.Read)
                {
                    ChanceSpawnItems chanceSpawn = tempData.GetObject<ChanceSpawnItems>();
                    Dictionary<long, ChanceSpawnItems> group = null;
                    if (!groups.TryGetValue(chanceSpawn.GroupId, out group))
                    {
                        group = new Dictionary<long, ChanceSpawnItems>();
                        groups.Add(chanceSpawn.GroupId, group);
                    }
                    group.Add(chanceSpawn.Id, chanceSpawn);
                }
            }
            return groups;
        }

        public async Task<bool> AddLoot(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<ChanceSpawnItems>(data));
                if (lastId > 0)
                    isAdd = true;
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<bool> UpdateLoot(NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                long Id = Convert.ToInt64(data["Id"]);
                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<ChanceSpawnItems>(data), " Id={0}", Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveLoot(long Id)
        {
            bool isAdd = false;
            try
            {
                isAdd = await Controller.Storage.Remove.Where<ChanceSpawnItems>("Id={0}", Id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<Dictionary<long, ItemData>> GetItems()
        {
            Dictionary<long, ItemData> result = new Dictionary<long, ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemData>();
                    result.Add(itemData.ItemId, itemData);
                }
            }
            return result;
        }
        public async Task<Dictionary<long, ItemData>> GetItemTemplateLoot()
        {
            Dictionary<long, ItemData> result = new Dictionary<long, ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>(""))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemData>();
                    itemData.ExpiryDate = default;
                    itemData.Filter = null;
                    result.Add(itemData.ItemId, itemData);
                }
            }

            return result;
        }
        public override async Task Start(RequestContext Context)
        {
            try
            {
                string key = Context.Path[3].ToString();
                long Id;

                string Hash = Context.postParams.Get("Hash");
                if (Hash != null)
                {
                    Hash = Hash.Trim();
                    var session = SessionManager.GetSession(Hash);
                    if (session != null)
                        if (session.GroupRight.Can("editScene"))
                        {

                            switch (key)
                            {
                                case "add":
                                    Draw(JsonConvert.SerializeObject(await this.AddLoot(Context.postParams)));
                                    break;
                                case "update":
                                    Draw(JsonConvert.SerializeObject(await this.UpdateLoot(Context.postParams)));
                                    break;
                                case "getallgroups":
                                    Draw(JsonConvert.SerializeObject(await this.GetAllGroups()));
                                    break;
                                case "getall":
                                    Draw(JsonConvert.SerializeObject(await this.GetAll()));
                                    break;
                                case "getitems":
                                    Draw(JsonConvert.SerializeObject(await this.GetItems()));
                                    break;
                                case "getitemsloot":
                                    Draw(JsonConvert.SerializeObject(await this.GetItemTemplateLoot()));
                                    break;
                                case "remove":
                                    Id = Convert.ToInt64(Context.Path[4].ToString());
                                    Draw(JsonConvert.SerializeObject(await this.RemoveLoot(Id)));
                                    break;
                                default:
                                    Draw(JsonConvert.SerializeObject(
                                        new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                                    }
                                    ));
                                    break;
                            }
                            return;
                        }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                Draw(ex.ToString());
            }
            Draw("<b>Доступ запрещен!</b>");
        }

        private async Task<Dictionary<long, Dictionary<long, ChanceSpawnItems>>> GetAll()
        {
            Dictionary<long, Dictionary<long, ChanceSpawnItems>> loots = new Dictionary<long, Dictionary<long, ChanceSpawnItems>>();
            using (var tempData = await Controller.Storage.GetAll.WhereBase<ChanceSpawnItems>(""))
            {
                while (tempData.Read)
                {
                    ChanceSpawnItems lootData = tempData.GetObject<ChanceSpawnItems>();
                    if (!loots.ContainsKey(lootData.GroupId))
                    {
                        loots.Add(lootData.GroupId, new Dictionary<long, ChanceSpawnItems>());
                    }
                    var lootGroup = loots[lootData.GroupId];
                    lootGroup.Add(lootData.Id, lootData);
                }
            }
            return loots;
        }
    }
}
