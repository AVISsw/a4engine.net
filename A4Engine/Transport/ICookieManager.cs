﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Transport
{
    public interface ICookieManager
    {
        void Update(Cookie cookieValue);
        void Remove(string cookieName);
        Cookie Get(string cookieName);
        Cookie this[string cookieName] { get; }

        CookieCollection GetUpdates();
    }
    public interface ICookie
    {
        bool Updated { get; set; }
        Cookie Cookie { get; set; }
    }
}