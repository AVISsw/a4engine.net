﻿using A4Engine.User;
using AVISNetworkCommon;
using AVISNetworkCommon.Utility;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using A4Engine.Modules;
using System.Threading.Tasks;

namespace A4Engine.Modules.Auth
{
    public class Auth : Handler
    {
        Logger Log;
        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {

            return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            Log.Info("------AuthRequest uri " + Context.Request.Request.RawUrl);
            foreach(string postRow in Context.postParams)
            {
                Console.WriteLine(postRow + "=" + Context.postParams[postRow]);
            }
            string password = Crypt.EncryptSHA256(Context.postParams["password"].Trim());
            string login = Context.postParams["login"];
            AuthData authData = await GetItemTemplate(login, password);
 
            authData.LastIp = Context.Request.Request.RemoteEndPoint.Address.ToString();
            authData.LastMac = Network.ConvertIpToMAC(Context.Request.Request.RemoteEndPoint.Address);
            authData.LastAuth = DateTime.Now;
            authData.Session = login +"|"+ Crypt.EncryptSHA256(authData.Password + authData.Login + authData.Id);
            bool isUpdate = await UpdateUser(authData);
            AuthResult authResult = new AuthResult();
            if (authData != null && isUpdate)
            {
                var session = new Session(this.SessionManager, this.Controller.Storage, authData, true);
                Context.Request.Cookies.Update(new System.Net.Cookie("SessionHash", authData.Session, "/"));
                authResult.Success = true;
                authResult.IKType = IkType.Undifined;
                bool partner = session.GroupRight.Can("partner");
                bool admCmd = session.GroupRight.Can("AdminCmd");

                if (partner)
                {
                    authResult.IKType = IkType.Partner;
                }
                if (admCmd)
                {
                    authResult.IKType = IkType.Adm;
                }
            }
            else
            {
                authResult.Success = false;
                authResult.ErrorCode = 1;
                authResult.Message = " Ошибка при авторизации";
            }
            Draw(JsonConvert.SerializeObject(authResult));
        }

        public async Task<AuthData> GetItemTemplate(string login, string password)
        {
            return await Controller.Storage.Get.Where<AuthData>("Login={0} AND Password={1}", login, password);
        }
        public async Task<bool> UpdateUser(AuthData authData)
        {
            return await Controller.Storage.Update.Where(authData, "Id={0}", authData.Id);
        }
        public enum IkType
        {
            Undifined,
            Partner,
            Adm
        }
        public struct AuthResult
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
            public IkType IKType;
        }
    }
}

