﻿using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.UserRights
{
    public class GroupRightHandler : Handler
    {

        Logger Log;
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole); return Task.FromResult(0);
        }
        private async Task<List<UserGroups>> GetUserGroups()
        {
            List<UserGroups> result = new List<UserGroups>();

            using (var data = await Controller.Storage.GetAll.WhereBase<UserGroups>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<UserGroups>());
                }
            }
            return result;
        }
        private async Task<List<Groups>> GetGroups()
        {
            List<Groups> result = new List<Groups>();

            using (var data = await Controller.Storage.GetAll.WhereBase<Groups>(""))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<Groups>());
                }
            }
            return result;
        }
 
        private async Task<Dictionary<long, RightContiner>> GetRights()
        {
            Dictionary<long, RightContiner> result = new Dictionary<long, RightContiner>();

            using (var data = await Controller.Storage.GetAll.WhereBase<RightData>(""))
            {
                while (data.Read)
                {
                    var rightData = data.GetObject<RightData>();
                    result.Add(rightData.Id, new RightContiner() { RightData = rightData });
                }
            }
            return result;
        }
        private async Task<Dictionary<long, GroupRights>> GetGroupRights(long GroupId)
        {
            Dictionary<long, GroupRights> result = new Dictionary<long, GroupRights>();

            using (var data = await Controller.Storage.GetAll.WhereBase<GroupRights>(" GroupId={0}", GroupId))
            {
                while (data.Read)
                {
                    var groupRight = data.GetObject<GroupRights>();
                    result.Add(groupRight.RightId, groupRight);
                }
            }
            return result;
        }
        private async Task<List<Right>> GetRightsByGroup(long GroupId)
        {
            var groupRights = await GetGroupRights(GroupId);
            var tempRights = await GetRights();
            List<Right> rights = new List<Right>();
            foreach (var rightData in tempRights)
            {
                Right tempValue = new Right() { RightData = rightData.Value.RightData };
                tempValue.Value = groupRights.TryGetValue(rightData.Value.RightData.Id, out tempValue.GroupRights);
                rights.Add(tempValue);
            }
            return rights;
        }
        private async Task<List<Right>> SetRightsByGroup(long GroupId, List<Right> rights)
        {
            try
            {
                foreach (var groupRight in rights)
                {
                    if (groupRight.Value == true)
                    {
                        if (groupRight.GroupRights == null)
                        {
                            groupRight.GroupRights = new GroupRights() { GroupId = GroupId, RightId = groupRight.RightData.Id };
                            groupRight.GroupRights.Id = await Controller.Storage.Add.In(groupRight.GroupRights);
                        }
                        else
                        {
                            var isUpdate = await Controller.Storage.Update.Where(groupRight.GroupRights, "Id={0}", groupRight.GroupRights.Id);
                        }
                    }
                    else
                    {
                        if (groupRight.GroupRights != null)
                        {
                            var isRemove = await Controller.Storage.Remove.Where<GroupRights>("Id={0}", groupRight.GroupRights.Id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return rights;
        }
        private async Task<List<RightContiner>> SetRights(List<RightContiner> rights)
        {
            try
            {
                foreach (var rightData in rights)
                {
                    if (rightData.Remove)
                    {
                        var isRemove = await Controller.Storage.Remove.Where<RightData>("Id={0}", rightData.RightData.Id);
                    }
                    else
                    if (rightData.Add)
                    {
                        rightData.RightData.Id = await Controller.Storage.Add.In(rightData.RightData);
                    }
                    else
                    {
                        var isUpdate = await Controller.Storage.Update.Where(rightData.RightData, "Id={0}", rightData.RightData.Id);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return rights;
        }
        public class Right
        {
            public RightData RightData;
            public GroupRights GroupRights;
            public bool Value;
        }
        public class RightContiner
        {
            public RightData RightData;
            public bool Add;
            public bool Remove;
        }
        public class UserContiner
        {
            public AuthData AuthData;
            public List<UserGroups> Groups = new List<UserGroups>();
        }

        public override async Task Start(RequestContext Context)
        {
            try
            {
                string key = Context.Path[3].ToString();
                Console.WriteLine("---" + key);
                Console.WriteLine("Json " + Context.rawParams);
                RequestData requestData = JsonConvert.DeserializeObject<RequestData>(Context.rawParams);

                if (User.GroupRight.Can("editScene"))
                {
                    Console.WriteLine("---can right");
                    switch (key)
                    {
                        case "get-groups":
                            Draw(JsonConvert.SerializeObject(await this.GetGroups()));
                            break;
                        case "get-group-rights":
                            Draw(JsonConvert.SerializeObject(await this.GetRightsByGroup(requestData.GroupId)));
                            break;
                        case "get-rights":
                            Draw(JsonConvert.SerializeObject(await this.GetRights()));
                            break;
                        case "set-group-rights":
                            Console.WriteLine("---json " + JsonConvert.SerializeObject(requestData));
                            Draw(JsonConvert.SerializeObject(await SetRightsByGroup(requestData.GroupId, requestData.GroupRights)));
                            break;
                        case "set-rights":
                            Console.WriteLine(JsonConvert.SerializeObject(requestData));
                            Draw(JsonConvert.SerializeObject(await SetRights(requestData.Rights)));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Draw("<b>Доступ запрещен!</b>");
        }

        [Serializable]
        public class RequestData
        {
            public long GroupId = -1;
            public long UserId = -1;
            public List<Right> GroupRights = new List<Right>();
            public List<RightContiner> Rights = new List<RightContiner>();
        }

    }
}
