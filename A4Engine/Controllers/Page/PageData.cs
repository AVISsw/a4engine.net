﻿using AVISNetworkEngineORM.Data;

namespace A4Engine.Controllers.Page
{
    [DBTable("pages")]
    public class PageData
    {
        [DBField]
        public int Id;
        [DBField]
        public string Key;
        [DBField]
        public string Name;
        [DBField]
        public string Pattern;

    }


}
