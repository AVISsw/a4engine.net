﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVISNetworkEngineTools
{
    public class SettingsData
    {
        public int WPPort;
        public string Host;
        public string Name;
        public int Port;
        public string User;
        public string Password;

        // smtp
        public string SmtpHost;
        public int SmtpPort;
        public string senderEmail;
        public string senderName;
        public string senderPassword;

        public string FileHost = "https://www.sota-games.ru";

        public List<string> Assembly;
    }
}
