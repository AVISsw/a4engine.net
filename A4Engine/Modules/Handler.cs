﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4Engine;
using A4Engine.User;
using A4Engine.Controllers;
using A4Engine.Modules.Interface;
using System.Net;
using System.IO;
using A4Engine.Transport;
using AVIS.Localization;

namespace A4Engine.Modules
{
    public abstract class Handler : IModule
    {
        public string DirModule { get { return string.Format("{0}"+Asset.Folder+"/modules/{1}", AppDomain.CurrentDomain.BaseDirectory, this.GetType().Name.ToLower()); } }
        public string UriModule { get { return string.Format("/files/modules/{0}", this.GetType().Name.ToLower()); } }
        public BaseController Controller { get; set; }
        public string Alias { get; set; } = string.Empty;
        public string Name
        {
            get
            {
                string result = string.Empty;
                if (Alias.Length < 1)
                {
                    result = GetType().Name;
                }
                else
                {
                    result = Alias;
                }
                return result.ToLower();
            }
        }
        public void SetLang(RequestContext Context)
        {
            string langId = Context.postParams["lang"];
            if (!string.IsNullOrWhiteSpace(langId))
            {
                User.Data.Save("lang", langId);
            }
            else
            {
                langId = User.Data.Get<string>("lang");
            }
            if (string.IsNullOrWhiteSpace(langId))
                langId = "en";
            Localization.Get().SetLang(langId);
        }
        public Session User { get; set; }
        public SessionManager SessionManager { get; set; }

        protected string result_data = string.Empty;

        public abstract Task Before(RequestContext Context);

        // вызывается при создании экземпляра модуля
        public abstract Task Start(RequestContext Context);

        /*
        * Вызывается для загрузки общих ресурсов модуля, всех его экземпляров
        * */
        public abstract Task OnLoad();

        public string Request(string uri)
        {
            string line = "";

            try
            {
                //    ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
                //   // ЕМАЕЕЕ ЭТА ШНЯГА НУЖНА БЫЛАААА
                //    ServicePointManager.SecurityProtocol = (SecurityProtocolType)0; //TLS 1.2
                if (uri.IndexOf("https") != -1)
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | (SecurityProtocolType)3072 | (SecurityProtocolType)768 | SecurityProtocolType.Ssl3;

                WebRequest request = WebRequest.Create(uri);
                WebResponse response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {

                        line = reader.ReadToEnd();
                    }
                }
                response.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(uri + " Не удалось установить соединение с сервером \n" + ex.ToString());
            }
            return line;
        }
        public string Render()
        {
            var temp = result_data;
            result_data = string.Empty;
            return temp;
        }
        public HttpResponseCode StatusCode = HttpResponseCode.Ok;
        virtual public void Draw(string result, HttpResponseCode code = HttpResponseCode.Ok)
        {
            this.result_data = result;
            StatusCode = code;
        }
 
        public struct Result
        {
            public bool Success;
            public int ErrorCode;
            public string Message;
        }
    }
}