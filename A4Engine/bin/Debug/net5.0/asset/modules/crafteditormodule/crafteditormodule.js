
$(window).on('load', function () {
    var itemListModule;

    jsonRequestData("/handler/TradeListHandler/get-item-list", {}, function (requestDataItemList) {

        modalModule = new Vue({
            el: '#modalModule',
            data: {
                Type: -1,
                Title: "",
                ItemName: "",
                Items: requestDataItemList
            },
            methods: {
                SelectItem(ItemData) {

                    modalModule.$data.Type = -1;
                    itemListModule.selectItemData = ItemData;
                },
                Close() {
                    modalModule.$data.Type = -1;
                }
            },
            computed: {
                filteredList: function () {
                    var name = this.ItemName;
                    let result = Object.values(this.Items).filter(function (elem) {
                        console.log(elem);
                        if (name === '') return true;
                        else return elem.Name.indexOf(name) > -1;
                    });

                    return result;
                }
            }
        });

        jsonRequestData("/handler/CraftEditorHandler/get-recept-list", {}, function (requestDataReceptList) {
            console.log(requestDataReceptList);
            var firstElem;
            requestDataReceptList = requestDataReceptList.reverse();
            for (var i in requestDataReceptList) {
                firstElem = requestDataReceptList[i];
                break;
            }
            itemListModule = new Vue({
                el: '#itemListModule',
                data: {
                    Items: requestDataReceptList,
                    Recept: firstElem,
                    selectItemData: firstElem
                },
                methods: {
                    SelectRecept(index, item) {
                        this.Recept = Object.assign({}, item);
                        console.log(this.Recept);
                    }, Clone(index, item) {
                        var id = this.Recept.ReceptData.Id;              
 
                        this.Recept = JSON.parse(JSON.stringify(item));
                        this.Recept.ReceptData.Id = id;
                        this.Recept.ReceptData.CacheId = id;
                        this.Recept.Conditions.forEach(element => {
                            element.Id = -1;
                            element.CacheId = -1;
                            element.IsDbExits = false;
                            element.ReceptId = id;
                        });
                        this.Recept.Result.forEach(element => {
                            element.Id = -1;
                            element.CacheId = -1;
                            element.IsDbExits = false;
                            element.ReceptId = id;
                        });
                    },
                    AddRecept() {
                        var items = this.Items;
                        postRequestData("/handler/CraftEditorHandler/add-recept", {}, function (responseData) {
                            console.log(responseData);
                            console.log(items);
                            Vue.set(items, responseData.Id, responseData);
                            this.Recept = responseData;
                        });
                    },AddCondition() {
                        console.log(this.Recept);
                        this.Recept.Conditions.push({ReceptId:this.Recept.ReceptData.Id, Count: 0, Strength:0, Wear: 0, IsRemove: false});
                    },RmCondition(index, condition) {
                        console.log(condition);
                        condition.IsRemove = true;
                    },AddResult() {
                        console.log(this.Recept.ReceptData);
                        this.Recept.Result.push({ReceptId:this.Recept.ReceptData.Id, Count: 1, IsRemove: false});
                    },RmResult(index, condition) {
                        console.log(condition);
                        condition.IsRemove = true;
                    },
                    SelectItemOpen() {
                        console.log("select window");
                        modalModule.$data.Type = 1;
                    },
                    SaveRecept(Recept) {
                        console.log("post request save");
                        Recept.Conditions.forEach(element => {
                            console.log(element);
                            if (!Array.isArray(element.ItemId)) {
                                element.ItemId = element.ItemId.split(",");
                            }
                        });
                        console.log(Recept);
                        postRequestData("/handler/CraftEditorHandler/update-recept", Recept, function (responseData) {
                            console.log("from server ");
                            itemListModule.Recept = responseData;
                            console.log( itemListModule.Recept);
                        });
                    },
                    RemoveRecept(index, Recept) {
                        var items = this.Items;
                        console.log(Recept.ReceptData);
                        $.post(
                            "/handler/CraftEditorHandler/remove-recept",
                            { "Id": Recept.ReceptData.Id },
                            function (responseData) {
                                Vue.delete(items, Recept.ReceptData.Id);
                            }
                        );
                    }
                },
                computed: {
                    filteredList: function () {
                        var name = this.ItemName;
                        let result = Object.values(this.Items).filter(function (elem) {
                            if (name === '') return true;
                            else return elem.Desc.indexOf(name) > -1;
                        });
                        return result;
                    }
                }
            });

        });

    });
});
