var openEditorButton;
function SaveCustomization(id) {
  var form = $('#' + id + "_customization");
  var data = Serilaze(form);
  updateCustomizationRequest(id, data);
}

function RemoveCustomization(id) {
  var descArea = $('#removeCustomization').html();
  descArea = fill_pattern(descArea, { Id: id });
  ShowModal(descArea, "Редактироване описания");
}

function OpenCustomizationEditor(elem) {
  openEditorButton = $(elem);
  var id = openEditorButton.val();
  var CustomizationEditorTemplate = $('#CustomizationEditor').html();
  CustomizationEditorTemplate = fill_pattern(CustomizationEditorTemplate, { Id:id});
  ShowModal(CustomizationEditorTemplate, "Редактор элементов кастомизации");
  updateCustomizationList(id);
}

function updateCustomizationList(id) {
  getItemCustomizationRequest(id,function (data) {
    var CustomizationTemplate = $('#CustomizationTemplate').html();
    var tempCustomizations = "";
    var types = [
      { Name: "Mesh", Value: "Mesh" },
      { Name: "SkinnedMesh", Value: "SkinnedMesh" }
    ];
    var slots = [
      { Name: "Правая рука", Value: "RightHand" },
      { Name: "Торс", Value: "Tors" },
      { Name: "Штаны", Value: "Leg" },
      { Name: "Ботинки", Value: "Foot" },
      { Name: "Сумка", Value: "BackPack" },
      { Name: "Слот без замены головы Маска", Value: "GasMask" },
      { Name: "Слот без замены головы Шлем", Value: "Helmet" },
      { Name: "Слот без замены головы Очки", Value: "Glasses " },
      { Name: "Слот с заменой головы", Value: "Head" },
      { Name: "Перчатки", Value: "Arm" },
    ];

    if (Array.isArray(data))
    data.forEach(element => {

      element.SelectType = DrawOptions(types, element.Type);
      element.SelectSlot = DrawOptions(slots, element.Slot);
      var temprow = fill_pattern(CustomizationTemplate, element);
      var divRow = $(temprow);
      tempCustomizations += divRow[0].outerHTML;
      $("#addNewCustomizationRequest").hide();
    });
    $("#CustomizationList").html(tempCustomizations);
  });

}

function updateCustomizationRequest(id, data) {
  $.post(
    "/handler/customizationhandler/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function getItemCustomizationRequest(id,callback) {
  $.post(
    "/handler/customizationhandler/getitem/"+id,
    {
    },
    callback
  );
}


function addNewCustomizationRequest() {
  // addItem();
  var th = $(this);
  $.post(
    "/handler/customizationhandler/add/",
    {},
    function (data) {
      console.log(data);
       if(data)
       if(data.Id)
       if(data.Id > 0){
        openEditorButton.val(data.Id);
       }
         updateCustomizationList(data.Id); }
  );
}

function removeCustomizationRequest(id) {
  $.post(
    "/handler/customizationhandler/remove/" + id,
    {},
    function (data) { ShowModal(data, "Результат удаления"); }
  );
}
