﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
 
	public class MultiPartStreamReader : IEnumerable<MultiPartStreamValue>
	{
		readonly Encoding Encoding;
		readonly Stream SourceStream;
		readonly string BoundaryIndicator;

		public MultiPartStreamReader(Stream sourceStream, Encoding encoding, string contentType)
		{
			if (sourceStream == null)
				throw new ArgumentNullException("SourceStream");
			if (encoding == null)
				throw new ArgumentNullException("Encoding");
			if (string.IsNullOrEmpty(contentType))
				throw new ArgumentNullException("ContentType");

			SourceStream = sourceStream;
			Encoding = encoding;
			BoundaryIndicator = GetBoundaryIndicator(contentType);
		}

		string GetBoundaryIndicator(string contentType)
		{
			if (!contentType.ToLowerInvariant().StartsWith("multipart/form-data"))
				throw new InvalidOperationException("Must be multipart/form-data");
			string[] values = contentType.Split(';').Skip(1).ToArray();
			var namesAndValues = string.Join(";", values);
			return namesAndValues.ToNameValueCollection()["Boundary"].Trim('"');
		}


		public IEnumerator<MultiPartStreamValue> GetEnumerator()
		{
			return new MultiPartStreamEnumerator(SourceStream, Encoding, BoundaryIndicator);
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
 