﻿using A4Engine.Modules;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AVISNetworkEngineTools.Modules.Monitoring
{
    public class Monitoring : Handler
    {
        public static event Action GameServerOffNotify;
        public static event Action GameServerStartNotify;

        MonitoringData monitoringData = new MonitoringData();
        Timer timer;
        public override async Task Before(RequestContext Context)
        {
            Start(Context);
        }

        public override async Task OnLoad()
        {
            timer = new Timer();
            timer.AutoReset = true;
            timer.Interval = 3000f;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
           
        }
        public static void Cmd(string cmd)
        {
            serverCmdData.Cmd.Add(cmd);
        }
        bool IsServerOnline;
        bool IsSendOffServerNotify = false;
        bool IsSendOnServerNotify = false;
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (lastUpdate.AddSeconds(60) > DateTime.Now)
            {
                IsServerOnline = true;
                if (IsSendOnServerNotify == false)
                {
                    IsSendOnServerNotify = true;
                    IsSendOffServerNotify = false;
                    if (GameServerStartNotify != null)
                    {
                        GameServerStartNotify();
                    }
                }
            }
            else
            {
                IsServerOnline = false;
                if (IsSendOffServerNotify == false)
                {
                    IsSendOffServerNotify = true;
                    IsSendOnServerNotify = false;
                    if (GameServerOffNotify != null)
                    {
                        GameServerOffNotify();
                    }
                }
            }
        }
        DateTime lastUpdate;
        static ServerCmdData serverCmdData = new ServerCmdData();
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();

            switch (key)
            {
                case "restart":

                    if (User != null)
                    {
                        if (User.GroupRight.Can("editItem"))
                        {
                            serverCmdData.Restart = true;
                            Draw("restart ok");
                            return;
                        }
                    }
                    break;
                case "update_cache":

                    if (User != null)
                    {
                        if (User.GroupRight.Can("editItem"))
                        {
                            serverCmdData.UpdateCache = true;
                            Draw("update cache ok");
                            return;
                        }
                    }
                    break;
                case "update":
                    string Hash = Context.getParams.Get("Hash");
                    if (Hash != null)
                    {
                        Hash = Hash.Trim();

                        var session = SessionManager.GetSession(Hash);
                        if (session != null)
                        {
                            if (session.GroupRight.Can("editItem"))
                            {

                                monitoringData = JsonConvert.DeserializeObject<MonitoringData>(Context.rawParams);
                                lastUpdate = DateTime.Now;
                                if(monitoringData.ChatReciver.Count > 0)
                                {
                                   foreach(var message in monitoringData.ChatReciver)
                                    {
                                        VKBot.VKBot.SendMsgAdmVK(message);
                                    }
                                }
                                Draw(JsonConvert.SerializeObject(serverCmdData));
                                serverCmdData = new ServerCmdData();
                                return;
                            }
                        }
                    }
                    break;
                case "get":
                    if (User != null)
                    {
                        if (User.GroupRight.Can("editItem"))
                        {
                            monitoringData.IsServerOnline = IsServerOnline;
                            Draw(JsonConvert.SerializeObject(monitoringData));
                            return;
                        }
                    }

                    break;
                default:
                    Draw(JsonConvert.SerializeObject(
                        new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                    }
                    ));
                    break;
            }
            Draw("<b>Доступ запрещен!</b>");
        }
    }
    public class ServerCmdData
    {
        public bool Restart;
        public bool UpdateCache; 
        public List<string> Cmd = new List<string>();
    }
    public class MonitoringData
    {
        public int Online;
        public int MaxOnline;
        public int Entity;
        public long QueryCount;
        public long InsertCount;
        public long SelectCount;
        public long UpdateCount;
        public long RemoveCount;
        public long Banned;
        public long TotalBanned;
        public long TotalConnection;
        public bool IsServerOnline;
        public List<string> ChatReciver = new List<string>();
    }
}
