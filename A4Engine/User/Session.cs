﻿using AVISNetworkEngineData.Models;
using AVISNetworkEngineORM.DB;
using AVISNetworkEngineORM.DB.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.User
{
    public class Session
    {
        public Group GroupRight { get; set; }

        public Storage Storage { get; set; }

        public SessionManager sessionManager { get; set; }

        public Data Data { get; set; }
        public AuthData UserData { get; set; }

        string Hash = "";
        public Session(SessionManager sessionManager, Storage Storage, string Hash, bool reload = false)
        {
            this.Storage = Storage;
            this.Hash = Hash;
            this.sessionManager = sessionManager;
            Data = new Data();
            var hashTask = GetUserByHash(Hash);
            hashTask.Wait();
            UserData = hashTask.Result;
            sessionManager.AddSession(Hash, this, reload);
            if (UserData != null)
            {
                GroupRight = new Group(Storage, UserData.Id);
            }
            else
            {
                GroupRight = new Group(Storage, -1);
            }
        }
   
        public Session(SessionManager sessionManager, Storage Storage, AuthData authData, bool reload = false)
        {
            this.Storage = Storage;
            this.UserData = authData;
            Data = new Data();
            this.sessionManager = sessionManager;
            Hash = authData.Session;
            sessionManager.AddSession(Hash, this, reload);
            GroupRight = new Group(Storage, UserData.Id);

        }


        public Task<AuthData> GetUserByHash(string hash)
        {
            return Storage.Get.Where<AuthData>("Session={0}", hash);
        }
    }


    public class Data
    {
        public Dictionary<string, object> data = new Dictionary<string, object>();

        public void Save(string key, object value)
        {
            if (value != null)
            {
                if (data.ContainsKey(key))
                    data[key] = value;
                else
                    data.Add(key, value);
            }
        }
        public void Clear()
        {
            data.Clear();
        }
        public string Get(string key)
        {
            if (data.ContainsKey(key))
                return (string)data[key];
            else
                return null;
        }
        public T Get<T>(string key, T def = default(T))
        {
            if (data.ContainsKey(key))
                return (T)data[key];
            else
                return def;
        }
        public void Remove(string key)
        {
            if (data.ContainsKey(key))
                data.Remove(key);
        }
    }
}
