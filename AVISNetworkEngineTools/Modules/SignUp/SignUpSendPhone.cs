﻿
using A4Engine;
using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkEngineData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static A4Engine.Modules.SignUp.SignUpConfirmMail;

namespace AVISNetworkEngineTools.Modules.SignUp
{
    public class SignUpSendPhone : Handler
    {
        Logger Log;
        Random rand;
        public override Task OnLoad()
        {
            rand = new Random();
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }
        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            SignUpResult authResult = new SignUpResult();

            switch (SignUpSettings.SettingsData.Type)
            {
                case DataORM.Models.SignUpSettingsData.SignUpType.Close:
                    authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format("Регистрация закрыта...") };
                    Draw(JsonConvert.SerializeObject(authResult));
                    return;
            }


            if (SignUpSettings.SettingsData.CheckPhone == false){
                authResult = new SignUpResult() { Success = false, ErrorCode = 6, Message = string.Format("Проверка телефона отключена...") };
                Draw(JsonConvert.SerializeObject(authResult));
                return;
            }
 
            DateTime confirmLastRequest = User.Data.Get<DateTime>("SignUpSendPhoneLastRequest");
            DateTime сonfirmEndRequest = User.Data.Get<DateTime>("SignUpSendPhoneEndRequest");
            Console.WriteLine("confirmLastRequest " + confirmLastRequest + " > " + сonfirmEndRequest);
            TimeSpan deltaTime = confirmLastRequest.AddSeconds(20) - DateTime.Now;
            if (сonfirmEndRequest > DateTime.Now)
            {
                if (deltaTime.TotalSeconds <= 0)
                {
                    confirmLastRequest = DateTime.Now;
                    User.Data.Save("SignUpSendPhoneLastRequest", confirmLastRequest);
                    AuthData authData = User.Data.Get<AuthData>("AuthData");
                    authData.Phone = Context.postParams["phone"];

                    if (PhoneValidation(authData.Phone))
                    {
                        var findedAccount = await FindAccountByData(authData.Phone);
                        if (findedAccount != null)
                        {
                            authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format("Этот телефон уже используется") };
                        }
                        else
                        {
                            int sms_count_for_user = User.Data.Get<int>("sms_count");
                            if (sms_count_for_user >= Settings.Server.MaxSmsCountForUser)
                            {
                                authResult = new SignUpResult() { Success = false, ErrorCode = 5, Message = string.Format("Превышено количество попыток отправки смс") };
                            }
                            else
                            {
                                int phoneCode = rand.Next(100000, 999999);
                                User.Data.Save("confirmPhoneCode", phoneCode);
                                string message = string.Format("SOTA Games, Код регистрации: {0}", phoneCode);
                                Log.Info("------------send_sms--------------\n Phone:{0}, code:{1}", authData.Phone, phoneCode);
                                /*
                                SmsService service = new SmsService();
                                var result = service.send_sms(authData.Phone, message, 0, "", 0, 0, "SOTA Games");
                                string id = result[0];
                                sms_status sms_status = service.get_status(authData.Phone, id);
                                */
                                SmsAeroProject.SmsAero smsAero = new SmsAeroProject.SmsAero(Settings.Server.SmsServiceLogin, Settings.Server.SmsServicePassword);
                                string responseJson = await smsAero.SmsSend(message, authData.Phone);
                                SmsServiceResponse response = JsonConvert.DeserializeObject<SmsServiceResponse>(responseJson);
                                if (response.success)
                                {
                                    Log.Info(JsonConvert.SerializeObject(response.data));
                                }
                                if (!string.IsNullOrWhiteSpace(response.message))
                                {
                                    Log.Info("------------send_sms_response_message--------------\n", response.message);
                                }
                            }
                            sms_count_for_user++;
                            User.Data.Save("sms_count", sms_count_for_user);
                        }
                    }
                    else
                    {
                        authResult = new SignUpResult() { Success = false, ErrorCode = 3, Message = string.Format("Телефон не прошел валидацию")};
                    }
                }
                else
                {
                    authResult = new SignUpResult() { Success = false, ErrorCode = 2, Message = string.Format("Вы можете повторить попытку через {0} секунд", (int)deltaTime.TotalSeconds) };
                }
            }
            else
            {
                authResult = new SignUpResult() { Success = false, ErrorCode = 4, Message = string.Format("Время ожидания запроса истекло...") };
            }
            Draw(JsonConvert.SerializeObject(authResult));
        }

        public Task<AuthData> FindAccountByData(string phone)
        {
            return Controller.Storage.Get.Where<AuthData>(" Phone={0} ", phone);
        }
        private bool PhoneValidation(string phone)
        {
            return  true;
        }
        [Serializable]
        public class SmsServiceResponse
        {
            public bool success;
            public PhoneResponse data;
            public string message;
        }
        [Serializable]
        public class PhoneResponse
        {
            public long id;
            public string from;
            public string number;
            public string text;
            public int status;
            public string extendStatus;
            public string channel;
            public double cost;
            public long dateCreate;
            public long dateSend;
        }
    }
}