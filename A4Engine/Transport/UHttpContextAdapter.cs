﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
 

namespace A4Engine.Transport
{
    public class UHttpContextAdapter : IHttpContext
    {
        uhttpsharp.IHttpContext uhttpContext;
        IHttpRequest request;

        public IHttpRequest Request { get; set; }
        public IHttpResponse Response { get; set; }
        public CookieManager Cookies
        {
            get; private set;
        }

        public UHttpContextAdapter(uhttpsharp.IHttpContext uhttpContext)
        {
            this.uhttpContext = uhttpContext;
            Request = new UHttpRequestAdapter(this, uhttpContext, uhttpContext.Request);
            Response = new UHttpResponseAdapter(this, uhttpContext, uhttpContext);
            Cookies = new CookieManager(uhttpContext.Cookies);
        }
 
        public IAsyncResult BeginGetClientCertificate(AsyncCallback requestCallback, object state)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 EndGetClientCertificate(IAsyncResult asyncResult)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 GetClientCertificate()
        {
            throw new NotImplementedException();
        }

        public Task<X509Certificate2> GetClientCertificateAsync()
        {
            throw new NotImplementedException();
        }
    }
}
