﻿using AVISNetworkCommon.Utility;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AVISNetworkEngineTools.Modules.Payment.PaymentSystem
{
    public class PayMasterPaymentSystem : BasePaymentSystem
    {
        public string mrh_pass = "CfH37sprKyB0MiQRz96h";
 
        public string mrh_id = "36c82703-ec01-4992-bfae-2f8d478c2d69";
        public string mrh_currency = "RUB";
        //nonce - одноразовый тикет запроса. Это произвольная строка не более, чем из 255 символов, не содержащая символа ‘;’. Вам следует генерировать одноразовый тикет для каждого запроса. Это сделано для того, чтобы злоумышленник, даже перехватив однажды запрос, не смог его повторить.
        public enum PayMasterError
        {
            UnknownError = -1, //Неизвестная ошибка. Сбой в системе PayMaster. Если ошибка повторяется, обратитесь в техподдержку
            NetworkError = -2, //Сетевая ошибка. Сбой в системе PayMaster. Если ошибка повторяется, обратитесь в техподдержку
            AuthError = -6, //Нет доступа. Неверно указан логин, или у данного логина нет прав на запрошенную информацию
            UnknownHash = -7, //Неверная подпись запроса. Неверно сформирован хеш запроса
            SellerCancelPayment = -8, //Платеж отклонен продавцом на этапе предзапроса.
            InvoiceExpired = -9, //Истек срок действия счета
            PaySystemCancelPayment = -10, //Платеж отклонен платежной системой
            PaymentNotFound = -13, //Платеж не найден по номеру счета
            RepeatedRequestWithSameID = -14, //Повторный запрос с тем же nonce
            PaymentExpired = -15, //Срок жизни платежа истек, и платеж отменен
            UserCanceledPay = -17, //Пользователь отказался от оплаты
            InvalidAmountValue = -18, //Неверное значение суммы (в случае возвратов имеется в виду значение amount)
            UserInsufficientFunds = -19, //У пользователя недостаточно средств для оплаты
            FailedAuthInPaymentSystem = -22, //	Не удалось авторизоваться в платежной системе
            Error3dSecure = -25, //3D Secure авторизация не пройдена
            InvalidCardNumber = -26, //Неверный номер карты
            CardExpired = -27, //Истек срок действия карты
            CardBlocked = -28, //Карта блокировна
            PaymentLimitExceeded = -29, //Превышен лимит по сумме платежа
            PayCountLimitExceeded = -30, //Превышен лимит по количеству платежей
            InvalidCardOperation = -31, //Недопустимая операция по карте
        }
        public enum PaymsterPayStatus
        {
            INITIATED = 0, // Платеж начат
            PROCESSING = 1, // Платеж проводится
            COMPLETE = 2, // Платеж завершен успешно
            CANCELLED = 3, // Платеж завершен неуспешно
            HOLD = 4 //Средства захолдированы
        }
        public enum PaymsterRefoundStatus
        {
            PENDING = 0, // Поставлен в очередь на совершение операции возврата
            EXECUTING = 1, // Проведение транзакции возврата платежа
            SUCCESS = 2, // Возврат средств успешно завершен
            FAILURE = 3 // Возврат средств не осуществлен
        }
 
        public static string Base64Encode(byte[] plainText)
        {
            return Convert.ToBase64String(plainText);
        }
        string GetSIGN(string merchant_id, string order_id, string paid_amount, string lmi_currency, string secret_key)
        {
            string str = merchant_id + order_id + paid_amount + lmi_currency + secret_key;
            logger.Info("str {0}", str.ToString());
            return Base64Encode(Crypt.EncryptMD5Bin(Encoding.ASCII.GetBytes(str)));
        }
        string GetHash(string plain_string)
        {
            return Base64Encode(Crypt.EncryptMD5Bin(Encoding.ASCII.GetBytes(plain_string)));
        }
        
        public override string RedirectLink(RequestContext Context)
        {
            var inv_desc = "Приобритение игровых рублей";
            string out_summ = GetOrderTotal(Context.getParams.Get("amount"));
            long characterId = Convert.ToInt64(Context.getParams.Get("characterid"));
            string sing = GetSIGN(mrh_id, "", out_summ, mrh_currency, mrh_pass);

            StringBuilder stringBuilder = new StringBuilder("https://paymaster.ru/payment/init?");
            stringBuilder.AppendFormat("{0}={1}&", "LMI_MERCHANT_ID", HttpUtility.UrlEncode(mrh_id));
            stringBuilder.AppendFormat("{0}={1}&", "LMI_PAYMENT_AMOUNT", out_summ);
            stringBuilder.AppendFormat("{0}={1}&", "LMI_CURRENCY", mrh_currency);
          //  stringBuilder.AppendFormat("{0}={1}&", "LMI_PAYMENT_NO", "");
            stringBuilder.AppendFormat("{0}={1}&", "LMI_PAYMENT_DESC", HttpUtility.UrlEncode(inv_desc));
            if (isTest) stringBuilder.AppendFormat("{0}={1}&", "LMI_SIM_MODE", 0);
            stringBuilder.AppendFormat("{0}={1}&", "CHARACTER_ID", characterId);
            stringBuilder.AppendFormat("{0}={1}", "SIGN", HttpUtility.UrlEncode(sing));
 
            return stringBuilder.ToString();
        }
        public static IDictionary<string, string> ToDictionary(NameValueCollection col)
        {
            return col.AllKeys.ToDictionary(x => x, x => col[x]);
        }
        public override bool RequestAuthentication(RequestContext Context)
        {
            string lmi_hash = Context.postParams["LMI_HASH"];
            string lmi_sign = Context.postParams["SIGN"];
            string payment_amount = Context.postParams["LMI_PAYMENT_AMOUNT"];
            string paid_amount = Context.postParams["LMI_PAID_AMOUNT"];
            string lmi_currency = Context.postParams["LMI_CURRENCY"];
            string lmi_payment_no = Context.postParams["LMI_PAYMENT_NO"];
            if (string.IsNullOrEmpty(lmi_payment_no))
                lmi_payment_no = "";
            StringBuilder planned_string = new StringBuilder();
            planned_string.AppendFormat("{0};", mrh_id);
            planned_string.AppendFormat("{0};", lmi_payment_no);
            planned_string.AppendFormat("{0};", Context.postParams["LMI_SYS_PAYMENT_ID"]);
            planned_string.AppendFormat("{0};", Context.postParams["LMI_SYS_PAYMENT_DATE"]);
            planned_string.AppendFormat("{0};", payment_amount);
            planned_string.AppendFormat("{0};", lmi_currency);
            planned_string.AppendFormat("{0};", paid_amount);
            planned_string.AppendFormat("{0};", Context.postParams["LMI_PAID_CURRENCY"]);
            planned_string.AppendFormat("{0};", Context.postParams["LMI_PAYMENT_SYSTEM"]);
            planned_string.AppendFormat("{0};", Context.postParams["LMI_SIM_MODE"]);
            planned_string.AppendFormat("{0}", mrh_pass);

            logger.Info("----------RequestAuthentication------------");
            logger.Info("planned_string {0}", planned_string.ToString());

            string calculated_hash = GetHash(planned_string.ToString());
            string calculated_sing = GetSIGN(mrh_id, lmi_payment_no, paid_amount, lmi_currency, mrh_pass);
 
            logger.Info("hash {0} == {1}", calculated_hash, lmi_hash);
            logger.Info("sign {0} == {1}", calculated_sing, lmi_sign); 
            logger.Info("-----RequestContext ", string.Join(";", ToDictionary(Context.postParams)));
            return calculated_hash == lmi_hash && calculated_sing == lmi_sign;
        }

        public override PaymentContext GetPaymentContext(RequestContext requestContext)
        {
            string out_summ = requestContext.postParams.Get("LMI_PAID_AMOUNT");
            string character_id = requestContext.postParams.Get("CHARACTER_ID");
            string order_id = requestContext.postParams.Get("LMI_SYS_PAYMENT_ID");
 
            return new PaymentContext() { 
                Amount = Convert.ToInt64(ReadFloat(out_summ)),
                CharacterId = Convert.ToInt64(character_id),
                OrderId = order_id,
                PaymentSystem = BasePaymentSystem.PaymentSystemType
            };
        }

        public override bool TestRequest(RequestContext requestContext)
        {
            string LMI_SIM_MODE = requestContext.postParams["LMI_SIM_MODE"];
            return string.IsNullOrWhiteSpace(LMI_SIM_MODE) || isTest;
        }

        public override bool ClientSuccess(RequestContext requestContext)
        {
            return RequestAuthentication(requestContext);
        }

        public override string ExchangeRates(RequestContext context)
        {
            throw new NotImplementedException();
        }

        public override float GetPayAmount(string gameAmount)
        {
            throw new NotImplementedException();
        }
    }
}
