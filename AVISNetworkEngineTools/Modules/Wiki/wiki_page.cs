﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Wiki
{
    public class wiki_page : Module
    {
        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            string page = Context.Path[3].ToString();
            string index = FileGetContents(DirModule + "/html/"+ page + ".html");
            Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
        }
    }
}
