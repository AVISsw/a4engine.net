﻿using AVISNetworkCommon;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using A4Engine.Controllers;
using A4Engine.Modules.Interface;
using A4Engine;
using System.Net;
using A4Engine.Transport;
using System.Threading.Tasks;

public class Router
{
    public Dictionary<string, BaseController> Route = new Dictionary<string, BaseController>();
    public Dictionary<string, A4Engine.Modules.Module> Modules = new Dictionary<string, A4Engine.Modules.Module>();
    public Dictionary<string, A4Engine.Modules.Handler> Handlers = new Dictionary<string, A4Engine.Modules.Handler>();
    private List<Assembly> assemblies = new List<Assembly>();
    public static Logger Log;

    public bool IsRequereSecureConnection { get; internal set; }

    public Router()
    {
        Log = new Logger(Logger.LoggerType.WriteConsole);
        assemblies = GetListOfEntryAssemblyWithReferences(Assembly.GetExecutingAssembly());
        foreach (Assembly asm in LoadPlugins())
        {
            assemblies.Add(asm);
        }
        GetRouters(assemblies);
        GetModules(assemblies);
        GetHandlers(assemblies);
        foreach (var module in Modules)
        {
            try
            {
                module.Value.OnLoad();
            }
            catch (Exception e)
            {
                Log.Info(module.Key + " " + e.ToString());
            }
        }
        foreach (var module in Handlers)
        {
            try
            {
                module.Value.OnLoad();
            }
            catch (Exception e)
            {
                Log.Info(module.Key + " " + e.ToString());
            }
        }
        foreach (var controller in Route)
        {
            try
            {
                controller.Value.OnLoad();
            }
            catch (Exception e)
            {
                Log.Info(controller.Key + " " + e.ToString());
            }
        }
    }

    private List<Assembly> LoadPlugins()
    {

        List<Assembly> list = new List<Assembly>();

        foreach (string path in Settings.Server.Assembly)
        {
            Assembly pluginAssembly;
            pluginAssembly = Assembly.LoadFrom(path);
            list.Add(pluginAssembly);
            Log.Info(pluginAssembly.ToString());
        }
        return list;
    }

    public BaseController GetController(string key)
    {
        key = key.ToLower();
        if (Route.ContainsKey(key))
        {
            return Route[key];
        }
        else
        {
            return null;
        }
    }

    public A4Engine.Modules.Module GetModule(string key)
    {
        key = key.ToLower();
        if (Modules.ContainsKey(key))
        {
            return Modules[key];
        }
        else
        {
            return null;
        }
    }

    public A4Engine.Modules.Handler GetHandler(string key)
    {
        key = key.ToLower();
        if (Handlers.ContainsKey(key))
        {
            return Handlers[key];
        }
        else
        {
            return null;
        }
    }

    public void GetRouters(List<Assembly> assemblies)
    {
        foreach (Assembly assembly in assemblies)
        {
            var types = assembly.GetTypes();

            foreach (var type in types)
            {

                if (type.IsSubclassOf(typeof(BaseController)))
                {
                    ConstructorInfo ci = type.GetConstructor(new Type[] { });
                    BaseController controller = (BaseController)ci.Invoke(new object[] { });
                    Route.Add(controller.Name, controller);
                }
            }
        }
    }

    public void GetModules(List<Assembly> assemblies)
    {
        foreach (Assembly assembly in assemblies)
        {
            var types = assembly.GetTypes();
            if (assembly.ToString().IndexOf("AVISNetworkEngineTools") != -1)
            {

                Log.Info(assembly.ToString());
                Log.Info(types.Count().ToString());
            }
            foreach (var type in types)
            {

                if (type.IsSubclassOf(typeof(A4Engine.Modules.Module)))
                {
                    ConstructorInfo ci = type.GetConstructor(new Type[] { });
                    A4Engine.Modules.Module controller = (A4Engine.Modules.Module)ci.Invoke(new object[] { });
                    Log.Info(controller.Name);
                    Modules.Add(controller.Name, controller);

                }
            }
        }
    }

    public void GetHandlers(List<Assembly> assemblies)
    {
        foreach (Assembly assembly in assemblies)
        {
            var types = assembly.GetTypes();

            foreach (var type in types)
            {

                if (type.IsSubclassOf(typeof(A4Engine.Modules.Handler)))
                {
                    ConstructorInfo ci = type.GetConstructor(new Type[] { });
                    A4Engine.Modules.Handler controller = (A4Engine.Modules.Handler)ci.Invoke(new object[] { });
                    Handlers.Add(controller.Name, controller);
                    Log.Info(controller.Name);
                }
            }
        }
    }


    public List<Assembly> GetListOfEntryAssemblyWithReferences(Assembly projectAsm)
    {
        List<Assembly> listOfAssemblies = new List<Assembly>();

        listOfAssemblies.Add(projectAsm);

        foreach (var refAsmName in projectAsm.GetReferencedAssemblies())
        {
            listOfAssemblies.Add(Assembly.Load(refAsmName));
        }

        return listOfAssemblies;
    }

}

public class RequestContext
{
    public IHttpContext Request { get; set; }
    public NameValueCollection getParams { get; set; } = new NameValueCollection();
    public NameValueCollection postParams { get; set; } = new NameValueCollection();
    public Dictionary<string, string> moduleParams { get; set; } = new Dictionary<string, string>();
    public string rawParams { get; set; } = "";
    public string[] Path { get; set; }
    public Router Router { get; set; }
    public FormData FormData { get; private set; }

    public RequestContext()
    {

    }
    public async Task Init(IHttpContext Request, Router Router)
    {
        try
        {
            this.Router = Router;
            this.Request = Request;

            string originalUri = Request.Request.Url.OriginalString;
            string[] parsedUri = originalUri.Split("?");
            string pathStr = parsedUri[0];
            this.Path = pathStr.Split('/');

            string paramStr = string.Empty;
            if (parsedUri.Length > 1)
            {
                paramStr = parsedUri[1];
                getParams = HttpUtility.ParseQueryString(paramStr);
            }
            else
            {
                getParams = new NameValueCollection();
            }

            string contentType = Request.Request.ContentType;
            if (string.IsNullOrWhiteSpace(contentType))
            {
                contentType = "";
            }
            Console.WriteLine("-------contentType " + contentType);
            if (contentType.IndexOf("json") != -1) // json
            {
                var buffer = await GetPostData(Request.Request.InputStream, Request.Request.Headers);
                this.rawParams = Request.Request.ContentEncoding.GetString(buffer);
            }
            else if ((contentType.IndexOf("multipart/form-data") != -1) && contentType.IndexOf("application/x-www-form") == -1) // multipart
            {
                FormData = new FormData(Request.Request);
            }
            else
            {
                var buffer = await GetPostData(Request.Request.InputStream, Request.Request.Headers);
                this.rawParams = Request.Request.ContentEncoding.GetString(buffer);
                if (this.rawParams.Length > 0)
                {
                    NameValueCollection data = HttpUtility.ParseQueryString(this.rawParams);
                    foreach (var key in data.AllKeys)
                    {
                        var value = data[key];
                        this.postParams.Add(key, value);
                    }
                }
            }
            Console.WriteLine("rawParams " + this.rawParams);
            Console.WriteLine("uri " + Request.Request.Url.OriginalString);
            if (Router.IsRequereSecureConnection && Request.Request.IsLocal == false && Request.Request.IsSecureConnection == false)
            {
                Redirect(Request.Request.Url.OriginalString);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

    private static async Task<byte[]> GetPostData(Stream stream, NameValueCollection headers)
    {
 
        int postContentLength;
        string contentLength = headers.Get("content-length");
        postContentLength = Convert.ToInt32(contentLength);

        byte[] raw = new byte[postContentLength];

        try
        {
  
            if (!string.IsNullOrWhiteSpace(contentLength) && postContentLength > 0)
            {
                Console.WriteLine("read buffer " + postContentLength);

               int count = await stream.ReadAsync(raw, 0, postContentLength).ConfigureAwait(false);
                Console.WriteLine("raw readed  " + count);
            }
 
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        return raw;
 
    }
 
    public string RedirectHeader { get; private set; }
    public string ServerHost
    {
        get
        {
            string protocol = "http://";
            if ((Request.Request.IsSecureConnection || Router.IsRequereSecureConnection) && Request.Request.IsLocal == false)
            {
                protocol = "https://";
            }
            return protocol + Request.Request.UserHostName;
        }
    }
    public void Redirect(string uri, bool absoulute = false)
    {

        if (absoulute == false)
        {
            RedirectHeader = ServerHost + uri;
            Router.Log.Info("Redirect "+ RedirectHeader);

        }
        else
        {
            RedirectHeader = uri;
            Router.Log.Info("Redirect " + uri);
        }
    }
}

