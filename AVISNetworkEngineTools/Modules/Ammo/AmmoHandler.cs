﻿using A4Engine.Modules.ItemEditor;
using AVISNetworkCommon;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace A4Engine.Modules.Ammo
{
    public class AmmoHandler : Handler
    {
        Logger Log;
        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
           
        }

        public override Task Before(RequestContext Context)
        {
              return Start(Context);
        }


        public async Task<long> AddAmmo(NameValueCollection data)
        {
            long lastId = -1;
            try
            {
                lastId = await Controller.Storage.Add.In(Controller.GetObjectByParams<ProjectileData>(data));
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return lastId;
        }

        public async Task<bool> UpdateAmmo(int id, NameValueCollection data)
        {
            bool isAdd = false;
            try
            {
                foreach(var row in data.AllKeys)
                {
                    Console.WriteLine(row+"="+data[row]);
                }
                isAdd = await Controller.Storage.Update.Where(Controller.GetObjectByParams<ProjectileData>(data), " Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }
        public async Task<bool> RemoveAmmo(int id)
        {
            bool isAdd = false;
            try
            {

                isAdd = await Controller.Storage.Remove.Where<ProjectileData>(" Id={0}", id);
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return isAdd;
        }

        public async Task<List<ItemData>> GetItemsByType()
        {
            List<ItemData> result = new List<ItemData>();

            using (var data = await Controller.Storage.GetAll.WhereBase<ItemData>("IntType={0} OR IntType={1}", (int)ItemType.Ammo, (int)ItemType.Grenade))
            {
                while (data.Read)
                {
                    result.Add(data.GetObject<ItemData>());
                }
            }
            return result;
        }

        public async Task<List<ProjectileData>> GetList()
        {
            List<ProjectileData> result = new List<ProjectileData>();

            try
            {
                using (var data = await Controller.Storage.GetAll.WhereBase<ProjectileData>(""))
                {
                    while (data.Read)
                    {
                        result.Add(data.GetObject<ProjectileData>());
                    }
                }
            }
            catch (Exception e)
            {
                Log.Info(e.ToString());
            }
            return result;
        }
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            int id = 0;
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {

                    switch (key)
                    {
                        case "add":
                            Draw(JsonConvert.SerializeObject(
                             new Dictionary<string, object>() { { "Id",await this.AddAmmo(Context.postParams) } }
                                ));
                            break;
                        case "update":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.UpdateAmmo(id, Context.postParams)));
                            break;
                        case "remove":
                            id = Convert.ToInt32(Context.Path[4]);
                            Draw(JsonConvert.SerializeObject(await this.RemoveAmmo(id)));
                            break;
                        case "getlist":
                            Draw(JsonConvert.SerializeObject(await this.GetList()));
                            break;
                        case "getitems":
                            Draw(JsonConvert.SerializeObject(await this.GetItemsByType()));
                            break;
                        default:
                            Draw(JsonConvert.SerializeObject(
                                new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                            }
                            ));
                            break;
                    }
                    return;
                }
            }
            Draw("<b>Доступ запрещен!</b>");
        }

    }
}