 


var modalModule;
var editorModule;
var SelecElement;
var ItemList = [];

var Hashes = {};
var ListHashes = {};
var TradeListData = {
    Type: -1,
    Title: "",
    ItemName: "", Items: {}
};
var counter = -1;


 
var tradeListModule;
let TradeListId = 1;

function UpdateRender(callback) {
    postRequestData("/handler/TradeListHandler/get?TradeListId=" + TradeListId, {}, function (requestDataTradeList) {
        for (let index in requestDataTradeList) {
            let element = requestDataTradeList[index];
            element.Index = index;
            element.Item = ItemList[element.ItemId];
            
        }
        TradeListData.Items = requestDataTradeList;
        console.log(TradeListData.Items);
        Hashes = СalculateHashes(TradeListData.Items);
        if(callback)
        callback(requestDataTradeList);
    });
}

$(window).on('load', function () {

    jsonRequestData("/handler/TradeListHandler/get-list", {}, function (requestDataItemList) {
        for (let index in requestDataItemList) {
            let element = requestDataItemList[index];
            element.Index = index;
        }
        ListHashes = СalculateHashes(requestDataItemList);
        console.log(ListHashes);
        tradeListModule = new Vue({
            el: '#tradeListModule',
            data: {
                Type: -1,
                Title: "",
                ItemName: "",
                Items: requestDataItemList
            },
            methods: {
                SelectElement(index, item) {
                    TradeListId = index;
                    UpdateRender();
                },
                RemoveElement(index, item) {
                    console.log(this.Items);
                    console.log(item.Id);
                    Vue.delete(this.Items, item.Index);
                },
                AddElement() {
                    let element = { ItemId: 0, Name: "" };
                    element.Index = counter--;
                    Vue.set(this.Items, element.Index, element);
                }, SaveAll() {
                    let modifiedItems = [];
                    console.log(ListHashes);
                    console.log(this.Items);
                    for(let index in this.Items) {
                        let element = this.Items[index];
                        let lastElement = ListHashes[element.Index];
                        let Hash = md5(JSON.stringify(element));

                        if (lastElement) {
                            if (lastElement.Hash != Hash) {

                                modifiedItems.push({ Type: "update", Element: element });
                            }
                        } else {
                            modifiedItems.push({ Type: "add", Element: element });
                        }
                        delete ListHashes[index];
                    }
                    for (let index in ListHashes) {
                        let element = ListHashes[index];
                        modifiedItems.push({ Type: "remove", Element: element });
                    }
                    ListHashes = СalculateHashes(this.Items);
                    //   console.log(modifiedItems);

                    jsonRequestData("/handler/TradeListHandler/update-list", modifiedItems, function (requestDataItemList) {
                        console.log(requestDataItemList);
                    });
                }
            },
            computed: {
                filteredList: function () {
                    var name = this.ItemName;
                    let result = Object.values(this.Items).filter(function (elem) {
                        if (name === '') return true;
                        else return elem.Name.indexOf(name) > -1;
                    });
                    return result;
                }
            }
        });

    });
    jsonRequestData("/handler/TradeListHandler/get-item-list", {}, function (requestDataItemList) {
        ItemList = requestDataItemList;
        modalModule = new Vue({
            el: '#modalModule',
            data: {
                Type: -1,
                Title: "",
                ItemName: "",
                Items: requestDataItemList
            },
            methods: {
                SelectDialog(item) {
                    SelecElement.Item = item;
                    SelecElement.ItemId = item.ItemId;
                    modalModule.$data.Type = -1;
                },
                Close() {
                    modalModule.$data.Type = -1;
                }
            },
            computed: {
                filteredList: function () {
                    var name = this.ItemName;
                    let result = Object.values(this.Items).filter(function (elem) {
                        console.log(elem);
                        if (name === '') return true;
                        else return elem.Name.indexOf(name) > -1;
                    });

                    return result;
                }
            }
        });
 
        UpdateRender(function(requestDataTradeList){
 
            editorModule = new Vue({
                el: '#TradeList',
                data: TradeListData,
                methods: {
                    RemoveItem(index, element) {
                        console.log(element);
                        Vue.delete(TradeListData.Items, element.Index);
                    },
                    SelectItem(TradeListElement) {
                        console.log("select window");
                        modalModule.$data.Type = 1;
                        SelecElement = TradeListElement;
                    },
                    AddItem() {
                        let element = { ItemId: 0, Id: 0, Item: {}, Currency: 0, TradeListId: TradeListId };
                        element.Index = counter--;
                        Vue.set(this.Items, element.Index, element);
                    }, SaveAll() {
                        let modifiedItems = [];
                        for (let index in TradeListData.Items) {
                            let element = TradeListData.Items[index];
                            let lastElement = Hashes[element.Index];
                            let Hash = md5(JSON.stringify(element));

                            if (lastElement) {
                                if (lastElement.Hash != Hash) {

                                    modifiedItems.push({ Type: "update", Element: element });
                                }
                            } else {
                                modifiedItems.push({ Type: "add", Element: element });
                            }
                            delete Hashes[index];
                        }
                        for (let index in Hashes) {
                            let element = Hashes[index];
                            modifiedItems.push({ Type: "remove", Element: element });
                        }
                        Hashes = СalculateHashes(TradeListData.Items);
                        console.log(modifiedItems);

                        jsonRequestData("/handler/TradeListHandler/update-element", modifiedItems, function (requestDataItemList) {
                            console.log(requestDataItemList);
                        });
                    }
                }
            });
        });

    });
});