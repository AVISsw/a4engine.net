﻿using A4Engine.Transport;
using A4Engine.User;
using AVISNetworkEngineORM.DB;using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace A4Engine.Controllers.Interface
{
    public interface IController
    {
        Storage Storage { get; set; }
        Task Init(RequestContext Context, SessionManager sessionManager);
        void Draw(string value);
        void Draw(string value, HttpResponseCode code);
        Task Render(RequestContext Context);
    }
}
