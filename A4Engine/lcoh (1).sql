-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: lcoh
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `accounts` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `GroupId` int(255) NOT NULL DEFAULT '1',
  `Session` varchar(255) DEFAULT NULL,
  `RegIp` varchar(255) DEFAULT NULL,
  `LastIp` varchar(255) DEFAULT NULL,
  `RegMac` varchar(255) DEFAULT NULL,
  `LastMac` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'admin','f626cac9d093515f0cb0fc431c78e3c1','mail','Anton 386',777,'','','','',''),(20,'qwerty','7815696ecbf1c96e6894b779456d330e','ytykuykuy','jtyjty',1,'06399ffd03b6b83b7c5c5bcb0ac4d986',' ',' ',' ',' '),(22,'qwerty2','f626cac9d093515f0cb0fc431c78e3c1','strelokzi@mail.ru','Иванов Иван Иванович',0,NULL,' ',' ',' ',' '),(23,'asdfg3','f626cac9d093515f0cb0fc431c78e3c1','teefgergerg','fregtrgh',2,'37693cfc748049e45d87b8c7d8b9aacd',' ',' ',' ',' ');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ammo`
--

DROP TABLE IF EXISTS `ammo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ammo` (
  `instance_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `mesh` varchar(255) DEFAULT NULL,
  `item_type` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `box_count` int(11) DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ammo`
--

LOCK TABLES `ammo` WRITE;
/*!40000 ALTER TABLE `ammo` DISABLE KEYS */;
INSERT INTO `ammo` VALUES (4,'9x18','9x18',8,8,8),(8,'9x18','9x18',8,8,8),(9,'9x18','9x18',8,8,8);
/*!40000 ALTER TABLE `ammo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chance_spawn_items`
--

DROP TABLE IF EXISTS `chance_spawn_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chance_spawn_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemId` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  `chance` float DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chance_spawn_items`
--

LOCK TABLES `chance_spawn_items` WRITE;
/*!40000 ALTER TABLE `chance_spawn_items` DISABLE KEYS */;
INSERT INTO `chance_spawn_items` VALUES (1,1,1,3,0.3),(2,2,1,1,0.3),(3,3,1,3,0.3);
/*!40000 ALTER TABLE `chance_spawn_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characters`
--

DROP TABLE IF EXISTS `characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `characters` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeId` int(3) DEFAULT '0',
  `Name` varchar(255) NOT NULL DEFAULT '',
  `TransformId` int(11) DEFAULT '0',
  `CharacterId` int(11) DEFAULT NULL,
  `Account` int(255) NOT NULL,
  `Exp` int(255) DEFAULT '0',
  `Money` int(255) DEFAULT '0',
  `Skin` varchar(255) NOT NULL DEFAULT '',
  `Lvl` int(255) DEFAULT '1',
  `Desc` varchar(255) DEFAULT '',
  `Walk` float DEFAULT '0',
  `BagId` int(11) DEFAULT NULL,
  `EquipmentId` int(11) DEFAULT NULL,
  `Heal` float(11,0) DEFAULT '100',
  `Stamina` float(255,0) DEFAULT NULL,
  `Fatigue` float(255,0) DEFAULT NULL,
  `Hunger` float(255,0) DEFAULT NULL,
  `Thirst` float(255,0) DEFAULT NULL,
  `Radiation` float(255,0) DEFAULT NULL,
  `Mind` float(255,0) DEFAULT NULL,
  `Bleeding` float(255,0) DEFAULT NULL,
  `Temperature` float(255,0) DEFAULT NULL,
  `Run` float(255,0) DEFAULT NULL,
  `Sprint` float(255,0) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `transform_id` (`TransformId`),
  CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`TransformId`) REFERENCES `transform` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characters`
--

LOCK TABLES `characters` WRITE;
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` VALUES (1,1,'Boby',1,0,1,355,100,'Male_01',3,'Тут описание',3,1,2,25,100,100,100,100,0,0,0,37,2,4),(2,1,'Jary',1,0,1,3,3,'Male_01',1,'fff',3,1,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'Mary',1,0,1,3,3,'Male_01',2,'ffff',3,1,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'Lala',1,0,1,2,3333,'Male_01',6,'#fffffff',3,1,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,2,'Zveroboy',1,0,-1,355,3000,'Male_01',32,'Первый нпс',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,2,'Test',1,0,-1,222,222,'Male_01',3,'frfrfrrg',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,2,'fff',1,0,-1,3,3,'Male_01',3,'fefeff',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,2,'fff',1,0,-1,3,3,'Male_01',3,'fefeff',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,2,'fff',1,0,-1,3,3,'Male_01',3,'fefeff',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,3,'Bot',1,0,-1,3,100,'Male_01',1,'',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,2,'Bot',1,0,-1,3,100,'Male_01',1,'',4,3,2,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customization`
--

DROP TABLE IF EXISTS `customization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customization` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) DEFAULT NULL,
  `Mesh` varchar(255) DEFAULT NULL,
  `Material` varchar(255) DEFAULT NULL,
  `Slot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customization`
--

LOCK TABLES `customization` WRITE;
/*!40000 ALTER TABLE `customization` DISABLE KEYS */;
INSERT INTO `customization` VALUES (1,'',NULL,NULL,'RightHand'),(2,NULL,NULL,NULL,'RightHand'),(3,'Mesh','Weapon/Pistols/Handgun_M1911A_Black/Model/Handgun_M1911A_Black',NULL,'RightHand'),(4,'SkinnedMesh','Сlothes/Tors/Kurtka_01/Model/Kurtka_01',NULL,'Tors'),(5,'SkinnedMesh','Сlothes/Legs/Legs_01/Model/Legs_01',NULL,'Leg'),(6,'SkinnedMesh','Сlothes/Foot/Foot_01/Model/Foot_01',NULL,'Foot'),(7,'SkinnedMesh','Сlothes/Tors/Tors_02/Model/Tors_02',NULL,'Tors');
/*!40000 ALTER TABLE `customization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_category`
--

DROP TABLE IF EXISTS `dialog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_category` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT 'Новая категория',
  `path` varchar(2048) NOT NULL DEFAULT '/',
  `level` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_category`
--

LOCK TABLES `dialog_category` WRITE;
/*!40000 ALTER TABLE `dialog_category` DISABLE KEYS */;
INSERT INTO `dialog_category` VALUES (1,'story','/story',0);
/*!40000 ALTER TABLE `dialog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_events`
--

DROP TABLE IF EXISTS `dialog_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_events` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_events`
--

LOCK TABLES `dialog_events` WRITE;
/*!40000 ALTER TABLE `dialog_events` DISABLE KEYS */;
INSERT INTO `dialog_events` VALUES (1,'dialog_close');
/*!40000 ALTER TABLE `dialog_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_filters`
--

DROP TABLE IF EXISTS `dialog_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_filters`
--

LOCK TABLES `dialog_filters` WRITE;
/*!40000 ALTER TABLE `dialog_filters` DISABLE KEYS */;
INSERT INTO `dialog_filters` VALUES (1,'Возраст'),(2,'Имя'),(3,'Местный');
/*!40000 ALTER TABLE `dialog_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_filters_link`
--

DROP TABLE IF EXISTS `dialog_filters_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_filters_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `filter_name` varchar(50) NOT NULL,
  `filter_value` varchar(50) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_filters_link`
--

LOCK TABLES `dialog_filters_link` WRITE;
/*!40000 ALTER TABLE `dialog_filters_link` DISABLE KEYS */;
INSERT INTO `dialog_filters_link` VALUES (1,1,'Возраст','65','npc'),(2,1,'Имя','Боб','npc'),(3,1,'Местный','Да','npc'),(4,1,'Возраст','65','pl'),(5,1,'Имя','Боб','pl'),(6,1,'Местный','Да','pl'),(7,2,'Возраст','65','npc'),(8,3,'Respect','1','npc'),(9,2,'Respect','1','pl'),(10,2,'Возраст','65','pl'),(11,4,'Who','1','npc'),(12,2,'Who','1','pl'),(13,3,'Respect','1','pl');
/*!40000 ALTER TABLE `dialog_filters_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_npc_answers`
--

DROP TABLE IF EXISTS `dialog_npc_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_npc_answers` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_npc_answers`
--

LOCK TABLES `dialog_npc_answers` WRITE;
/*!40000 ALTER TABLE `dialog_npc_answers` DISABLE KEYS */;
INSERT INTO `dialog_npc_answers` VALUES (1,'story','Привет меня зовут {name}, мне {age} лет, я живу тут всю свою жизнь'),(2,'story','Привет, я {name} мне  {age} лет'),(3,'story','Ой спасибо чувак, я думаю ты тоже очень классны!'),(4,'story','Сижу пержу, а вообще у нас тут тихо ; )');
/*!40000 ALTER TABLE `dialog_npc_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dialog_player_answers`
--

DROP TABLE IF EXISTS `dialog_player_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dialog_player_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT '' COMMENT '...',
  `event` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dialog_player_answers`
--

LOCK TABLES `dialog_player_answers` WRITE;
/*!40000 ALTER TABLE `dialog_player_answers` DISABLE KEYS */;
INSERT INTO `dialog_player_answers` VALUES (1,'story','Ты очень крутой чувак','say','{key:\"Respect\",value:\"1\"}'),(2,'story','Пока...','bye',' '),(3,'story','Как тут у вас?','say','{key:\"Who\",value:\"1\"}');
/*!40000 ALTER TABLE `dialog_player_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grouprights`
--

DROP TABLE IF EXISTS `grouprights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grouprights` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Right` varchar(255) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grouprights`
--

LOCK TABLES `grouprights` WRITE;
/*!40000 ALTER TABLE `grouprights` DISABLE KEYS */;
INSERT INTO `grouprights` VALUES (1,'AdminCmd','Команда администратора'),(2,'AllUserCmd','Команда для всех пользователей');
/*!40000 ALTER TABLE `grouprights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groups` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'All'),(2,'Admins');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_continers`
--

DROP TABLE IF EXISTS `item_continers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_continers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `tileX` int(11) DEFAULT '30',
  `tileY` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_continers`
--

LOCK TABLES `item_continers` WRITE;
/*!40000 ALTER TABLE `item_continers` DISABLE KEYS */;
INSERT INTO `item_continers` VALUES (1,'Инвентарь',0,5,6),(2,'Экипировка',1,10,0),(3,'Лут',2,5,5),(4,'Рандом лут',2,5,6);
/*!40000 ALTER TABLE `item_continers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_template`
--

DROP TABLE IF EXISTS `item_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `continer_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `count` int(11) DEFAULT '1',
  `value` float DEFAULT '0',
  `int_type` int(11) DEFAULT '0',
  `is_equipment` tinyint(4) DEFAULT '0',
  `slot` int(11) DEFAULT '0',
  `desc` varchar(255) DEFAULT '',
  `icon` varchar(255) DEFAULT NULL,
  `customization_id` int(11) DEFAULT NULL,
  `is_infinite` tinyint(1) DEFAULT NULL,
  `sizeX` int(11) DEFAULT '1',
  `sizeY` int(11) DEFAULT '1',
  `slotX` int(11) DEFAULT '0',
  `slotY` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_template`
--

LOCK TABLES `item_template` WRITE;
/*!40000 ALTER TABLE `item_template` DISABLE KEYS */;
INSERT INTO `item_template` VALUES (2,1,'Колбаса',1,66,0,0,0,'Кусок старой колбасы',NULL,NULL,NULL,1,1,0,0),(3,1,'Бронижелет',1,1,1,1,1,'Это бронижелет','Сlothes/Torso/Kurtka_01/Icon/Kurtka_01',4,NULL,1,1,0,0),(4,1,'Тестовое оружие',1,20,2,1,2,'Это тестовое оружие','Weapon/Pistols/Handgun_M1911A_Black/Icon/Handgun_M1911A_Black',NULL,NULL,1,1,0,0),(5,2,'Штаны',1,12,1,1,3,'Штаны','Сlothes/Legs/Legs_01/Icon/Legs_01',5,NULL,1,1,0,0),(6,1,'Ботинки',1,10,1,1,4,'Ботинки','Сlothes/Foot/Foot_01/Icon/Foot_01',6,NULL,1,1,0,0),(7,1,'Куртка',1,30,1,1,1,'Куртка','Сlothes/Tors/Tors_02/Icon/Tors_02',7,NULL,1,1,0,0),(8,0,'alexi',0,0,0,0,0,'',NULL,0,0,1,1,0,0),(9,0,'o2sss',0,0,0,0,0,'','',0,0,1,1,0,0);
/*!40000 ALTER TABLE `item_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `instance_id` int(11) NOT NULL AUTO_INCREMENT,
  `continer_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `count` int(11) DEFAULT '1',
  `value` float DEFAULT '0',
  `int_type` int(11) DEFAULT '0',
  `is_equipment` tinyint(4) DEFAULT '0',
  `slot` int(11) DEFAULT '0',
  `desc` varchar(255) DEFAULT '',
  `icon` varchar(255) DEFAULT NULL,
  `customization_id` int(11) DEFAULT NULL,
  `is_infinite` tinyint(1) DEFAULT '0',
  `sizeX` int(3) DEFAULT '1',
  `sizeY` int(3) DEFAULT '1',
  `slotX` int(3) DEFAULT '0',
  `slotY` int(3) DEFAULT '0',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,1,1,'Хлеб',1,20,0,0,0,'Это хлеб',NULL,NULL,0,1,1,0,0),(2,1,2,'Колбаса',1,66,0,0,0,'Кусок старой колбасы',NULL,NULL,0,1,1,0,1),(3,1,3,'Бронижелет',1,1,1,1,1,'Это бронижелет','Сlothes/Torso/Kurtka_01/Icon/Kurtka_01',4,1,1,1,0,2),(4,1,4,'Тестовое оружие',1,20,2,1,2,'Это тестовое оружие','Weapon/Pistols/Handgun_M1911A_Black/Icon/Handgun_M1911A_Black',NULL,1,1,1,0,3),(5,2,5,'Штаны',1,12,1,1,3,'Штаны','Сlothes/Legs/Legs_01/Icon/Legs_01',5,1,1,1,0,4),(6,1,6,'Ботинки',1,10,1,1,4,'Ботинки','Сlothes/Foot/Foot_01/Icon/Foot_01',6,1,1,1,1,0),(7,1,7,'Куртка',1,30,1,1,1,'Куртка','Сlothes/Tors/Tors_02/Icon/Tors_02',7,1,2,3,2,2),(8,1,8,'Патроны для пистолета',1,8,3,0,0,'Патроны',NULL,NULL,1,1,1,1,2),(9,3,8,'Патроны',1,8,3,0,0,'Патроны',NULL,NULL,0,1,1,1,3);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) NOT NULL,
  `linkid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` VALUES (1,'menu1','Главная','/main','link.html',NULL),(5,'menu1','Авторизироваться','#login_dialog','link.html','login'),(7,'private','Главная','/main','link.html',NULL),(8,'private','Изменить пароль','#reset_password','link.html','reset_password'),(17,'menu1','Регистрация','/registration','link.html','');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mkey` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'menu1','menu.html','main'),(2,'private','menu.html','private'),(3,'menu1','menu.html','post_id'),(4,'menu1','menu.html','editor'),(5,'menu1','menu.html','feedback'),(6,'menu1','menu.html','catalog'),(7,'menu1','menu.html','catalog_id'),(8,'menu1','menu.html','finder');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pages` (
  `Id` int(3) NOT NULL AUTO_INCREMENT,
  `Key` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Pattern` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (5,'main','Главная','index.html'),(6,'npc_answers','Ответы нпс','npc_answers.html'),(7,'private','Личный кабинет','private.html'),(8,'editor','Редактирование','editor.html'),(9,'feedback','feedback','feedback.html'),(10,'catalog','Каталог','catalog.html'),(11,'catalog_id','Резюме','catalog_page.html'),(12,'finder','Поиск','finder.html'),(13,'registration','Регистрация','registration.html'),(14,'regsektion','Запись на кружок','regsektion.html');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scene`
--

DROP TABLE IF EXISTS `scene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '0',
  `transformid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scene`
--

LOCK TABLES `scene` WRITE;
/*!40000 ALTER TABLE `scene` DISABLE KEYS */;
INSERT INTO `scene` VALUES (1,0,1);
/*!40000 ALTER TABLE `scene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transform`
--

DROP TABLE IF EXISTS `transform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scene` varchar(50) DEFAULT NULL,
  `position` varchar(150) DEFAULT '{"x":0.0,"y":0.0,"z":0.0}',
  `rotation` varchar(150) DEFAULT '{"x":0.0,"y":0.0,"z":0.0,"w":0.0}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transform`
--

LOCK TABLES `transform` WRITE;
/*!40000 ALTER TABLE `transform` DISABLE KEYS */;
INSERT INTO `transform` VALUES (1,'MainWorld','{\"x\":0.0,\"y\":0.0,\"z\":0.0}','{\"x\":0.0,\"y\":0.0,\"z\":0.0}');
/*!40000 ALTER TABLE `transform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useringroup`
--

DROP TABLE IF EXISTS `useringroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `useringroup` (
  `GroupId` int(11) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`GroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useringroup`
--

LOCK TABLES `useringroup` WRITE;
/*!40000 ALTER TABLE `useringroup` DISABLE KEYS */;
INSERT INTO `useringroup` VALUES (1,1);
/*!40000 ALTER TABLE `useringroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weapons`
--

DROP TABLE IF EXISTS `weapons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `weapons` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Delay` float(9,0) DEFAULT '0',
  `Speed` float(9,0) DEFAULT '0',
  `AmmoItemId` int(11) DEFAULT '0',
  `AmmoInstateId` int(11) DEFAULT NULL,
  `Value` int(11) DEFAULT '0',
  `MuzzleFlash` varchar(255) DEFAULT '',
  `Type` int(6) DEFAULT NULL,
  `ItemId` int(11) DEFAULT NULL,
  `CustomizationId` int(11) DEFAULT NULL,
  `Slot` varchar(255) DEFAULT 'RightHand',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weapons`
--

LOCK TABLES `weapons` WRITE;
/*!40000 ALTER TABLE `weapons` DISABLE KEYS */;
INSERT INTO `weapons` VALUES (3,1,1,NULL,NULL,-10,'',0,-1,2,'RightHand'),(4,1,1,NULL,NULL,-10,'',0,-2,1,'RightHand'),(5,1,120,8,7,-30,'PistolMuzzleFire',1,4,3,'RightHand');
/*!40000 ALTER TABLE `weapons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-14 20:46:38
