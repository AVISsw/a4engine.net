
var groupId = 0;
var moduleData = { 
    Groups:{},
    Rights:{},
    GroupRights:{}
};
$(window).on('load', function () {

    tradeListModule = new Vue({
        el: '#groupList',
        data: {
            Type: -1,
            Title: "",
            ItemName: "",
            moduleData: moduleData
        },
        methods: {
            SelectGroup(index, item) {
                groupId = item.Id;
                jsonRequestData("/handler/GroupRightHandler/get-group-rights", {GroupId:groupId}, function (requestDataItemList) {
                    console.log(requestDataItemList);
                    moduleData.GroupRights = requestDataItemList;
                });
            }
        },
        computed: {
            filteredList: function () {
                var name = this.ItemName;
                let result = Object.values(this.Items).filter(function (elem) {
                    if (name === '') return true;
                    else return elem.Name.indexOf(name) > -1;
                });
                return result;
            }
        }
    });

    var index = -1;
    tradeListModule = new Vue({
        el: '#rightList',
        data: {
            Type: -1,
            Title: "",
            RightItem: {RightData:{Alias:"Новое право", Right: "newRight", Id:-1}, Remove:false, Add:false},
            moduleData: moduleData
        },
        methods: {
            AddRight(item) {
                item.Add = true;
                Vue.set(moduleData.Rights, index, item);  
                index--;
            },RmRight(item, index) {
                item.Remove = true;
 
            },
            SaveAll() {
                jsonRequestData("/handler/GroupRightHandler/set-rights", {GroupId:groupId,GroupRights:null, Rights: Object.values(moduleData.Rights) }, function (requestDataItemList) {
                    console.log(requestDataItemList);
                    moduleData.Rights = requestDataItemList;
                    jsonRequestData("/handler/GroupRightHandler/get-group-rights", {GroupId:groupId}, function (requestDataItemList) {
                        console.log(requestDataItemList);
                        moduleData.GroupRights = requestDataItemList;
                    });
                });
 
            }
        },
        computed: {
            filteredList: function () {
                var name = this.ItemName;
                let result = Object.values(this.Items).filter(function (elem) {
                    if (name === '') return true;
                    else return elem.Name.indexOf(name) > -1;
                });
                return result;
            }
        }
    });
    var groupRights = new Vue({
        el: '#groupRights',
        data: {
            Type: -1,
            Title: "",
            ItemName: "",
            moduleData: moduleData
        },
        methods: {
            SelectElement(index, item) {
         
            },
            SaveAll() {
                jsonRequestData("/handler/GroupRightHandler/set-group-rights", {GroupId:groupId, Rights: null, GroupRights: Object.values(moduleData.GroupRights) }, function (requestDataItemList) {
                    console.log(requestDataItemList);
                    moduleData.GroupRights = requestDataItemList;
                });
            }
        },
        computed: {
            filteredList: function () {
                var name = this.ItemName;
                let result = Object.values(this.Items).filter(function (elem) {
                    if (name === '') return true;
                    else return elem.Name.indexOf(name) > -1;
                });
                return result;
            }
        }
    });

    jsonRequestData("/handler/GroupRightHandler/get-groups",  {GroupId:-1, Rights: null, GroupRights: null }, function (requestDataItemList) {
        console.log(requestDataItemList);
        moduleData.Groups = requestDataItemList;
    });
    jsonRequestData("/handler/GroupRightHandler/get-rights", {}, function (requestDataItemList) {
        console.log(requestDataItemList);
        moduleData.Rights = requestDataItemList;
    });
    jsonRequestData("/handler/GroupRightHandler/get-group-rights", {GroupId:groupId}, function (requestDataItemList) {
        console.log(requestDataItemList);
        moduleData.GroupRights = requestDataItemList;
    });
});