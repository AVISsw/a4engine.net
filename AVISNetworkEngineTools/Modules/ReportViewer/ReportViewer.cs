﻿using A4Engine.Modules;
using AVISNetworkCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.ReportViewer
{
    public class ReportViewer : Handler
    {
        Logger Log;
        public ReportViewer()
        {
 
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }

        public override Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
            return Task.FromResult(0);
        }

        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
         
            
            if (User != null)
            {
                switch (key)
                {
                    case "get-list":
                        Draw(JsonConvert.SerializeObject(await this.GetList()));
                        break;
                    case "get":
                        string hash = Context.Path[4];
                        Draw(JsonConvert.SerializeObject(await this.Get(hash)));
                        break;
                    default:
                        Draw(JsonConvert.SerializeObject(
                            new Dictionary<string, string> {
                        { "error","1"},
                        {"desc","не указан тип запроса" }
                        }
                        ));
                        break;
                }
                return;
            }
            Draw("<b>Доступ запрещен!</b>");  
        }
        Dictionary<string, ReportHeaderData> headers;
        private async Task<Dictionary<string,ReportHeaderData>> GetList()
        {
            headers = new Dictionary<string, ReportHeaderData>();
            var reportHeadersPath = Directory.GetFiles(UserReporting.userreporting.ReportPath, "report_header_data.json", SearchOption.AllDirectories);
            foreach(string path in reportHeadersPath)
            {
                if (File.Exists(path))
                {
                    try
                    {
                        string data = this.Controller.FileGetContents(path);
                        ReportHeaderData reportHeaderData = JsonConvert.DeserializeObject<ReportHeaderData>(data);
                        if(reportHeaderData == null)
                        {
                            throw new Exception("reportHeaderData == null path:"+ path);
                        }
                        reportHeaderData.Path = path.Replace("\\report_header_data.json", "");
                        bool isShow = false;

                        switch (reportHeaderData.Type)
                        {
                            case ReportType.Bug:
                                isShow = User.GroupRight.Can("ViewBug");
                                break;
                            case ReportType.PerformanceIssue:
                                isShow = User.GroupRight.Can("ViewPerformanceIssue");
                                break;
                            case ReportType.ComplaintsToTheAdministration:
                                isShow = User.GroupRight.Can("ViewComplaintsToTheAdministration");
                                break;
                            case ReportType.PlayerComplaint:
                                isShow = User.GroupRight.Can("ViewPlayerComplaint");
                                break;
                            case ReportType.Suggestions:
                                isShow = User.GroupRight.Can("ViewSuggestions");
                                break;
                        }

 
                        if (isShow)
                        {
                            try
                            {
                                headers.Add(reportHeaderData.Hash, reportHeaderData);
                            }
                            catch (Exception ex)
                            {
                                reportHeaderData.Hash = reportHeaderData.Hash + "1";
                                headers.Add(reportHeaderData.Hash, reportHeaderData);
                            }
                        }
                    }catch(Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("header not exist "+ path);
                }
            }
            return headers;
        }

        private async Task<ReportData> Get(string hash)
        {
            ReportData reportData = new ReportData();
 
            Console.WriteLine("GetReport " + hash);
            if (headers.TryGetValue(hash, out reportData.HeaderData))
            {
                bool isShow = false;

                switch (reportData.HeaderData.Type)
                {
                    case ReportType.Bug:
                        isShow = User.GroupRight.Can("ViewBug");
                        break;
                    case ReportType.PerformanceIssue:
                        isShow = User.GroupRight.Can("ViewPerformanceIssue");
                        break;
                    case ReportType.ComplaintsToTheAdministration:
                        isShow = User.GroupRight.Can("ViewComplaintsToTheAdministration");
                        break;
                    case ReportType.PlayerComplaint:
                        isShow = User.GroupRight.Can("ViewPlayerComplaint");
                        break;
                    case ReportType.Suggestions:
                        isShow = User.GroupRight.Can("ViewSuggestions");
                        break;
                }
                if(isShow == false)
                {
                    return new ReportData();
                }
                Console.WriteLine("header.Path " + reportData.HeaderData.Path);
                if (File.Exists(reportData.HeaderData.Path))
                {
                    try
                    {
                        string data = this.Controller.FileGetContents(reportData.HeaderData.Path);
                        reportData.DeviceMetadata = JsonConvert.DeserializeObject<List<UserReportNamedValue>>(data);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
              reportData.ScreanShots = Directory.GetFiles(reportData.HeaderData.Path, "*"+UserReporting.userreporting.ImageFormat, SearchOption.TopDirectoryOnly);
              Console.WriteLine(JsonConvert.SerializeObject(reportData.ScreanShots));
            }
            return reportData;
        }
 
        [Serializable]
        public class ReportHeaderData
        {
            public string Hash;
            public string Name;
            public string UserName;
            public DateTime Date;
            public string Description;
            public string Path;
            public ReportType Type;
        }
 
        [Serializable]
        public class ReportData
        {
            public ReportHeaderData HeaderData = new ReportHeaderData();
            public List<UserReportNamedValue> DeviceMetadata = new List<UserReportNamedValue>();
            public string Logs;
            public string[] ScreanShots;
        }
    }
}
