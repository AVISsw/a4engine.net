import { Field } from "./field.js";
import { Rect } from "./rect.js";

class EditorCanvas {
  constructor(width, height) {
    this.rect = new Rect("#1E1E1E", 0, 0, width, height);
    this.canvas = document.getElementById("canvas");
    this.nodes = []; //Массив всех нод
    this.field = new Field();
    this.selected = null;
  }
  Init() {
    this.context = this.canvas.getContext("2d");
    window.OnResize = this.SetActualScreenSize.bind(this);
    this.SetActualScreenSize();
  }
  Draw() {
    var oX = this.field.offsetX;
    var oY = this.field.offsetY;
    var squareSize = 35;

    var relativeX = (oX % squareSize) / 2;
    var relativeY = (oY % squareSize) / 2;

    var context = this.context;

    context.fillStyle = this.rect.color;
    context.fillRect(
      this.rect.x,
      this.rect.y,
      this.rect.width,
      this.rect.height
    );

    context.beginPath();
    context.lineWidth = 1;
    for (
      var x = relativeX - squareSize;
      x <= this.rect.width + squareSize;
      x += squareSize
    ) {
      context.moveTo(x + relativeX, 0);
      context.lineTo(x + relativeX, this.rect.height);
    }

    for (
      var y = relativeY - squareSize;
      y <= this.rect.height + squareSize;
      y += squareSize
    ) {
      context.moveTo(0, y + relativeY);
      context.lineTo(this.rect.width, y + relativeY);
    }

    context.strokeStyle = "#262626";
    context.stroke();

    squareSize *= 3;
    relativeX = (oX % squareSize) / 2;
    relativeY = (oY % squareSize) / 2;
    context.beginPath();

    for (
      x = relativeX - squareSize;
      x <= this.rect.width + squareSize;
      x += squareSize
    ) {
      context.moveTo(x + relativeX, 0);
      context.lineTo(x + relativeX, this.rect.height);
    }

    for (
      y = relativeY - squareSize;
      y <= this.rect.height + squareSize;
      y += squareSize
    ) {
      context.moveTo(0, y + relativeY);
      context.lineTo(this.rect.width, y + relativeY);
    }

    context.strokeStyle = "#282828";
    context.stroke();

    this.nodes.forEach(function (node) {
      node.Draw(oX, oY, context);
    });
  }
  AddNode(node) {
    this.nodes.push(node);
  }
  DeleteNode(nodeForDeleting) {
    //Убрать ноду из списка
    var index = this.nodes.indexOf(nodeForDeleting);
    if (index > -1) {
      this.nodes.splice(index, 1);
    }

    //Убрать все коннекшены родительских нод
    this.nodes.forEach(function (node) {
      index = node.connections.indexOf(nodeForDeleting);
      if (index > -1) {
        node.connections.splice(index, 1);
      }
    });

    //Проверить все дочерние ноды на наличие родителей помимо этой ноды.
    //  Если таковых нет, удалить и их этой же функцией
    nodeForDeleting.connections.forEach(
      function (connectedChild) {
        this.CheckNodeHasParent(connectedChild);
      }.bind(this)
    );
  }
  DeleteNodeConnection(node, connection_id) {
    for (var i = 0; i < node.connections.length; i++) {
      if (node.connections[i].id === connection_id) {
        var connectedNode = node.connections[i];
        node.connections.splice(i, 1);
        //Чек если у присоединенной ноды больше нет родителей
        this.CheckNodeHasParent(connectedNode);
      }
    }
  }
  UpNodeConnection(node, connection_id) {
    console.log(node.connections);
    var connectedNode;
    for (var i = 0; i < node.connections.length; i++) {
      if (node.connections[i].id === connection_id) {
        connectedNode = node.connections[i];
        break;
      }
    }
    if(connectedNode){
    moveUp(connectedNode, node.connections);
    }
    console.log(node.connections);
  }
  DownNodeConnection(node, connection_id) {
    console.log(node.connections);
    var connectedNode;
    for (var i = 0; i < node.connections.length; i++) {
      if (node.connections[i].id === connection_id) {
        connectedNode = node.connections[i];
        break;
      }
    }
    if(connectedNode){
      moveDown(connectedNode, node.connections);
    }
    console.log(node.connections);
  }
  CheckNodeHasParent(checkingNode) {
    var count = 0;
    this.nodes.forEach(function (node) {
      if (node.connections.indexOf(checkingNode) > -1) {
        count++;
      }
    });
    if (count < 1) {
      this.DeleteNode(checkingNode);
    }
  }
  CheckPointInsideNode(x, y) {
    var result = null;
    this.nodes.every((node) => {
      result = node.IsPointInside(x, y);

      return !result;
    });

    return result;
  }
  CheckPointOnNodeBorder(x, y) {
    var result = false;
    this.nodes.every((node) => {
      result = node.IsPointOnBorder(x, y);
      return !result;
    });

    return result;
  }
  CheckPointInsideCanvas(x, y) {
    return x >= 0 && x <= this.rect.width && y >= 0 && y <= this.rect.height;
  }
  SetActualScreenSize() {
    let parent = document.getElementById("canvas_parent");
    let parentRect = parent.getBoundingClientRect();
    this.canvas.width = this.rect.width = parentRect.width;
    this.canvas.height = this.rect.height = parentRect.height;
  }
}
function moveDown(val, array) {
  let index = array.indexOf(val);
  array.splice(index,1)
  array.splice(index+1, 0, val)
  return array;
}
function moveUp(val, array) {
  let index = array.indexOf(val);
  array.splice(index,1)
  array.splice(index-1, 0, val)
  return array;
}
export { EditorCanvas };
