﻿ 
using A4Engine.Modules;
using AVISNetworkCommon;
using AVISNetworkCommon.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.UserReporting
{
    public class SystemInfoRequest : Handler
    {
        public const string ReportPath = "asset/report/user";
        public const string ImageFormat = ".png";
        Logger Log;
        public override async Task Before(RequestContext Context)
        {
            Start(Context);
        }

        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }
        [Serializable]
        public class SystemInfoResponse
        {
            public UserReportScreenshot Screen;
            public string Name;
            public string Folder;
            public string Hash;
        }
        public override async Task Start(RequestContext Context)
        {
            var reportData = JsonConvert.DeserializeObject<SystemInfoResponse>(Context.rawParams);
 
            string userPath = A4Engine.Controllers.Asset.Folder + "/Screens/"+  reportData.Folder;
            string soult = "kjkszpj";
            string hash = Crypt.EncryptMD5(reportData.Name + reportData.Folder + soult);
            if (hash != reportData.Hash)
            {
                Log.Error("Попытка подделать данные SystemInfoRequest");
                return;
            }
            try
            {
                if (!Directory.Exists(userPath))
                {
                    Directory.CreateDirectory(userPath);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
 
            try
            {
                var bytes = Convert.FromBase64String(reportData.Screen.DataBase64);
                string filePath = userPath + "/" + reportData.Name + ImageFormat;
                Log.Info("Make screen " + filePath);
                using (var imageFile = new FileStream(filePath, FileMode.Create))
                {
                    imageFile.Write(bytes, 0, bytes.Length);
                    imageFile.Flush();
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            Draw("ok");
        }
    }
}
