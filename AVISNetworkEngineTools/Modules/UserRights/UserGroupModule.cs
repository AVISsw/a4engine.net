﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.UserRights
{
    public class UserGroupModule : Module
    {

        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            if (User != null)
            {
                if (User.GroupRight.Can("editItem"))
                {
                    long currentPage = 0;
                    try
                    {
                        string pageParam = Context.Path[3];
                        currentPage = Convert.ToInt32(pageParam);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    string index = FileGetContents(DirModule + "/form.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "currentPage", currentPage.ToString() } }));
                    return;
                }
            }
            Context.Redirect("/page/auth");
            Draw("<b>Доступ запрещен!</b>");
        }


    }
}
