using System;
using System.Dynamic;
using System.Net;
using uhttpsharp.Headers;

namespace uhttpsharp
{
    internal class HttpContext : IHttpContext
    {
        private readonly IHttpRequest _request;
        private readonly EndPoint _remoteEndPoint;
        private CookieCollection _cookies;
        private readonly ExpandoObject _state = new ExpandoObject();

        private static readonly string[] CookieSeparators = { "; ", "=" };
        public HttpContext(IHttpRequest request, EndPoint remoteEndPoint)
        {
            _request = request;
            _remoteEndPoint = remoteEndPoint;
            string cookie = request.Headers.Get("cookie");
            if (string.IsNullOrWhiteSpace(cookie))
            {
                cookie = string.Empty;
            }
            var keyValues = cookie.Split(CookieSeparators, StringSplitOptions.RemoveEmptyEntries);
            _cookies = new CookieCollection();

            for (int i = 0; i < keyValues.Length; i += 2)
            {
                var key = keyValues[i];
                var value = keyValues[i + 1];
                _cookies.Add(new Cookie(key, value));
            }
        }
 
        public IHttpRequest Request
        {
            get { return _request; }
        }

        public IHttpResponse Response { get; set; }
 
        public dynamic State
        {
            get { return _state; }
        }
        public EndPoint RemoteEndPoint
        {
            get { return _remoteEndPoint; }
        }

        public CookieCollection Cookies
        {
            get { return _cookies; }
            set { _cookies = value; }
        }

        public bool SecureConnection { get; set; }
    }
}