var EditorName = "scope";
var HandlerName = "scopeeditor";
var ItemType = 4;

function SaveItem(id) {
    var form = $("#" + id + "_item");
    var data = Serilaze(form);

    $.each(data, function (name, value) {
        console.log(name + '=' + value);
    });
    updateItemRequest(id, data);
}
function updateItemRequest(id, data) {
    $.post(
        "/handler/itemeditor/update/" + id,
        data,
        function (requestData) {
            $.post(
                "/handler/" + HandlerName + "/update/" + id,
                data,
                function (requestData2) { 
                    if(requestData == true && requestData2 == true){
                        ShowModal("Все ок", "Результат сохранения");
                    }else{
                        ShowModal("Ошибка при сохранении", "Результат сохранения");
                    }
                }
            );
        }
    );
   
}
function removeItemRequest(id) {
    $.post(
        "/handler/itemeditor/remove/" + id,
        {},
        function (requestData) {
            $.post(
                "/handler/ScopeEditor/remove/" + id,
                {},
                function (requestData2) {
                    if(requestData == true && requestData2 == true){
                        ShowModal("Все ок", "Результат сохранения");
                    }else{
                        ShowModal("Ошибка при сохранении", "Результат сохранения");
                    }
                    loadItemList();
                }
            );
        });
}
function addItemRequest() {
    $.post(
        "/handler/itemeditor/add/",
        { IntType: ItemType },
        function (data) {
            $.post(
                "/handler/ScopeEditor/add/",
                { ItemId: data },
                function () {
                    loadItemList();
                }
            );
        }
    );

}
function RemoveItem(id) {
    var descArea = $('#removeElement').html();
    descArea = fill_pattern(descArea, { ItemId: id });
    ShowModal(descArea, "Вы действительно хотите удалить?");
}
function loadItemList() {
    $.post(
        "/handler/" + HandlerName + "/getlist",
        {
        },
        function (data) {
            var scopeMechanicData = data;
            $.post(
                "/handler/" + HandlerName + "/getitems",
                {
                },
                function (data) {

                    if (Array.isArray(data) && Array.isArray(scopeMechanicData)) {
                        let tempData = MergeArrayObjectsByItemId(data, scopeMechanicData);
                        tempData.reverse();
                        component.renderTable(tempData, function(template,element){
                            var temprow = fill_pattern(template, element);
                            var divRow = $(temprow);
                            SetSelect(divRow.find('.TypeOfUse'), element.TypeOfUse);
                            SetSelect(divRow.find('.IntType'), element.IntType);
                            divRow.find('.CanSpoil').attr("checked", element.CanSpoil);
                            divRow.find('.IsInfinite').attr("checked", element.IsInfinite);
                            divRow.find('.Up').attr("checked", element.Up);
                            divRow.find('.Premium').attr("checked", element.Premium);
                            SetSelect(divRow.find('.Slot'), element.Slot);
                            return divRow[0].outerHTML;
                        });
                    }
                }
            );
        }
    );
}
var component;
$(document).ready(function () {
    let headerRowTemplate = "<tr>" + $("#headerRowTemplate").html() + $("#" + EditorName + "HeaderRowTemplate").html(); +"</tr>";
    let bodyRowTemplate = "<tr class=\"content\" id=\"{ItemId/}_item\" itemid=\"{ItemId/}\">" + $("#bodyRowTemplate").html() + $("#" + EditorName + "BodyRowTemplate").html() + "</tr>";
    component = new itemComponent(
        $("#table_workspace"),
        headerRowTemplate,
        bodyRowTemplate
    );
    loadItemList();
});
