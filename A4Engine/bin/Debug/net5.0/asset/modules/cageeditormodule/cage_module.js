$(document).ready(function () {
  
  var itemList = [];

  Vue.component('continer-component', {
    template: '#continer-component-template',
    props: ['item'],
    methods:{
      Update: function (item) {
        if( typeof(item["AmmoItemId"]) == "string")
        item["AmmoItemId"] = item["AmmoItemId"].split(",");
        console.log(item);
        jsonRequestData("/handler/CageEditorHandler/update", item, function (requestDataItemList) {
        });
      },
      Remove: function (item) {
 
        jsonRequestData("/handler/CageEditorHandler/remove", item , function (requestDataItemList) {

          item.Id = requestDataItemList.Id;
          var index = itemList.indexOf(item);
          itemList.splice(index,1);
        });
      }
    }
  })
  Vue.component('item-list', {
    template: '#item-list',
    props: ['items']
  })
  jsonRequestData("/handler/CageEditorHandler/getlist", {}, function (requestDataItemList) {
    requestDataItemList= requestDataItemList.reverse();
  itemList = requestDataItemList;
  console.log(requestDataItemList);
    new Vue({
      el: '#app-table',
      data: {
        newTodoText: '',
        items: requestDataItemList
      },
      methods: {
        Add: function () {
          var item = {
            Mesh: "",
            ItemId: -1,
            MaxCount: 8,
            AmmoItemId: "",
            ExtraRechargeTime: 0
          };   

          jsonRequestData("/handler/CageEditorHandler/add", item , function (requestDataItemList) {
 
            item.Id = requestDataItemList.Id;
            itemList.push(item);
          });
        }
      }
    });
  });


});