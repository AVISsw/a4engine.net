﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.DialogEditor
{
    public class DialogEditor : Module
    {

        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            if (User != null)
            {
                if (User.GroupRight.Can("editDialogs"))
                {
                    string index = FileGetContents(DirModule + "/form.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }
            Context.Redirect("/page/auth");
            Draw("<b>Доступ запрещен!</b>");
        }
    }
}
