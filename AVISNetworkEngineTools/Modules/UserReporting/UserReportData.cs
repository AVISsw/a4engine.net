﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVISNetworkEngineTools.Modules.UserReporting
{
    public class UserReportData
    {


        #region Constructors

        /// <summary>
        /// Creates a new instance of the <see cref="UserReportData"/> class.
        /// </summary>
        public UserReportData()
        {
            this.AggregateMetrics = new List<UserReportMetric>();
            this.Attachments = new List<UserReportAttachment>();
            this.ClientMetrics = new List<UserReportMetric>();
            this.DeviceMetadata = new List<UserReportNamedValue>();
            this.Events = new List<UserReportEvent>();
            this.Fields = new List<UserReportNamedValue>();
            this.Measures = new List<UserReportMeasure>();
            this.Screenshots = new List<UserReportScreenshot>();
        }

        public List<UserReportMetric> AggregateMetrics { get; }

        #endregion

        #region Properties
        public string NickName { get; set; }
        public ReportType Type { get; set; }
        /// <summary>
        /// Gets or sets the attachments.
        /// </summary>
        public List<UserReportAttachment> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the client metrics.
        /// </summary>
        public List<UserReportMetric> ClientMetrics { get; set; }

        /// <summary>
        /// Gets or sets the device metadata.
        /// </summary>
        public List<UserReportNamedValue> DeviceMetadata { get; set; }

        /// <summary>
        /// Gets or sets the events.
        /// </summary>
        public List<UserReportEvent> Events { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        public List<UserReportNamedValue> Fields { get; set; }

        /// <summary>
        /// Gets or sets the measures.
        /// </summary>
        public List<UserReportMeasure> Measures { get; set; }

        /// <summary>
        /// Gets or sets the screenshots.
        /// </summary>
        public List<UserReportScreenshot> Screenshots { get; set; }

        #endregion



        /// <summary>
        /// Gets or sets the content length. This property will be overwritten by the server if provided.
        /// </summary>
        public long ContentLength { get; set; }

        /// <summary>
        /// Gets or sets the dimensions.
        /// </summary>
        public List<UserReportNamedValue> Dimensions { get; set; }

        /// <summary>
        /// Gets or sets the time at which the user report expires. This property will be overwritten by the server if provided.
        /// </summary>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Gets or sets the geo country.
        /// </summary>
        public string GeoCountry { get; set; }

        /// <summary>
        /// Gets or sets the identifier. This property will be overwritten by the server if provided.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the IP address. This property will be overwritten by the server if provided.
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user report is hidden in the UI if a dimension filter is not specified. This is recommended for automated or high volume reports.
        /// </summary>
        public bool IsHiddenWithoutDimension { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user report is silent. Silent user reports do not send events to integrations. This is recommended for automated or high volume reports.
        /// </summary>
        public bool IsSilent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user report is temporary. Temporary user reports are short lived and not queryable.
        /// </summary>
        public bool IsTemporary { get; set; }

        /// <summary>
        /// Gets or sets the project identifier.
        /// </summary>
        public string ProjectIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the time at which the user report was received. This property will be overwritten by the server if provided.
        /// </summary>
        public DateTime ReceivedOn { get; set; }

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail. This screenshot will be resized by the server if too large. Keep the last screenshot small in order to reduce report size and increase submission speed.
        /// </summary>
        public UserReportScreenshot Thumbnail { get; set; }

        public string LogInfo { get; set; }

        public UserReportNamedValue FieldByName(string name)
        {
            foreach (var element in Fields)
            {
               if(element.Name.ToUpper() == name.ToUpper())
                {
                    return element;
                }
            }
            return default;
        }
    }
}
public struct UserReportAttachment
{
    #region Constructors

    /// <summary>
    /// Creates a new instance of the <see cref="UserReportAttachment"/> struct.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="fileName">The file name.</param>
    /// <param name="contentType">The content type.</param>
    /// <param name="data">The data.</param>
    public UserReportAttachment(string name, string fileName, string contentType, byte[] data)
    {
        this.Name = name;
        this.FileName = fileName;
        this.ContentType = contentType;
        this.DataBase64 = Convert.ToBase64String(data);
        this.DataIdentifier = null;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Get or sets the content type.
    /// </summary>
    public string ContentType { get; set; }

    /// <summary>
    /// Gets or sets the data (base 64 encoded).
    /// </summary>
    public string DataBase64 { get; set; }

    /// <summary>
    /// Gets or sets the data identifier. This property will be overwritten by the server if provided.
    /// </summary>
    public string DataIdentifier { get; set; }

    /// <summary>
    /// Gets or sets the file name.
    /// </summary>
    public string FileName { get; set; }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    public string Name { get; set; }

    #endregion
}

public struct UserReportMetric
{
    #region Properties

    /// <summary>
    /// Gets the average.
    /// </summary>
    public double Average
    {
        get { return this.Sum / this.Count; }
    }

    /// <summary>
    /// Gets the count.
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    /// Gets the maximum.
    /// </summary>
    public double Maximum { get; set; }

    /// <summary>
    /// Gets the minimum.
    /// </summary>
    public double Minimum { get; set; }

    /// <summary>
    /// Gets the name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets the sum.
    /// </summary>
    public double Sum { get; set; }

    #endregion

    #region Methods

    /// <summary>
    /// Samples a value.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Sample(double value)
    {
        if (this.Count == 0)
        {
            this.Minimum = double.MaxValue;
            this.Maximum = double.MinValue;
        }

        this.Count++;
        this.Sum += value;
        this.Minimum = Math.Min(this.Minimum, value);
        this.Maximum = Math.Max(this.Maximum, value);
    }

    #endregion
}
 
public struct UserReportEvent
{
    #region Properties

 
    /// <summary>
    /// Gets or sets the frame number.
    /// </summary>
    public int FrameNumber { get; set; }

    /// <summary>
    /// Gets or sets the full message.
    /// </summary>
    public string FullMessage
    {
        get { return string.Format("{0}{1}{2}", this.Message, Environment.NewLine, this.StackTrace); }
    }

    /// <summary>
    /// Gets or sets the level.
    /// </summary>
    public UserReportEventLevel Level { get; set; }
 
    /// <summary>
    /// Gets or sets the message.
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Gets or sets the stack trace.
    /// </summary>
    public string StackTrace { get; set; }

    /// <summary>
    /// Gets or sets the timestamp.
    /// </summary>
    public DateTime Timestamp { get; set; }

    #endregion
}
public struct UserReportNamedValue
{
    #region Constructors

    /// <summary>
    /// Creates a new instance of the <see cref="UserReportNamedValue"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    public UserReportNamedValue(string name, string value)
    {
        this.Name = name;
        this.Value = value;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    public string Value { get; set; }

    #endregion
}
public struct UserReportMeasure
{
    #region Properties

    /// <summary>
    /// Gets or sets the end frame number.
    /// </summary>
    public int EndFrameNumber { get; set; }

    /// <summary>
    /// Gets or sets the metadata.
    /// </summary>
    public List<UserReportNamedValue> Metadata { get; set; }

    /// <summary>
    /// Gets or sets the metrics.
    /// </summary>
    public List<UserReportMetric> Metrics { get; set; }

    /// <summary>
    /// Gets or sets the start frame number.
    /// </summary>
    public int StartFrameNumber { get; set; }

    #endregion
}
public struct UserReportScreenshot
{
    #region Properties

    /// <summary>
    /// Gets or sets the data (base 64 encoded). Screenshots must be in PNG format.
    /// </summary>
    public string DataBase64 { get; set; }

    /// <summary>
    /// Gets or sets the data identifier. This property will be overwritten by the server if provided.
    /// </summary>
    public string DataIdentifier { get; set; }

    /// <summary>
    /// Gets or sets the frame number.
    /// </summary>
    public int FrameNumber { get; set; }

    /// <summary>
    /// Gets the height.
    /// </summary>
    public int Height
    {
        get;
        set;
    }

    /// <summary>
    /// Gets the width.
    /// </summary>
    public int Width
    {
        get;
        set;
    }

    #endregion
}
public enum UserReportEventLevel
{
    /// <summary>
    /// Info.
    /// </summary>
    Info = 0,

    /// <summary>
    /// Success.
    /// </summary>
    Success = 1,

    /// <summary>
    /// Warning.
    /// </summary>
    Warning = 2,

    /// <summary>
    /// Error.
    /// </summary>
    Error = 3
}
public enum ReportType
{
    Bug,
    PerformanceIssue,
    PlayerComplaint,
    Suggestions,
    ComplaintsToTheAdministration
}