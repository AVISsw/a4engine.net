using System;
using System.Text;
 
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Net;
using A4Engine.Transport;

public class FormData
{
    readonly NameValueCollection FormValues;
    readonly NameValueCollection FileNames;
    readonly Dictionary<string, MultiPartStreamFileValue> FileObjects;

    public FormData(IHttpRequest request)
    {
        if (request == null)
            throw new ArgumentNullException("Request");
        /*
        var streamReader = new StreamReader(request.InputStream);
        string line = streamReader.ReadToEnd();
        Console.WriteLine(line);*/
        FileNames = new NameValueCollection();
        FormValues = new NameValueCollection();
        FileObjects = new Dictionary<string, MultiPartStreamFileValue>();
        if (request.HasEntityBody)
            ParsePostedValues(request);
    }

    public string this[string name]
    {
        get { return FormValues[name]; }
    }

    public MultiPartStreamFileValue GetFile(string name)
    {
        name = FileNames[name];
        if (string.IsNullOrEmpty(name))
            return null;
        MultiPartStreamFileValue result;
        FileObjects.TryGetValue(name, out result);
        return result;
    }

    public IEnumerable<MultiPartStreamFileValue> Files
    {
        get { return FileObjects.Values.ToList().AsReadOnly(); }
    }

    void ParsePostedValues(IHttpRequest request)
    {
        Encoding encoding = request.ContentEncoding;

        if (string.Compare(request.ContentType, "application/x-www-form-urlencoded", true) == 0)
            ParseStandardPostValues(request, encoding);
        else
        {
            if (request.ContentType.Length > 20 &&
                    string.Compare(request.ContentType.Substring(0, 20), "multipart/form-data;", true) == 0)
                ParseMultiPartPostValues(request, encoding);
            else
                throw new NotImplementedException("Content type not implemented: " + request.ContentType);
        }
    }

    void ParseStandardPostValues(IHttpRequest request, Encoding encoding)
    {
        using (var reader = new StreamReader(request.InputStream, encoding))
        {
            string postDataString = reader.ReadToEnd();
            string[] lines = postDataString.Split('&');
            foreach (string line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    KeyValuePair<string, string> lineValues = line.ToNameAndValue();
                    AddParsedFormValue(HttpUtility.UrlDecode(lineValues.Key), HttpUtility.UrlDecode(lineValues.Value));
                }
            }
        }
    }

    void AddParsedFormValue(string key, string value)
    {
        if (FormValues.AllKeys.Contains(key))
            value = FormValues[key] + "," + value;
        FormValues[key] = value;
    }

    void ParseMultiPartPostValues(IHttpRequest request, Encoding encoding)
    {
        var reader = new MultiPartStreamReader(request.InputStream, encoding, request.ContentType);
        foreach (MultiPartStreamValue postedValue in reader)
        {
            if (postedValue is MultiPartStreamSimpleValue)
                AddParsedFormValue(postedValue.Name, (postedValue as MultiPartStreamSimpleValue).Value);
            else
            {
                var fileValue = (MultiPartStreamFileValue)postedValue;
                FileNames.Add(fileValue.Name, fileValue.Name);
                FileObjects.Add(fileValue.Name, fileValue);
            }
        }

    }
}

