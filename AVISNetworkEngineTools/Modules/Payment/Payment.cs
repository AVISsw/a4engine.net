﻿using A4Engine.Modules;
using A4Engine.Transport;
using AVISNetworkCommon;
using AVISNetworkCommon.Utility;
using AVISNetworkEngineORM.Data;
using AVISNetworkEngineTools.Modules.Payment.PaymentSystem;
using DataORM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Payment
{

    public class Payment : Handler
    {
        //pswd1 CfH37sprKyB0MiQRz96h
        //pswd2 fBg6rOt3R0z0hK0uzxpt

        //pswd1 test zYa1zwUC64RBJ7WRfBy3
        //pswd2 test Glk1X42LWAubZRu7o2fn
        // регистрационная информация (пароль #2)
 
        public static Logger Log { get; private set; }

        public override Task Before(RequestContext Context)
        {
           return Start(Context);
        }

        public override async Task OnLoad()
        {
            Log = new Logger(Logger.LoggerType.WriteConsole);
        }
      
        async Task result(RequestContext Context)
        {
            try
            {
                PaymentSystemType paymentSystemType = User.Data.Get<PaymentSystemType>("PaymentSystem", BasePaymentSystem.PaymentSystemType);
                Log.Info("User payment system " + paymentSystemType.ToString());
                var paymentSystem = BasePaymentSystem.Get(paymentSystemType);
                if (!paymentSystem.RequestAuthentication(Context))
                {
                    Log.Info("result bad sign result");
                    Draw("bad sign\n", HttpResponseCode.BadRequest);
 
                    return;
                }
                var paymentContext = paymentSystem.GetPaymentContext(Context);
                PaymentResult paymentResult = new PaymentResult();
                bool isTest = paymentSystem.TestRequest(Context);
 
                paymentResult = await paymentSystem.AddAwaitingProcessing(Controller, paymentContext.OrderId, paymentContext.CharacterId, paymentContext.Amount, paymentContext.PaymentSystem, isTest);

                Log.Info("---PaymentContext " + JsonConvert.SerializeObject(paymentContext));
                if (paymentResult.Success)
                { 
                    Draw("OK " + paymentContext.OrderId + "\n", HttpResponseCode.Ok);
                    Log.Info(paymentResult.Message);
                }
                else
                {
                    Draw("NE OK " + paymentContext.OrderId + "\n", HttpResponseCode.NotImplemented);
                    Log.Info(paymentResult.Message);
                }
            }
            catch (Exception e)
            {
                Log.Info("---payment " +e.ToString());
                Draw("bad sign\n", HttpResponseCode.BadRequest);
            }
        }
 
 
        public override async Task Start(RequestContext Context)
        {
            string key = Context.Path[3].ToString();
            switch (key)
            {
                case "result":
                case "success":
                    await result(Context);
                    break;
                case "confirm":
                    await confirm(Context);
                    break;
                case "exchange_rates":
                    await exchange_rates(Context);
                    break;
            }
        }

        private async Task exchange_rates(RequestContext Context)
        {
            PaymentSystemType paymentSystemType = User.Data.Get<PaymentSystemType>("PaymentSystem", BasePaymentSystem.PaymentSystemType);
            Log.Info("User payment system " + paymentSystemType.ToString());
            var paymentSystem = BasePaymentSystem.Get(paymentSystemType);
            Draw(paymentSystem.ExchangeRates(Context));
        }

        private async Task confirm(RequestContext context)
        {
            Draw("YES", HttpResponseCode.Ok);
        }
    }
 
}
