﻿using A4Engine.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVISNetworkEngineTools.Modules.Partner
{
    public class PartnerModule : Module
    {
        public async override Task OnLoad()
        {
            
        }

        public async override Task Start(RequestContext Context)
        {
            if (User != null)
            {
                bool isAdmCreater = User.GroupRight.Can("createPromo");
                bool isPartner = User.GroupRight.Can("partner");
                if (isPartner || isAdmCreater)
                {

                    string index = FileGetContents(DirModule + "/html/index.html");
                    Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
                    return;
                }
            }

            Draw("<b>Доступ запрещен! <a href=\"/page/auth\">Авторизоваться</a></b>");
        }
    }
}
