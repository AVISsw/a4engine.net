﻿using System.IO;
using System.Net;
using System.Net.Sockets;

namespace uhttpsharp.Clients
{
    public interface IClient
    {

        Stream Stream { get; }

        bool Connected { get; }

        bool SecureConnection { get; set; }

        void Close();

        EndPoint RemoteEndPoint { get; }
 
    }
}
