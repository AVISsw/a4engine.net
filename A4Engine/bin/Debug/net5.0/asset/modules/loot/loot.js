var openEditorButton;
var groupId = get_path()[3];

function Save(id) {
  var form = $('#' + id + "_loot");
  var data = Serilaze(form);
  data.GroupId = groupId;
  updateLootRequest(id, data);
}

function Remove(id) {
  var descArea = $('#removeLoot').html();
  descArea = fill_pattern(descArea, { Id: id });
  ShowModal(descArea, "Редактироване описания");
}




function updateLootRequest(id, data) {
  $.post(
    "/handler/ChanceSpawnItemsHandler/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function getSpawnItemsRequest(callback) {
  $.post(
    "/handler/ChanceSpawnItemsHandler/getitems/",
    {
    },
    callback
  );
}

function SelectSpawnGroup(element, id) {
  getSpawnItemsRequest(function (data) {
    console.log(data);


      let selectData = [];
      $.each(data, function (index, element) {
        selectData[element.ItemId] = { Id: element.ItemId, Name:element.Name };
      });
      ShowSelectWindow(selectData, function (elem) {
        let Id = elem.val();
        let Name = selectData[Id].Name;

        $(element).val(Id);
        $(element).text(Id + "| " + Name);
        CloseModal();
      }, id);

  }
  );
}

var lootList = [];
function getLootsRequest(callback) {

  $.post(
    "/handler/ChanceSpawnItemsHandler/get/" + groupId,
    {
    },
    function (data) {
      $.each(data, function (index, element) {
        lootList[element.TransformId] = element;
      });
      console.log(data);
      if (callback)
        callback(data);
    }
  );
}

function addNewLootRequest() {
  // addItem();
  var th = $(this);
  $.post(
    "/handler/ChanceSpawnItemsHandler/add/",
    {GroupId:groupId},
    function (data) {
      console.log(data);
      updateLootList();
    }
  );
}
function updateLootList(callback) {
  getSpawnItemsRequest(function (itemData) {
  var lootTemplate = $('#lootTemplate').html();
  var temploots = "";
  getLootsRequest(function (data) {

    $.each(data, function (index, element) {
      if (data[element.Id]) {
        if(itemData)
        if(itemData[element.ItemId])
        element.ItemName =  element.ItemId+"| "+ itemData[element.ItemId].Name;
      }

      var temprow = fill_pattern(lootTemplate, element);
      var divRow = $(temprow);
      temploots += divRow[0].outerHTML;

    });
    $("#lootList").html(temploots);
    if (callback)
      callback(data);
  });
});
}

function removeLootRequest(id) {
  $.post(
    "/handler/ChanceSpawnItemsHandler/remove/" + id,
    {},
    function (data) { updateLootList(); ShowModal(data, "Результат удаления"); }
  );
}

function SaveAll() {
  if (Array.isArray(editItemList))
    editItemList.forEach(id => {
      Save(id);
    });
}

$(document).ready(function () {
  $('#searchInput').keyup(function () {

    // Search text
    var text = $(this).val().toLowerCase();

    // Hide all content class element
    $('.content').hide();

    // Search 
    $('.content').each(function () {
      $(this).find("input").each(function () {
        if ($(this).val().toLowerCase().indexOf("" + text + "") != -1) {
          $(this).closest('.content').show();
        }
      });
    });
  });
});


var editItemList = [];
$(document).ready(function () {
  Menu();
  updateLootList(function (data) {
    $('.content').find('input').change(function () {
      let elem = $(this).parent().parent();
      let itemid = elem.attr("itemid");
      console.log(elem);
      elem.css("background-color", "#8eb6bea8");
      if (editItemList.indexOf(itemid) == -1) {
        editItemList.push(itemid);
      }
    });

  });

});
