﻿
using A4Engine.User;
using AVISNetworkCommon;
using AVISNetworkEngineORM.DB;
using System;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Controllers
{

    public class Handler : BaseController
    {

        public override async Task Init(RequestContext Context, SessionManager sessionManager)
        {
            try
            {
                Storage = new Storage(Settings.Server.Host, Settings.Server.Name, Settings.Server.User, Settings.Server.Password, Settings.Server.Port);
                SessionManager = sessionManager;

                string moduleName = Context.Path[2];
                Draw(moduleName);
                Modules.Handler module = Context.Router.GetHandler(moduleName);
                if (module != null)
                {
                    module.Controller = this;
                    module.SessionManager = SessionManager;
                    module.User = SessionManager.GetSessionByCookie(Context);
                    await module.Before(Context);
                    
                    Draw(module.Render(), module.StatusCode);
                }
                else
                {
                    Draw("query " + moduleName + " Handler not found");
                }
                Context.Request.Response.ContentType = "application/json";
                Context.Request.Response.StatusCode = this.statusCode;
                Context.Request.Response.ContentEncoding = Encoding.UTF8;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
        }

        internal override Task OnLoad()
        {
            return Task.FromResult(0);
        }
    }
}
