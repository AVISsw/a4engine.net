﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Modules.Element
{
    public class Element : Module
    {

        public override Task OnLoad()
        {
            return Task.FromResult(0);
        }

        public override async Task Start(RequestContext Context)
        {
            string index = FileGetContents(DirModule + "\\html\\index.html");
            Draw(FillPattern(index, new Dictionary<string, string> { { "header1", "Блаблаблаа" } }));
        }
    }
}