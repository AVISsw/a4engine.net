﻿using A4Engine.Modules.ItemEditor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Modules.SearchItem
{
    public class GetItemList : Handler
    {

        public override Task OnLoad()
        {
            return Task.FromResult(0);
        }

        public override Task Before(RequestContext Context)
        {
            return Start(Context);
        }
        async Task<long> GetItemCount()
        {
            using (var data = await Controller.Storage.Query("SELECT COUNT(*) FROM item_template"))
            {
                if (data.Read)
                {
                    return data.GetInt64(0);
                }
            }
            return 0;
        }

        public async Task<List<ItemData>> GetItemTemplate(int offset, int limit)
        {
            List<ItemData> result = new List<ItemData>();
 
            using (var data = await Controller.Storage.GetAll.QueryBase<ItemData>("ORDER BY ItemId DESC LIMIT {0} OFFSET {1}", limit, offset))
            {
                while (data.Read)
                {
                    var itemData = data.GetObject<ItemData>();
                    result.Add(itemData);
                }
            }

            return result;
        }
        const int limit = 1000;
        public override async Task Start(RequestContext Context)
        {
            string action = Context.Path[3];
            switch (action)
            {
                case "page":
                    string pageParam = Context.Path[4];
                    int page = 0;
                    try
                    {
                        page = Convert.ToInt32(pageParam);
                    }catch(Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    int offset = page * limit;
                    Console.WriteLine("offset " + offset);
                    Draw(JsonConvert.SerializeObject(await this.GetItemTemplate(offset, limit)));
                    break;
                case "pages":
                    long elemntCount = await GetItemCount();
                    long pageCount =  (long)Math.Ceiling((double)elemntCount / (double)limit);
                    Draw(JsonConvert.SerializeObject(new Dictionary<string, long> { { "PageCount", pageCount } }));
                    break;
            }
        }
    }
}