
function SaveEffect(id) {
  var form = $('#' + id + "_effect");
  var data = Serilaze(form);
  updateEffectRequest(id, data);
}

function RemoveEffect(id) {
  var descArea = $('#removeEffect').html();
  descArea = fill_pattern(descArea, { ItemId: id });
  ShowModal(descArea, "Редактироване описания");
}
var propertylist = [];
function OpenEffectEditor(id) {
  $.post(
    "/handler/effecteditor/getpropertylist",
    {},
    function (datapropertylist) {
      propertylist = datapropertylist;
      var EffectEditorTemplate = $('#EffectEditor').html();
      EffectEditorTemplate = fill_pattern(EffectEditorTemplate,{ItemId:id});
      ShowModal(EffectEditorTemplate, "Редактор эффектов");
      updateEffectList(id);
    }
  );
}

function updateEffectList(id){
  getItemEffectRequest(id, function (data) {
    var EffectTemplate = $('#EffectTemplate').html();
    var tempEffects = "";
    var options = [];
    if(Array.isArray(propertylist)){
      propertylist.forEach(element => {
        options.push({Name:element.Key,Value:element.Key});
      });
    }
  
    if (Array.isArray(data))
      data.forEach(element => {
        var fields = [
          {
            Name: "Value",
            Value: "Value"
          },
          {
            Name: "MinValue",
            Value: "MinValue"
          },
          {
            Name: "MaxValue",
            Value: "MaxValue"
          },
          {
            Name: "Per",
            Value: "Per"
          },
          {
            Name: "PerTime",
            Value: "PerTime"
          }
        ];
        console.log(element);
        element.SelectPropertyName = DrawOptions(options, element.PropertyName);
        element.SelectValueName = DrawOptions(fields,element.ValueName);
        var temprow = fill_pattern(EffectTemplate, element);
        var divRow = $(temprow);
  
        tempEffects += divRow[0].outerHTML;
      });
    $("#EffectList").html(tempEffects);
  });
}

function updateEffectRequest(id, data) {
  $.post(
    "/handler/effecteditor/update/" + id,
    data,
    function (data) { ShowModal(data, "Результат сохранения"); }
  );
}

function getItemEffectRequest(id, callback) {
  $.post(
    "/handler/effecteditor/get/" + id,
    {
    },
    callback
  );
}


function addNewEffectRequest(itemId) {
  // addItem();
  $.post(
    "/handler/effecteditor/add/",
    {ItemId:itemId},
    function (data) { updateEffectList(itemId); }
  );
}

function removeEffectRequest(id) {
  $.post(
    "/handler/effecteditor/remove/" + id,
    {},
    function (data) { ShowModal(data, "Результат удаления"); }
  );
}
