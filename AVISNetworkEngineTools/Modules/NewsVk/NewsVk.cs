﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using A4Engine.Modules;
using System.Threading.Tasks;

namespace A4Engine.Modules.NewsVk
{
    public class NewsVk : Handler
    {
        TimeSpan interval = new TimeSpan(4, 0, 0);
        TimeSpan lastRequest = default;
        string lastResult = "";
        public override async Task OnLoad()
        {
        }

        public override Task Before(RequestContext Context)
        {
           return Start(Context);
        }


        public override async Task Start(RequestContext Context)
        {
            if (lastRequest < DateTime.Now.TimeOfDay)
            {
                string url = "https://api.vk.com/method/wall.get";

                using (var webClient = new WebClient())
                {

                    var pars = new NameValueCollection();
                    pars.Add("v", "5.95");
                    pars.Add("filter", "owner");
                    pars.Add("owner_id", "-122569092");
                    pars.Add("count", "5");
                    pars.Add("access_token", Settings.Server.VKAppServiceKey);
                    var response = webClient.UploadValues(url, pars);
                    lastResult = Encoding.UTF8.GetString(response);
                }
                lastRequest = DateTime.Now.TimeOfDay + interval;
                Console.WriteLine("vk request");
            }
            Draw(lastResult);

            // Draw(JsonConvert.SerializeObject(Logs));
        }
    }
}