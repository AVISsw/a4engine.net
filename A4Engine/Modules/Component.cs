﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4Engine;
using A4Engine.User;
using A4Engine.Controllers;
using A4Engine.Modules.Interface;


namespace A4Engine.Modules
{
    abstract class Components : IModule
    {
        string result_data = string.Empty;

        public string DirModule { get { return string.Format("{0}assets/modules/{1}", AppDomain.CurrentDomain.BaseDirectory, this.GetType().Name.ToLower()); } }
        public string UriModule { get { return string.Format("/files/modules/{0}", this.GetType().Name.ToLower()); } }
        public BaseController Controller { get; set; }
        public SessionManager SessionManager { get; set; }
        public string Alias { get; set; } = string.Empty;
        public string Name
        {
            get
            {
                string result = string.Empty;
                if (Alias.Length < 1)
                {
                    result = GetType().Name;
                }
                else
                {
                    result = Alias;
                }
                return result.ToLower();
            }
        }
        public Session User { get; set; }

        public abstract Task Before(RequestContext Context);

        // вызывается при создании экземпляра модуля
        public abstract Task Start(RequestContext Context);

        /*
        * Вызывается для загрузки общих ресурсов модуля, всех его экземпляров
        * */
        public abstract Task OnLoad();

        virtual public void Draw(string result)
        {
            this.result_data = result;
        }
    }
}