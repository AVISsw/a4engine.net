﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using A4Engine;
using A4Engine.Modules;
using Newtonsoft.Json;

namespace AVISNetworkEngineTools.Modules.AuthVk
{
    public class VKConfirm : Module
    {
        public override async Task OnLoad()
        {

        }

        public override async Task Start(RequestContext Context)
        {
            Console.WriteLine("VKConfirm");
            try
            {
                string code = null;
                code = Context.getParams["code"];
                string email = null;
                email = Context.getParams["email"];
                string login = null;
                login = Context.getParams["login"];
                string state = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "email", email }, { "login", login } });
                string host;
                string uri = null;
                if (string.IsNullOrWhiteSpace(code))
                {
                    // show link
                    host = Context.Request.Request.UserHostName;
                    uri = string.Format("https://oauth.vk.com/authorize?client_id={0}&display=page&redirect_uri=http://{1}/page/vk_confirm&scope=friends&response_type=code&v=5.103&state={2}",
                        Settings.Server.VKAppId,
                        host,
                        state
                        );
                    Context.Redirect(uri, true);
                }
                else
                {
                    string requestState = Context.getParams["state"];
                    uri = string.Format("/handler/SignUpVk?code={0}&state={1}",
                        code,
                        requestState.Replace("\"", "\\\"")
                        );
                    string path = DirModule + "/confirm_finish.html";
                    string index = FileGetContents(path);

                    Draw(FillPattern(index, new Dictionary<string, string> { { "uri", uri } }));
                }

            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
