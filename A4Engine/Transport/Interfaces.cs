﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace A4Engine.Transport
{
    public enum HttpResponseCode
    {
        // Informational
        Continue = 100,
        SwitchingProtocols = 101,
        Processing = 102,

        // Success
        Ok = 200,
        Created = 201,
        Accepted = 202,
        NonAuthorativeInformation = 203,
        NoContent = 204,
        ResetContent = 205,
        PartialContent = 206,
        MultiStatus = 207,
        AlreadyReported = 208,
        IMUsed = 226,

        // Redirection
        MovedPermanently = 301,
        Found = 302,
        SeeOther = 303,
        NotModified = 304,
        UseProxy = 305,
        SwitchProxy = 306,
        TemporaryRedirect = 307,
        PermanentRedirect = 308,

        // Client Error
        BadRequest = 400,
        Unauthorized = 401,
        PaymentRequired = 402,
        Forbidden = 403,
        NotFound = 404,
        MethodNotAllowed = 405,
        NotAcceptable = 406,
        ProxyAuthenticationRequired = 407,
        RequestTimeout = 408,
        Conflict = 409,
        Gone = 410,
        LengthRequired = 411,
        PreconditionFailed = 412,
        RequestEntityTooLarge = 413,
        RequestUriTooLong = 414,
        UnsupportedMediaType = 415,
        RequestedRangeNotSatisfiable = 416,
        ExpectationFailed = 417,
        ImATeapot = 418,
        AuthenticationTimeout = 419,
        MethodFailure = 420,
        UnprocessableEntity = 422,
        Locked = 423,
        FailedDependency = 424,
        UnorderedCollection = 425,
        UpgradeRequired = 426,
        PrecondittionRequired = 428,
        TooManyRequests = 429,
        RequestHeaderFieldsTooLarge = 431,
        LoginTimeout = 440,
        NoResponse = 444,
        RetryWith = 449,
        BlockedByWindowsParentalControls = 450,
        UnavailableForLegalReasons = 451,
        RequestHeaderTooLarge = 494,
        CertError = 495,
        NoCert = 496,
        HttpToHttps = 497,
        ClientClosedRequest = 499,

        // Server Errors
        InternalServerError = 500,
        NotImplemented = 501,
        BadGateway = 502,
        ServiceUnavailable = 503,
        GatewayTimeout = 504,
        HttpVersionNotSupported = 505,
        VariantAlsoNegotiates = 506,
        InsufficientStorage = 507,
        LoopDetected = 508,
        BandwidthLimitExceeded = 509,
        NotExtended = 510,
        NetworkAuthenticationRequired = 511,
        OriginError = 520,
        WebServerIsDown = 521,
        ConnectionTimedOut = 522,
        ProxyDeclinedRequest = 523,
        ATimeoutOccured = 524,
        NetworkReadTimeoutError = 598,
        NetworkRConnectTimeoutError = 599,
    }  /// <summary>
       /// Specifies the methods of an http request.
       /// </summary>
    public enum HttpMethods
    {
        /// <summary>
        /// Requests a representation of the specified resource.
        /// </summary>
        Get,

        /// <summary>
        /// Asks for the response identical to the one that would correspond to a GET request, but without the response body
        /// </summary>
        Head,

        /// <summary>
        /// Requests that the server accept the entity enclosed in the request as a new subordinate of the web resource identified by the URI
        /// </summary>
        Post,

        /// <summary>
        /// Requests that the enclosed entity be stored under the supplied URI
        /// </summary>
        Put,

        /// <summary>
        /// Deletes the specified resource.
        /// </summary>
        Delete,

        /// <summary>
        /// Echoes back the received request so that a client can see what (if any) changes or additions have been made by intermediate servers
        /// </summary>
        Trace,

        /// <summary>
        /// Returns the HTTP methods that the server supports for the specified URL
        /// </summary>
        Options,

        /// <summary>
        /// Converts the request connection to a transparent TCP/IP tunnel
        /// </summary>
        Connect,

        /// <summary>
        /// Is used to apply partial modifications to a resource
        /// </summary>
        Patch
    }
    public interface IHttpRequest:IDisposable
    {
        bool IsSecureConnection { get; }
        string UserHostAddress { get; }
        string UserAgent { get; }
        Uri UrlReferrer { get; }
        Uri Url { get; }
        string ServiceName { get; }
        IPEndPoint RemoteEndPoint { get; }
        string RawUrl { get; }
        NameValueCollection QueryString { get; }
        Version ProtocolVersion { get; }
        IPEndPoint LocalEndPoint { get; }
        bool KeepAlive { get; }
        bool IsWebSocketRequest { get; }
        string UserHostName { get; }
        string[] UserLanguages { get; }
        bool IsAuthenticated { get; }
        Stream InputStream { get; }
        string HttpMethod { get; }
        NameValueCollection Headers { get; }
        bool HasEntityBody { get; }
        string ContentType { get; }
        long ContentLength64 { get; }
        Encoding ContentEncoding { get; }
        int ClientCertificateError { get; }
        string[] AcceptTypes { get; }
        bool IsLocal { get; }
        IAsyncResult BeginGetClientCertificate(AsyncCallback requestCallback, object state);
        X509Certificate2 EndGetClientCertificate(IAsyncResult asyncResult);
        X509Certificate2 GetClientCertificate();
        Task<X509Certificate2> GetClientCertificateAsync();
    }
    public interface IHttpResponse: IDisposable
    {
        long ContentLength64 { get; set; }
        bool SendChunked { get; set; }
        Version ProtocolVersion { get; set; }
        Stream OutputStream { get; }
        bool KeepAlive { get; set; }
        NameValueCollection Headers { get; set; }
        string ContentType { get; set; }
        HttpResponseCode StatusCode { get; set; }
        string StatusDescription { get; set; }
        Encoding ContentEncoding { get; set; }
        void Abort();
        void Close(byte[] responseEntity, bool willBlock);
        void Close();
        Task GetResponse(RequestContext requestContext);
        void Redirect(string url);
 
    }
    public interface IHttpContext
    {
        IHttpRequest Request { get; set; }
        IHttpResponse Response { get; set; }
        CookieManager Cookies { get; }
    }
}
