﻿using AVISNetworkCommon;
using AVISNetworkCommon.Utility;

using AVISNetworkEngineData.Models;
using AVISNetworkEngineORM.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace A4Engine.User
{
    public class SessionManager
    {
        static long id = 0;
        static Dictionary<string, Session> Sessions = new Dictionary<string, Session>();
        public Group GroupRight { get; set; }
        public Storage Storage { get; set; }
        Logger Log;
 
        public SessionManager(Storage Storage)
        {
            this.Storage = Storage; Log = new Logger(Logger.LoggerType.WriteConsole);
        }

        public Session GetSession(string hash)
        {
            if (hash.Length < 5)
                return null;

            if (Sessions.ContainsKey(hash))
                return Sessions[hash];

            var session = new Session(this, Storage, hash);
            return session;
        }
        public Session GetSessionByCookie(RequestContext requestContext)
        {
            var cookie = requestContext.Request.Cookies["SessionHash"];

            if (cookie == null)
            {
                id++;
                string hash = "<f#g02ef?!" + id;
                hash = Crypt.EncryptSHA256(hash);
                try
                {
                    requestContext.Request.Cookies.Update(new System.Net.Cookie("SessionHash", hash, "/"));
                }
                catch (Exception e)
                {
                    Log.Info("Не удается записать куки :{0}", e.ToString());
                }
                return GetSession(hash);
            }

            return GetSession(cookie.Value);
        }
        static object sessionLook = new object(); 
        public void AddSession(string hash, Session session, bool reload = false)
        {
            lock (sessionLook)
            {
                if (!Sessions.ContainsKey(hash))
                {
                    Sessions.Add(hash, session);
                }
                else if (reload)
                {
                    Sessions[hash] = session;
                }
            }
        }
    }
}
