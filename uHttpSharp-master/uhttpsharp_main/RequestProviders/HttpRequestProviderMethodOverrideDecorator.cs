﻿using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace uhttpsharp.RequestProviders
{
    public class HttpRequestProviderMethodOverrideDecorator : IHttpRequestProvider
    {
        private readonly IHttpRequestProvider _child;

        public HttpRequestProviderMethodOverrideDecorator(IHttpRequestProvider child)
        {
            _child = child;
        }

        public async Task<IHttpRequest> Provide(IStreamReader streamReader)
        {
            var childValue = await _child.Provide(streamReader).ConfigureAwait(false);

            if (childValue == null)
            {
                return null;
            }

            string methodName = childValue.Headers.Get("X-HTTP-Method-Override");
            if (string.IsNullOrWhiteSpace(methodName))
            {
                return childValue;
            }

            return new HttpRequestMethodDecorator(childValue, HttpMethodProvider.Default.Provide(methodName));
        }
    }
}